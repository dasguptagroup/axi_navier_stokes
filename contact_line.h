
bool contact_cell(int r,int c) {
	if interface(r,c) {
		if (r==1 || r==NoOfRows){
			if (Cellattrib[r][c-1].VolFrac<LOWER_LIMIT || Cellattrib[r][c+1].VolFrac<LOWER_LIMIT )
			{
				return 1;
			}
		}
	}
	return 0;
}
bool adjacent_cell(int r,int c){
	
	if (r==1 || r==NoOfRows){
			if (contact_cell(r,c-1) || contact_cell(r,c+1))
			{
				return 1;
			}
		}
		
		return 0;
	}

// return heights normal
double calculate_height_normal(int r,int c, int orient)
{
	int r_temp, c_temp;	

	double HF_0, HF_1, HF_2, Fx, Fxx;
	//double err_new, norm;
		int sign_normal=1;
	
	HF_0 = 0; HF_1 = 0; HF_2 = 0; 
	
		
		
		switch (orient)
		{	// Case 1, when the normal is mainly pointing upwards
				//~ if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny>0 )
				//~ if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y>0)	
				case 1:
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1; ;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT  );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						r_temp = r_temp + 1; // go up 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						r_temp = r_temp - 1; // go down 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						r_temp = r_temp + 1; // go up 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r-1; c_temp = c+1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						r_temp = r_temp - 1; // go down 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}
				break;
				//	Case 2, when the normal is mainly pointing downwards
				//~ else if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny<0)
				//~ else	if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y<0)	
				case 3:
				{	/***************Left Column*******************/
					r_temp = r+1; c_temp = c-1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 

					/***************Middle Column*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						r_temp = r_temp + 1; // go up
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Right Column*******************/
					r_temp = r+1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						r_temp = r_temp + 1; // go up
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}
				break;

				/*************Case 3 When Normal is mainly pointing right****************************/

				//~ else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx>0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X>0)
				case 2:
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						c_temp = c_temp + 1; // go right
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						c_temp = c_temp + 1; // go right 
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r; c_temp = c-1;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						c_temp = c_temp + 1; // go right
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r+1; c_temp = c-1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}	
				break;		
				//	Case 4, when the normal is mainly pointing left
				//~ else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx<0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X<0)
				case 4:
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Middle Row*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Top Row*******************/
					r_temp = r+1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}
				break;
				
				default:
				
				printf("\norient 0 found in height_function.h: debug this if this error comes up");
				printf("\n%d\t%d",r,c);
				exit(0);
					
		}
						// modify heights if contact angle boundary condition is enforced
					if (contact && contact_cell(r,c) ){
						if(orient==2 || orient==4)
						HF_0 = HF_1 + del/tan(contact_angle);
					}
	
					return (HF_2-HF_0)/(2.0*del);
					
}

void IdentifyCell_AssignArea_contact(int r, int c,int Quad, char CellName[30],  double Area)
{
  int R, C;
  if(!strcmp(CellName,"NorthWest") )
    {
       switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c-1;
               break;

               case 2:
                 R = r-1;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c+1;
               break;

               case 4:
                 R = r+1;
                 C = c+1;
               break;
        }
    }
  else if(!strcmp(CellName,"North") )
    {
      switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c;
               break;

               case 2:
                 R = r;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c;
               break;

               case 4:
				 R =r;
				 C =c+1;
			   break;
		}
    }
  else if(!strcmp(CellName,"NorthEast") )
    {
        switch (Quad)
		{
			   case 1:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 2:
				R = r+1;
				C = c-1;
			   break;

       		   case 3:
				 R = r-1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c+1;
			   break;
		}
    }
  else if( !strcmp(CellName,"West") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c-1;
			   break;

       		   case 2:
				R = r-1;
				C = c;
			   break;

       		   case 3:
			    R = r;
				C = c+1;
			   break;

       		   case 4:
				R = r+1;
				C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"East") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c;
			   break;

       		   case 3:
				 R = r;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthWest") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c-1;
			   break;

       		   case 2:
				 R = r-1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 4:
				 R = r+1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"South") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c;
			   break;

       		   case 2:
				 R = r;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c;
			   break;

       		   case 4:
				 R = r;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthEast") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"Central") )
    {
      R = r;
      C = c;
    }
  else //ERROR HANDLING
    {
      printf("\n\n***********************************************************\n\n");
	  printf("\nERROR IN SUBROUTINE IdentifyCell_AssignArea_contact\n");
	  printf("\n\n***********************************************************\n\n");
	  exit(0);
    }
    
			
		         //~ Cellattrib[R][C].ext_volfrac=Area;

		    if (R==0){
			    //~ if(contact_cell(R+1,c)){
		         Cellattrib[R][C].ext_volfrac=Area/del_sq; // divide by del_sq in original function too it seems a bug left earlier
		         Cellattrib[R][C].VolFrac=Area/del_sq;
			    //~ }
		    }

		    //~ else if(Cellattrib[r+1][c].VolFrac>UPPER_LIMIT){
			     //~ Cellattrib[R][C].ext_volfrac=1.0;
		    //~ }
		//~ else if(Cellattrib[r+1][c].VolFrac>LOWER_LIMIT){
			//~ Cellattrib[R][C].ext_volfrac=0;
		    //~ }
	    
    

		    
}


	
void Extrapolate_Line_contact(int r, int c)
{
  /****LOCAL VARIABLES****/
  int    Quad;
  long double y_0, y_del, y_2del, y_3del;   //the value of y at x = 0 , x = del , x = 2*del and x = 3*del
  long double x_0, x_del, x_2del, x_3del ;  //the value of x at y = 0 , y = del , y = 2*del and y = 3*del
  long double Area;
  long double Theta; 
  long double Slope;      
  long double P;
  char   Shape[30];
  long double al;
  long double m;
  /**********************/

  Quad        =  Find_Quad(r,c);
  P           =  Cellattrib[r][c].P;
  Theta  = Cellattrib[r][c].Theta;
  al = Cellattrib[r][c].al;
 
  IdentifyCell_AssignArea_contact(r,c,Quad,"Central",Cellattrib[r][c].VolFrac);   //for the central cell, assign the same VolFrac

  //if rectangle, 
  if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
    {
      Area = P*del;
      if(Quad == 1)
        {
            //~ Write_struct_Cellattrib(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r  ,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
	
		Cellattrib[r+1][c+1].Area = 0;
		Cellattrib[r][c+1].Area = 0;
		Cellattrib[r-1][c+1].Area = 0;
		
            //~ Write_struct_Cellattrib(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r+1][c].Area = Area;
		Cellattrib[r-1][c].Area = Area;

            //~ Write_struct_Cellattrib(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r  ,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r+1][c-1].Area = del_sq;
		Cellattrib[r][c-1].Area = del_sq;
		Cellattrib[r-1][c-1].Area = del_sq;
		
        }
      else if(Quad == 2)
        {
            //~ Write_struct_Cellattrib(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r+1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		Cellattrib[r+1][c-1].Area = 0;
		Cellattrib[r+1][c].Area = 0;
		Cellattrib[r+1][c+1].Area = 0;
		
            //~ Write_struct_Cellattrib(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r][c-1].Area = Area;
		Cellattrib[r][c+1].Area = Area;

            //~ Write_struct_Cellattrib(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r-1][c-1].Area = del_sq;
		Cellattrib[r-1][c].Area = del_sq;
		Cellattrib[r-1][c+1].Area = del_sq;
        }
      else if(Quad == 3)
        {
            //~ Write_struct_Cellattrib(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r  ,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		Cellattrib[r+1][c-1].Area = 0;
		Cellattrib[r][c].Area = 0;
		Cellattrib[r-1][c-1].Area = 0;
		
            //~ Write_struct_Cellattrib(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
	
		Cellattrib[r+1][c].Area = Area;
		Cellattrib[r-1][c].Area = Area;

		//~ Write_struct_Cellattrib(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r  ,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r+1][c+1].Area = del_sq;
		Cellattrib[r][c+1].Area = del_sq;
		Cellattrib[r-1][c+1].Area = del_sq;
		
		
        }
      else if(Quad == 4)
        {
            //~ Write_struct_Cellattrib(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		Cellattrib[r-1][c-1].Area = 0;
		Cellattrib[r-1][c].Area = 0;
		Cellattrib[r-1][c+1].Area = 0;
		
            //~ Write_struct_Cellattrib(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r][c-1].Area = Area;
		Cellattrib[r][c+1].Area = Area;
		
            //~ Write_struct_Cellattrib(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r+1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_Cellattrib(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		Cellattrib[r+1][c-1].Area = del_sq;
		Cellattrib[r+1][c].Area = del_sq;
		Cellattrib[r+1][c+1].Area = del_sq;
		
        } 
      return;
    }
 
         Theta       =  Cellattrib[r][c].Theta;
         Slope       =  tan(Theta); 
	 m            =   Slope/(Slope+1);
         /// ALL CONDITIONS ARE CHECKED USING THE FOLLOWING EQUATION
         /// y = tan(theta)[h + p*cosec(theta) - x] + h 
         /// P IS MEASURED FROM THE BOTTOM LHS CORNER

         y_0   =  Slope*( del + (P/sin(Theta))           ) + del;
         y_del =  Slope*( del + (P/sin(Theta)) - del     ) + del;
         y_2del = Slope*(       (P/sin(Theta)) - del     ) + del;
         y_3del = Slope*(       (P/sin(Theta)) - del2    ) + del;

         x_0    = del + (P/sin(Theta)) + (del/Slope);
         x_del  = del + (P/sin(Theta))              ;
         x_2del = del + (P/sin(Theta)) - (del/Slope);
         x_3del = del + (P/sin(Theta)) - (del2/Slope);

      //SHADED AREA IS A TRIANGLE

         if( al <= m && al <= (1-m) )    //both for Theta <> PI/4.0     
          {
                //ASSIGN VOLFRAC TO CELLS WHICH ARE 0 OR 1
                IdentifyCell_AssignArea_contact(r,c,Quad,"SouthWest",del_sq);
                IdentifyCell_AssignArea_contact(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea_contact(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea_contact(r,c,Quad,"East",0.0);
                /******************************************/

                /*****************************CALCULATE AREAS FOR NORTHWEST & WEST CELL*****************************/
                 if (y_0 <= del2)  //IF NORTHWEST IS 0
                   {
                     IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea_contact(r,c,Quad,"West",Area);
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //IF NORTHWEST IS A TRIANGLE
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment.for NorthWest
                     IdentifyCell_AssignArea_contact(r,c,Quad,"West",Area);
                   }
                 else if( y_0 > del3 )   //IF NORTHWEST IS A TRAPEZIUM
                   {
                     Area = ( x_2del + x_3del )*delBy2;                       // Calculate the area of the trapezium for NorthWest
                     IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea_contact(r,c,Quad,"West",Area);
                   }
                 /***********************END*****************************************************************************/

                 /********CALCULATE AREAS FOR South AND SOUTHEAST CELLS (exchange x and y in all expressions above)***********/
                  if( x_0 <= del2 )    //IF SOUTHEAST IS 0
                  {
                    IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",0.0);

                    Area = ( (x_0 - del) + (x_del - del) )*delBy2;       //calculate area of trapezium for South
                    IdentifyCell_AssignArea_contact(r,c,Quad,"South",Area);
                  }
                 else if ( x_0 > del2 && x_0 <= del3 )                  //IF SOUTHEAST IS A TRIANGLE
                   {
                     Area = 0.5*(x_0 - del2)*y_2del;                       //Calculate area of the triangle for SouthEast
                     IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea_contact(r,c,Quad,"South",Area);
                   }
                 else if( x_0 > del3 )      //IF SOUTHEAST IS A TRAPEZIUM
                   {
                     Area = (y_2del + y_3del)*delBy2;                       // Calculate the area of the trapezium for SouthEast
                     IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea_contact(r,c,Quad,"South",Area);
                   }
                 /***********************END*********************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA < PI / 4
         else if ( al > m && al <= (1-m) )  // for Theta < PI / 4.0
        {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea_contact(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea_contact(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea_contact(r,c,Quad,"SouthWest",del_sq);
                IdentifyCell_AssignArea_contact(r,c,Quad,"South",del_sq);
                /******************************************/
                                
                /*****************************CALCULATE AREAS FOR NORTHWEST AND WEST CELLS*****************************/
                 if (y_0 <= del2)  //if NorthWest is 0
                   {
                     IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea_contact(r,c,Quad,"West",Area);                  
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //if NorthWest is a triangle
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);                     

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea_contact(r,c,Quad,"West",Area);                  
                   }            
                 /***********************END*****************************************************************************/

                 /*****************************CALCULATE AREAS FOR SOUTHEAST AND EAST CELLS*****************************/
                if(y_3del >= del)    //if SouthEast is 1
                  {
                     IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",del_sq);
                     
                     Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
                     IdentifyCell_AssignArea_contact(r,c,Quad,"East",Area);
                  }
                else if(y_3del > 0 && y_3del < del)   //if SouthEast is a complement of a triangle
                  {
                    Area = del_sq  - ( 0.5*(del3 - x_del)*(del-y_3del) ); //calculate area of the triangle and then take complement for SouthEast
                    IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);

                    Area = 0.5*(y_2del - del)*(x_del - del2);     //calculate area of area of the trinagle for East
                    IdentifyCell_AssignArea_contact(r,c,Quad,"East",Area);
                  }
                 /***********************END****************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA > PI / 4
         else if ( al> (1-m) &&  al <= m )   // for Theta > PI / 4.0
         {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea_contact(r,c,Quad,"West",del_sq);
                IdentifyCell_AssignArea_contact(r,c,Quad,"SouthWest",del_sq);
                IdentifyCell_AssignArea_contact(r,c,Quad,"East",0.0);
                IdentifyCell_AssignArea_contact(r,c,Quad,"NorthEast",0.0);
                /******************************************/
                
                /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST*****************************/
                if(y_del >= del3)   //if NorthWest is 1
                  {
                    IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",del_sq);
                    //printf("\nNW is 1\n");

                    Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
                    IdentifyCell_AssignArea_contact(r,c,Quad,"North",Area);
                    // printf("\nN %f \n",Area);
                  }
                else if(y_del < del3) //if NorthWest is complement of a triangle
                  {
                    Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest 
                    IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);

                    Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
                    IdentifyCell_AssignArea_contact(r,c,Quad,"North",Area);                 
                  }             
                /***********************END**************************************************************************/

                /*****************************CALCULATE AREAS FOR SOUTH & SOUTHEAST**********************************/
                if(x_0 <= del2)   //if SouthEast is 0
                  {
                    //printf("\nSE is 0\n");
                    IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",0.0);             

                    Area = ( (x_del - del) + (x_0 - del) )*delBy2;   //calculate area of the trapezium for South
                    IdentifyCell_AssignArea_contact(r,c,Quad,"South",Area);                 
                    //printf("\nN %f \n",Area);
                  }
                else if(x_0 >  del2)    //if SouthEast is a triangle
                  {
                    Area = 0.5*y_2del*(x_0 - del2);    //calculate area of the triangle for SouthEast
                    IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);            

                    Area = del_sq - (del - y_2del)*(del2 - x_del);      //calculate area of the triangle and take complement for South
                    IdentifyCell_AssignArea_contact(r,c,Quad,"South",Area);                 
                  }             
                /***********************END**************************************************************************/
        }

      //SHADED AREA IS A 5 SIDED FIGURE WHICH IS THE COMPLEMENT OF A TRIANGLE
         else if( al > m && al > (1-m) )  //both for Theta <> PI/4
        {
            //Assign VolFRac to cells which are 0 or 1
            IdentifyCell_AssignArea_contact(r,c,Quad,"West",del_sq);
            IdentifyCell_AssignArea_contact(r,c,Quad,"SouthWest",del_sq);
            IdentifyCell_AssignArea_contact(r,c,Quad,"South",del_sq);
            IdentifyCell_AssignArea_contact(r,c,Quad,"NorthEast",0.0);
            /******************************************/

          /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST**********************************/
          if(x_3del >= del)   //if NorthWest is 1
           {
             //printf("NW is 1\n");
              IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",del_sq);

              Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
              IdentifyCell_AssignArea_contact(r,c,Quad,"North",Area);
              //printf("\nN is %f",Area);
           }
          else if(x_3del < del  && x_3del > 0) //if NorthWest is complement of a triangle
           {
            Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest 
            IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);
            //printf("NW is %f",Area);

            Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
            IdentifyCell_AssignArea_contact(r,c,Quad,"North",Area);                 
           }            
          else if(x_3del  < 0)    //if NorthWest is a trapezium
           {
             Area = ( (y_0 - del2) + (y_del - del2) )*delBy2;    //calculate area of the trapezium for NorthWest
             IdentifyCell_AssignArea_contact(r,c,Quad,"NorthWest",Area);

             Area =0.5*(y_del - del2)*(x_2del - del);   //calculate area of the triangle for North
             IdentifyCell_AssignArea_contact(r,c,Quad,"North",Area);
           }
          /****************************************END**********************************************************/

          /*****************************CALCULATE AREAS FOR EAST & SOUTHEAST**********************************/
           if(y_3del >= del)       //if SouthEast is 1
            {
              IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",del_sq);

              Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
              IdentifyCell_AssignArea_contact(r,c,Quad,"East",Area);
            }
          else if(y_3del < del && y_3del > 0)   //if SouthEast is complement of a triangle
            {
              Area = del_sq - ( 0.5*(del3 - x_del)*(del - y_3del) );   //calculate area of the triangle and take complement for SouthEast 
              IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);

              Area = 0.5*(x_del - del2)*(y_2del - del);          //calculate area of the triangle for East
              IdentifyCell_AssignArea_contact(r,c,Quad,"East",Area);
            }
          else if(y_3del < 0)    //if SouthEast is a trapezium
            {         
              Area = ( (x_0 - del2) + (x_del - del2) )*delBy2;    //calculate area of the trapezium for SouthEast
              IdentifyCell_AssignArea_contact(r,c,Quad,"SouthEast",Area);

              Area =0.5*(x_del - del2)*(y_2del - del);   //calculate area of the triangle for East
              IdentifyCell_AssignArea_contact(r,c,Quad,"East",Area);
            }
          /****************************************END**********************************************************/
        }
	 else //ERROR HANDLING
        {
	  printf("\n***********************************************************\n");
	  printf("\nERROR - ERROR IN SUBROUTINE EXTRAPOLATE_LINE\n");
	  printf("\n***********************************************************\n");
	  //exit(0);
        }
}



