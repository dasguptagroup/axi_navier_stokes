
/*********************************************************************************************************************************************************************************************************************************************
THIS IS A PRESSURE-VELOCITY NAVIER STOKES SOLVER FOR TWO PHASE FLOW
AUTHOR: PALAS KUMAR FARSOIYA

*******************
DESCRIPTION:
1. Finite Volume Approach
2. Pressure-Velocity Formulation
3. Explicit for Velocity and Implicit for pressure
4. First order upwind for advection term

/****************
REFERENCES:
1. Direct numerical simulations of gas-liquid multiphase flows
2. A Navier-Stokes solver for single and two phase flow, Kim Motooyoshi Kalland, ( Masters Thesis), University of Oslo
3. A Front tracking/Finite-Volume Navier-Stokes Solver for Direct Numerical Simulations of Multiphase flows, Gretar Tryggvason ( Tutorial document at http://www3.nd.edu/~gtryggva/MultiphaseDNS/DNS-Solver.pdf )
4. Numerical Heat transfer and Fluid Flow, Suhas V Patankar
************************************************************************************************************************************************************************************************************************************************/


/***INCLUDE***/
#include "stdio.h"

#include "stdlib.h"

#include "unistd.h"

#include "math.h"

#include "string.h"

#include "sys/types.h"

#include "sys/stat.h"

#include "unistd.h"

#include "stdbool.h"

#include "time.h"

#include "global_variables.h"

#include "multigrid.h"

#include "boundary.h"

#include "vof_recon.h"

#include "vof_advect.h"

#include "obsolete.h"

#include "output.h"

#include "tension.h"

#include "multigrid_implicit.h"

#include "embed_solid.h"

#if Linear_Solver==3
	#include "HYPRE_struct_ls.h"
	#include "hypre.h"
#endif

void run_ns()
{

	check_input();

	   /*********DECLARATIONS***********/
	   int r, c; //Step;
	   int check;
	   int Togg=0;
	   int Togg_solid=0;
	   double RunTimeStart, RunTimeEnd;
		RunTimeStart = time(0);
		int Count;
	

	   /*******************************************/

	   /*STEPS BEFORE CALLING RECON_INTERFACE() IN A LOOP*/
	   StepT  = 0;

	   //~ Togg  = 0;
	   //~ Time =0;
	Write_Count = 0;
			
	   Init_GlobalVars();
			
		Count =ceil(Time/step);	
			//~ printf("count= %d",Count);
		
	if (end<0)
	{
		end = nodata;
	}
	event_init(); 
	   /**********************************************************/
	//~ printf("Step\tdelT\t\tTime\t\tRun Time(s)\tPoissonSolver\tResidue");
	printf("Bombay Class Solver(BCS2D) Running\n\n"); fflush(stdout);
 	    while(Time<=end+delT) // (Time<end)// //(U_residue>1e-8 && V_residue>1e-8)//////(U_residue>1e-8 && V_residue>1e-8)	 // 
 		{
			//Apply_Default_BoundaryConditions(); //keep this here at the top of the loop
			
				//~ Apply_Default_BoundaryConditions(); //keep this here at the top of the loop
				event_bc();	// event for every computational step, required for debugging, time dependent boundary conditions, source etc.

				Update_Fluid_Properties();  //keep this above NS
				//~ printf("\nt = %e\t stc = %e\n\t ct = %d",Time,step,Count);
			
			if (VOF_Module==1)	// keeping here ensures having interface if user asks for plotting the lines in the user_event, as event has to be executed before updation of velocity and volfracs 
			{
			   /*********CONSTRUCT INTERFACE*******************************/
			     
				 /***********************VOF PART*********************************/
				Calculate_InitialSlope(); // keep above recon interface    //POPULATES NX AND NY INITIALLY FROM A GUESS BASED ON NEIGHBOURING CELLS VF
				
				for(r=1;r <= NoOfRows ; r++)
				{
				   for(c=1;c <= NoOfCols ; c++)
					{
					  Recon_Interface(r,c);   //CALCULATE INTERFACE
					}
				}
	 			/*********************************************************/
				plot_lines();	// keep here to keep the coordinates of line variables populated with interface data in case event needs it, such as output interface 
				/******************************************************/
			}
			
			if (1)	// keeping here ensures having interface if user asks for plotting the lines in the user_event, as event has to be executed before updation of velocity and volfracs 
			{
			   /*********CONSTRUCT INTERFACE*******************************/
			     
				 /***********************VOF PART*********************************/
				Calculate_InitialSlope_solid(); // keep above recon interface    //POPULATES NX AND NY INITIALLY FROM A GUESS BASED ON NEIGHBOURING CELLS VF
				
				for(r=1;r <= NoOfRows ; r++)
				{
				   for(c=1;c <= NoOfCols ; c++)
					{
					  Recon_Interface_solid(r,c);   //CALCULATE INTERFACE
					}
				}
	 			/*********************************************************/
				//~ plot_lines_solid();	// keep here to keep the coordinates of line variables populated with interface data in case event needs it, such as output interface 
				/******************************************************/
			}
			/*******************************user defined events to be executed for data analysis and output***************************/
			
			
				if ( absolute(Time-step*Count)<=1e-10)
				 {
									 
					 	event_phy(step*Count);	// event at specialized physical time step, such as data output or tracking a variable etc.
					
					 Count = Count + 1;
				}
				
				
			//~ if (bugger==1)	
				//~ event((double )StepT);
				
				
			/*******************************user defined events to be executed for data analysis and output***************************/
	
			
				
				
				
				
			/***************************************computational time step control system*************************************/

				CFL_Check();// calculate the require delT keep above NS, keep below events and BC, to account if user puts some time dependent conditions

				// Control dt for events to execute, below will ensure that dt should not exceed which surpass a event step
				if ( Time+delT>step*Count)
				 {
					 delT = step*Count - Time;
					 
					
				 }
			/******************delT dependent variables keep them here***********************/
						delTBydel      = delT/del;
						delTdel        = delT*del;
			
				 //~ printf("\nt = %e\t stco = %e\t cto = %d\n",Time,step,Count);
				
				
				/********************Write the total VolFrac in output file*********************************/
				//~ Tot_VolFrac = 0.0;
				 //~ Tot_Vol = 0.0;
  					 //~ for(r=1;r <= NoOfRows; r++)
					 //~ {
					    //~ for(c=1;c <= NoOfCols ; c++)
						 //~ {
							 //~ Tot_VolFrac = Tot_VolFrac + (Cellattrib[r][c].VolFrac);
							 //~ if (r != 1)
							 //~ {
								 //~ Tot_Vol = Tot_Vol + 2*PI*r*del_sq*(Cellattrib[r][c].VolFrac);
							 //~ }
							 //~ else if (r ==1)
							 //~ {
								 //~ Tot_Vol = Tot_Vol + PI*r*r*del*(Cellattrib[r][c].VolFrac);
								 
							 //~ }
						 //~ }
					 //~ }
						//~ fprintf(ptrWrite_Output,"\n%d\t%e\t%e\t%e", StepT,delT,Time,Tot_VolFrac);
							RunTimeEnd= time(0);
							RunTimeEnd = (RunTimeEnd - RunTimeStart);
							double complete=end<nodata ? Time/end*100.0: 0.0;
							double est= end<nodata ? ((end-Time)/((Time+1.0e-15)/(RunTimeEnd+1.0e-15)))/60 : 0;
							
						if(Progress==1)
						{	if(end<nodata)
							{
								if (est<60.0)
								{	
									printf("\nStep: %d delT: %e Time: %e runtime: %.0lf min Progress: %.2lf%% EST: %.0lf min", StepT,delT,Time,RunTimeEnd/60,complete,est);
								}
								else{
									
									printf("\nStep: %d delT: %e Time: %e runtime: %.0lf h Progress: %.2lf%% EST: %.0lf h", StepT,delT,Time,RunTimeEnd/3600,complete,est/60.0);
								}
							}
							else
							{
								printf("\nStep: %d delT: %e Time: %e runtime: %.0lf s Progress: NA EST: NA", StepT,delT,Time,RunTimeEnd);
							}
							fflush(stdout);
						}
						//~ printf("%e",Tot_Vol);
						event_cp(); // keep before NS
				/***********************************************************/
				if (NS_Module==1)   // Needed for VOF only test cases 
				{
					 // NAVIER STOKES HERE
				
					 Advance_Velocity_Field();
				
				 /***********************************************/
					
					event_bc(); // keep this here update ghost cell velocities for Neumann BC to get VOF fluxes right
					
					
				}
			if (VOF_Module==1)	// Switch this from global_variables.h, don't touch here
			{
				
								
				
				
				
				
				
				//~ /***********Write data here ko keep file_names consistent with steps*********/
				//~ if ( absolute(Time-step*(Count-1))<1e-5 && Plot_Data!=0)
				 //~ {
					
					
					 //~ WriteToFile(step*(Count-1));	// GENERATE Plot_Line{Step}.dat	
				//~ }
		
		
				
				if(Togg == 0)     //1ST HORIZONTAL AND 2ND VERTICAL PASS
				 {
					 Pass_main(1,Togg);
					InterfaceRecon_main();
					 Pass_main(2,Togg);
					 Togg = Togg + 1;
				}
				 else if (Togg == 1)   //1ST VERTICAL AND 2ND HORIZONTAL PASS
				 {
					 Pass_main(2,Togg);

					 InterfaceRecon_main();
					Pass_main(1,Togg);
					 Togg = Togg - 1;

				 }
				 
				 if(Togg_solid == 0)     //1ST HORIZONTAL AND 2ND VERTICAL PASS
				 {
					 Pass_main_solid(1,Togg_solid);
					InterfaceRecon_main_solid();
					 Pass_main_solid(2,Togg_solid);
					 Togg_solid = Togg_solid + 1;
				}
				 else if (Togg_solid == 1)   //1ST VERTICAL AND 2ND HORIZONTAL PASS
				 {
					 Pass_main_solid(2,Togg_solid);

					 InterfaceRecon_main_solid();
					Pass_main_solid(1,Togg_solid);
					 Togg_solid = Togg_solid - 1;

				 }



			 }

			if (create_restore_point)
				 restore_point();	// create a restore point at each time step if not writing bcs_data, if want to restart change the end variable and put restore=1 in main file

				//~ /*INCREMENT COUNTERS*/
				StepT++;

				/******************/
				 //* Compute cumulative time *//
				 Time = Time + delT;





	       }//END OF WHILE
	       
	    //   printf("\n%e\t%e\n",U_residue,V_residue);
		//~ WriteToFile_NS(StepT);
	    //   exit(0);


	/*#################################################################*/

	/***CLOSE ALL FILE POINTERS*******/
	//~ fclose(ptrWrite_Output);
	//fclose(ptrWrite_Y);
	/************END**************/
	RunTimeEnd= time(0);
	RunTimeEnd = RunTimeEnd - RunTimeStart;
	printf("\nThe total time for run is %.0lf seconds\n",RunTimeEnd);
	       
	
	      
		//RUN IS OVER
}

/****************************************************************
METHOD NAME       : Init_GlobalVars
DESCRIPTION          : INITIALISES ALL THE GLOBAL VARIABLES
LIMITATIONS            : NONE
****************************************************************/
void Init_GlobalVars()
  {
	/*********DECLARE LOCAL VARIABLES*************/
	int r,c;//junk variables to fool sscanf
          double VolFrac;
        char arr[100];
	
	
	  //printf("del = %lf", del);

	  //delT = 0.00125;

	/**********************************************/
		/*******************/
			int i;
	  
			X_Velocity = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Velocity = (double **)calloc(NoOfRows+2,sizeof(double*));
			X_VelC= (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_VelC = (double **)calloc(NoOfRows+2,sizeof(double*));
			X_Velocity_star = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Velocity_star = (double **)calloc(NoOfRows+2,sizeof(double*));
			X_Flux = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Flux = (double **)calloc(NoOfRows+2,sizeof(double*));
			mu = (double **)calloc(NoOfRows+2,sizeof(double*));
			rho = (double **)calloc(NoOfRows+2,sizeof(double*));
			Press = (double **)calloc(NoOfRows+2,sizeof(double*));
			vorticity = (double **)calloc(NoOfRows+2,sizeof(double*));
			streamfunction = (double **)calloc(NoOfRows+2,sizeof(double*));
			vel_potential = (double **)calloc(NoOfRows+2,sizeof(double*));
			Cellattrib = (STRUCT_CELL_ATTRIB **)calloc(NoOfRows+2,sizeof(STRUCT_CELL_ATTRIB*));
			solid = (STRUCT_CELL_ATTRIB **)calloc(NoOfRows+2,sizeof(STRUCT_CELL_ATTRIB*));
			//~ kappa = (double **) calloc(NoOfRows+2,sizeof(double *));
			X_Velocity_imp = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Velocity_imp = (double **)calloc(NoOfRows+2,sizeof(double*));
			eta = (double *)calloc(NoOfCols+2,sizeof(double*));
			RF = (double *)calloc(NoOfCols+2,sizeof(double*));
			phi = (double **)calloc(NoOfRows+2,sizeof(double*));
			cc = (double **)calloc(NoOfRows+2,sizeof(double*));
			ccs = (double **)calloc(NoOfRows+2,sizeof(double*));
			kappa = (double **)calloc(NoOfRows+2,sizeof(double*));
			
			Xp1 = (double **)calloc(NoOfRows+2,sizeof(double*));
			Yp1 = (double **)calloc(NoOfRows+2,sizeof(double*));
			Xp2 = (double **)calloc(NoOfRows+2,sizeof(double*));
			Yp2 = (double **)calloc(NoOfRows+2,sizeof(double*));
			
			ST_Fx = (double **) calloc(NoOfRows+2,sizeof(double *));
			ST_Fy = (double **) calloc(NoOfRows+2,sizeof(double *));
			x = (double **) calloc(NoOfRows+2,sizeof(double *));
			y = (double **) calloc(NoOfRows+2,sizeof(double *));
			ht_x = (double **) calloc(NoOfRows+2,sizeof(double *));
			ht_y = (double **) calloc(NoOfRows+2,sizeof(double *));
			
			SourceU = (double **) calloc(NoOfRows+2,sizeof(double *));
			SourceV = (double **) calloc(NoOfRows+2,sizeof(double *));
			
			
			for (i=0; i<NoOfRows+2; i++)
			{
				X_Velocity[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Velocity[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_VelC[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_VelC[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_Velocity_imp[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Velocity_imp[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_Velocity_star[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Velocity_star[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_Flux[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Flux[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				mu[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				rho[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Press[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				vorticity[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				streamfunction[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				vel_potential[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				phi[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				cc[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				ccs[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Cellattrib[i] = (STRUCT_CELL_ATTRIB *) calloc (NoOfCols+2, sizeof (STRUCT_CELL_ATTRIB));
				solid[i] = (STRUCT_CELL_ATTRIB *) calloc (NoOfCols+2, sizeof (STRUCT_CELL_ATTRIB));
				//~ kappa[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				Xp1[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Yp1[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Xp2[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Yp2[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				
				ST_Fx[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				ST_Fy[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				x[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				y[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				ht_x[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				ht_y[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				SourceU[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				SourceV[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
				
			}
		//exit(0);
			

	
 

/***************************Restore Simulation if needed use restore=1 in simulation file***********************/	  
if (restore)
{	
	
			//~ if( access( "restore.bcs", F_OK ) != -1 ) {
			if( access( passname, F_OK ) != -1 ) {
					    // file exists do nothing
			} else {
			   printf("********************\n Cannot restore: restore bcs file not found \n **************************");
				exit(0);
			}
			
			FILE *ptrRead;
			 //~ char LineFormat[] = "./NS_Data/NS%g.dat";
			//~ char LineFileName[20];
		    //~ sprintf(LineFileName,LineFormat,Start_T);
		 
		       ptrRead =fopen(passname,"rt");
					
					// restore geometry info 
			
				if(fgets(arr,100, ptrRead)!=NULL)
				       sscanf (arr,"%lf\t%lf\t%d\t%d\n",&Time,&del,&NoOfRows,&NoOfCols);	
				
			double junk;
			for(r=0;r <= NoOfRows+1 ; r++)
		       {
			 for(c=0;c <= NoOfCols+1 ; c++)
				{
					
				if(fgets(arr,100, ptrRead)!=NULL)
				      sscanf (arr,"%lf\t%lf\t%lf\%lf\n",&X_Velocity[r][c],&Y_Velocity[r][c],&Press[r][c],&Cellattrib[r][c].VolFrac);
				     
				    

					 
				}
		       }
				fclose(ptrRead);  
       }
       else { // starting with not a restore file, initializing the dependent variables
			if (volfrac_gen)
			{	Initialise_VolFrac(1);
			}
			else
			{
				Initialise_VolFrac_old(1);	// more accurate but slower
			}
			
			if (1)
			{	Initialise_VolFrac(0);
			}
			else
			{
				Initialise_VolFrac_old(0);	// more accurate but slower
			}
				
			//~ /********************INTIALISE THE VELOCITY/FLUX FIELD**************/
			for(r=1;r <= NoOfRows ; r++)
		       {
			       for(c=1;c <= NoOfCols ; c++)
			       {
				
				//~ if (solid[r][c].VolFrac<alpha_max){	
				       X_Velocity[r][c] = Init_U((c-1)*del , (r-0.5)*del);	  
				      
				       Y_Velocity[r][c] = Init_V((c-0.5)*del , (r-1)*del);
				      
				      
				       Press[r][c] = Init_P((c-0.5)*del , (r-0.5)*del);
				//~ }
				  
					
			       }
		       }
	 
       }
/***************************End***********************/	
  
// Code below should run even it restored*****************/       
	int f;
	
	for (f=1;f<=NoOfCols+2;f++)
	{
		if (axi ==0)
		{
			eta[f] = 1;
			RF[f]=1;
		}
		else if (axi ==1)
		{
			eta[f] = (f-1)*del;	//left face
			RF[f] = (f-0.5)*del; // center of the cell
		}
	}

		       
		/******************INITIALISE GLOBAL VARIABLES**********/
		  
		    
		    del_sq         = pow(del,2);
		    del_sqBy2      = del_sq / 2.0;
		    del2           = 2.0*del;
		    del3           = 3.0*del;
		    delBy2         = del/2.0;
		    PiBy2          = PI/2.0;
		    PiBy4          = PI/4.0;
		    ThreePiBy2     = 3.0*PI/2.0;
		    delTBydel      = delT/del;
		    delTdel        = delT*del;
		     /*******************************************************/
		// Initialize some variables with a big number, this is done for post processing, calloc initializes as zero, but zero is actual data most of the times
		      for (r=0; r<=NoOfRows+1; r++)
			{
				for(c=0; c<=NoOfCols+1; c++)
				{
					Xp1[r][c]=nodata;
					Yp1[r][c]=nodata;
					Xp2[r][c]=nodata;
					Yp2[r][c]=nodata;
					
					x[r][c] = (c-0.5)*del;
					y[r][c]=(r-0.5)*del;
				}
			}


       
  }
  
 

/*******************************
NAME: CFL_Check()
Description: Restrict the time steps in accordance with CFL criteria
******************************/
void CFL_Check()
{				int r,c, index=0;
				double umax,vmax,delTcalc[5],max_vel, V,Vmax;
				// find maximum velocity
				umax = 0; vmax=0;
				for(r=1;r <= NoOfRows ; r++)
				{
				   for(c=1;c <= NoOfCols; c++)
					{
						umax = max(umax,absolute(X_Velocity[r][c]));
						vmax = max(vmax,absolute(Y_Velocity[r][c]));
						max_vel = max(umax,vmax);
					//	V = pow(pow(X_Velocity[r][c],2) + pow(Y_Velocity[r][c],2),0.5);
					//	Vmax = max(Vmax,V);
					}
				}
				
				
					delTcalc[0]=nodata;
					delTcalc[1]=nodata;
					delTcalc[2]=nodata;
					delTcalc[3]=nodata;
					delTcalc[4]=nodata;
				
					//~ printf("\t%e\t%e",umax,vmax);
					if (max_vel>1e-7)
					{
						delTcalc[0]= CFL*0.5*del/max_vel;
						
						//~ if (delT_adv<delT){
							//~ delT = delT_adv;
							//~ sim=1;
						//~ }
						
						
					}
					if (Diffusion_term ==1 && Diffusion_explicit == 1)
					{	
						//~ nu_max = VOF_Module==1?max(mu_G/rho_G, mu_L/rho_L):mu_G/rho_G; // max dynamic viscosity
						delTcalc[1] = (0.25*min(rho_L,rho_G)/max(mu_L,mu_G))*pow(del,2);	//Diffusion term criterian
						
						//~ if (delT_Diff<delT){
							//~ delT = delT_Diff;
							//~ sim=2;
						//~ }
						
					}
					 if (Sigma > 1e-7)
					{
						delTcalc[2] = pow(((rho_G+rho_L) * pow(del,3)/(2.0*PI*Sigma)),0.5);
						
						//~ if (delT_Sigma<=delT){
							//~ delT = delT_Sigma;
							//~ sim=3;
						//~ }
					}
					//~ // body force time control
					 for(r=1;r <= NoOfRows ; r++)
					{
					   for(c=1;c <= NoOfCols; c++)
						{
							umax = max(umax,absolute(SourceU[r][c]));
							vmax = max(vmax,absolute(SourceV[r][c]));
							max_vel = max(umax,vmax);
						//	V = pow(pow(X_Velocity[r][c],2) + pow(Y_Velocity[r][c],2),0.5);
						//	Vmax = max(Vmax,V);
						}
					}
					if (max_vel>1e-7)
					{
						delTcalc[3]= CFL*pow(2.0*del/max_vel,0.5);

					}		
					
					
						//~ delT = delT;
					//~ printf("delT : %lf",delT);
						//~ exit(0);
						//delT = 1e-5; //remove this after the test
						
						
						double minimum = delTcalc[0];
    
						    for ( int j = 1 ; j < 5 ; j++ ) 
						    {
							if ( delTcalc[j] < minimum ) 
							{
								minimum = delTcalc[j];
								index = j;
							}
						    }
						//~ if (delT<minimum	){
							//~ delT = 1.1*delT;
							//~ index = 5;
						//~ }
						//~ else{
							delT = minimum;
						//~ }
						if (Forced_delT>0)
						{
							delT = Forced_delT;
							index=4;
						}
						switch(index)
						{
							case 0: 
								printf(" delT_Control: Advection");
								break;
							case 1: 
								printf(" delT_Control: Diffusion");
								break;
							case 2: 
								printf(" delT_Control: tension");
								break;
							case 3: 
								printf(" delT_Control: Body Force");
								break;
							case 4: 
								printf(" delT_Control: Forced");
								break;
							case 5: 
								printf(" delT_Control: increase");
								break;
							default:
								printf(" delT_Control: None");
								break;
						}
						
						if(delT<=0)
						{
							printf("CFL_Check() Error: delT=0:");
						}
						
						
}
/******************************************************************************************************
NAME       : Pass_main()
DESCRIPTION: THIS IS A DUMY FUNCTION WHICH IS USED TO REMOVE CODE FROM main()
			 TOGG IS A VARIABLE USED FOR ALTERNATING BETWEEN VERTICAL AND HORIZONTAL PASSES
			 FLAG IS A VARIABLE USED FOR DECIDING WHETHER THE CURRENT PASS IS HORIZONATAL / VERTICAL
LIMITATIONS: NONE
/******************************************************************************************************/
void Pass_main(int Flag, int Togg)
{
	int r, c;
	BookKeeping();
	CalculateF_Flux(Flag);   //CALCULATE X FLUXES
	
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
		  AdvanceF_field(r,c,Flag,Togg);  //ADVANCE F IN TIME BASED ON FLAG VALUE AND HORIZONTAL FLUXES
		}
	}
	BookKeeping();

	
}

/**************************************************************************
NAME       : BookKeeping
DESCRIPTION:
LIMITATIONS:
***************************************************************************/
void BookKeeping()
{
    /*********DECLARATIONS***********/
	int r, c;
	  double VolFrac;
	/*******************************/

	//ASSIGN 0 AND 1 TO CELLS WHICH ARE OUT OF BOUND
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
			if(Cellattrib[r][c].VolFrac < LOWER_LIMIT)
			  {
				//printf("\nCame here\n");
				Cellattrib[r][c].VolFrac  = 0;
				//~ Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
			  }
			if(Cellattrib[r][c].VolFrac > UPPER_LIMIT)
			  {
				Cellattrib[r][c].VolFrac  = 1;
				//~ Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
			  }
	   }
	}
	/***/
}






void Update_Fluid_Properties()
  {

	  int r,c;


		

        // run on ghost cells also it implements Neumann on mu and rho also, which it should to avoid any jump on boundaries
	       for (r=0; r<=NoOfRows+1; r++)
		{
			for(c=0; c<=NoOfCols+1; c++)
			{

			    mu[r][c] = mu_L*Cellattrib[r][c].VolFrac + mu_G*(1.0-Cellattrib[r][c].VolFrac);
			    rho[r][c] = rho_L*Cellattrib[r][c].VolFrac + rho_G*(1.0-Cellattrib[r][c].VolFrac);
				cc[r][c] = (Cellattrib[r][c].VolFrac>0.5);	
				ccs[r][c] = (solid[r][c].VolFrac>0.5);	
					
					
				
			}
		}
	

}


void Advance_Velocity_Field()

{

	int r,c;


	{
	
	double sum = 0.0;
		
	
			// Let this be below, it will be needed to test lid driven cavity

			//~ for (r=0; r<=NoOfRows+1; r++)
			//~ {
				//~ for (c=0; c<=NoOfCols+1; c++)
				//~ {
					//~ X_Velocity_old[r][c] = X_Velocity[r][c];
					//~ Y_Velocity_old[r][c]	=	Y_Velocity[r][c];
				//~ }
			//~ }		
		
				
					//Calculate_Surface_Tension_MAC();
				if (Sigma!=0)
				{
					Calculate_Surface_Tension_Height_Function_DAC();
				}
					// X momentum equation
			
					Calculate_X_Velocity_star();
			

					// Y momentum equation
					Calculate_Y_Velocity_star();
					// Diffusion term
				if (Diffusion_term==1 && Diffusion_explicit==0)
				{
					//~ Calculate_XY_Velocity_star_implicit()	;
					
					Poissons_Solver_Multigrid_implicit();
				}
		

				//printf("delT = %lf",delT);
			
				
			switch(Linear_Solver)
			{
				case 1 :
					Poissons_Solver_Multigrid();
					
					break;
				case 2:
					Poissons_Solver_SOR();
					break;
				
			}
			#if (Linear_Solver==3)
				Poissons_Solver_hypre();
			#endif
				//exit(0);
	
				X_Velocity_Correction();
				Y_Velocity_Correction();
			
	//~ for(r=1;r<=NoOfRows;r++)
	//~ {
		//~ for(c=1;c<=NoOfCols;c++)
		//~ {
			//~ printf("\nconst=%e\n",(((X_Velocity[r][c+1]*eta[c+1]   -	X_Velocity[r][c]*eta[c])/RF[c] + (Y_Velocity[r+1][c]	-	Y_Velocity[r][c]))/del));
		//~ }
	//~ }
			
		//~ // Let this be below, it will be needed to test lid driven cavity
		//~ // Calculate residue
		//~ U_residue = 0;			//reset residue to zero only here, dont move up or down the loop
		//~ V_residue =0;
		//~ for (r=0; r<=NoOfRows+1; r++)
		//~ {
			//~ for (c=0; c<=NoOfCols+1; c++)
			//~ {
				//~ U_residue =  max( U_residue , absolute( X_Velocity[r][c] - X_Velocity_old[r][c] ) );
				//~ V_residue =  max( V_residue , absolute( Y_Velocity[r][c] - Y_Velocity_old[r][c] ) );
			//~ }
		//~ }

	//	printf("\t%e\t%e",U_residue,V_residue);

			//printf("%d Time Step \n",StepT);

				//StepT++;
	}



}





/********************
Name: Ad_Flux_Calc
Description: Switch to the advection scheme specified in Global_Variables.h
**************************/
  double Ad_Flux_Calc(double Vel_left,   double Vel_right,   double phi_UUU,  double phi_UU, double phi_U,   double phi_D,  double phi_DD , double phi_DDD)
{
	
	/*********Conventional Schemes Variables********/
	 double temp,Vel_Wall,phi_Wall_p,phi_Wall_n;
	double w1,w2,w3;
	
	/******************References for WENO***********************
	Essentially Non-Oscillatory and Weighted Essentially
	Non-Oscillatory Schemes for Hyperbolic
	Conservation Laws
	Author: Chi-Wang Shu
	Affiliation: Brown University
	/*****************************************************************/
	
	/********WENO variables*********/
	double phi_0,phi_1,phi_2;
	double w_0,w_1,w_2;
	double w_0_tilde,w_1_tilde,w_2_tilde;
	double beta_0,beta_1,beta_2;
	double epsilon = 1e-7;
	/*************************************/
	
	Vel_Wall = (Vel_right+Vel_left)/2;	// face-centered velocity

	 switch(Advection_Scheme ) 
	{

		case 1  :	// First order upwind
      
			w1=0; w2=1; w3=0;	
   
			break;
	
		case 2  :		// Centeral difference
	   
			w1=0.5 ; w2=0.5; w3=0;   
			break; 
  
		case 3 :		// second order upwind
			
			w1=0; w2=1.5; w3= -0.5;
			break;
		
		case 4 :		// Quick
			
			w1=0.375; w2=0.75; w3= -0.125;
			break;
		
		case 5 :		//WENO
			
		{	/*****************Left stencils biasing********************************************/
			phi_0 = 1.0/3*phi_UUU -7.0/6*phi_UU + 11.0/6*phi_U ;
			phi_1 = -1.0/6*phi_UU +5.0/6*phi_U + 1.0/3*phi_D ;
			phi_2 = 1.0/3*phi_U +5.0/6*phi_D - 1.0/6*phi_DD ;
			
			beta_0 = 13.0/12*pow((phi_UUU-2*phi_UU+phi_U),2) + 1.0/4*pow((phi_UUU - 4*phi_UU +3*phi_U),2);
			
			beta_1 = 13.0/12*pow((phi_UU-2*phi_U+phi_D),2) + 1.0/4*pow((phi_UU - phi_D),2);
			
			beta_2 = 13.0/12*pow((phi_U-2*phi_D+phi_DD),2) + 1.0/4*pow((3*phi_U- 4*phi_D +phi_DD),2);

			


			w_0_tilde = 0.1/pow((epsilon+beta_0),2);
			w_1_tilde = 0.6/pow((epsilon+beta_1),2);
			w_2_tilde = 0.3/pow((epsilon+beta_2),2);


			w_0 = w_0_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_1 = w_1_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_2 = w_2_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			
			phi_Wall_n = w_0*phi_0 + w_1*phi_1 + w_2*phi_2;
			/****************************************************************************************/
			
			/*****************Right stencils biasing********************************************/
			phi_0 = 1.0/3*phi_DDD -7.0/6*phi_DD + 11.0/6*phi_D ;
			phi_1 = -1.0/6*phi_DD +5.0/6*phi_D + 1.0/3*phi_U ;
			phi_2 = 1.0/3*phi_D +5.0/6*phi_U -1.0/6*phi_UU ;
			
			beta_0 = 13.0/12*pow((phi_DDD-2*phi_DD+phi_D),2) + 1.0/4*pow((phi_DDD - 4*phi_DD +3*phi_D),2);
			
			beta_1 = 13.0/12*pow((phi_DD-2*phi_D+phi_U),2) + 1.0/4*pow((phi_DD - phi_U),2);
			
			beta_2 = 13.0/12*pow((phi_D-2*phi_U+phi_UU),2) + 1.0/4*pow((3*phi_D- 4*phi_U +phi_UU),2);

			


			w_0_tilde = 0.1/pow((epsilon+beta_0),2);
			w_1_tilde = 0.6/pow((epsilon+beta_1),2);
			w_2_tilde = 0.3/pow((epsilon+beta_2),2);


			w_0 = w_0_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_1 = w_1_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_2 = w_2_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			
			phi_Wall_p = w_0*phi_0 + w_1*phi_1 + w_2*phi_2;
			/****************************************************************************************/
			temp = (max(Vel_Wall,0))*phi_Wall_n+ (min(Vel_Wall,0))*phi_Wall_p;
			//printf("%lf\n",temp);
			return temp;
			
		}
		
		default : 
		
		printf("\n ERROR: Specify the advection scheme in global_Variables.h properly\n");
		exit(0);
	}
	
		
		phi_Wall_n = w1*phi_D + w2*phi_U + w3*phi_UU;
		phi_Wall_p = w1*phi_U + w2*phi_D + w3*phi_DD;
		temp = (max(Vel_Wall,0))*phi_Wall_n + (min(Vel_Wall,0))*phi_Wall_p;
		//printf("%lf\n",temp);
	return temp;
}

//~ /********************
//~ Name: Ad_FOU
//~ Description: Calculates the momentum fluxes due to advection on LHS or Bottom of the control volume or cell
//~ Scheme: centeral difference
//~ **************************/
  //~ double Ad_FOU(  double Vel_left,   double Vel_right,   double phi_left,   double phi_right)
//~ {
	  //~ double temp,Vel_Wall,phi_Wall;

	//~ Vel_Wall = (Vel_right+Vel_left)/2;
	//~ phi_Wall = (phi_left + phi_right)/2;
	//~ //temp = (fmax(Vel_Wall,0))*phi_left + (fmin(Vel_Wall,0))*phi_right;
	//~ temp = (Vel_Wall*phi_Wall);
	//~ return temp;
//~ }
/*******************
Name: Calculate_X_Velocity_star
Description: Calculates the temporary velocities without accounting pressure
Limitations:
*******************/
void Calculate_X_Velocity_star()
{
	int r,c;
	int stage; // for TVD WENO
	  double mu_bottom, rho_r;
		//~ double  Ad_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2], Ad_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	  //~ double Diff_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2],Diff_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	//~ //double X_Vel_UU,X_Vel_U, X_Vel_D,X_Vel_DD;
	int i;
	
	double sum =0.00;
	
	double **Ad_Flux_Vertical_Faces,**Ad_Flux_Horizontal_Faces;
	double **Diff_Flux_Vertical_Faces,**Diff_Flux_Horizontal_Faces;
	
	Ad_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Ad_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Diff_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Diff_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	
			for (i=0; i<NoOfRows+2; i++)
			{
				Ad_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Ad_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}
	//Copying the updated values to starred variables, necessary for WENO TVD
	for(r=0;r<=NoOfRows+1;r++)
	{
		for(c=0;c<=NoOfCols+1;c++)
		{
			X_Velocity_star[r][c] = X_Velocity[r][c];
		}
	}
	
	for(r=1;r<=NoOfRows;r++)
	{
		for(c=1;c<=NoOfCols;c++)
		{
			sum=sum+X_Velocity_star[r][c];
		}
	}
       
/***********************************************ADVECTION FLUXES***********************************************/
	if (Advection_term==1) //Switch "Advection term" from global_Variables.h don't touch here
	{	
		//Calculate Advection fluxes on the four faces of the cell
			// Boundary Control Volumes special treatment, linear extrapolation of super-ghost values
		for (stage=1;stage<=3;stage++)
		{
			for(r=1;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols+1;c++)
				{
					if (c==1 && axi ==1)
					{
						Ad_Flux_Vertical_Faces[r][c] = 0;
					}						
					if (c==2)
					{	
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-1], X_Velocity_star[r][c-1], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );
					}
					else if (c==3)
					{
						
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-2], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );
					}
					else if(c==NoOfCols)
					{
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+1] );	
					}
					else if(c==NoOfCols+1)
					{
									
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c], X_Velocity_star[r][c]);	
						
					}
					else
					{	
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );
						
					}
				}
			}
			// Horizontal Faces
			for(r=1;r<=NoOfRows+1;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					if (r==1)
					{	
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], 	X_Velocity_star[r-1][c],	X_Velocity_star[r-1][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
					else if (r==2)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-2][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
					else if (r==NoOfRows)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+1][c]);
					}
						
					else if(r==NoOfRows+1)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r][c], X_Velocity_star[r][c]);
					}
					else
					{	
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
				}
			}
			//TVD stages
			if (stage==1)
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=2;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						X_Velocity_star[r][c] = X_Velocity[r][c]	-	delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*RF[c]- Ad_Flux_Vertical_Faces[r][c]*RF[c-1])/eta[c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] );	
						}
						//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
			}
			else if (stage==2 && Advection_Scheme==5 )
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=2;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						X_Velocity_star[r][c] = 0.75*X_Velocity[r][c]	+	0.25*(X_Velocity_star[r][c] - delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*RF[c]- Ad_Flux_Vertical_Faces[r][c]*RF[c-1])/eta[c]+ Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						}//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
			}
			
			else if (stage==3 && Advection_Scheme==5 )
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=2;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						X_Velocity_star[r][c] = 1.0/3*X_Velocity[r][c]	+	2.0/3*(X_Velocity_star[r][c] - delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*RF[c]- Ad_Flux_Vertical_Faces[r][c]*RF[c-1])/eta[c]+ Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						}//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
			}
			
			else	// if not WENO don't follow stages exit the loop
			{
				break;
			}
		
		}
	}
       
	
	
	if (Diffusion_term==1 && Diffusion_explicit==1)	//Switch of diffusion term
	{
		//vertical faces
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=1;c<=NoOfCols+1;c++)
			{
				double mu_temp = mu[r][c-1];
				
				if (solid[r][c-1].VolFrac<alpha_max)
				{	
					mu_temp = mu[r][c-1]/(1.0-solid[r][c-1].VolFrac) ;
				}
						
				Diff_Flux_Vertical_Faces[r][c]		=	2*(	mu_temp*( X_Velocity[r][c]	-	X_Velocity[r][c-1] )*RF[c-1]/del	);
				if (isnan(Diff_Flux_Vertical_Faces[r][c]))
								printf("BC");	
				//~ if  (axi ==1 && c==1)
				//~ {
					//~ Diff_Flux_Vertical_Faces[r][c] =0.00;
				//~ }
			}
		}
		//Horizontal Faces
		for(r=1;r<=NoOfRows+1;r++)
		{
			for(c=1;c<=NoOfCols+1;c++)
			{	
				mu_bottom	=	0.25*( mu[r][c] /(solid[r][c].VolFrac<0.96?(1.0-solid[r][c].VolFrac):1)	+	mu[r][c-1]/ (solid[r][c-1].VolFrac<0.96?(1.0-solid[r][c-1].VolFrac):1)	+	mu[r-1][c-1] / (solid[r-1][c-1].VolFrac<0.96?(1.0-solid[r-1][c-1].VolFrac):1)	+	mu[r-1][c] /(solid[r-1][c].VolFrac<0.96?(1.0-solid[r-1][c].VolFrac):1) );
				
				
				Diff_Flux_Horizontal_Faces[r][c]	=	(	mu_bottom*( ( X_Velocity[r][c]	-	X_Velocity[r-1][c] ) 	+ ( Y_Velocity[r][c]	-	Y_Velocity[r][c-1])	) /del	);
			}
		}
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
				
				if (solid[r][c].VolFrac<0.96){
				X_Velocity_star[r][c] = X_Velocity_star[r][c]	+	(1.0/rho_r)*delT* ( (Diff_Flux_Vertical_Faces[r][c+1]/eta[c]- Diff_Flux_Vertical_Faces[r][c]/eta[c]+ Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c])/del - axi*(mu[r][c]+mu[r][c-1]) * ( X_Velocity[r][c]/pow(eta[c],2) ) );
				}
				//~ if (isnan(X_Velocity_star[r][c]))
								//~ printf("BC");	
				//printf("\n%e", (1.0/del)*(Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] ));
				
				
			}
		}
	}
	
		
	if(SourceU!=0)	// if there is a non-zero source term
	{
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				if (solid[r][c].VolFrac<0.96){
				X_Velocity_star[r][c] = X_Velocity_star[r][c] + delT*SourceU[r][c];
				}
				
			}
		}
	}

	
	
	
	
	if(Sigma!=0)	// if there is a non-zero surface tension
	{
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				
					
				rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
				if (solid[r][c].VolFrac<0.96){
					X_Velocity_star[r][c] = X_Velocity_star[r][c] + (1.0/rho_r)*delT*ST_Fx[r][c];
				}
			
			}
		}
	}
	

	
	
	
			freeArray(Ad_Flux_Vertical_Faces);
			freeArray(Ad_Flux_Horizontal_Faces);	
			freeArray(Diff_Flux_Vertical_Faces);	
			freeArray(Diff_Flux_Horizontal_Faces);
	//X_Velocity_star[r][c]	=	X_Velocity[r][c]	+	delT* (	 );		//no body force in X-direction
	
       
}
        

/*******************
Name: Calculate_Y_Velocity_star
Description: Calculates the temporary velocities without accounting pressure
Limitations:
*******************/
void Calculate_Y_Velocity_star()
{	
			int r,c;
			int stage; // for TVD WENO
			double mu_left, rho_r;
		//~ double  Ad_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2], Ad_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	  //~ double Diff_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2],Diff_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
		int i;
	
	double sum;
		double **Ad_Flux_Vertical_Faces,**Ad_Flux_Horizontal_Faces;
		double **Diff_Flux_Vertical_Faces,**Diff_Flux_Horizontal_Faces;
	
		Ad_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Ad_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Diff_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Diff_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
			
			for (i=0; i<NoOfRows+2; i++)
			{
				Ad_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Ad_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}
	
	//Copying the updated values to starred variables, necessary for WENO TVD
	for(r=0;r<=NoOfRows+1;r++)
	{
		for(c=0;c<=NoOfCols+1;c++)
		{
			Y_Velocity_star[r][c] = Y_Velocity[r][c];
		}
	}
	
/***********************************************ADVECTION FLUXES***********************************************/
	if (Advection_term==1) //Switch "Advection term" from global_Variables.h don't touch here
	{	
		for (stage=1;stage<=3;stage++)
		{
			//Vertical faces
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols+1;c++)
				{
					if (c==1)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-1],Y_Velocity_star[r][c-1], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1], Y_Velocity_star[r][c+2]);
					}
					else if (c==2)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1],Y_Velocity_star[r][c+2]);
					}
					else if(c==NoOfCols)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1], Y_Velocity_star[r][c+1]);
					}
					else if(c==NoOfCols+1)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c], Y_Velocity_star[r][c]);
						
					}	
					else
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1],Y_Velocity_star[r][c+2]);
					}
				}
			}
			
			// Horizontal Faces
			for(r=2;r<=NoOfRows+1;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					if (r==2)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					else if(r==3)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-2][c], Y_Velocity_star[r-2][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					else if(r==NoOfRows)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+1][c]);
					}
					else if (r==NoOfRows+1)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c],Y_Velocity_star[r][c]);
					}
					else
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-2][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					
				}
			}
			//TVD stages
			if (stage==1)
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						Y_Velocity_star[r][c] = Y_Velocity[r][c]	-	delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*eta[c+1]- Ad_Flux_Vertical_Faces[r][c]*eta[c])/RF[c]+ Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] );	
						}//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
					}
				}
			}
			else if (stage==2 && Advection_Scheme==5 )
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						Y_Velocity_star[r][c] = 0.75*Y_Velocity[r][c] + 0.25*(Y_Velocity_star[r][c]	-	delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*eta[c+1]- Ad_Flux_Vertical_Faces[r][c]*eta[c])/RF[c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						}//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
					}
				}
			}
			else if (stage==3 && Advection_Scheme==5 )
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						if (solid[r][c].VolFrac<0.96){
						Y_Velocity_star[r][c] = 1.0/3*Y_Velocity[r][c] + 2.0/3*(Y_Velocity_star[r][c]	-	delTBydel* ((Ad_Flux_Vertical_Faces[r][c+1]*eta[c+1]- Ad_Flux_Vertical_Faces[r][c]*eta[c])/RF[c]+ Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						}//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
						
					}
				}
			}
			
		}
		
	}
	
/*************************Diffusion Fluxes****************************/
	
		if (Diffusion_term==1 && Diffusion_explicit==1)	//Switch "Diffusion term" from global_Variables.h don't touch here
		{	
			//Vertical faces
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols+1;c++)
				{	
					//~ mu_left		=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
					
					mu_left		=	0.25*( mu[r][c]/(solid[r][c].VolFrac<0.96?(1.0-solid[r][c].VolFrac):1) 	+	mu[r][c-1]/(solid[r][c-1].VolFrac<0.96?(1.0-solid[r][c-1].VolFrac):1) 	+	mu[r-1][c-1]/(solid[r-1][c-1].VolFrac<0.96?(1.0-solid[r-1][c-1].VolFrac):1) 	+	mu[r-1][c]/(solid[r-1][c].VolFrac<0.96?(1.0-solid[r-1][c].VolFrac):1) );
					
					Diff_Flux_Vertical_Faces[r][c] = (	mu_left*( ( X_Velocity[r][c]	-	X_Velocity[r-1][c] ) 	+ ( Y_Velocity[r][c]	-	Y_Velocity[r][c-1])	)*eta[c] /del	);
					
				}
			}
			
			//Horizontal Faces
			for(r=2;r<=NoOfRows+1;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					double mu_temp = mu[r-1][c] ;
					
					if (solid[r-1][c].VolFrac<0.96)
					{
						mu_temp = mu[r-1][c]/(1.0-solid[r-1][c].VolFrac) ;
					}
					
					Diff_Flux_Horizontal_Faces[r][c]		=	2.0*(	mu_temp*( Y_Velocity[r][c]	-	Y_Velocity[r-1][c] )/del	);
				}
			}
			
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
					if (solid[r][c].VolFrac<0.96){
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c]	+	(1.0/rho_r)*delT* ( (Diff_Flux_Vertical_Faces[r][c+1] /RF[c]- Diff_Flux_Vertical_Faces[r][c] /RF[c]+ Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] )/del  );	
					}//printf("\n%e",Y_Velocity_star[r][c]);
					//printf("\n%d\t%d\t%e",r,c,Diff_Flux_Horizontal_Faces[r][c]);
					//~ if (isnan(Y_Velocity_star[r][c]))
								//~ printf("BC");
				}
			}
		}
		//exit(0)	;
		
		if (SourceV!=0)
		{
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					if (solid[r][c].VolFrac<0.96){
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c] + delT*SourceV[r][c] ;
					}//printf("\n sourceV = %e",SourceV);
					//exit(0);
				}
			}
		}

		if (Sigma!=0)
		{
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
					if (solid[r][c].VolFrac<0.96){
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c] + (1.0/rho_r)*delT*ST_Fy[r][c] ;
					}
					//~ if (r==13 && c==1)
						//~ printf("bug");
					
					//~ if (r==14 && c==1)
						//~ printf("bug");
					//printf("\n sourceV = %e",SourceV);
					//exit(0);
				}
			}
		}
		//~ sum = 0.0;
		//~ for(r=1;r<=NoOfRows;r++)
		//~ {
			//~ for(c=1;c<=NoOfCols;c++)
			//~ {
				//~ sum = sum + (((X_Velocity[r][c+1]*eta[c+1]   -	X_Velocity[r][c]*eta[c])/RF[c] + (Y_Velocity[r+1][c]	-	Y_Velocity[r][c]))/delTdel)*0.5;
			//~ }
		//~ }
		//~ printf ("after Ydiffusion = %e",  Y_Velocity[3][64]);
			freeArray(Ad_Flux_Vertical_Faces);
			freeArray(Ad_Flux_Horizontal_Faces);	
			freeArray(Diff_Flux_Vertical_Faces);	
			freeArray(Diff_Flux_Horizontal_Faces);				
			
}

/******************
Name: Calculate_XY_Velocity_star_implicit
Description: Used for making diffusion term implicit
******************/
void Calculate_XY_Velocity_star_implicit()	
{
	
			int r,c;
			double mu_left, mu_right, rho_r,mu_bottom,mu_up;
			
	
			
					
		int N1=0; double error_implicit=1.0,error1,error2;
	
		
		while (error_implicit>1.0e-10)
		{	
				N1 = N1+1;
				error1 = 0;
				error2 = 0;
				error_implicit=0;
				
		
				BC_implicit();  		
	
			for(r=1;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{	
					
					if (c!=1){
						rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
						mu_bottom	=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
						mu_up =   0.25*( mu[r+1][c] 	+	mu[r+1][c-1]	+	mu[r][c-1]	+	mu[r][c] );
						
						
						//basilisk
						double denominator = 1.0 + delT/rho_r*0.5 * (mu[r][c]+mu[r][c-1])/pow(eta[c],2)*axi + delT/rho_r/del_sq*0.5 * (2.0*mu[r][c]*RF[c]/eta[c]  + 2.0*mu[r][c-1]*RF[c-1]/eta[c]  + mu_up + mu_bottom);
						
						
						double numerator = delT/rho_r/del_sq*0.5 * (2.0*mu[r][c]* X_Velocity_imp[r][c+1] *RF[c]/eta[c]  + 2.0* mu[r][c-1]*X_Velocity_imp[r][c-1] *RF[c-1]/eta[c] + mu_up * (X_Velocity_imp[r+1][c] + (Y_Velocity_imp[r+1][c] - Y_Velocity_imp[r+1][c-1])  ) 
													- mu_bottom* ( - X_Velocity_imp[r-1][c] + Y_Velocity_imp[r][c] - Y_Velocity_imp[r][c-1]));
						
						
						//~ X_Velocity_star[r][c] = numerator/denominator;
						X_Velocity_imp[r][c] = (X_Velocity_star[r][c] + delT/rho_r/del_sq*(0.5) * (2.0*mu[r][c]* (X_Velocity[r][c+1] - X_Velocity[r][c])*RF[c]/eta[c] - 2.0* mu[r][c-1]*(X_Velocity[r][c] - X_Velocity[r][c-1])*RF[c-1]/eta[c]
													+ mu_up *( X_Velocity[r+1][c] - X_Velocity[r][c] + 
													(Y_Velocity[r+1][c] - Y_Velocity[r+1][c-1])  ) 
													- mu_bottom* (X_Velocity[r][c] - X_Velocity[r-1][c] + Y_Velocity[r][c] - Y_Velocity[r][c-1])) - delT/rho_r*(0.5)*(mu[r][c]+mu[r][c-1]) * ( X_Velocity[r][c]/pow(eta[c],2) )*axi  + numerator ) / denominator; 
						
					}
			
					
				
					if (r!=1){
						mu_left		=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
						mu_right	=	0.25*( mu[r][c+1] 	+	mu[r][c]	+	mu[r-1][c]	+	mu[r-1][c+1] );
						rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
						
						
						double denominator = 1.0 + delT/rho_r/del_sq*0.5* (2.0*mu[r][c] + 2.0*mu[r-1][c] + mu_right*eta[c+1]/RF[c]  + mu_left*eta[c]/RF[c]  );
						
						double numerator =  delT/rho_r/del_sq*0.5 * (2.0*mu[r][c] * Y_Velocity_imp[r+1][c] + 2.0*mu[r-1][c]*Y_Velocity_imp[r-1][c] + mu_right * (Y_Velocity_imp[r][c+1] + X_Velocity_imp[r][c+1] - X_Velocity_imp[r-1][c+1]) *eta[c+1]/RF[c] 
										- mu_left* ( - Y_Velocity_imp[r][c-1] + X_Velocity_imp[r][c] - X_Velocity_imp[r-1][c] )*eta[c]/RF[c]  );
						
						//~ Y_Velocity_star[r][c] = numerator/denominator;
						Y_Velocity_imp[r][c] = ( Y_Velocity_star[r][c] + delT/rho_r/del_sq*(0.5) * (2.0*mu[r][c] * (Y_Velocity[r+1][c] - Y_Velocity[r][c] )- 2.0*mu[r-1][c]*(Y_Velocity[r][c] -Y_Velocity[r-1][c] )+ mu_right * (Y_Velocity[r][c+1]  - Y_Velocity[r][c] + X_Velocity[r][c+1] - X_Velocity[r-1][c+1])*eta[c+1]/RF[c] 
										- mu_left* (Y_Velocity[r][c]  - Y_Velocity[r][c-1] + X_Velocity[r][c] - X_Velocity[r-1][c] )*eta[c]/RF[c] ) + numerator ) /denominator;
					}	
					
				}
			}
			BC_implicit();  	
			
		
			for(r=1;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{	
					if (c!=1){
						rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
						mu_bottom	=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
						mu_up =   0.25*( mu[r+1][c] 	+	mu[r+1][c-1]	+	mu[r][c-1]	+	mu[r][c] );
						
						
						//basilisk
						double denominator = 1.0 + delT/rho_r*0.5 * (mu[r][c]+mu[r][c-1])/pow(eta[c],2)*axi + delT/rho_r/del_sq*0.5 * (2.0*mu[r][c]*RF[c]/eta[c]  + 2.0*mu[r][c-1]*RF[c-1]/eta[c]  + mu_up + mu_bottom);
						
						
						double numerator = delT/rho_r/del_sq*0.5 * (2.0*mu[r][c]* X_Velocity_imp[r][c+1] *RF[c]/eta[c]  + 2.0* mu[r][c-1]*X_Velocity_imp[r][c-1] *RF[c-1]/eta[c] + mu_up * (X_Velocity_imp[r+1][c] + (Y_Velocity_imp[r+1][c] - Y_Velocity_imp[r+1][c-1])  ) 
													- mu_bottom* ( - X_Velocity_imp[r-1][c] + Y_Velocity_imp[r][c] - Y_Velocity_imp[r][c-1]));
						
						
						//~ X_Velocity_star[r][c] = numerator/denominator;
						error1 = X_Velocity_imp[r][c] - (X_Velocity_star[r][c] + delT/rho_r/del_sq*0.5 * (2.0*mu[r][c]* (X_Velocity[r][c+1] - X_Velocity[r][c])*RF[c]/eta[c] - 2.0* mu[r][c-1]*(X_Velocity[r][c] - X_Velocity[r][c-1])*RF[c-1]/eta[c]
													+ mu_up *( X_Velocity[r+1][c] - X_Velocity[r][c] + 
													(Y_Velocity[r+1][c] - Y_Velocity[r+1][c-1])  ) 
													- mu_bottom* (X_Velocity[r][c] - X_Velocity[r-1][c] + Y_Velocity[r][c] - Y_Velocity[r][c-1]) )- delT/rho_r*0.5*(mu[r][c]+mu[r][c-1]) * ( X_Velocity[r][c]/pow(eta[c],2) )*axi  + numerator ) / denominator; 
						
						
						
						if (absolute(error1)>error_implicit)
						{
							error_implicit = absolute(error1);
						}
					
					}						
					
						if (r!=1){
							mu_left		=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
							mu_right	=	0.25*( mu[r][c+1] 	+	mu[r][c]	+	mu[r-1][c]	+	mu[r-1][c+1] );
							rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
							
							
							double denominator = 1.0 + delT/rho_r/del_sq*0.5* (2.0*mu[r][c] + 2.0*mu[r-1][c] + mu_right*eta[c+1]/RF[c]  + mu_left*eta[c]/RF[c]  );
							
							double numerator =  delT/rho_r/del_sq*0.5 * (2.0*mu[r][c] * Y_Velocity_imp[r+1][c] + 2.0*mu[r-1][c]*Y_Velocity_imp[r-1][c] + mu_right * (Y_Velocity_imp[r][c+1] + X_Velocity_imp[r][c+1] - X_Velocity_imp[r-1][c+1]) *eta[c+1]/RF[c] 
											- mu_left* ( - Y_Velocity_imp[r][c-1] + X_Velocity_imp[r][c] - X_Velocity_imp[r-1][c] )*eta[c]/RF[c]  );
							
							//~ Y_Velocity_star[r][c] = numerator/denominator;
							error1 = Y_Velocity_imp[r][c] - ( Y_Velocity_star[r][c] + delT/rho_r/del_sq*(0.5) * (2.0*mu[r][c] * (Y_Velocity[r+1][c] - Y_Velocity[r][c] )- 2.0*mu[r-1][c]*(Y_Velocity[r][c] -Y_Velocity[r-1][c] )+ mu_right * (Y_Velocity[r][c+1]  - Y_Velocity[r][c] + X_Velocity[r][c+1] - X_Velocity[r-1][c+1])*eta[c+1]/RF[c] 
											- mu_left* (Y_Velocity[r][c]  - Y_Velocity[r][c-1] + X_Velocity[r][c] - X_Velocity[r-1][c] )*eta[c]/RF[c] ) + numerator ) /denominator;
							
							if (absolute(error1)>error_implicit)
							{
								error_implicit = absolute(error1);
							}
						}
				}
			}
				
			//~ printf("iteration implicit = %d \t",N1);
			//~ printf("error implicit = %e \n",error_implicit);
			//~ fflush(stdout);
			
				
		}
		
			for(r=1;r<=NoOfRows+1;r++)
			{
				for(c=0;c<=NoOfCols+1;c++)
				{
					X_Velocity_star[r][c] = X_Velocity_imp[r][c];
					
				}
			}
			for(r=1;r<=NoOfRows+1;r++)
			{
				for(c=0;c<=NoOfCols+1;c++)
				{
					
					Y_Velocity_star[r][c] = Y_Velocity_imp[r][c];
				}
			}
	
}

/******************
Name: Poisson_Solver_SOR
Description: Solves poisson equations for pressure
Scheme: Successive Over Relaxation method (SOR)
******************/
void Poissons_Solver_SOR()
{
	printf(" PS: SOR "); fflush(stdout);
	
	int r,c,Step;
	  double  Coeff_Poissons_solver, Relaxation_param, Res_inf_norm ;
	  double temp1,temp2,temp3,temp4, sum_Press, mean_Press, NoOfCells;
	  //~ double Source_Poisson[NoOfRows+2][NoOfCols+2];
	//~ double Poissons_residue[NoOfRows+2][NoOfCols+2];
	double sum_poisson =0.00 ;
	double sum1 = 0.00;  
	double sum2 = 0.00;  
	double sum3 = 0.00;  
	double sum4 = 0.00;  //erase
	Step=0;
	Res_inf_norm = 1.;
	Relaxation_param = alpha_bcs;
	
	
	double **Source_Poisson,**Poissons_residue;
		
	
		Source_Poisson = (double **) calloc(NoOfRows+2,sizeof(double *));
		Poissons_residue = (double **) calloc(NoOfRows+2,sizeof(double *));
		int i;
			for (i=0; i<NoOfRows+2; i++)
			{
				Source_Poisson[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Poissons_residue[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
			}
	
//~ for (r=0; r<=NoOfRows+1; r++)
		//~ {
			//~ for (c=0; c<=NoOfCols+1; c++)
			//~ {

                //~ Source_Poisson[r][c] = 0;
                //~ Poissons_residue[r][c]=0;

            //~ }
        //~ }

    // Francais: calculer le terme source de l'equation de Poisson
    // English: Calculate the source term for Poisson's equation
	
	for (r=1; r<=NoOfRows; r++)
		{
			for (c=1; c<=NoOfCols; c++)
			{

			Source_Poisson[r][c] = 0.5*((X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c] + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
						//~ if (isnan(Source_Poisson[r][c]))
								//~ printf("BC");
				sum_poisson = sum_poisson + Source_Poisson[r][c];
				sum1 =sum1+ (X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c];
				//~ sum2 = sum2+(Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c])/delTdel;
				//~ sum3 = sum3+ Y_Velocity_star[r+1][c];
				//~ sum4 = sum4+ Y_Velocity_star[r][c];
				//~ printf("\n%d\t%d\t%e", r,c,X_Velocity_star[r][c]);
				
            }
        } 
	//~ printf("\n source poisson = %e\n",sum_poisson);
	//~ printf("\n sum = %e\t%e\t%e\t%e\n",sum1/delTdel*0.5,sum2,sum3,sum4);
	//~ exit(0);
							//~ for (c=1;c<=NoOfCols;c++)
							//~ {
								//~ Source_Poisson[0][c] = Source_Poisson[1][c];                      //bottom wall
								//~ Source_Poisson[NoOfRows+1][c] = Source_Poisson[NoOfRows][c];      // top wall
							//~ }
							//~ for (r=1;r<=NoOfRows;r++)
							//~ {
								
								//~ Source_Poisson[r][0] = Source_Poisson[r][NoOfCols];                      //left wall
								//~ Source_Poisson[r][NoOfCols+1] = Source_Poisson[r][1];      //right wall
							//~ }


    /* worrying about compatibility condition, well when we are using conservative form of equations
    with staggered grid we are implicitly making the compatibility conditions true. However, even the compatibility conditions fail here
    the crucial issue is not to find pressure but to ensure the solenoidality(zero divergence) of velocity field accurately. Still not satisfied,
    read this DOI: 10.1007/BFb0107103 */

	while(Res_inf_norm>tolerance)		// if greater than the maximum allowed residue
	{


                        	sum_Press = 0; NoOfCells = 0; mean_Press = 0;
							//~ // Set Boundary Conditions
							//~ for (c=1;c<=NoOfCols;c++)
							//~ {
								//~ Press[0][c] = Press[1][c];                      //bottom wall
								//~ Press[NoOfRows+1][c] = Press[NoOfRows][c];      // top wall
							//~ }
							//~ for (r=1;r<=NoOfRows;r++)
							//~ {
								
								
								//~ Press[r][0] = Press[r][1];                      //left wall	//neumann
								//~ Press[r][NoOfCols+1] =  Press[r][NoOfCols];      //right wall 	//neumann
							//~ }

							
		// Pressure Boundary Conditions function call
				
							Pressure_BC();
							 // solid BC
		       for (r=1; r<=NoOfRows; r++)
			{
				for (c=1; c<=NoOfCols; c++)
				{
					
					
					if (solid[r][c].VolFrac>0.96){
					
						double volfrac[4] = {solid[r][c-1].VolFrac, solid[r+1][c].VolFrac, solid[r][c+1].VolFrac, solid[r-1][c].VolFrac};
							int index=0;
						double minimum = volfrac[0];
    
						    for ( int j = 1 ; j < 4 ; j++ ) 
						    {
							if ( volfrac[j] < minimum ) 
							{
								minimum = volfrac[j];
								index = j;
							}
						    }
						    
						switch(index)
						{
							case 0: 
								Press[r][c] = Press[r][c-1];
								break;
							case 1: 
								Press[r][c] = Press[r+1][c];
								break;
							case 2: 
								Press[r][c] = Press[r][c+1];
								break;
							case 3: 
								Press[r][c] = Press[r-1][c];
								break;
							default:
								Press[r][c] = Press[r][c-1];
								break;
						}
						
						
						
					}
				}
			}
                    // Successive-overrelaxation Iteration
                    for (r=1; r<=NoOfRows; r++)
                    {
                        for (c=1; c<=NoOfCols; c++)
                        {
                            Coeff_Poissons_solver = (1.0*eta[c+1]/((rho[r][c]+rho[r][c+1])*RF[c] )+ 1.0*eta[c]/((rho[r][c]+rho[r][c-1])*RF[c])+ 1.0/(rho[r+1][c]+rho[r][c]) +1.0/(rho[r][c]+rho[r-1][c]) ) / del_sq;
							temp1 = (Press[r][c+1]*eta[c+1]/(rho[r][c]+rho[r][c+1])		+	Press[r][c-1]*eta[c]/(rho[r][c]+rho[r][c-1]))/RF[c] ;
							temp2 = Press[r+1][c]/(rho[r+1][c]+rho[r][c])		+	Press[r-1][c]/(rho[r][c]+rho[r-1][c]);


							//Now Pressure
						if(solid[r][c].VolFrac<0.96){
							Press[r][c] 	=	Relaxation_param/Coeff_Poissons_solver * ((temp1+temp2)/del_sq	-	(Source_Poisson[r][c]))	+	(1.0-Relaxation_param)*Press[r][c] ;
							if (isnan(Press[r][c]))
								printf("BC");
						}
							sum_Press = sum_Press + Press[r][c];
							NoOfCells = NoOfCells + 1;      //why to calculate this if we know this apriori? we may want to work with other domains where we want to make some cells dead.
						}
                    }
		
		Pressure_BC();
		    
                    // calculate the mean pressure : Why? Read the case of Neumann conditions for Poisson's equation
                    mean_Press = sum_Press/NoOfCells;
	
	//NOTE:- Switch on below only if Pressure BC on all walls are Neumann, else don't use below
	//~ if (BcNeumann_P_left==1 && BcNeumann_P_right ==1 && BcNeumann_P_bottom ==1 && BcNeumann_P_top==1)
	//~ {
		//~ for (r=1; r<=NoOfRows; r++)
		    //~ {
			//~ for (c=1; c<=NoOfCols; c++)
			//~ {
			    //~ Press[r][c] = Press[r][c] - mean_Press;
			//~ }
		    //~ }
	//~ }
      
		    // solid BC
		       for (r=1; r<=NoOfRows; r++)
			{
				for (c=1; c<=NoOfCols; c++)
				{
					
					
					if (solid[r][c].VolFrac>0.96){
					
						double volfrac[4] = {solid[r][c-1].VolFrac, solid[r+1][c].VolFrac, solid[r][c+1].VolFrac, solid[r-1][c].VolFrac};
							int index=0;
						double minimum = volfrac[0];
    
						    for ( int j = 1 ; j < 4 ; j++ ) 
						    {
							if ( volfrac[j] < minimum ) 
							{
								minimum = volfrac[j];
								index = j;
							}
						    }
						    
						switch(index)
						{
							case 0: 
								Press[r][c] = Press[r][c-1];
								break;
							case 1: 
								Press[r][c] = Press[r+1][c];
								break;
							case 2: 
								Press[r][c] = Press[r][c+1];
								break;
							case 3: 
								Press[r][c] = Press[r-1][c];
								break;
							default:
								Press[r][c] = Press[r][c-1];
								break;
						}
						
						
						
					}
				}
			}
				
							//~ // Set Boundary Conditions
							//~ for (c=1;c<=NoOfCols;c++)
							//~ {
								//~ Press[0][c] = Press[1][c];                      //bottom wall
								//~ Press[NoOfRows+1][c] =  Press[NoOfRows][c];      // top wall
							//~ }
							//~ for (r=1;r<=NoOfRows;r++)
							//~ {
								
								
								//~ Press[r][0] = Press[r][1];                      //left wall	//neumann
								//~ Press[r][NoOfCols+1] = - Press[r][NoOfCols];      //right wall 	//neumann
							//~ }
							
							
							
							
				//~ X_Velocity_Correction();
				//~ Y_Velocity_Correction();					
							
		// Calculate residue
		Res_inf_norm = 0;			//reset residue norm to zero only here, dont move up or down the loop
		
		for (r=1; r<=NoOfRows; r++)
		{
			for (c=1; c<=NoOfCols; c++)
			{
				temp1 = (Press[r][c+1]-Press[r][c])*eta[c+1]/((rho[r][c]+rho[r][c+1])*RF[c]);
				temp2 = (Press[r][c]-Press[r][c-1])*eta[c]/((rho[r][c]+rho[r][c-1])*RF[c]);
				temp3 = (Press[r+1][c]-Press[r][c])/(rho[r][c]+rho[r+1][c]);
				temp4 = (Press[r][c]-Press[r-1][c])/(rho[r][c]+rho[r-1][c]);
				Poissons_residue[r][c] = Source_Poisson[r][c] - (temp1 - temp2 + temp3 - temp4)/del_sq;
				
				
				
						//~ Poissons_residue[r][c] = ((X_Velocity[r][c+1]*eta[c+1]   -	X_Velocity[r][c]*eta[c])/RF[c] + (Y_Velocity[r+1][c]	-	Y_Velocity[r][c]))/del;
							
							//~ sum1 =sum1+ (X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c];
							//~ sum2 = sum2+(Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c])/delTdel;
							//~ sum3 = sum3+ Y_Velocity_star[r+1][c];
							//~ sum4 = sum4+ Y_Velocity_star[r][c];
							//~ printf("\n%d\t%d\t%e", r,c,X_Velocity_star[r][c]);
							
				    
					
				
				
				if (absolute(Poissons_residue[r][c])>Res_inf_norm)
				{
						Res_inf_norm=absolute(Poissons_residue[r][c]);
				}
			}
		}
		
		Res_inf_norm = 2*delT*Res_inf_norm;
		
		Step++ ; // Step counter
		printf(" Div: %e\n",Res_inf_norm);
		if (Step>1e8)
		{
			printf("\n\n*************Maximum Iterations Reached**********************************************\n\n");
			printf("\nERROR IN SUBROUTINE Poissons_Solver \n");
			printf("\n\n***********************************************************\n\n");
			return;

		}
	}
	
	
	    //~ for (r=1; r<=NoOfRows; r++)
            //~ {
                //~ for (c=1; c<=NoOfCols; c++)
                //~ {
                    //~ sum_Press= sum_Press + Press[r][c] ;
                //~ }
            //~ }
	 
	    //~ mean_Press = sum_Press/(NoOfCols*NoOfRows);
	    
           

	
	//~ printf("\n\n***********************************************************\n\n");
	//~ for (r=0; r<=NoOfRows+1; r++)
            //~ {
                //~ for (c=0; c<=NoOfCols+1; c++)
                //~ {
                   //~ printf("%lf\t",Press[r][c]);
			
                //~ }
		
		//~ printf("\n");
            //~ }	
	   //~ printf("\n\n***********************************************************\n\n"); 
	
	//~ exit(0);	
		
		
		printf(" Div: %e",Res_inf_norm); 
		
		freeArray(Source_Poisson);
		freeArray(Poissons_residue);
	
	
}

/****************************
Name: Velocity_Correction
Description:  Adds pressure correction in the temporary velocities
Limitations:
***************************/

void X_Velocity_Correction()
{
	
	int r,c;
	double q;
	
	
	for (r=1; r<=NoOfRows; r++)
	{
		for (c=2;c<=NoOfCols; c++)
		{
			//~ if (StepT>=223)
				//~ if(r==39 && c==5)
				//~ printf("debug");
				
				if (solid[r][c].VolFrac<0.96){
			X_Velocity[r][c] = X_Velocity_star[r][c] - 2*delTBydel*(Press[r][c] - Press[r][c-1])/(rho[r][c] + rho[r][c-1]); // the 2 appears from 0.5 in denominator for mean rho
				}
			//~ if (r==1 && c==15)
				//~ if (X_Velocity[r][c]>0)
					//~ printf("bug");
		}
	}
	
}
	
	//~ if(StepT==1)
	//~ {
		//~ for (r=0; r<=NoOfRows+1; r++)
		//~ {
			//~ for (c=0;c<=NoOfCols+1; c++)
			//~ {
				//~ printf("\n%e",X_Velocity_star[r][c]);
			//~ }
		//~ }
		//~ exit(0);
	//~ }
	


void Y_Velocity_Correction()
{
	int r,c;

	for (r=2; r<=NoOfRows; r++)
	{
		for (c=1;c<=NoOfCols; c++)
		{
			if (solid[r][c].VolFrac<0.96){
			Y_Velocity[r][c] = Y_Velocity_star[r][c] - 2*delTBydel*(Press[r][c] - Press[r-1][c])/(rho[r-1][c] + rho[r][c]);
			}
			//~ if (r==13 && c==1)
				//~ if (Y_Velocity[r][c]<0)
					//~ printf("bug");
			
			//~ if (r==13 && c==1)
				//~ if (Y_Velocity[r][c]<0)
					//~ printf("bug");
		}
	}
	
}



		
	
		
void freeArray(double **a) {
    int i;
    for (i = 0; i < NoOfRows+2; ++i) {
        free(a[i]);
    }
    free(a);
}	
	
		
/*****************new less precise module but fast ************************************/	
	
void Initialise_VolFrac(bool flag)
{

int r,c;
  //  const double radius1 = 0.1; // in x direction semi-major
   // const double radius2 = 0.09; // in y direction semi-minor
    //~ const double dx = 1.0 / width;
    //~ const double dy = 1.0 / height;

    //Buffer f(width, height);
    double x0,y0,x1,y1;

    // Calculate volume fractions.
    for ( r = 1; r <= NoOfRows+1; ++r) {
         y0 = ((r-1) * del ) ;
      
        for ( c = 1; c <= NoOfCols+1; ++c) {
             x0 = ((c-1) * del ) ;
         
            if (flag){
		phi[r][c] = Init_F(x0,y0);
	    }
		    else{
			 phi[r][c] = Init_solid(x0,y0);
		    }	
		
		//~ double area = volumeFraction(x0, y0, x1, y1, 12);
            //~ Cellattrib[r][c].VolFrac = area;
            //std::cout << (int)(0.5 + 9.0 * std::max(0.0, std::min(1.0, f(i, j))));
		//std::cout <<  std::setprecision (5) << (f(i,j));
		
        }
        //std::cout << std::endl;
    }
	
	   // Calculate volume fractions.
    for ( r = 1; r <= NoOfRows; ++r) {
         y0 = ((r-1) * del ) ;
         y1 = ((r ) * del ) ;
        for ( c = 1; c <= NoOfCols; ++c) {
             x0 = ((c-1) * del ) ;
             x1 = ((c) * del ) ;
            double area = volumeFraction(x0, y0, x1, y1,phi[r][c],phi[r][c+1],phi[r+1][c],phi[r+1][c+1], 18);
	
	if (flag){		
            Cellattrib[r][c].VolFrac = area;
	}
	else{
		solid[r][c].VolFrac = area;
	}
            //std::cout << (int)(0.5 + 9.0 * std::max(0.0, std::min(1.0, f(i, j))));
		//std::cout <<  std::setprecision (5) << (f(i,j));
		
        }
        //std::cout << std::endl;
    }
	
}	



// Calculates the volume fraction of the cells
double volumeFraction(double x0, double y0, double x1, double y1,double p1,double p2,double p3, double p4, int recursion)
{	// takes input like basilisk puts ones inside the circle r*r-x*x-y*y
    bool inside00 = p1 > 0;	
    bool inside01 =   p2 > 0;
    bool inside10 =  p3 > 0;
    bool inside11 =  p4 > 0;

    if (inside00 && inside01 && inside10 && inside11)
        return 1.0;

    if (!inside00 && !inside01 && !inside10 && !inside11)
        return 0.0;

    if (recursion == 0)
        return 0.5;

    double xm = 0.5 * (x0 + x1);
    double ym = 0.5 * (y0 + y1);
	
    double p_bottom = 0.5 * (p1 + p2);
    double p_left = 0.5 * (p1 + p3);
     double p_center = 0.5 * (p1 + p4);
    double p_top = 0.5 * (p3 + p4);
      double p_right = 0.5 * (p2 + p4);
    
    --recursion;
    return 0.25 * (volumeFraction(x0, y0, xm, ym,p1,p_bottom,p_left,p_center, recursion)	//0.25 is because 
                 + volumeFraction(xm, y0, x1, ym,p_bottom,p2,p_center,p_right, recursion)
                 + volumeFraction(x0, ym, xm, y1,p_left,p_center,p3,p_top, recursion)
                 + volumeFraction(xm, ym, x1, y1,p_center,p_right,p_top,p4, recursion));
}

/*****************old more precise module but slow ************************************/

	
void Initialise_VolFrac_old(bool flag)
{

int r,c;
  //  const double radius1 = 0.1; // in x direction semi-major
   // const double radius2 = 0.09; // in y direction semi-minor
    //~ const double dx = 1.0 / width;
    //~ const double dy = 1.0 / height;

    //Buffer f(width, height);
    

    // Calculate volume fractions.
    for ( r = 1; r <= NoOfRows; ++r) {
        double y0 = ((double)(r-1) * del ) ;
        double y1 = ((double)(r ) * del ) ;
        for ( c = 1; c <= NoOfCols; ++c) {
            double x0 = ((double)(c-1) * del ) ;
            double x1 = ((double)(c) * del ) ;
            double area = volumeFraction_old(x0, y0, x1, y1, 12,flag);
	
	if (flag){		
            Cellattrib[r][c].VolFrac = area;
	}
	else{
		solid[r][c].VolFrac = area;
	}
            //std::cout << (int)(0.5 + 9.0 * std::max(0.0, std::min(1.0, f(i, j))));
		//std::cout <<  std::setprecision (5) << (f(i,j));
		
        }
        //std::cout << std::endl;
    }
	
	
}	


// Calculates the volume fraction of the unit circle within the given rectangle.
double volumeFraction_old(double x0, double y0, double x1, double y1, int recursion, bool flag)
{
	bool inside00, inside01, inside10, inside11;
	if (flag){
			     inside00 = Init_F(x0,y0) > 0;
			     inside01 =   Init_F(x1,y0) > 0;
			     inside10 =   Init_F(x0,y1) > 0;
			     inside11 =  Init_F(x1,y1) > 0;
	}
	else{
		
			     inside00 = Init_solid(x0,y0) > 0;
			     inside01 =   Init_solid(x1,y0) > 0;
			     inside10 =   Init_solid(x0,y1) > 0;
			     inside11 =  Init_solid(x1,y1) > 0;
	}

    if (inside00 && inside01 && inside10 && inside11)
        return 1.0;

    if (!inside00 && !inside01 && !inside10 && !inside11)
        return 0.0;

    if (recursion == 0)
        return 0.5;

    double xm = 0.5 * (x0 + x1);
    double ym = 0.5 * (y0 + y1);

    --recursion;
    return 0.25 * (volumeFraction_old(x0, y0, xm, ym, recursion, flag)	//0.25 is because 
                 + volumeFraction_old(xm, y0, x1, ym, recursion, flag)
                 + volumeFraction_old(x0, ym, xm, y1, recursion, flag)
                 + volumeFraction_old(xm, ym, x1, y1, recursion,flag));
}

/**********************************************************************************************************/

void check_input()
{

	if (NoOfRows<0)
    {
	    printf("\n ***************** ERROR : NoOfRows = ?? **************** \n");
	    exit(0);
    }
    
        if (NoOfCols<0)
    {
	    printf("\n ***************** ERROR : NoOfCols = ?? ******************\n");
	    exit(0);
    }
	
	if (del<0)
    {
	    printf("\n ***************** ERROR : del = ?? **********\nHint: initialize the del = Length of domain/NoOfRows or Breadth of domain/NoOfCols --- \n");
	    exit(0);
    }
    
 if (step<0)
    {
	    printf("\n ***************** ERROR : step = ?? **********\n");
	    exit(0);
    }
    
    
}
