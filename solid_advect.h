

/********************************************************************************************************************************
NAME       : AdvanceF_field
DESCRIPTION:
LIMITATIONS:
*********************************************************************************************************************************/
void AdvanceSolid_field(int r,int c, int Flag, int Togg)
{
	  /*******LOCAL VARIABLES*******/
	    double VolFrac;
	    double Zeta, Zeta1, Zeta2;
	    double NetXFlux, NetYFlux, NetXFlux_Corr, NetYFlux_Corr;
	    double X_Flux_Out, X_Flux_In, X_Flux_Out_Corr, delV_Corr;
	    double Y_Flux_Out, Y_Flux_In, Y_Flux_Out_Corr;
	    double X_Flux_Right,X_Flux_Left, X_Flux_Right_Corr, X_Flux_Left_Corr;
	    double Y_Flux_Up, Y_Flux_Down, Y_Flux_Up_Corr, Y_Flux_Down_Corr;
	    double Ustar_Left, Ustar_Right, Vstar_Up, Vstar_Down;
	  /*****************************/
	
	/****************WEYMOUTH IMPLEMENTATION*******************************************/
	if (weymouth==1)
	{
		if (CFL>0.5)
		{
			printf("keep CFL below 0.5 to ensure conservature of volume fractions");
			exit(0);
		}
						
		
		if (Flag==1)
		{
			solid[r][c].VolFrac = solid[r][c].VolFrac + 1.0/RF[c]* ( (X_Flux[r][c]*eta[c]-X_Flux[r][c+1]*eta[c+1] )/del_sq + delT*ccs[r][c]*(X_Velocity[r][c+1]*eta[c+1]-X_Velocity[r][c]*eta[c])/del );
		}
		else if(Flag==2)
		{
			solid[r][c].VolFrac = solid[r][c].VolFrac + (Y_Flux[r][c]-Y_Flux[r+1][c] )/del_sq + delT*ccs[r][c]*(Y_Velocity[r+1][c]-Y_Velocity[r][c])/del ;
		}
		
		return;
	}
	/****************WEYMOUTH IMPLEMENTATION END*******************************************/
					
					
	/*  LORSTAD IMPLEMENTATION*********************/
				//~ if (StepT>=221 )
					//~ {
						//~ if(r==39 && c==4)
						//~ printf(" ");
					//~ }
	  	  if(Flag == 1)    //HORIZONTAL SWEEP
			{
				
			 //   if(X_Velocity[r][c+1] >= 0.0)     //RIGHT SIDE OF THE CELL EFFLUXES
				{
					 X_Flux_Right = X_Flux[r][c+1]/del_sq;  //fraction of volume out
					 X_Flux_Left  =  X_Flux[r][c]/del_sq;		//fraction of volume in


							Ustar_Right  = delTBydel*X_Velocity[r][c+1];

			  				Zeta1 = X_Flux_Right/Ustar_Right;
							Zeta2 = (solid[r][c].VolFrac - X_Flux_Right)/(1.0 - Ustar_Right);

							if( absolute(Zeta1 - 0.5) >= absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( absolute(Zeta1 - 0.5) < absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 0)   //HORIZONTAL PASS IS THE FIRST
								{
									 solid[r][c].delV = (1.0 - delTBydel*(X_Velocity[r][c+1]*eta[c+1] - X_Velocity[r][c]*eta[c])/RF[c]);

									 NetXFlux     = (X_Flux_Right*eta[c+1] - X_Flux_Left*eta[c])/RF[c];
									
									 //~ NetXFlux     = (X_Flux_Right*(c+1)*del - X_Flux_Left*c*del)/((c+0.5)*del);

									 VolFrac = (solid[r][c].VolFrac - NetXFlux)/solid[r][c].delV;
								}
								else if (Togg == 1) //HORIZONTAL PASS IS THE SECOND
								{

									 delV_Corr = (solid[r][c].delV + (1-solid[r][c].delV)*Zeta);

									 X_Flux_Right_Corr = solid[r][c].delV*(X_Flux_Right- Ustar_Right) + delV_Corr*Ustar_Right;

									 NetXFlux_Corr     = (X_Flux_Right_Corr*eta[c+1] - X_Flux_Left*eta[c])/RF[c];
									
									 //~ NetXFlux_Corr     = ((X_Flux_Right_Corr)*(c)*del - (X_Flux_Left)*(c-1)*del)/((c-0.5)*del);

									VolFrac = ((solid[r][c].VolFrac*solid[r][c].delV) - NetXFlux_Corr);
								}





				}


			}

		  else if(Flag == 2)   //VERTICAL SWEEP
		{

				//if(Y_Velocity[r+1][c] >= 0.0)     //UPPER PART OF THE CELL EFFLUXES
				{
					Y_Flux_Up    =Y_Flux[r+1][c]/del_sq;
					Y_Flux_Down  = Y_Flux[r][c]/del_sq;
					Vstar_Up  = delTBydel*Y_Velocity[r+1][c];

			  				Zeta1 = Y_Flux_Up/Vstar_Up;
							Zeta2 = (solid[r][c].VolFrac - Y_Flux_Up)/(1.0 - Vstar_Up);

							if( absolute(Zeta1 - 0.5) >= absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( absolute(Zeta1 - 0.5) < absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 1)   //VERTICAL PASS IS THE FIRST
								{
									solid[r][c].delV = 1.0 - delTBydel*(Y_Velocity[r+1][c] - Y_Velocity[r][c]);
									NetYFlux     = Y_Flux_Up - Y_Flux_Down;

									VolFrac = (solid[r][c].VolFrac - NetYFlux)/solid[r][c].delV;
								}
								else if (Togg == 0) //VERTICAL PASS IS THE SECOND
								{
									delV_Corr = solid[r][c].delV + (1-solid[r][c].delV)*Zeta;
									 Y_Flux_Up_Corr = solid[r][c].delV*(Y_Flux_Up - Vstar_Up) + delV_Corr*Vstar_Up;
									NetYFlux_Corr     = Y_Flux_Up_Corr - Y_Flux_Down;

									VolFrac = (solid[r][c].VolFrac*solid[r][c].delV - NetYFlux_Corr);


								}




				}


		} //Vertical Sweep
					//~ if (StepT>=221 )
					//~ {
						//~ if(r==39 && c==4)
						//~ printf(" ");
					//~ }
		
		  //~ Write_struct_solid(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		 solid[r][c].VolFrac=VolFrac;
		/*************************LORSTAD END***********/ 
		
			
			
			
		
		

}


/******************************************************************************
NAME:		 AssignVal_FluxVariables
DESCRIPTION: Calculates the interface orientation details, for the cell whose identifier is passed.
			 The calculated values are written into the addresses passed.
LIMITATIONS:
******************************************************************************/
void AssignVal_FluxVariablesSolid(int r, int c,   double *ptr2,   double *ptr3,   double *ptr4,   double *ptr5,   double *ptr6,   double *ptr7,   double *ptr8,   double *ptr9,   double *ptr10,   double *ptr11,   double *ptr12)
{
  /*****LOOKUP FOR ARGUMENTS****
     double *ptr2  - Theta
     double *ptr3  - XVeldelT
     double *ptr4  - YVeldelT
     double *ptr5  - x_0
     double *ptr6  - y_0
     double *ptr7  - x_del
     double *ptr8  - y_del
     double *ptr9  - x_YVeldelT
     double *ptr10 - y_XVeldelT
     double *ptr11 - XVel
     double *ptr12 - YVel
  /****************************/

  /*********DECLARATIONS***********/
    double P, XVel, YVel;
    double Theta;
    double sinT,cosT,tanT;
  /********************************/

  /*************INITIALIZE*************/
  P				= solid[r][c].P;
  XVel          = *ptr11;                   //DO NOT RETRIEVE VELOCITY FROM CELL WALL .RETRIEVE ONLY FROM THE PASSED ADDRESS
  YVel          = *ptr12;                   //DO NOT RETRIEVE VELOCITY FROM CELL. RETRIEVE ONLY FROM THE PASSED ADDRESS SINCE CELL WALL IDENTIFIER IS DIFFRERENT FROM THE IDENTIFIER PASSED
  Theta         = solid[r][c].Theta;
  sinT			= sin(Theta);
  cosT			= cos(Theta);
  tanT			= tan(Theta);
  /************************************/


  /*********WRITING TO THE ADDRESSES PASSED************/
  *ptr2			= solid[r][c].Theta;
  *ptr3			= XVel*delT;
  *ptr4			= YVel*delT;
  *ptr5			= P/sinT;
  *ptr6			= P/cosT;
  *ptr7			= (*ptr6 - del)/tanT;
  *ptr8			= (*ptr6 - (del*tanT));
  *ptr9			= (*ptr6 - *ptr4)/tanT;
  *ptr10		= (*ptr6) - ((*ptr3)*tanT);
  /****************************************************/
  //FLAG: the following block is written to add the rectangle case in flux calculation

  if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
  {
			 *ptr2			= solid[r][c].Theta;
			  *ptr5			= P;
			 // *ptr6			= P/cosT;
			  *ptr7			= P;
			 // *ptr8			= (*ptr6 - (del*tanT));
			  *ptr9			= P;
			  //*ptr10		= (*ptr6) - ((*ptr3)*tanT);


  }
}


/**********************************************************************************************************
NAME: CalculateF_Flux
DESCRIPTION:
			1. If Flag = 1 , this subroutine does a horizontal pass and calculates all horizontal fluxes
			2. If Flag = 2 , this subroutine does a vertical pass and calculates all vertical fluxes
LIMITATIONS:
**********************************************************************************************************/
void CalculateSolid_Flux(int flag)
{
  /*******LOCAL VARIABLES**********/
  int		  r,c, Quad;
  double Theta;
  char		  Shape[30];
  double x_0,y_0,x_del,y_del,y_XVeldelT,x_YVeldelT;
  double X_Vel, Y_Vel;
  double XVeldelT,YVeldelT;
  double P;
  double al;
  double Area;
  /********************************/

		//HORIZONTAL PASS
		if(flag == 1)
		{
		  for(r=1;r <= NoOfRows+1;r++)
		   {
			 for(c=1;c <= NoOfCols+1 ;c++)
			  {
				  
				
				/************INITIALISE**********/
				 X_Vel = X_Velocity[r][c];
				 Y_Vel = Y_Velocity[r][c];
				 /******************************/

				 if(X_Vel > 0.0)   //THE CELL TO THE LEFT OF THE BOUNDARY DECIDES THE EFFLUX AND THAT CELL WILL FLUX FROM ITS RIGHT BOUNDARY
				  {
					//~ if(c == 0)
					//~ {
						//~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
						//~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
						//~ continue;
					//~ }

					if(solid[r][c-1].VolFrac > LOWER_LIMIT && solid[r][c-1].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c-1] - INIT BLOCK
						Quad = Find_Quad_solid(r,c-1);
						//~ strcpy(Shape,solid[r][c-1].Shape);  //GET SHAPE FROM [r][c-1]
						P = solid[r][c-1].P;
						al = solid[r][c-1].al;
						Area = solid[r][c-1].Area;
						
						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariables() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						{
							AssignVal_FluxVariablesSolid(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 2)
						{
							AssignVal_FluxVariablesSolid(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 3)
						{
							AssignVal_FluxVariablesSolid(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 4)
						{
							AssignVal_FluxVariablesSolid(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
					}
					else if(solid[r][c-1].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(solid[r][c-1].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = XVeldelT*del; //4-6-15

					}
				  }
				else if (X_Vel < 0.0)   //THE CELL TO THE RIGHT OF THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LEFT BOUNDARY
				  {

					//~ if(c == NoOfCols)
					//~ {
						  //~ if(r==1 && c==3)
						//~ printf("haha");
					  //~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
					  //~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = absolute(X_Vel);
					Y_Vel = absolute(Y_Vel);

					if(solid[r][c].VolFrac > LOWER_LIMIT && solid[r][c].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c] - INIT BLOCK
						Quad = Find_Quad_solid(r,c);
						strcpy(Shape,solid[r][c].Shape);  //GET SHAPE FROM [r][c]
						P = solid[r][c].P;
						al = solid[r][c].al;
						Area = solid[r][c].Area;
						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariablesSolid() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 					X_Flux[r][c] = -CalculateX_Flux("Left",  Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
							//if(r == 30 & c == 5)
							//	printf("\nVelocity Negative, Quad 2 %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
						    X_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
								//if(r == 15 & c == 38)
								//printf("\nVelocity Negative Quad is 3rd %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
					}
					else if(solid[r][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(solid[r][c].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = -XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = -XVeldelT*del;   // corrected on 4-6-15


					}
				  }
				else //NO FLUX
				  {
					X_Flux[r][c] = 0.0;
				  }

				
			}
		   }
		   

		}
		
	  //VERTICAL PASS
		else if(flag == 2)
		{
		   for(r=1;r <= NoOfRows+1;r++) 
		  {
			 for(c=1;c <= NoOfCols+1;c++)
			  {
					
					//~ if (StepT==641)
					//~ {
						//~ if(r==64 && c==4)
						//~ printf("debug");
						
					//~ }  
				  
				  
				
				X_Vel = X_Velocity[r][c];
				Y_Vel = Y_Velocity[r][c];

		  		if(Y_Vel > 0.0)   //THE CELL BELOW THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS UPPER BOUNDARY
				  {
					//~ if(r == 0)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					if(solid[r-1][c].VolFrac > LOWER_LIMIT && solid[r-1][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r-1][c] - INIT
						Quad = Find_Quad_solid(r-1,c);
						//~ strcpy(Shape, solid[r-1][c].Shape);  //GET SHAPE FROM [r - 1][c]
						P = solid[r-1][c].P;
						al = solid[r-1][c].al;
						Area = solid[r-1][c].Area;
						/**************************************/
					
					
						
						if(Quad == 1)
						 {
							AssignVal_FluxVariablesSolid(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariablesSolid(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
	  					    Y_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariablesSolid(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 4)
						 {	
							AssignVal_FluxVariablesSolid(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);

						 }
					}
					else if (solid[r-1][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if (solid[r-1][c].VolFrac >= UPPER_LIMIT)  //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = YVeldelT*del/delTdel;//
						Y_Flux[r][c] = YVeldelT*del;  // 4-6-15
					}

				  }
				else if (Y_Vel < 0.0)   //THE CELL ABOVE THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LOWER BOUNDARY
				  {
					//~ if(r == NoOfRows)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = absolute(X_Vel);
					Y_Vel = absolute(Y_Vel);

					if(solid[r][c].VolFrac > LOWER_LIMIT && solid[r][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r][c] - INIT
						Quad = Find_Quad_solid(r,c);
						//~ strcpy(Shape, solid[r][c].Shape);  //GET SHAPE FROM [r][c]
						P = solid[r][c].P;
						al = solid[r][c].al;
						Area = solid[r][c].Area;
						/**************************************/
						
							
						if(Quad == 1)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 				    Y_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = -CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariablesSolid(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
						    Y_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
					}
					else if(solid[r][c].VolFrac <= LOWER_LIMIT)  //NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if(solid[r][c].VolFrac >= UPPER_LIMIT)   //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = -YVeldelT*del/delTdel;
						Y_Flux[r][c] = -YVeldelT*del; //4-6-15
					}
				  }
				else //NO FLUX
				  {
						Y_Flux[r][c] = 0.0;
				  }
			  }
		  }
		}
		

}

/**************************************************************************
NAME       : BookKeeping
DESCRIPTION:
LIMITATIONS:
***************************************************************************/
void BookKeeping_solid()
{
    /*********DECLARATIONS***********/
	int r, c;
	  double VolFrac;
	/*******************************/

	//ASSIGN 0 AND 1 TO CELLS WHICH ARE OUT OF BOUND
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
			if(solid[r][c].VolFrac < LOWER_LIMIT)
			  {
				//printf("\nCame here\n");
				solid[r][c].VolFrac  = 0;
				//~ Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
			  }
			if(solid[r][c].VolFrac > UPPER_LIMIT)
			  {
				solid[r][c].VolFrac  = 1;
				//~ Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
			  }
	   }
	}
	/***/
}
/******************************************************************************************************
NAME       : Pass_main()
DESCRIPTION: THIS IS A DUMY FUNCTION WHICH IS USED TO REMOVE CODE FROM main()
			 TOGG IS A VARIABLE USED FOR ALTERNATING BETWEEN VERTICAL AND HORIZONTAL PASSES
			 FLAG IS A VARIABLE USED FOR DECIDING WHETHER THE CURRENT PASS IS HORIZONATAL / VERTICAL
LIMITATIONS: NONE
/******************************************************************************************************/
void Pass_main_solid(int Flag, int Togg)
{
	int r, c;
	BookKeeping_solid();
	CalculateSolid_Flux(Flag);   //CALCULATE X FLUXES
	
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
		  AdvanceSolid_field(r,c,Flag,Togg);  //ADVANCE F IN TIME BASED ON FLAG VALUE AND HORIZONTAL FLUXES
		}
	}
	BookKeeping_solid();

	
}