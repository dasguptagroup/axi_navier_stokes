
#define index_p(r,c) (3*r+c)
/***********************PROST******************************************************/
double parabola(double x, double y, double *guess_vector)
{
	double a = guess_vector[0];
	double k = guess_vector[1];
	double theta = guess_vector[2];
	
	double nx=sin(theta);
	double ny=-cos(theta);
	//~ double ny = pow((1-pow(nx,2)),0.5);
	
	//~ double parabola_eq = k + nx*(x-1.5*del) +  ny*(y-1.5*del) + a*pow(ny,2)*pow(x-1.5*del,2)  - 2*a*nx*ny*(x-1.5*del)*(y-1.5*del) + a*pow(nx,2)*pow(y-1.5*del,2);
	double parabola_eq = k + nx*(x-1.5*del) +  ny*(y-1.5*del) + a*pow((nx*(y-1.5*del)-ny*(x-1.5*del)),2);
	//~ double parabola_eq = k + nx*(x) +  ny*(y) + a*pow((nx*(y)-ny*(x)),2);
	
	//~ return ((y-0.5) - 1.5*pow((x-0.5),2));
	
	return parabola_eq;
	
}
//~ # include "working.h"

double f (double *xk1)
{
	double k = pow((pow(xk1[0],2)+xk1[1] + xk1[2] -11),2) + pow((xk1[0]+pow(xk1[1] ,2) + xk1[2] -7),2) ;
	
	return k;
	
}



// Calculates the volume fraction of a cell beneath the given interface
// Franais: Ces calcule les fractions de volume pour l'interface en dessous d'une cellule
double volumeFraction_parabola(double x0, double y0, double x1, double y1, double *guess_vector, int recursion)
{
    bool inside00 = parabola(x0,y0, guess_vector) < 0;
    bool inside01 =   parabola(x1,y0,  guess_vector) < 0;
    bool inside10 =   parabola(x0,y1, guess_vector) < 0;
    bool inside11 =  parabola(x1,y1, guess_vector) < 0;

    if (inside00 && inside01 && inside10 && inside11)
        return 1.0;

    if (!inside00 && !inside01 && !inside10 && !inside11)
        return 0.0;

    if (recursion == 0)
        return 0.5;

    double xm = 0.5 * (x0 + x1);
    double ym = 0.5 * (y0 + y1);

    --recursion;
    return 0.25 * (volumeFraction_parabola(x0, y0, xm, ym, guess_vector, recursion)	//0.25 is because 
                 + volumeFraction_parabola(xm, y0, x1, ym, guess_vector, recursion)
                 + volumeFraction_parabola(x0, ym, xm, y1, guess_vector, recursion)
                 + volumeFraction_parabola(xm, ym, x1, y1, guess_vector, recursion));
}

// Calculate the volume fractions of parabola of 3 X 3 stencil
// Calcule les fractions de volume de parabole d'une 3 X 3 pochoir 
double  *calculate_volfrac_parabola_stencil( double *guess_vector, double *VolFrac_parabola)
{
	int r,c;
	double sum_of_volfrac_para = 0;
	//~ double parabola_sign=1.0;
	
	
	// Calculate volume fractions.
	    for ( r = 0; r <= 2; ++r) {
		double y0 = ((double)(r) * del ) ;
		double y1 = ((double)(r+1 ) * del ) ;
		for ( c = 0; c <= 2; ++c) {
		    double x0 = ((double)(c) * del ) ;
		    double x1 = ((double)(c+1) * del ) ;
		    double area = volumeFraction_parabola(x0, y0, x1, y1, guess_vector, 8);
		    VolFrac_parabola[index_p(r,c)] = area;
			//~ sum_of_volfrac_para = sum_of_volfrac_para + area;
			//~ printf("\nVP_o1 = %lf", VolFrac_parabola[index_p(r,c)]);
		    
		}
	       
	    }
	    
	  //~ exit(0);
	    return VolFrac_parabola;
	    
	    
}

//ce calcule la fonction de cout 
double calculate_cost_function(double *VolFrac_actual, double *VolFrac_parabola)
{
	int r,c;
	double w1, w, G=0;
	
		double w2[9] = { 0.25, 0.5, 0.25, 0.5, 1.0, 0.5, 0.25, 0.5, 0.25 }; 
	 
		    for ( r = 0; r <= 2; ++r) {
			for ( c = 0; c <= 2; ++c) {
				
				w1 = 1.0/(VolFrac_actual[index_p(r,c)]*(1-VolFrac_actual[index_p(r,c)]) + 0.01);
			
				w = w1*w2[index_p(r,c)];
				//~ w=1.0;
				G = w*pow((VolFrac_actual[index_p(r,c)] - VolFrac_parabola[index_p(r,c)]),2) + G; // FLAG change r,c for cellattrib
			}
		}
	return G;	
}
// this will calculate the area under parabola in 3 X 3 stencil
// ce calcule les fractions de volume pour parabola

double  prost_kappa(int r, int c)
{
	
	
	double *VolFrac_parabola, *VolFrac_actual;
	
	
	VolFrac_actual  = ( double * ) calloc ( 9,sizeof ( double ) );
	VolFrac_parabola  = ( double * ) calloc ( 9,sizeof ( double ) );
	
	double inc[3] = {1.0, del ,M_PI/4};	// increment for a,k, theta respectively
	
	// for nearly empty and nearly empty cells
	if (absolute(VolFrac_actual[4]-0.5)>0.49)
	{
			inc[0] =1.0;
			inc[1] = del/2;
			inc[2] = M_PI/4;
	}
		
	double xP[3];
	
	
	double xk[3]; // store initial guess in case of failure
	
	int Quad;
	
	double ig[3] = {Cellattrib[r][c].para_a,Cellattrib[r][c].para_k,Cellattrib[r][c].para_theta}; // a, k,theta
	
	//~ if (StepT==0)
	//~ {
		//~ ig[0] = 1.0;
		//~ ig[1] = 0.0;
		//~ ig[2] = 1.0;
	//~ }	

	
	
	
	
	double xk1[3];
	
	
	
	
	double  xk2[3];
	
	int count=0;
	int j,i;
	int step_count=0;
	
	double epsilon = 1.e-8;
	double beta[3] = { 2.0, 2.0 , 2.0};
	double fk=10,fk1p,fk1n,fk2;
	double f_best=100.00;
	double f_best_global=10000.00, global_best_kappa;
	double mod_inc,mod_xk1xk;
	
	mod_inc = pow(pow(inc[0],2) + pow(inc[1],2) + pow(inc[2],2),0.5);	
	
	int step1,step_e=0;
	double rot=M_PI/16;
	double end_step1=5;
	
    
	for ( j = 0; j <= 2; ++j) {
		for ( i = 0; i <= 2; ++i) {
				
			VolFrac_actual[index_p(j,i)] = Cellattrib[r+j-1][c+i-1].VolFrac;
			//~ if (r==49 && c==61)
			//~ printf("\nvf = %lf", VolFrac_actual[index_p(j,i)]);
		}
	}
	
	

	
	xk[0] = ig[0];
	xk[1] = ig[1];
	xk[2] = ig[2];
	
search:		
	xk1[0] = xk[0];
	xk1[1] = xk[1];
	xk1[2] = xk[2];
	
	step_count=0;
	while (mod_inc>epsilon)
	{
		
		
			
	/********************exploratory search**************************/
		for (count=0; count<3; count++)
		{		
			xP[0]= xk1[0]; 
			xP[1] = xk1[1];
			xP[2] = xk1[2];
			
			
			
			VolFrac_parabola = calculate_volfrac_parabola_stencil(xP,VolFrac_parabola);
			//~ VolFrac_parabola = calculate_volfrac_parabola_stencil_new(xP,VolFrac_parabola);
			
			
			fk =   calculate_cost_function(VolFrac_actual,VolFrac_parabola);
			
			//~ if (r==117 && c==5)
			//~ {
				//~ printf("\n%d\t%d\t%d\t%e\t%e\t%e\t%e\t%e\n",r,c,step_e,fk,xP[0],xP[1],f_best_global,global_best_kappa);
				//~ printf("%e\t%e\t%e\n", VolFrac_parabola[0], VolFrac_parabola[1], VolFrac_parabola[2]);
				//~ printf("%e\t%e\t%e\n", VolFrac_parabola[3], VolFrac_parabola[4], VolFrac_parabola[5]);
				//~ printf("%e\t%e\t%e\n", VolFrac_parabola[6], VolFrac_parabola[7], VolFrac_parabola[8]);
			//~ }
			
		
			//~ if (r==54 && c==52)
			//~ printf(" ");
			
			
			xP[count] = xP[count] + inc[count];
		
			VolFrac_parabola = calculate_volfrac_parabola_stencil(xP,VolFrac_parabola);
			//~ VolFrac_parabola = calculate_volfrac_parabola_stencil_new(xP,VolFrac_parabola);
			
			fk1p =  calculate_cost_function(VolFrac_actual,VolFrac_parabola);
		
			xP[count] = xP[count] - 2* inc[count];
			
			VolFrac_parabola = calculate_volfrac_parabola_stencil(xP,VolFrac_parabola);
			//~ VolFrac_parabola = calculate_volfrac_parabola_stencil_new(xP,VolFrac_parabola);
			
			fk1n =  calculate_cost_function(VolFrac_actual,VolFrac_parabola);
			
		
				
				
			if  (fk<=fk1p && fk<=fk1n )
			{
				inc[count] = inc[count]/beta[count];
				f_best = fk;
				
			}
			 if (fk1p<=f_best && fk1p<=fk1n)
			{
				xk1[count] = xk1[count]+inc[count] ;
					f_best = fk1p;
				
			}
			else if (fk1n<=f_best && fk1n<=fk1p)
			{
				xk1[count] = xk1[count]-inc[count] ;
					f_best = fk1n;
			}
			//~ else
			//~ {
				//~ inc[count] = inc[count]/beta[count];
				//~ f_best = fk;
			//~ }
				//~ if (r==125 && c==13 && step_count<100)
			//~ {printf("\n%d\t%d\t%d\t%e\t%e\t%e\t%e\t%e\n",r,c,count,fk,f_best,2*xk1[0],f_best_global,global_best_kappa);
			//~ }
			//~ else if  (r==125 && c==13 && step_count>100)
				//~ exit(0);
			
		}	
		
			
	
			VolFrac_parabola = calculate_volfrac_parabola_stencil(xk1,VolFrac_parabola);
			//~ VolFrac_parabola = calculate_volfrac_parabola_stencil_new(xk1,VolFrac_parabola);
			
			fk =  calculate_cost_function(VolFrac_actual,VolFrac_parabola);
			
			
			
			if (fk<=f_best) // accept the pattern move
			{
				
				
				xk[0] =  xk1[0];
				xk[1] = xk1[1];
				xk[2] = xk1[2];
				
			}
			
		
			
			mod_inc = pow(pow(inc[0],2) + pow(inc[1],2) + pow(inc[2],2),0.5);	
		
		
			step_count++;
			
			
		
			if (f_best_global>f_best)
			{
				f_best_global = f_best;	
				global_best_kappa =2.0* xk1[0];
				
				if(f_best_global<1.0e-8)
				{
					free(VolFrac_actual);
					free(VolFrac_parabola);
						
					Cellattrib[r][c].para_a =  xk1[0];
					Cellattrib[r][c].para_k = xk1[1];
					Cellattrib[r][c].para_theta = xk1[2];
				
					return global_best_kappa;
				}
			}
			
			//~ if (r==125 && c==13 && step_count<100)
			{
				printf("\n%d\t%d\t%d\t%e\t%e\t%e\t%e\t%e\n",r,c,step_count,fk,mod_inc,2*xk1[0],f_best_global,global_best_kappa);
			}
			//~ else if  (r==125 && c==13 && step_count>100)
				//~ exit(0);
			//~ if (step_count>=1000)
				//~ break;
	}


	
	if (step_e<=5)
	{
	
		
		
		rot = 10.0;
			
			
		
	
		xk[0] = ig[0]+rot;
		
		xk[1] =0.0; 		// this is k, start always from zero so that interface remains in the cell, otherwise for high k, 
						//	volfrac parabola's will be always one or zero, since parabola does not remain in stencil(which we are looking at), and have a local minima vast surface from where you can never come out.
		xk[2] = ig[2]+rot;
		
		inc[0] =1.0;
		inc[1] = del/2;
		inc[2] = M_PI/4;
		
		if (absolute(VolFrac_actual[4]-0.5)>0.49)
		{
			inc[0] =1.0;
			inc[1] = del/2;
			inc[2] = M_PI/4;
		}
		mod_inc = pow(pow(inc[0],2) + pow(inc[1],2) + pow(inc[2],2),0.5);	
		
		
		
		//~ step1++;
		step_e++;
		
		//~ if(step_e>20)
		//~ {
			//~ printf("too many iter");
			//~ exit(0);
			//~ goto out_h;
		//~ }
		goto search;
		
		
	}

	
	if (step_e>5 && step_e<10)
	{
	
		
		rot = 10.0;
			
			
		
	
		xk[0] = ig[0]-rot;
		
		xk[1] =0.0; 		// this is k, start always from zero so that interface remains in the cell, otherwise for high k, 
						//	volfrac parabola's will be always one or zero, since parabola does not remain in stencil(which we are looking at), and have a local minima vast surface from where you can never come out.
		xk[2] = ig[2]-rot;
		
		inc[0] =1.0;
		inc[1] = del/2;
		inc[2] = M_PI/4;
		
		if (absolute(VolFrac_actual[4]-0.5)>0.49)
		{
			inc[0] =1.0;
			inc[1] = del/2;
			inc[2] = M_PI/4;
		}
		mod_inc = pow(pow(inc[0],2) + pow(inc[1],2) + pow(inc[2],2),0.5);	
		
		
		
		//~ step1++;
		step_e++;
		
		//~ if(step_e>20)
		//~ {
			//~ printf("too many iter");
			//~ exit(0);
			//~ goto out_h;
		//~ }
		goto search;
		
		
	}

	

			free(VolFrac_actual);
			free(VolFrac_parabola);
					Cellattrib[r][c].para_a =  xk1[0];
					Cellattrib[r][c].para_k = xk1[1];
					Cellattrib[r][c].para_theta = xk1[2];
	
	//~ exit(0);
	//~ printf("\n%d\t%d\t%d\t%e\t%e\t%e\t%e\t%e\n",r,c,step_e,fk,mod_inc,2*xk1[0],f_best_global,global_best_kappa);
			return global_best_kappa;
	
}


/*********************************************************************************************************/