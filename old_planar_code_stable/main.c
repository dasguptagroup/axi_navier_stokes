/*#################################################################################
| FILE NAME:	HF implementation																						 |
|																											 |
| INTERFACE_SOLVER                                                                                                                       								 |
|																											 |
| DESCRIPTION:																								 |
| 1. Reconstructs a piecewise-linear interface from a given distribution of volume fractions using the LVIRA algorithm [1,2]			 |
| 2. Given a velocity field and an F distribution, it calculates the F distribution at the next time-step.								 |
|															              											 |
| LIMITATIONS:																								 |
| 1. Presently works only for an uniform grid.																		 |
| 2. Presently works only for 2D																					 |
| 3. Has to be modified for an open domain. See function CalculateF_Flux()													 |
|                                                                                                                                         										 |
| REFERENCES:                                                                                                                             									 |
| 1. A higher order projection method for tracking fluid interfaces in variable-density flows - JCP (130), 269-282 (1997), Puckett et al       |
| 2. Second-order volume-of-fluid algorithms for tracking material interfaces - JCP(199), 465-502 (2004), Pilliod et al                  		 |
| 3. Comparison of volume-of-fluid methods for surface-tension dominant two-phase flows - IJHMT(49), 740-754(2006), Gerlach et al       	 |
| 4. High-order surface-tension VOF model for 3D bubble flows with high density ratio - JCP (200), 153-176 (2004), Lorstad et al              |
##################################################################################


/***INCLUDE***/
#include <stdio.h>

#include <stdlib.h>

#include <math.h>

#include <string.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <unistd.h>

#include <stdbool.h>

#include <time.h>

#include <global_variables.h>

#include <multigrid.h>

/****************************************************************/


int main(int argc, char *argv[] )
{

	

	   /*********DECLARATIONS***********/
	   int r, c; //Step;
	   int check;
	   int Togg;
	   int RunTimeStart, RunTimeEnd;
		RunTimeStart = time(0);
		int Count;
	


	   char LineFormat_debug[] = "./DebugData/Sim_%e.dat";
  	   char LineFileName_debug[40];
	   FILE *ptrWrite_debug;

  	   char FileName_Output[] = "./FError/Output.dat";
	  // char FileName_Y[] = "./FError/Y.dat";
	   FILE *ptrWrite_Output;
	  // FILE *ptrWrite_Y;
	     double Tot_VolFrac;
	   /*****************/
			/************************Create Directories **************************/
			struct stat st = {0};

			if (stat("DebugData", &st) == -1)
			{
				mkdir("DebugData", 0700);
			}
			if (stat("FError", &st) == -1)
			{
				mkdir("FError", 0700);
			}
			if (stat("Plots", &st) == -1)
			{
				mkdir("Plots", 0700);
			}
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data", 0700);
			}
			/**********************************************************************/

	   /***************OPEN ALL FILE POINTERS*******/
	    ptrWrite_Output =fopen(FileName_Output,"w");
	   // ptrWrite_Y =fopen(FileName_Y,"w");
	   /*******************************************/

	   /*STEPS BEFORE CALLING RECON_INTERFACE() IN A LOOP*/
	   StepT  = 0;

	   Togg  = 0;
	   Time =0;
	Write_Count = 0;
	   Init_GlobalVars();
			
		Count =0;	
		

	   /**********************************************************/
	printf("Step\tdelT\t\tTime\t\tTotal_VolFrac");
 	    while(StepT<10000) // (Time<end)// //(U_residue>1e-8 && V_residue>1e-8)//////(U_residue>1e-8 && V_residue>1e-8)	 // 
 		{
			
			

			   /*********CONSTRUCT INTERFACE*******************************/
			     
			
				//Apply_Default_BoundaryConditions(); //keep this here at the top of the loop
			
				Apply_Extra_BoundaryConditions(); //keep this here at the top of the loop
			
				Update_Fluid_Properties();  //keep this above NS
			
				CFL_Check();// calculate the require delT keep above NS
			
			
				
				//~ if ( StepT%1==0)
				 //~ {
					
					//~ WriteToFile_NS((double)StepT);	//keep it above NS keeping so that filename will sync with time
					 
				//~ }
			
				
				/********************Write the total VolFrac in output file*********************************/
				Tot_VolFrac = 0.0;
  					 for(r=1;r <= NoOfRows; r++)
					{
						for(c=1;c <= NoOfCols; c++)
							{
							 Tot_VolFrac = Tot_VolFrac + (Cellattrib[r][c].VolFrac);
						 }
					 }
				
				 fprintf(ptrWrite_Output,"\n%d\t%e\t%e\t%e", StepT,delT,Time,Tot_VolFrac);
					 printf("\n%d\t%e\t%e\t%e", StepT,delT,Time,Tot_VolFrac);
					 fflush(stdout);
				/***********************************************************/
					 /***********************VOF PART*********************************/
				Calculate_InitialSlope(); // keep above recon interface    //POPULATES NX AND NY INITIALLY FROM A GUESS BASED ON NEIGHBOURING CELLS VF
				
				for(r=1;r <= NoOfRows ; r++)
				{
				   for(c=1;c <= NoOfCols ; c++)
					{
					  Recon_Interface(r,c);   //CALCULATE INTERFACE
					}
				}
	 			/*********************************************************/
					
				/******************************************************/
					 // NAVIER STOKES HERE
				
					 Advance_Velocity_Field();
				
				 /***********************************************/
				
				
			if (VOF_Module==1)	// Switch this from global_variables.h, don't touch here
			{
				
								
				
				
				
				
				
				
				//~ /***********Write data here ko keep file_names consistent with steps*********/
				if ( StepT%1==0)
				 {
					
					
					 WriteToFile((double)StepT);	// GENERATE Plot_Line{Step}.dat	
				}
			
			
				
				if(Togg == 0)     //1ST HORIZONTAL AND 2ND VERTICAL PASS
				 {
					 Pass_main(1,Togg);
					InterfaceRecon_main();
					 Pass_main(2,Togg);
					 Togg = Togg + 1;
				}
				 else if (Togg == 1)   //1ST VERTICAL AND 2ND HORIZONTAL PASS
				 {
					 Pass_main(2,Togg);

					 InterfaceRecon_main();
					Pass_main(1,Togg);
					 Togg = Togg - 1;

				 }



			 }



				//~ /*INCREMENT COUNTERS*/
				StepT++;

				/******************/
				 //* Compute cumulative time *//
				 Time = Time + delT;





	       }//END OF WHILE
	       
	    //   printf("\n%e\t%e\n",U_residue,V_residue);
		WriteToFile_NS(StepT);
	    //   exit(0);


	/*#################################################################*/

	/***CLOSE ALL FILE POINTERS*******/
	fclose(ptrWrite_Output);
	//fclose(ptrWrite_Y);
	/************END**************/
	RunTimeEnd= time(0);
	RunTimeEnd = RunTimeEnd - RunTimeStart;
	printf("\nThe total time for run is %d seconds\n",RunTimeEnd);
	       
	 
	       return 0;
		//RUN IS OVER
}

/****************************************************************
METHOD NAME       : Init_GlobalVars
DESCRIPTION          : INITIALISES ALL THE GLOBAL VARIABLES
LIMITATIONS            : NONE
****************************************************************/
void Init_GlobalVars()
  {
	/*********DECLARE LOCAL VARIABLES*************/
	int r,c;//junk variables to fool sscanf
          double VolFrac;
        char arr[100];
	
	
	  //printf("del = %lf", del);

	  //delT = 0.00125;

	/**********************************************/
		/*******************/
			int i;
	  
			X_Velocity = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Velocity = (double **)calloc(NoOfRows+2,sizeof(double*));
			X_Velocity_star = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Velocity_star = (double **)calloc(NoOfRows+2,sizeof(double*));
			X_Flux = (double **)calloc(NoOfRows+2,sizeof(double*));
			Y_Flux = (double **)calloc(NoOfRows+2,sizeof(double*));
			mu = (double **)calloc(NoOfRows+2,sizeof(double*));
			rho = (double **)calloc(NoOfRows+2,sizeof(double*));
			Press = (double **)calloc(NoOfRows+2,sizeof(double*));
			Cellattrib = (STRUCT_CELL_ATTRIB **)calloc(NoOfRows+2,sizeof(STRUCT_CELL_ATTRIB*));
			
			
			for (i=0; i<NoOfRows+2; i++)
			{
				X_Velocity[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Velocity[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_Velocity_star[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Velocity_star[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				X_Flux[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Y_Flux[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				mu[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				rho[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Press[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Cellattrib[i] = (STRUCT_CELL_ATTRIB *) calloc (NoOfCols+2, sizeof (STRUCT_CELL_ATTRIB));
			}
		ST_Fx = (double **) calloc(NoOfRows+2,sizeof(double *));
		ST_Fy = (double **) calloc(NoOfRows+2,sizeof(double *));
			for (i=0; i<NoOfRows+2; i++)
			{
				ST_Fx[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				ST_Fy[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
			}


		//exit(0);

     //exit(0);
     /***********************************************************************/
	//~ FILE *ptrRead1;
        //~ ptrRead1 =fopen("NSW289.dat","rt");

       //~ for(r=0;r <= NoOfRows+1 ; r++)
      //~ {
		//~ for(c=0;c <= NoOfCols+1 ; c++)
	       //~ {
		        //~ //50 is some random no. it will read the first 50 characters of the word
			//~ fgets(arr, 100, ptrRead1);
			//~ sscanf (arr,"%e\t%e",&X_Velocity[r][c],&Y_Velocity[r][c]);
		    //~ //  sscanf (arr,"%e", &X);
		       //~ if (r==17 && c==49)
		       //~ printf("%e\t%e\n",X_Velocity[r][c],Y_Velocity[r][c]);
	       //~ }
      //~ }
     //~ fclose(ptrRead1);
 
 
 Initialise_VolFrac();
    /******************INITIALISE GLOBAL VARIABLES**********/
    del_sq         = pow(del,2);
    del_sqBy2      = del_sq / 2.0;
    del2           = 2.0*del;
    del3           = 3.0*del;
    delBy2         = del/2.0;
    PiBy2          = PI/2.0;
    PiBy4          = PI/4.0;
    ThreePiBy2     = 3.0*PI/2.0;
    delTBydel      = delT/del;
    delTdel        = delT*del;
     /*******************************************************/

	//~ /********************INTIALISE THE VELOCITY/FLUX FIELD**************/
		 /*for(r=0;r <= NoOfRows+1 ; r++)
       		 {
	       		 for(c=0;c <= NoOfCols+1 ; c++)
	      		 {
				if ((r-0.5)*del<=(BoxLength+H) && c<=Refine*24)
				{
					X_Velocity[r][c] = Uav;
					
		   		}  
			//	if (c>Refine*24 && (c-0.5)*del<=(BoxLength*24+H))
			//	{
			//			Y_Velocity[r][c] = Uav;
			//	}
			
	      		 }
      		 }*/

       //~ for(r=0;r <= NoOfRows+1 ; r++)
       //~ {
	       //~ for(c=0;c < NoOfCols+1 ; c++)
	       //~ {
			//~ //Y_Velocity[r][c] = -cos(c*del*PI/30.0)*sin(r*del*PI/30.0);//(c*del*PI/30.0) - 1.5;
			//~ //Y_Velocity[r][c] =0;
		       //~ Y_Velocity[r][c] = sin(2.0*PI*(c-0.5)*del)*cos(2.0*PI*(r-1)*del);	//(sin (2.*M_PI*x)*cos (2.*M_PI*y))
		     //~ //  Y_Velocity[r][c] = 0.5*((c+0.5)*del-2);
			
		//~ }
	//~ }
	
	 //~ for(r=0;r <= NoOfRows+1 ; r++)
       //~ {
	       //~ for(c=0;c < NoOfCols+1 ; c++)
	       //~ {
			
		       //~ Press[r][c] = -0.25*(cos(4.0*PI*(c-0.5)*del) + cos(4.0*PI*(r-0.5)*del));
		     
			
		//~ }
	//~ }
		/******************READ THE INTIAL DATA FROM FILE**************/

	  
	  
if (StepT>0)
{	
        FILE *ptrRead;
	 char LineFormat[] = "./NS_Data/NS%d.dat";
	char LineFileName[20];
    sprintf(LineFileName,LineFormat,StepT);
    
       ptrRead =fopen(LineFileName,"rt");
 	double x,y;
        for(r=0;r <= NoOfRows+1 ; r++)
       {
	 for(c=0;c <= NoOfCols+1 ; c++)
	        {
		        //50 is some random no. it will read the first 50 characters of the word
			fgets(arr,100, ptrRead);
		       sscanf (arr,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf",&Time,&x,&y,&X_Velocity[r][c],&Y_Velocity[r][c],&Press[r][c],&Cellattrib[r][c].VolFrac);
		     
		    

		       	 
	        }
       }
     fclose(ptrRead);  
       }
	/**************************************************************/
  }
  
  /********************************
NAME : Apply_Extra_BoundaryConditions()
DESCRIPTION: apply velocity Boundary conditions
*****************************/
void Apply_Extra_BoundaryConditions()
{
	int r,c;
	

	/**********************************LEFT BC*******************************/
	//X-Velocity BC***************************
	for (r=1; r<=NoOfRows; r++)
	{
		
			X_Velocity[r][1] 		= 0; //BcDirichlet_U_left;
		
	}
	
		
		
	// }
	//~ // Y velocity BC*******************************************/
	for (r=1;r<=NoOfRows; r++)		//no slip for Y velocity
	{
		Y_Velocity[r][0] =  0 - Y_Velocity[r][1];					//left wall
		//~ Y_Velocity[r][0] =   Y_Velocity[r][1];					//left wall
		
	}
	
	// VolFrac BC*******************************************/
			//Neumann on VolFrac
			for (r=1; r<=NoOfRows; r++)
			{
				
				Cellattrib[r][0].VolFrac = Cellattrib[r][1].VolFrac;	                            //left wall
				
			}
			
			
				
				
			// }
	/*********************************************************************************/
		/**********************************EXTRA LEFT BC*******************************/
	//~ //X-Velocity BC***************************
	//~ for (r=1; r<=Refine; r++)
	//~ {
		//~ X_Velocity[r][Refine*24+1] 		= BcDirichlet_U_left;
		
		
	//~ }
	//~ for (r=1; r<=Refine+1; r++)
	//~ {
		//~ Y_Velocity[r][Refine*24] = 0- Y_Velocity[r][Refine*24+1];					//left wall
		
		
	//~ }
	//~ for (r=1; r<=Refine; r++)
	//~ {
		//~ Cellattrib[r][Refine*24].VolFrac = 1.0;
		
		
	//~ }
	/**********************************RIGHT  BC*******************************/
	//No slip U 
	for (r=1; r<=NoOfRows; r++)
	{
		
		X_Velocity[r][NoOfCols+1] 	= 0;
		
		
		
	}
	//~ // Y velocity BC*******************************************/
	 for (r=1;r<=NoOfRows; r++)		//No slip for Y velocity
	{
		
		//~ Y_Velocity[r][NoOfCols+1] =   Y_Velocity[r][NoOfCols];	//right wall
		Y_Velocity[r][NoOfCols+1] = 0 - Y_Velocity[r][NoOfCols];
		//printf("%e\t%e\n",Y_Velocity[r][0],Y_Velocity[r][1]);
	}
	
	//~ // VolFrac BC*******************************************/
			for (r=1; r<=NoOfRows; r++)
			{
				
				
				Cellattrib[r][NoOfCols+1].VolFrac = Cellattrib[r][NoOfCols].VolFrac;	            //right wall
			}
	//~ /*********************************************************************************/
	
	/*******************************ALL***BOTTOM  BC*******************************/
	for (c=1;c<=NoOfCols; c++)
	{
		//~ X_Velocity[0][c] = 0 -  X_Velocity[1][c];				//bottom wall
		X_Velocity[0][c] =  0 - X_Velocity[1][c];				//bottom wall
		
		
		
	}
	//bottom Dirichlet V
	for (c=1; c<=NoOfCols; c++)
	{
		Y_Velocity[0][c] = 0;
		
		
	}
			//~ //Neumann on VolFrac (Keep here as Default)
			//~ for (c=1;c<=NoOfCols; c++)
			//~ {
				
				//~ Cellattrib[0][c].VolFrac = Cellattrib[1][c].VolFrac;    //bottom wall
			//~ }
			
			for (c=0;c<=NoOfCols+1; c++)
			{
				Cellattrib[0][c].VolFrac = Cellattrib[1][c].VolFrac;				                //bottom wall
				
			}
	/*********************************************************************************/
	//~ /**********************************EXTRA BOTTOM  BC*******************************/
	//~ for (c=1;c<=Refine*24; c++)
	//~ {
		//~ X_Velocity[Refine][c] =  0 - X_Velocity[Refine+1][c];				//bottom wall
		
	//~ }		
	//~ //bottom Dirichlet V
	//~ for (c=1; c<=Refine*24; c++)
	//~ {
		//~ Y_Velocity[Refine+1][c] = 0;
		
		
	//~ }		
	//~ for (c=1; c<=Refine*24; c++)
	//~ {
		//~ Cellattrib[Refine][c].VolFrac =1;
	//~ }
	/**********************************TOP  BC*******************************/
	for (c=1;c<=NoOfCols; c++)
	{
		
		X_Velocity[NoOfRows+1][c] = 0 - X_Velocity[NoOfRows][c];	// no sliptop wall
		//~ X_Velocity[NoOfRows+1][c] = X_Velocity[NoOfRows][c]; //Neumann on wall
	}
	//~ //top Dirichlet V
	for (c=1; c<=NoOfCols; c++)
	{
		
		Y_Velocity[NoOfRows+1][c] = 0;
		
		
	}
	//~ //Neumann on VolFrac (Keep here as Default)
			for (c=1;c<=NoOfCols; c++)
			{
				
				Cellattrib[NoOfRows+1][c].VolFrac = Cellattrib[NoOfRows][c].VolFrac;    //bottom wall
			}
			
	/*********************************************************************************/
/********************Eat the droplets**************************************/
			//~ for (r=(Refine+1); r<=NoOfRows; r++)
			//~ {
				//~ for (c=1; c<=NoOfCols; c++)
				//~ {
					//~ if ((r-0.5)*del>=(BoxLength+0.3))
					//~ {
						//~ Cellattrib[r][c].VolFrac = 0;
					//~ }
				//~ }
		
			//~ }
			
			
}
  


/*******************************
NAME: CFL_Check()
Description: Restrict the time steps in accordance with CFL criteria
******************************/
void CFL_Check()
{				int r,c, CASE=0;
				double umax,vmax,delT_Diff=0, delT_adv=0,delT_Sigma=0,max_vel, nu_max, V,Vmax;
				// find maximum velocity
				umax = 0; vmax=0;
			for(r=1;r <= NoOfRows ; r++)
				{
				   for(c=1;c <= NoOfCols ; c++)
					{
						umax = fmax(umax,fabs(X_Velocity[r][c]));
						vmax = fmax(vmax,fabs(Y_Velocity[r][c]));
						max_vel = fmax(umax,vmax);
					}
				}
				//~ for(r=Refine+1;r <= NoOfRows ; r++)
				//~ {
				   //~ for(c=1;c <= NoOfCols ; c++)
					//~ {
						//~ umax = fmax(umax,fabs(X_Velocity[r][c]));
						//~ vmax = fmax(vmax,fabs(Y_Velocity[r][c]));
						//~ max_vel = fmax(umax,vmax);
					//~ }
				//~ }

					 delT = 1e-1; // Keep this here as some non-zero(not very low) value for delT
 
					printf("\t%e\t%e",umax,vmax);
					
			
					
					if (max_vel>1e-7)
					{
						delT_adv= 0.5*del/max_vel;
						delT = min(delT,delT_adv);
					}
					if (Diffusion_term > 1e-7)
					{	
						nu_max = max(mu_G/rho_G, mu_L/rho_L); // max dynamic viscosity
						delT_Diff = (0.25/nu_max)*pow(del,2);	//Diffusion term criterian
						delT = min(delT,delT_Diff);
						//delT = (CFL*min(delT_adv,delT_Diff));
					}
					 if (Sigma > 1e-7)
					{
						delT_Sigma = pow(((rho_G+rho_L) * pow(del,3)/(4*PI*Sigma)),0.5);
						delT = min(delT,delT_Sigma);
					}
					
						delT = CFL*delT;			
						//delT = 1e-5; //remove this after the test
						
					// if (Advection_term==1 && Diffusion_term==0)
					// {
					// 	delT = CFL*delT_adv;
					// }
					// else if (Diffusion_term ==1 && Advection_term == 0)
					// {
					// 	nu_max = fmax(mu_G/rho_G, mu_L/rho_L); // max dynamic viscosity
					// 	delT_Diff = (0.25/nu_max)*pow(del,2);	//Diffusion term criterian
					// 	delT = CFL*delT_Diff;
					// }i
					// else if (Advection_term == 1 && Diffusion_term==1)
					// {
					// 	nu_max = fmax(mu_G/rho_G, mu_L/rho_L); // max dynamic viscosity
					// 	delT_Diff = (0.25/nu_max)*pow(del,2);	//Diffusion term criterian
					// 	delT = CFL*delT_Diff;
					// 	delT = (CFL*fmin(delT_adv,delT_Diff));
					// }
					
					//delT =0.0156;
					//~ Jump_Time = Time + delT;
					//~ Write_Time = (Write_Count+1)*step;  // +1 is necessary to get the next Write_Time
				//~ //printf("Jump_Time = %e \t Write_Time = %e \t Write_Count = %d \t step = %e",Jump_Time,Write_Time, Write_Count, step);
				//~ if (Jump_Time > Write_Time)
				//~ {
					//~ delT = Write_Time - Time;
					
				//~ }
				
				
					//delT = 2*del;
				    /******************INITIALISE GLOBAL VARIABLES**********/
				    del_sq         = pow(del,2);
				    del_sqBy2      = del_sq / 2.0;
				    del2           = 2.0*del;
				    del3           = 3.0*del;
				    delBy2         = del/2.0;
				    PiBy2          = PI/2.0;
				    PiBy4          = PI/4.0;
				    ThreePiBy2     = 3.0*PI/2.0;
				    delTBydel      = delT/del;
				    delTdel        = delT*del;
				     //~ /*******************************************************/
}
/******************************************************************************************************
NAME       : Pass_main()
DESCRIPTION: THIS IS A DUMY FUNCTION WHICH IS USED TO REMOVE CODE FROM main()
			 TOGG IS A VARIABLE USED FOR ALTERNATING BETWEEN VERTICAL AND HORIZONTAL PASSES
			 FLAG IS A VARIABLE USED FOR DECIDING WHETHER THE CURRENT PASS IS HORIZONATAL / VERTICAL
LIMITATIONS: NONE
/******************************************************************************************************/
void Pass_main(int Flag, int Togg)
{
	int r, c;
	BookKeeping();
	CalculateF_Flux(Flag);   //CALCULATE X FLUXES
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
		  AdvanceF_field(r,c,Flag,Togg);  //ADVANCE F IN TIME BASED ON FLAG VALUE AND HORIZONTAL FLUXES
		}
	}
	//~ for(r=Refine+1;r <= NoOfRows; r++)
	//~ {
	   //~ for(c=1;c <= NoOfCols; c++)
		//~ {
		  //~ AdvanceF_field(r,c,Flag,Togg);  //ADVANCE F IN TIME BASED ON FLAG VALUE AND HORIZONTAL FLUXES
		//~ }
	//~ }


	
}

/*********************************************************************************
NAME       : InterfaceRecon_main()
DESCRIPTION: THIS IS A DUMY FUNCTION WHICH IS USED TO REMOVE CODE FROM main()
LIMITATIONS: NONE
/********************************************************************************/
void InterfaceRecon_main()
{
	int r,c;

	Calculate_InitialSlope();
	for(r=1;r <= NoOfRows ; r++)
	{
	   for(c=1;c <= NoOfCols ; c++)
		{
		  Recon_Interface(r,c);
		}
	}
}

/****************************************************************************************************************
METHOD NAME: Recon_Interface
DESCRIPTION: GIVEN A CELL, THIS FUNCTION FINDS OUT ST. LINES TO REPRESENT THE INTERFACE IN THAT CELL. THE OUTPUT
			 IS WRITTEN TO A FILE NAMED PLOTLINE_{STEP}.DAT. HERE {STEP} REPRESENTS AN INTEGER, CORRESPONDING
			 TO THE ACTUAL NO. OF TIME-STEPS FOR WHICH THE CALCULATION IS PERFORMED.
LIMITATIONS:
****************************************************************************************************************/
void Recon_Interface(int r, int c)
{
     double Sum_OfDiff_Sq_clock, Sum_OfDiff_Sq_anticlock;
     double Sum_OfDiff_Sq_old, Sum_OfDiff_Sq_current;
   char sense1[20], sense2[20];
   char sense[20], Shape_current[20];
   int flag = 1;
     double Nx_current, Ny_current, Theta_current, P_current, Nx_clock, Ny_clock, Nx_anticlock, Ny_anticlock;

	strcpy(sense1,"Clockwise");
	strcpy(sense2,"Counterclockwise");

 	if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
	{
		 Calculate_Theta(r,c);

		 Nx_current = Cellattrib[r][c].Nx;   //preserve the old value
		 Ny_current = Cellattrib[r][c].Ny;   //preserve the old value

		 Identify_Shape(r,c);       //find the shape depending on VolFrac and Slope
		 Calculate_distance(r,c);   //find the perp. distance depending on shape
		Theta_current  = Cellattrib[r][c].Theta; //preserve the old value
		 P_current = Cellattrib[r][c].P;
		 strcpy(Shape_current,Cellattrib[r][c].Shape);
		 Extrapolate_Line(r,c);
		 Sum_OfDiff_Sq_current = Calculate_SumOfDiffSq(r,c);

		 /*ROTATE CLOCKWISE AND CALCULATE SUM OF SQUARES*/
		 Rotate_Line(r,c,sense1);   //change the Nx and Ny values
		 Nx_clock = Cellattrib[r][c].Nx;   //preserve the clockwise value
		 Ny_clock = Cellattrib[r][c].Ny;   //preserve the clockwise value
		 Calculate_Theta(r,c);
		 //printf("\nAfter clockwise rotation the value of Theta is %f\n", Cellattrib[r][c].Theta);
		 Identify_Shape(r,c);
		 Calculate_distance(r,c);
		 Extrapolate_Line(r,c);
		 Sum_OfDiff_Sq_clock = Calculate_SumOfDiffSq(r,c);
		 /************************************************/

		 //Write_struct_Cellattrib(r,c,-100000,-100000,Nx_current,Ny_current,-100000,"-100000",-100000,-100000);  //restore the current values
		 Write_struct_Cellattrib(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000);  //restore the current values
		 /*ROTATE COUNTER-CLOCKWISE AND CALCULATE SUM OF SQUARES*/
		 Rotate_Line(r,c,sense2);  //change the Nx and Ny values
		 Nx_anticlock = Cellattrib[r][c].Nx;   //preserve the clockwise value
		 Ny_anticlock = Cellattrib[r][c].Ny;   //preserve the clockwise value
		 Calculate_Theta(r,c);
		 //printf("\nAfter clockwise counterclockwise rotation the value of Theta is %f\n", Cellattrib[r][c].Theta);
		 Identify_Shape(r,c);
		 Calculate_distance(r,c);
		 Extrapolate_Line(r,c);
		 Sum_OfDiff_Sq_anticlock = Calculate_SumOfDiffSq(r,c);
		 /************************************************/

		 //Write_struct_Cellattrib(r,c,-100000,-100000,Nx_current,Ny_current,-100000,"-100000",-100000,-100000);  //restore the current values
		 Write_struct_Cellattrib(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000);  //restore the current values
		 if( Sum_OfDiff_Sq_current >= Sum_OfDiff_Sq_clock)
		   {
			 strcpy(sense,sense1);
			 Write_struct_Cellattrib(r,c,-100000,-100000,Nx_clock,Ny_clock,-100000,"-100000",-100000,-100000);  //make the clockwise value as current
			 //printf("\n THE LINE SHOULD BE ROTATED %s\n", sense);
		   }
		 else if(Sum_OfDiff_Sq_current >= Sum_OfDiff_Sq_anticlock)
		   {
			 strcpy(sense,sense2);
			 Write_struct_Cellattrib(r,c,-100000,-100000,Nx_anticlock,Ny_anticlock,-100000,"-100000",-100000,-100000);  //make the clockwise value as current
			 //printf("\n THE LINE SHOULD BE ROTATED %s\n", sense);
		   }
		 else //CURRENT VALUE IS THE MINIMUM - NO NEED FOR ITERATIONS
		   {
			 return;
		   }

		 flag = 1;
		 while(flag)
		   {
				Theta_current  = Cellattrib[r][c].Theta; //preserve the old value
				 P_current = Cellattrib[r][c].P;
				 strcpy(Shape_current,Cellattrib[r][c].Shape);
				  Nx_current = Cellattrib[r][c].Nx;   //preserve the old value
					 Ny_current = Cellattrib[r][c].Ny;   //preserve the old value
			 Rotate_Line(r,c,sense);
			 Calculate_Theta(r,c);
			 Identify_Shape(r,c);       //Find the shape depending on VolFrac and Slope
			 Calculate_distance(r,c);   //Find the perp. distance depending on shape
			 Extrapolate_Line(r,c);

			 Sum_OfDiff_Sq_old = Sum_OfDiff_Sq_current;
			 Sum_OfDiff_Sq_current = Calculate_SumOfDiffSq(r,c);

			 if (Sum_OfDiff_Sq_old <= Sum_OfDiff_Sq_current)
			{
				 flag = 0;
				Write_struct_Cellattrib(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000);  //restore the current values
				 //restore the previous value - TO BE DONE
			}
		}
	}
}

/**************************************************************************************************
METHOD NAME: Write_struct_Cellattrib
DESCRIPTION: ALL WRITE OPERATIONS TO THE STRUCTURE STRUCT_CELL_ATTRIB HAPPEN THROUGH THIS FUNCTION.
             SEND -100000 IF NOTHING IS TO BE WRITTEN FOR A GIVEN PARAMETER
LIMITATIONS: NONE
***************************************************************************************************/
void Write_struct_Cellattrib(int r,int c,  double VolFraction,   double Area,   double Nx,   double Ny,   double P, char Shape[30],   double Theta, int Type)
  {
    if (VolFraction != -100000)
      {
       Cellattrib[r][c].VolFrac = VolFraction;
      }
    if (Area != -100000)
      {
       Cellattrib[r][c].Area = Area;
      }
    if (Nx != -100000)
      {
        Cellattrib[r][c].Nx = Nx;
      }
    if (Ny != -100000)
      {
        Cellattrib[r][c].Ny = Ny;
      }
    if (P != -100000)
      {
        Cellattrib[r][c].P = P;
      }
    if (!strcmp(Shape,"-100000"))   //strcmp RETURNS 0 IF BOTH THE STRINGS MATCH
      {
        //DO NOTHING
      }
    else
      {
        strcpy(Cellattrib[r][c].Shape,Shape);
      }
    if(Theta != -100000)
      {
        Cellattrib[r][c].Theta = Theta;
      }
	if(Type != -100000)
	{
		Cellattrib[r][c].Type = Type;
	}
  }


void Update_Fluid_Properties()
  {

	  int r,c;


		

        
	       for (r=0; r<=NoOfRows+1; r++)
		{
			for(c=0; c<=NoOfCols+1; c++)
			{

			    mu[r][c] = mu_L*Cellattrib[r][c].VolFrac + mu_G*(1-Cellattrib[r][c].VolFrac);
			    rho[r][c] = rho_L*Cellattrib[r][c].VolFrac + rho_G*(1-Cellattrib[r][c].VolFrac);
				
			}
		}
	

}
/******************************************************************************************************************
METHOD NAME       : Calculate_InitialSlope()
DESCRIPTION       : CALCULATES INITIAL SLOPE FROM [4,5]
KNOWN LIMITATIONS : CANNOT WORK FOR THE BOUNDARY CELLS. THIS HAS TO BE CORRECTED.
******************************************************************************************************************/
void Calculate_InitialSlope()
{
  /*LOCAL VARIABLES**********/
  int r, c;
    double temp1, temp2, Nx, Ny;
  /**************************/

  for(r=1;r <= NoOfRows ; r++)
   {
      for(c=1;c <= NoOfCols ; c++)
       {
		 if(Cellattrib[r][c].VolFrac > 0 && Cellattrib[r][c].VolFrac < 1)
		   {
				  temp1 = Cellattrib[r+1][c+1].VolFrac + (2.0*Cellattrib[r][c+1].VolFrac) + Cellattrib[r-1][c+1].VolFrac;
				  temp2 = Cellattrib[r+1][c-1].VolFrac + (2.0*Cellattrib[r][c-1].VolFrac) + Cellattrib[r-1][c-1].VolFrac;

				  Nx =  (temp1 - temp2)/del;

				  if(Nx != 0.0)
					{
					  Nx = -Nx; //THIS IS REQUIRED BECAUSE REF[3] FROM WHERE THE FORMULA(Nx,Ny) IS TAKEN, HAS THE DEFN OPPOSITE
					}

				  temp1 = Cellattrib[r+1][c+1].VolFrac + (2.0*Cellattrib[r+1][c].VolFrac) + Cellattrib[r+1][c-1].VolFrac;
				  temp2 = Cellattrib[r-1][c+1].VolFrac + (2.0*Cellattrib[r-1][c].VolFrac) + Cellattrib[r-1][c-1].VolFrac;
				  Ny    = (temp1 - temp2)/del;    //CHANGE POINT

				  if(Ny != 0.0)
					{
					  Ny    = -Ny; //CHECK THIS
					}

				  Write_struct_Cellattrib(r,c,-100000,-100000,Nx,Ny,-100000,"-100000",-100000,-100000);
		   }
      }
   }
}

/**********************************************************************************************************************************************
METHOD NAME: Identify_Shape
DESCRIPTION: Takes [r,c] as a cell identifier and identifies the shape of the area based on the Slope and the VolFraction
LIMITATIONS: Check all greater than equal to signs
***********************************************************************************************************************************************/
void Identify_Shape(int r, int c)
{
  /*LOCAL VARIABLES*/
    double Theta;
    double Slope;
    double VolFrac;
    double Area;
  char   Shape[30];
  /*****************/
       VolFrac =  Cellattrib[r][c].VolFrac;
	   Area    =  VolFrac*del_sq;
       Theta   =  Cellattrib[r][c].Theta;

        if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
         {
           strcpy(Shape,"Rectangle");
           Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,Shape,-100000,-100000);
           return;
         }

        Slope   =  tan(Theta) ;   //DONT MOVE THIS UP OTHERWISE SLOPE WILL GO TO INFINITY FOR THETA = PI / 2.0
       //SHADED AREA IS A TRIANGLE
       if( (Area <= (del_sqBy2*Slope) ) && (Area <= (del_sqBy2/Slope) )  )    //both for Theta <> PI/4.0
        {
           strcpy(Shape,"Triangle");
        }
      //SHADED AREA IS A TRAPEZIUM WITH THETA < PI / 4
      else if ( (Area > del_sqBy2*Slope )  && Area < (del_sq - (del_sqBy2*Slope) )  )  // for Theta < PI / 4.0
        {
          strcpy(Shape,"Trapezium");
        }
      //SHADED AREA IS A TRAPEZIUM WITH THETA > PI / 4
      else if ( (Area > del_sqBy2/Slope )  && Area < (del_sq - (del_sqBy2/Slope) )  )  // for Theta > PI / 4.0
        {
          strcpy(Shape,"Trapezium");
        }
      //SHADED AREA IS A 5 SIDED FIGURE WHICH IS THE COMPLEMENT OF A TRIANGLE
      else
        {
          strcpy(Shape,"Triangle_Complement");
        }

       //WRITE SHAPE TO THE STRUCT
       Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,Shape,-100000,-100000);
}

/***********************************************************************************************************************************************************
METHOD NAME: Calculate_distance
DESCRIPTION: BASED ON THE SHAPE OF THE AREA, CALCULATES THE PERPENDICULAR DISTANCE. ASSUMES EVERYTHING TO BE IN THE 1ST QUADRANT
LIMITATIONS :NO CHECKS FOR BOUNDARY VALUES AS YET / NO CHECKS FOR NX, NY = 0
             NO CHECKS FOR SEEING IF THE STRING SHAPE IS NULL IN STRUCTCELLATTRIB
***********************************************************************************************************************************************************/
void Calculate_distance(int r, int c)
{
  /*LOCAL VARIABLES*/
    double Theta;
    double VolFrac;
  char   Shape[30];
    double P;
    double P_dash;
  /****************/

  /*INITIALIZE*/
  Theta   = 0.0;
  VolFrac = 0.0;
  strcpy(Shape,"000");
  P       = 0.0;
  P_dash  = 0.0;
  /****************************/

  /*FETCH THE VALUES INTO THE LOCAL VARIABLES*/
  strcpy(Shape,Cellattrib[r][c].Shape);
  Theta   =  Cellattrib[r][c].Theta;
  VolFrac =  Cellattrib[r][c].VolFrac;
  /****************************************/

  if(!strcmp(Shape,"Rectangle"))
    {
      P = VolFrac*del;
    }
  else if( !strcmp(Shape,"Triangle") )    //both for Theta <> PI/4.0
   {
     P = sqrt ( VolFrac*del_sq*sin(2.0*Theta) );
   }
  else if ( !strcmp(Shape,"Trapezium") && Theta < PI/4.0)  // for Theta < PI / 4.0
   {
       //THIS IS OLDER CORRECT EXPRESSION - CHANGED TO MAKE IT FASTER
       //P =  ( ( 2*VolFrac*del_sq*cos(Theta) ) + del_sq*sin(Theta) ) / del2;
       P = (VolFrac*del_sq*cos(Theta)/del) + (del*sin(Theta)*0.5);
   }
  else if ( !strcmp(Shape,"Trapezium") && Theta > PI/4.0 )  // for Theta > PI / 4.0
   {
    //THIS IS OLDER CORRECT EXPRESSION - CHANGED TO MAKE IT FASTER
     //P =  ( ( 2*VolFrac*del_sq*sin(Theta) ) + del_sq*cos(Theta) ) / del2;
	P =   (VolFrac*del_sq*sin(Theta)/del ) + (del*cos(Theta)*0.5);
   }
  else if ( !strcmp(Shape,"Triangle_Complement") )    //both for Theta <> PI/4.0
   {
     //THIS WORKS ONLY FOR A SQUARE CELL
     P_dash = sqrt( del_sq*(1.0 - VolFrac)*sin(2.0*Theta) );
     //THIS IS OLDER CORRECT EXPRESSION - CHANGED TO MAKE IT FASTER
     //P = (del/cos(Theta) ) - P_dash + ( del*sin(Theta)*( 1.0-tan(Theta) ) );
	P = del*( sin(Theta) +  cos(Theta) ) - P_dash;
   }
  else  //ERROR HANDLING
    {
      printf("\n***********************************************************\n");
      printf("\nERROR - EXECUTION TERMINATES IN SUBROUTINE CALCULATE_DISTANCE\n");
      printf("\n***********************************************************\n");
      exit(0);
    }

    //WRITE PERPENDICULAR DISTANCE TO THE STRUCT
    Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,P,"-100000",-100000,-100000);
}

/*************************************************************************************************************************************************
METHOD NAME: Extrapolate_Line
DESCRIPTION: EXTRAPOLATES THE LINE IN THE "CENTRAL" CELL TO THE 3X3 BLOCK CENTRED ON IT.
             ASSIGNS AREAS BASED ON CELL NAMES EG.NORTHWEST CELL'S COORDINATES ARE [r+1,c-1] FOR 1ST QUADRANT, [r-1,c-1] FOR 2ND QUADRANT ETC.
             ROTATES THE 3X3 BLOCK CLOCKWISE TO REDUCE EVERY QUADRANT TO THE FIRST QUADRANT.
LIMITATIONS:
**************************************************************************************************************************************************
 _______________________________________________________
|                 	|                    	|       	         |
|                 	|                    	|               	 |
|                 	|	             	|       	       	 |
| NorthWest	|      North     	|    NorthEast   |
|                 	|                    	|                        |
|                 	|                    	|                        |
|_______________	|_______________ |________________|
|                 	|                       |                |
|                 	|                       |                |
|                 	|                       |                |
|    West         	|      Central     |    East        |
|                 	|                       |                |
|                 	|                       |                |
|_______________|_______________ |________________|
|                 |                    |                |
|                 |                    |                |
|                 |                    |                |
|    SouthWest    |      South         |    SouthEast   |
|                 |                    |                |
|                 |                    |                |
|                 |                    |                |
|_________________|____________________|________________|
*/

void Extrapolate_Line(int r, int c)
{
  /****LOCAL VARIABLES****/
  int    Quad;
    double y_0, y_del, y_2del, y_3del;   //the value of y at x = 0 , x = del , x = 2*del and x = 3*del
    double x_0, x_del, x_2del, x_3del ;  //the value of x at y = 0 , y = del , y = 2*del and y = 3*del
    double Area;
    double Theta;
    double Slope;
    double P;
  char   Shape[30];
  /**********************/

  strcpy(Shape,Cellattrib[r][c].Shape);
  Quad        =  Find_Quad(r,c);
  P           =  Cellattrib[r][c].P;

  IdentifyCell_AssignArea(r,c,Quad,"Central",Cellattrib[r][c].VolFrac);   //for the central cell, assign the same VolFrac

  //if rectangle,
  if( !strcmp(Shape,"Rectangle") )
    {
      Area = P*del;
      if(Quad == 1)
        {
            Write_struct_Cellattrib(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r  ,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r  ,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
        }
      else if(Quad == 2)
        {
            Write_struct_Cellattrib(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r+1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
        }
      else if(Quad == 3)
        {
            Write_struct_Cellattrib(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r  ,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r  ,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
        }
      else if(Quad == 4)
        {
            Write_struct_Cellattrib(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);

            Write_struct_Cellattrib(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r+1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
            Write_struct_Cellattrib(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000);
        }
      return;
    }

         Theta       =  Cellattrib[r][c].Theta;
         Slope       =  tan(Theta);
         /// ALL CONDITIONS ARE CHECKED USING THE FOLLOWING EQUATION
         /// y = tan(theta)[h + p*cosec(theta) - x] + h
         /// P IS MEASURED FROM THE BOTTOM LHS CORNER

         y_0   =  Slope*( del + (P/sin(Theta))           ) + del;
         y_del =  Slope*( del + (P/sin(Theta)) - del     ) + del;
         y_2del = Slope*(       (P/sin(Theta)) - del     ) + del;
         y_3del = Slope*(       (P/sin(Theta)) - del2    ) + del;

         x_0    = del + (P/sin(Theta)) + (del/Slope);
         x_del  = del + (P/sin(Theta))              ;
         x_2del = del + (P/sin(Theta)) - (del/Slope);
         x_3del = del + (P/sin(Theta)) - (del2/Slope);

      //SHADED AREA IS A TRIANGLE

         if( !strcmp(Shape,"Triangle") )    //both for Theta <> PI/4.0
          {
                //ASSIGN VOLFRAC TO CELLS WHICH ARE 0 OR 1
                IdentifyCell_AssignArea(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea(r,c,Quad,"East",0.0);
                /******************************************/

                /*****************************CALCULATE AREAS FOR NORTHWEST & WEST CELL*****************************/
                 if (y_0 <= del2)  //IF NORTHWEST IS 0
                   {
                     IdentifyCell_AssignArea(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea(r,c,Quad,"West",Area);
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //IF NORTHWEST IS A TRIANGLE
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment.for NorthWest
                     IdentifyCell_AssignArea(r,c,Quad,"West",Area);
                   }
                 else if( y_0 > del3 )   //IF NORTHWEST IS A TRAPEZIUM
                   {
                     Area = ( x_2del + x_3del )*delBy2;                       // Calculate the area of the trapezium for NorthWest
                     IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea(r,c,Quad,"West",Area);
                   }
                 /***********************END*****************************************************************************/

                 /********CALCULATE AREAS FOR South AND SOUTHEAST CELLS (exchange x and y in all expressions above)***********/
                  if( x_0 <= del2 )    //IF SOUTHEAST IS 0
                  {
                    IdentifyCell_AssignArea(r,c,Quad,"SouthEast",0.0);

                    Area = ( (x_0 - del) + (x_del - del) )*delBy2;       //calculate area of trapezium for South
                    IdentifyCell_AssignArea(r,c,Quad,"South",Area);
                  }
                 else if ( x_0 > del2 && x_0 <= del3 )                  //IF SOUTHEAST IS A TRIANGLE
                   {
                     Area = 0.5*(x_0 - del2)*y_2del;                       //Calculate area of the triangle for SouthEast
                     IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea(r,c,Quad,"South",Area);
                   }
                 else if( x_0 > del3 )      //IF SOUTHEAST IS A TRAPEZIUM
                   {
                     Area = (y_2del + y_3del)*delBy2;                       // Calculate the area of the trapezium for SouthEast
                     IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea(r,c,Quad,"South",Area);
                   }
                 /***********************END*********************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA < PI / 4
         else if (  !strcmp(Shape,"Trapezium") && Theta < PI/4.0)  // for Theta < PI / 4.0
        {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea(r,c,Quad,"South",1.0);
                /******************************************/

                /*****************************CALCULATE AREAS FOR NORTHWEST AND WEST CELLS*****************************/
                 if (y_0 <= del2)  //if NorthWest is 0
                   {
                     IdentifyCell_AssignArea(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea(r,c,Quad,"West",Area);
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //if NorthWest is a triangle
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea(r,c,Quad,"West",Area);
                   }
                 /***********************END*****************************************************************************/

                 /*****************************CALCULATE AREAS FOR SOUTHEAST AND EAST CELLS*****************************/
                if(y_3del >= del)    //if SouthEast is 1
                  {
                     IdentifyCell_AssignArea(r,c,Quad,"SouthEast",1.0);

                     Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
                     IdentifyCell_AssignArea(r,c,Quad,"East",Area);
                  }
                else if(y_3del > 0 && y_3del < del)   //if SouthEast is a complement of a triangle
                  {
                    Area = del_sq  - ( 0.5*(del3 - x_del)*(del-y_3del) ); //calculate area of the triangle and then take complement for SouthEast
                    IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

                    Area = 0.5*(y_2del - del)*(x_del - del2);     //calculate area of area of the trinagle for East
                    IdentifyCell_AssignArea(r,c,Quad,"East",Area);
                  }
                 /***********************END****************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA > PI / 4
         else if ( !strcmp(Shape,"Trapezium")  && Theta > PI/4.0)  // for Theta > PI / 4.0
         {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea(r,c,Quad,"West",1.0);
                IdentifyCell_AssignArea(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea(r,c,Quad,"East",0.0);
                IdentifyCell_AssignArea(r,c,Quad,"NorthEast",0.0);
                /******************************************/

                /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST*****************************/
                if(y_del >= del3)   //if NorthWest is 1
                  {
                    IdentifyCell_AssignArea(r,c,Quad,"NorthWest",1.0);
                    //printf("\nNW is 1\n");

                    Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
                    IdentifyCell_AssignArea(r,c,Quad,"North",Area);
                    // printf("\nN %f \n",Area);
                  }
                else if(y_del < del3) //if NorthWest is complement of a triangle
                  {
                    Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest
                    IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);

                    Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
                    IdentifyCell_AssignArea(r,c,Quad,"North",Area);
                  }
                /***********************END**************************************************************************/

                /*****************************CALCULATE AREAS FOR SOUTH & SOUTHEAST**********************************/
                if(x_0 <= del2)   //if SouthEast is 0
                  {
                    //printf("\nSE is 0\n");
                    IdentifyCell_AssignArea(r,c,Quad,"SouthEast",0.0);

                    Area = ( (x_del - del) + (x_0 - del) )*delBy2;   //calculate area of the trapezium for South
                    IdentifyCell_AssignArea(r,c,Quad,"South",Area);
                    //printf("\nN %f \n",Area);
                  }
                else if(x_0 >  del2)    //if SouthEast is a triangle
                  {
                    Area = 0.5*y_2del*(x_0 - del2);    //calculate area of the triangle for SouthEast
                    IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

                    Area = del_sq - (del - y_2del)*(del2 - x_del);      //calculate area of the triangle and take complement for South
                    IdentifyCell_AssignArea(r,c,Quad,"South",Area);
                  }
                /***********************END**************************************************************************/
        }

      //SHADED AREA IS A 5 SIDED FIGURE WHICH IS THE COMPLEMENT OF A TRIANGLE
         else if( !strcmp(Shape,"Triangle_Complement")  )  //both for Theta <> PI/4
        {
            //Assign VolFRac to cells which are 0 or 1
            IdentifyCell_AssignArea(r,c,Quad,"West",1.0);
            IdentifyCell_AssignArea(r,c,Quad,"SouthWest",1.0);
            IdentifyCell_AssignArea(r,c,Quad,"South",1.0);
            IdentifyCell_AssignArea(r,c,Quad,"NorthEast",0.0);
            /******************************************/

          /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST**********************************/
          if(x_3del >= del)   //if NorthWest is 1
           {
             //printf("NW is 1\n");
              IdentifyCell_AssignArea(r,c,Quad,"NorthWest",1.0);

              Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
              IdentifyCell_AssignArea(r,c,Quad,"North",Area);
              //printf("\nN is %f",Area);
           }
          else if(x_3del < del  && x_3del > 0) //if NorthWest is complement of a triangle
           {
            Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest
            IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);
            //printf("NW is %f",Area);

            Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
            IdentifyCell_AssignArea(r,c,Quad,"North",Area);
           }
          else if(x_3del  < 0)    //if NorthWest is a trapezium
           {
             Area = ( (y_0 - del2) + (y_del - del2) )*delBy2;    //calculate area of the trapezium for NorthWest
             IdentifyCell_AssignArea(r,c,Quad,"NorthWest",Area);

             Area =0.5*(y_del - del2)*(x_2del - del);   //calculate area of the triangle for North
             IdentifyCell_AssignArea(r,c,Quad,"North",Area);
           }
          /****************************************END**********************************************************/

          /*****************************CALCULATE AREAS FOR EAST & SOUTHEAST**********************************/
           if(y_3del >= del)       //if SouthEast is 1
            {
              IdentifyCell_AssignArea(r,c,Quad,"SouthEast",1.0);

              Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
              IdentifyCell_AssignArea(r,c,Quad,"East",Area);
            }
          else if(y_3del < del && y_3del > 0)   //if SouthEast is complement of a triangle
            {
              Area = del_sq - ( 0.5*(del3 - x_del)*(del - y_3del) );   //calculate area of the triangle and take complement for SouthEast
              IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

              Area = 0.5*(x_del - del2)*(y_2del - del);          //calculate area of the triangle for East
              IdentifyCell_AssignArea(r,c,Quad,"East",Area);
            }
          else if(y_3del < 0)    //if SouthEast is a trapezium
            {
              Area = ( (x_0 - del2) + (x_del - del2) )*delBy2;    //calculate area of the trapezium for SouthEast
              IdentifyCell_AssignArea(r,c,Quad,"SouthEast",Area);

              Area =0.5*(x_del - del2)*(y_2del - del);   //calculate area of the triangle for East
              IdentifyCell_AssignArea(r,c,Quad,"East",Area);
            }
          /****************************************END**********************************************************/
        }
	 else //ERROR HANDLING
        {
	  printf("\n***********************************************************\n");
	  printf("\nERROR - ERROR IN SUBROUTINE EXTRAPOLATE_LINE\n");
	  printf("\n***********************************************************\n");
	  //exit(0);
        }
}
/************************************************************************************************************************************************************
METHOD NAME: IdentifyCell_AssignArea
DESCRIPTION: TAKES THE CELL NAME EG. NORTHWEST AND LOCATES THE POSITION OF THE CELL, BASED ON THE QUADRANT. ASSIGNS THE PASSED VALUE OF THE AREA TO THE CELL.
LIMITATIONS: NONE
*************************************************************************************************************************************************************/

void IdentifyCell_AssignArea(int r, int c,int Quad, char CellName[30],  double Area)
{
  int R, C;
  if(!strcmp(CellName,"NorthWest") )
    {
       switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c-1;
               break;

               case 2:
                 R = r-1;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c+1;
               break;

               case 4:
                 R = r+1;
                 C = c+1;
               break;
        }
    }
  else if(!strcmp(CellName,"North") )
    {
      switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c;
               break;

               case 2:
                 R = r;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c;
               break;

               case 4:
				 R =r;
				 C =c+1;
			   break;
		}
    }
  else if(!strcmp(CellName,"NorthEast") )
    {
        switch (Quad)
		{
			   case 1:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 2:
				R = r+1;
				C = c-1;
			   break;

       		   case 3:
				 R = r-1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c+1;
			   break;
		}
    }
  else if( !strcmp(CellName,"West") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c-1;
			   break;

       		   case 2:
				R = r-1;
				C = c;
			   break;

       		   case 3:
			    R = r;
				C = c+1;
			   break;

       		   case 4:
				R = r+1;
				C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"East") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c;
			   break;

       		   case 3:
				 R = r;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthWest") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c-1;
			   break;

       		   case 2:
				 R = r-1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 4:
				 R = r+1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"South") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c;
			   break;

       		   case 2:
				 R = r;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c;
			   break;

       		   case 4:
				 R = r;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthEast") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"Central") )
    {
      R = r;
      C = c;
    }
  else //ERROR HANDLING
    {
      printf("\n\n***********************************************************\n\n");
	  printf("\nERROR IN SUBROUTINE IDENTIFYCELL_ASSIGNAREA\n");
	  printf("\n\n***********************************************************\n\n");
	  exit(0);
    }

  Write_struct_Cellattrib(R,C,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000);
}

/*************************************************************************************************************************
NAME       : Calculate_SumOfDiffSq
DESCRIPTION: CALCULATES THE SUM OF SQUARE OF THE DIFFERENCE BETWEEN THE ACTUAL AREA AND CALCULATED AREA FOR THE 3X3 BLOCK
LIMITATIONS: NONE
**************************************************************************************************************************/
  double Calculate_SumOfDiffSq(int r, int c)
{
    double North_Area,South_Area,West_Area,East_Area,NorthWest_Area,NorthEast_Area,SouthWest_Area,SouthEast_Area;
    double North_VolFrac,South_VolFrac,West_VolFrac,East_VolFrac,NorthWest_VolFrac,NorthEast_VolFrac,SouthWest_VolFrac,SouthEast_VolFrac;
    double Square1, Square2, Square3, Square4, Square5, Square6, Square7, Square8;
    double Sum_Squares;

  North_Area    = Cellattrib[r+1][c].Area;
  North_VolFrac = Cellattrib[r+1][c].VolFrac*del_sq;

  South_Area    = Cellattrib[r-1][c].Area;
  South_VolFrac = Cellattrib[r-1][c].VolFrac*del_sq;

  West_Area     = Cellattrib[r][c-1].Area;
  West_VolFrac  = Cellattrib[r][c-1].VolFrac*del_sq;

  East_Area     = Cellattrib[r][c+1].Area;
  East_VolFrac  = Cellattrib[r][c+1].VolFrac*del_sq;

  NorthWest_Area     = Cellattrib[r+1][c-1].Area;
  NorthWest_VolFrac  = Cellattrib[r+1][c-1].VolFrac*del_sq;

  NorthEast_Area     = Cellattrib[r+1][c+1].Area;
  NorthEast_VolFrac  = Cellattrib[r+1][c+1].VolFrac*del_sq;

  SouthWest_Area     = Cellattrib[r-1][c-1].Area;
  SouthWest_VolFrac  = Cellattrib[r-1][c-1].VolFrac*del_sq;

  SouthEast_Area     = Cellattrib[r-1][c+1].Area;
  SouthEast_VolFrac  = Cellattrib[r-1][c+1].VolFrac*del_sq;

  Square1 = pow( North_Area      - North_VolFrac,2);
  Square2 = pow( South_Area      - South_VolFrac,2);
  Square3 = pow( West_Area       - West_VolFrac,2);
  Square4 = pow( East_Area       - East_VolFrac,2);
  Square5 = pow( NorthWest_Area  - NorthWest_VolFrac,2);
  Square6 = pow( SouthWest_Area  - SouthWest_VolFrac,2);
  Square7 = pow( NorthEast_Area  - NorthEast_VolFrac,2);
  Square8 = pow( SouthEast_Area  - SouthEast_VolFrac,2);

  Sum_Squares = Square1 + Square2 + Square3 + Square4 + Square5 + Square6 + Square7 + Square8;

  return Sum_Squares;
}

/*****************************************************************************************************************
NAME       : Find_Quad
DESCRIPTIOM: TAKES r AND c AS INPUT AND RETURNS THE QUADRANT BASED ON THE SIGNS OF Nx AND Ny
LIMITATIONS: None
*****************************************************************************************************************/
int Find_Quad(int r, int c)
{
      double Nx, Ny;
	int Quad;

    Nx =  Cellattrib[r][c].Nx;
    Ny =  Cellattrib[r][c].Ny;


    if (Nx > 0 && Ny >= 0)   //FIRST QUADRANT
      {
	Quad = 1;
      }
    else if (Nx <= 0 && Ny > 0) //SECOND QUADRANT
      {
	Quad = 2;
      }
    else if (Nx < 0 && Ny <= 0) //THIRD QUADRANT
      {
	Quad = 3;
      }
    else if (Nx >= 0 && Ny < 0) //FOURTH QUADRANT
      {
	Quad = 4;
      }
	else if (Nx==0 && Ny==0)
	{
		Quad=1;
	}
	else //ERROR HANDLING
    {
      printf("\n\n***********************************************************\n\n");
	  printf("\nERROR IN SUBROUTINE Find_Quad\n");
	  printf("\n\n***********************************************************\n\n");
	  printf("%d\t%d\t%e\t%e",r,c,Cellattrib[r][c].Nx,Cellattrib[r][c].Ny);
	    exit(0);
    }

    return Quad;
}

/*******************************************************************************************************************
NAME       : Calculate_Theta
DESCRIPTIOM: RETURNS THE ANGLE MADE BY THE LINE WITH THE NEGATIVE DIRECTION OF THE X-AXIS, IN AN ANTICLOCKWISE SENSE
LIMITATIONS:
********************************************************************************************************************/
void Calculate_Theta(int r, int c)
{
  /*LOCAL VARIABLES*/
    double Nx, Ny, Theta,Quad;
  /*****************/
  Nx =  Cellattrib[r][c].Nx;
  Ny =  Cellattrib[r][c].Ny;
  Quad = Find_Quad(r,c);

  //FLAG --  IS THERE A BUG? FIND IT (corrected the theta values it looks like they are the angle for normal, but they should be the angle with interface with horizontal axis - Palas )
  if( Ny == 0.0 && Nx > 0.0 )
    {
      //Theta = 0.0; //this should be pi/2 because Theta is the  positive acute angle of the INTERFACE(not normal) made with the horizontal axis
	    Theta = PI/2;
      Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta,-100000);
      return;
    }
  else if ( Nx == 0.0 && Ny > 0.0 )
    {
      Theta = PI/2.0;		// this is the second quad if you rotate the normal to quad 1, the interface will make pi/2 with x-axis

      Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000);
      return;
    }
  else if( Ny == 0.0 && Nx < 0.0 )
    {
      //Theta = PI;   //this should be zero because Theta is the  positive acute angle of the INTERFACE(not normal) made with the horizontal axis
	  Theta = PI/2;   // third quadrant, if you rotate the normal to quad 1 angle will remain the same
      Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000);
      return;
    }
  else if( (Nx == 0.0 && Ny < 0.0) )
    {
      //Theta = 3.0*PiBy2;		//
	    Theta = PI/2;		//this is the fourth quad therefore it is pi/2, if you rotate the normal to 1
      Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000);
      return;
    }
    else
    {
	    Theta  = (PiBy2) - fabs(atan(Ny/Nx));
	    if(Quad == 2 || Quad == 4)
	    {
	      Theta = PiBy2 - Theta;
	    }
    }

   /****/
     // Flag : I COMMENTED these because it will interfere with the rectangle cases for theta calculation, I took the non-rectangle cases in ELSE block above, the correction for QUADS 2nd and 4th are taken due consideration in rectangles case,
    // remember that theta has to be calculated in a way that if we rotate the cell to make the normal in the first quadrant, then what angle the interface makes with the x-axis that too positive acute angle.  --------Palas


  /*fabs is required only for the 2nd and 4th Quads. atan(Nx/Ny) is positive for the 1st and 3rd Quads
  and negative for 2nd and 4th quadrants. Theta thus calculated is always a positive acute angle*/
  //~ //Theta  = (PiBy2) - fabs(atan(Ny/Nx));   // Flag : I commented this

  //~ //FLAG - THIS IS NOT CLEAR - WHY IS THIS NECESSARY?????????????????
  //~ if(Quad == 2 || Quad == 4)
    //~ {
      //~ Theta = PiBy2 - Theta;
    //~ }
  //~ /********/

  Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000);
}

/*****************************************************************************************************************
NAME       : Rotate_Line
DESCRIPTIOM: ROTATES THE LINE CLOCKWISE / COUNTERCLOCKWISE
LIMITATIONS:
*****************************************************************************************************************/
void Rotate_Line(int r, int c, char sense[20])
{
    double Nx, Ny;
  int Quad;

  Nx   =  Cellattrib[r][c].Nx;
  Ny   =  Cellattrib[r][c].Ny;
  Quad =  Find_Quad(r,c);

  if(!strcmp(sense,"Clockwise"))
    {
		 if(Quad == 1)
		 {
		  Nx = Nx + Rotation_StepSize;
		  Ny = Ny - Rotation_StepSize;
		 }
		 else if(Quad == 2 )
		 {
		  Nx = Nx + Rotation_StepSize;
		  Ny = Ny + Rotation_StepSize;
		 }
		 else if(Quad == 3)
		 {
		  Nx = Nx - Rotation_StepSize;
		  Ny = Ny + Rotation_StepSize;
		 }
		 else if(Quad == 4)
		 {
		  Nx = Nx - Rotation_StepSize;
		  Ny = Ny - Rotation_StepSize;
		 }
    }
  else if(!strcmp(sense,"Counterclockwise"))
    {
		 if(Quad == 1)
		 {
		  Nx = Nx - Rotation_StepSize;
		  Ny = Ny + Rotation_StepSize;
		 }
		 else if(Quad == 2)
		 {
		  Nx = Nx - Rotation_StepSize;
		  Ny = Ny - Rotation_StepSize;
		 }
		 else if(Quad == 3)
		 {
  		  Nx = Nx + Rotation_StepSize;
		  Ny = Ny - Rotation_StepSize;
		 }
		 else if(Quad == 4)
		 {
  		  Nx = Nx + Rotation_StepSize;
		  Ny = Ny + Rotation_StepSize;
		 }
    }
 Write_struct_Cellattrib(r,c,-100000,-100000,Nx,Ny,-100000,"-100000",-100000,-100000);
}
/*******END************/

/*****************************************************************************************************************
NAME       : WriteToFile(int)
DESCRIPTIOM: Writes the (x1,y1) and (x2,y2) values for each interfacial cell into a file
LIMITATIONS:
*****************************************************************************************************************/
void WriteToFile(double Step)
{
  /*DECLARE LOCAL VARIABLES*/
  int r,c;
  int Quad;
    double Theta;
    double P;
  char   Shape[30];
    double X1, Y1, X2, Y2;
  FILE *ptrWrite;
  char LineFormat[] = "./Plots/PlotLine_%g.dat";
  char LineFileName[20];
  sprintf(LineFileName,LineFormat,Step);
  /*********************/

  /***********INITIALISE**************/
  ptrWrite =fopen(LineFileName,"w");
  /***********************************/

  for(r=1;r <= NoOfRows ; r++)
   {
     for(c=1;c <= NoOfCols ; c++)
       {		if(Cellattrib[r][c].VolFrac < LOWER_LIMIT && Cellattrib[r][c].VolFrac > UPPER_LIMIT)
	       { printf("%d\t%d\t%e\n",r,c,Cellattrib[r][c].VolFrac);}
		 if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
		   {
			 strcpy(Shape,Cellattrib[r][c].Shape);
			 Quad        =  Find_Quad(r,c);
			 P           =  Cellattrib[r][c].P;
			 Theta       =  Cellattrib[r][c].Theta;

			 if( !strcmp(Shape,"Rectangle") )
			   {
				 if(Quad == 1 || Quad == 3)
				   {
					  X1 = P + ((c-1)*del);
					  Y1 = (r-1)*del;

					  X2 = P + ((c-1)*del);
					  Y2 = (r)*del;
				   }
				 else if (Quad == 2 || Quad == 4)
				   {
					  X1 = ((c-1)*del);
					  Y1 = P + ((r-1)*del);

					  X2 = (c)*del;
					  Y2 = P + ((r-1)*del);
				   }
			   }
			 else if( !strcmp(Shape,"Triangle") )
			 {
				 if(Quad == 1)
				   {
					 X1 = (P/sin(Theta)) + ((c-1)*del);
					 Y1 =  (r-1)*del;

					 X2 = (c-1)*del;
					 Y2 = (P/cos(Theta)) + ((r-1)*del);
				   }
				 else if (Quad == 2)
				   {
					 X1 = del - (P/cos(Theta)) + ((c-1)*del);
					 Y1 = (r-1)*del;

					 X2 = (c)*del;
					 Y2 = (P/sin(Theta)) + ((r-1)*del);
				   }
				 else if (Quad == 3)
				   {
					 X1 = (c)*del - (P/sin(Theta));
					 Y1 = (r)*del;

					 X2 = (c)*del;
					 Y2 = (r)*del - (P/cos(Theta));
				   }
				 else if(Quad == 4)
				   {
					 X1 = (c-1)*del;
					 Y1 = (r)*del - (P/sin(Theta));

					 X2 = (c-1)*del + (P/cos(Theta));
					 Y2 = (r)*del;
				   }
				}
				else if (  !strcmp(Shape,"Trapezium") && Theta < PiBy4)  // for Theta < PI / 4.0
				{
					 if(Quad == 1)
					   {
						 X1 = (c-1)*del;
						 Y1 = (r-1)*del + (P/cos(Theta));

						 X2 = (c)*del;
						 Y2 = (P/cos(Theta)) - (del*tan(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c)*del - (P/cos(Theta));
						 Y1 =  (r-1)*del;

						 X2= (1+tan(Theta))*del - (P/cos(Theta)) + ((c-1)*del);
						 Y2= (r)*del;
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c-1)*del;
						 Y1 = (1+tan(Theta))*del - (P/cos(Theta)) + ((r-1)*del);

						 X2 = (c)*del;
						 Y2 = (r)*del - (P/cos(Theta));
					   }
					 else if(Quad == 4)
					   {
						 X1 = (P/cos(Theta)) - (del*tan(Theta)) + ((c-1)*del);
						 Y1 = (r-1)*del;

						 X2 = (P/cos(Theta)) + ((c-1)*del);
						 Y2 = (r)*del;
					   }
				   }
				 else if (  !strcmp(Shape,"Trapezium") && Theta > PiBy4)  // for Theta > PI / 4.0
				   {
					 if(Quad == 1)
					   {
						 X1 = (P/sin(Theta)) - (del/tan(Theta)) + ((c-1)*del);
						 Y1 = (r)*del;

						 X2 = (P/sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c-1)*del;
						 Y1 = (P/sin(Theta)) - (del/tan(Theta)) + ((r-1)*del);

						 X2 = (c)*del;
						 Y2 = (P/sin(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c)*del - (P/sin(Theta));
						 Y1 = (r)*del;

						 X2 = (1+(1/tan(Theta)))*del - (P/sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if(Quad == 4)
					   {
						 X1 = (c-1)*del;
						 Y1 = (r)*del - (P/sin(Theta));

						 X2 = (c)*del;
						 Y2 = (1+ (1/tan(Theta)))*del - (P/sin(Theta)) + ((r-1)*del);
					   }
				   }
				 else if( !strcmp(Shape,"Triangle_Complement")  )  //both for Theta <> PI/4
				   {
					 if(Quad == 1)
					   {
						 X1 = -(del/tan(Theta)) + (P/sin(Theta)) + ((c-1)*del);
						 Y1 = (r)*del;

						 X2 = (c)*del;
						 Y2 = (P/cos(Theta)) - (del*tan(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c-1)*del;
						 Y1 = (P*sin(Theta)) - (del/tan(Theta)) + (P*pow(cos(Theta),2)/sin(Theta)) + ((r-1)*del);

						 X2 = (1+tan(Theta))*del - (P*cos(Theta)) - (P*pow(sin(Theta),2)/cos(Theta)) + ((c-1)*del);
						 Y2 = (r)*del;
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c-1)*del;
						 Y1 = (1+tan(Theta))*del - (P*cos(Theta)) - (P*pow(sin(Theta),2)/cos(Theta)) + ((r-1)*del);

						 X2 = (1+(1/tan(Theta)))*del - (P*pow(cos(Theta),2)/sin(Theta)) - (P*sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if(Quad == 4)
					   {
						 X1 = (P/cos(Theta)) - (del*tan(Theta)) + ((c-1)*del);
						 Y1 = (r-1)*del;

						 X2 = (c)*del;
						 Y2 = (del/tan(Theta)) - (P/sin(Theta)) + (r)*del;
					   }
					}

				   fprintf(ptrWrite,"%e\t%e\n", X1,X2);
				   fprintf(ptrWrite,"%e\t%e\n", Y1,Y2);

				   

			   }
       }
  }

  fclose(ptrWrite);
}

/********************************************************************************************************************************
NAME       : AdvanceF_field
DESCRIPTION:
LIMITATIONS:
*********************************************************************************************************************************/
void AdvanceF_field(int r,int c, int Flag, int Togg)
{
	  /*******LOCAL VARIABLES*******/
	    double VolFrac;
	    double Zeta, Zeta1, Zeta2;
	    double NetXFlux, NetYFlux, NetXFlux_Corr, NetYFlux_Corr;
	    double X_Flux_Out, X_Flux_In, X_Flux_Out_Corr, delV_Corr;
	    double Y_Flux_Out, Y_Flux_In, Y_Flux_Out_Corr;
	    double X_Flux_Right,X_Flux_Left, X_Flux_Right_Corr, X_Flux_Left_Corr;
	    double Y_Flux_Up, Y_Flux_Down, Y_Flux_Up_Corr, Y_Flux_Down_Corr;
	    double Ustar_Left, Ustar_Right, Vstar_Up, Vstar_Down;
	  /*****************************/

	  	  if(Flag == 1)    //HORIZONTAL SWEEP
			{

			 //   if(X_Velocity[r][c+1] >= 0.0)     //RIGHT SIDE OF THE CELL EFFLUXES
				{
					 X_Flux_Right = X_Flux[r][c+1]/del_sq;  //fraction of volume out
					 X_Flux_Left  =  X_Flux[r][c]/del_sq;		//fraction of volume in


							Ustar_Right  = delTBydel*X_Velocity[r][c+1];

			  				Zeta1 = X_Flux_Right/Ustar_Right;
							Zeta2 = (Cellattrib[r][c].VolFrac - X_Flux_Right)/(1.0 - Ustar_Right);

							if( fabs(Zeta1 - 0.5) >= fabs(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( fabs(Zeta1 - 0.5) < fabs(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 0)   //HORIZONTAL PASS IS THE FIRST
								{
									Cellattrib[r][c].delV = 1.0 - delTBydel*(X_Velocity[r][c+1] - X_Velocity[r][c]);




									 NetXFlux     = X_Flux_Right - X_Flux_Left;

									VolFrac = (Cellattrib[r][c].VolFrac - NetXFlux)/Cellattrib[r][c].delV;
								}
								else if (Togg == 1) //HORIZONTAL PASS IS THE SECOND
								{

									 delV_Corr = Cellattrib[r][c].delV + (1-Cellattrib[r][c].delV)*Zeta;

									 X_Flux_Right_Corr = Cellattrib[r][c].delV*(X_Flux_Right - Ustar_Right) + delV_Corr*Ustar_Right;

									 NetXFlux_Corr     = X_Flux_Right_Corr - X_Flux_Left;

									VolFrac = (Cellattrib[r][c].VolFrac*Cellattrib[r][c].delV - NetXFlux_Corr);
								}





				}


			}

		  else if(Flag == 2)   //VERTICAL SWEEP
		{

				//if(Y_Velocity[r+1][c] >= 0.0)     //UPPER PART OF THE CELL EFFLUXES
				{
					Y_Flux_Up    =Y_Flux[r+1][c]/del_sq;
					Y_Flux_Down  = Y_Flux[r][c]/del_sq;
					Vstar_Up  = delTBydel*Y_Velocity[r+1][c];

			  				Zeta1 = Y_Flux_Up/Vstar_Up;
							Zeta2 = (Cellattrib[r][c].VolFrac - Y_Flux_Up)/(1.0 - Vstar_Up);

							if( fabs(Zeta1 - 0.5) >= fabs(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( fabs(Zeta1 - 0.5) < fabs(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 1)   //VERTICAL PASS IS THE FIRST
								{
									Cellattrib[r][c].delV = 1.0 - delTBydel*(Y_Velocity[r+1][c] - Y_Velocity[r][c]);
									NetYFlux     = Y_Flux_Up - Y_Flux_Down;

									VolFrac = (Cellattrib[r][c].VolFrac - NetYFlux)/Cellattrib[r][c].delV;
								}
								else if (Togg == 0) //VERTICAL PASS IS THE SECOND
								{
									delV_Corr = Cellattrib[r][c].delV + (1-Cellattrib[r][c].delV)*Zeta;
									 Y_Flux_Up_Corr = Cellattrib[r][c].delV*(Y_Flux_Up - Vstar_Up) + delV_Corr*Vstar_Up;
									NetYFlux_Corr     = Y_Flux_Up_Corr - Y_Flux_Down;

									VolFrac = (Cellattrib[r][c].VolFrac*Cellattrib[r][c].delV - NetYFlux_Corr);


								}




				}


		} //Vertical Sweep

		  Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000);

}




/**********************************************************************************************************
NAME: CalculateF_Flux
DESCRIPTION:
			1. If Flag = 1 , this subroutine does a horizontal pass and calculates all horizontal fluxes
			2. If Flag = 2 , this subroutine does a vertical pass and calculates all vertical fluxes
LIMITATIONS:
**********************************************************************************************************/
void CalculateF_Flux(int flag)
{
  /*******LOCAL VARIABLES**********/
  int		  r,c, Quad;
    double Theta;
  char		  Shape[30];
    double x_0,y_0,x_del,y_del,y_XVeldelT,x_YVeldelT;
    double X_Vel, Y_Vel;
    double XVeldelT,YVeldelT;
  /********************************/

		//HORIZONTAL PASS
		if(flag == 1)
		{
		  for(r=1;r <= NoOfRows+1;r++)
		   {
			 for(c=1;c <= NoOfCols+1 ;c++)
			  {
				/************INITIALISE**********/
				 X_Vel = X_Velocity[r][c];
				 Y_Vel = Y_Velocity[r][c];
				 /******************************/

				 if(X_Vel > 0.0)   //THE CELL TO THE LEFT OF THE BOUNDARY DECIDES THE EFFLUX AND THAT CELL WILL FLUX FROM ITS RIGHT BOUNDARY
				  {
					//~ if(c == 0)
					//~ {
						//~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
						//~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
						//~ continue;
					//~ }

					if(Cellattrib[r][c-1].VolFrac > LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c-1] - INIT BLOCK
						Quad = Find_Quad(r,c-1);
						strcpy(Shape,Cellattrib[r][c-1].Shape);  //GET SHAPE FROM [r][c-1]

						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariables() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						}
						else if(Quad == 2)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						}
						else if(Quad == 3)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						}
						else if(Quad == 4)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						}
					}
					else if(Cellattrib[r][c-1].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c-1].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = XVeldelT*del; //4-6-15

					}
				  }
				else if (X_Vel < 0.0)   //THE CELL TO THE RIGHT OF THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LEFT BOUNDARY
				  {

					//~ if(c == NoOfCols)
					//~ {
						  //~ if(r==1 && c==3)
						//~ printf("haha");
					  //~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
					  //~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = fabs(X_Vel);
					Y_Vel = fabs(Y_Vel);

					if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c] - INIT BLOCK
						Quad = Find_Quad(r,c);
						strcpy(Shape,Cellattrib[r][c].Shape);  //GET SHAPE FROM [r][c]
						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariables() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 					X_Flux[r][c] = -CalculateX_Flux("Left",  Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
							//if(r == 30 & c == 5)
							//	printf("\nVelocity Negative, Quad 2 %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
						    X_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
								//if(r == 15 & c == 38)
								//printf("\nVelocity Negative Quad is 3rd %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
					}
					else if(Cellattrib[r][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = -XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = -XVeldelT*del;   // corrected on 4-6-15


					}
				  }
				else //NO FLUX
				  {
					X_Flux[r][c] = 0.0;
				  }



			}
		   }
		}
	  //VERTICAL PASS
		else if(flag == 2)
		{
		  for(c=1;c <= NoOfCols;c++)
		  {
			  for(r=1;r <= NoOfRows;r++)
			  {
				X_Vel = X_Velocity[r][c];
				Y_Vel = Y_Velocity[r][c];

		  		if(Y_Vel > 0.0)   //THE CELL BELOW THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS UPPER BOUNDARY
				  {
					//~ if(r == 0)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					if(Cellattrib[r-1][c].VolFrac > LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r-1][c] - INIT
						Quad = Find_Quad(r-1,c);
						strcpy(Shape, Cellattrib[r-1][c].Shape);  //GET SHAPE FROM [r - 1][c]
						/**************************************/

						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
	  					    Y_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 4)
						 {	
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);

						 }
					}
					else if (Cellattrib[r-1][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if (Cellattrib[r-1][c].VolFrac >= UPPER_LIMIT)  //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = YVeldelT*del/delTdel;//
						Y_Flux[r][c] = YVeldelT*del;  // 4-6-15
					}

				  }
				else if (Y_Vel < 0.0)   //THE CELL ABOVE THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LOWER BOUNDARY
				  {
					//~ if(r == NoOfRows)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = fabs(X_Vel);
					Y_Vel = fabs(Y_Vel);

					if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r][c] - INIT
						Quad = Find_Quad(r,c);
						strcpy(Shape, Cellattrib[r][c].Shape);  //GET SHAPE FROM [r][c]
						/**************************************/

						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 				    Y_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = -CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
						    Y_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT);
						 }
					}
					else if(Cellattrib[r][c].VolFrac <= LOWER_LIMIT)  //NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c].VolFrac >= UPPER_LIMIT)   //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = -YVeldelT*del/delTdel;
						Y_Flux[r][c] = -YVeldelT*del; //4-6-15
					}
				  }
				else //NO FLUX
				  {
						Y_Flux[r][c] = 0.0;
				  }
			  }
		  }
		}
}

/******************************************************************************
NAME:		 CalculateX_Flux
DESCRIPTION: Calculates the fluxes for the vertical walls
LIMITATIONS:
******************************************************************************/
  double CalculateX_Flux(char Wall[20], char Shape[20],   double Theta,   double XVeldelT,   double YVeldelT,   double x_0,   double y_0,   double x_del,   double y_del,   double x_YVeldelT,   double y_XVeldelT)
{
	/***LOCAL VARIABLES***/
	  double Area_Flux, temp;
	  double tanT;
	/********************/

	/*****INITIALISE***/
	Area_Flux = 0.0;
	temp      = 0.0;
	tanT      = tan(Theta);
	/******************/

	  if(!strcmp(Wall,"Left"))      //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
	  {

		    if(!strcmp(Shape,"Rectangle"))
			{
				 if( (XVeldelT) >= x_0 )   //ENTIRE RECTANGLE LEAVES
				   {
					 Area_Flux = x_0*del;
				   }
				 else                      //A TRAPEZIUM LEAVES
				   {
					 Area_Flux = (XVeldelT*del);
				   }
			}
		else  if(!strcmp(Shape,"Triangle"))
			{
				 if( (XVeldelT) >= x_0 )   //ENTIRE TRIANGLE LEAVES
				   {
					 Area_Flux = 0.5*x_0*y_0;
				   }
				 else                      //A TRAPEZIUM LEAVES
				   {
					 Area_Flux = ( y_0 + y_XVeldelT )*(XVeldelT*0.5);
				   }
			}
		  else if(!strcmp(Shape,"Trapezium") && Theta < PiBy4)
			{
					 Area_Flux = ( y_0 + y_XVeldelT)*(XVeldelT*0.5);  //A TRAPEZIUM LEAVES
			}
		  else  if(!strcmp(Shape,"Trapezium") && Theta > PiBy4)
			{
				 if(XVeldelT <= x_del)   //A RECTANGLE LEAVES
				   {
					 Area_Flux = XVeldelT*del;



				   }
				 else if( (XVeldelT > x_del) && (XVeldelT < x_0)  )             //A 5-SIDED FIGURE LEAVES
				   {
					 Area_Flux = ( (x_del + x_0)*delBy2 ) - ( 0.5*(x_0 - XVeldelT)*y_XVeldelT );   //TRAPEZIUM - TRIANGLE
				   }
				 else if( XVeldelT >= x_0 )    //ENTIRE TRAPEZIUM LEAVES
					{
					  Area_Flux = (x_del + x_0)*delBy2;

					}
			}
		  else  if(!strcmp(Shape,"Triangle_Complement"))
			{
				 if(XVeldelT <= x_del)        // A RECTANGLE LEAVES
				   {
					 Area_Flux = del*XVeldelT;
				   }
				 else if(XVeldelT > x_del)   //A 5-sided figure leaves
				   {
					 Area_Flux = (x_del*del) + ( (del + y_XVeldelT)*(XVeldelT - x_del)*0.5 );  //RECTANGLE + TRAPEZIUM
				   }
			}
	  }
	  else if(!strcmp(Wall,"Right"))   //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
	  {
		  if(!strcmp(Shape,"Rectangle"))
			{
				if( (del - XVeldelT) >= x_0)  // NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
				else if((del - XVeldelT) < x_0)   // A RECTANGLE LEAVES
				{
					Area_Flux = del*(x_0 - del + XVeldelT );
				}
			}
		  else if(!strcmp(Shape,"Triangle"))
			{
				if( (del - XVeldelT) >= x_0)  // NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
				else if((del - XVeldelT) < x_0)   // A TRIANGLE LEAVES
				{
					Area_Flux = 0.5*pow( (x_0 - del + XVeldelT), 2 )*tanT;
				}
			}
		  else if(!strcmp(Shape,"Trapezium") && Theta < PiBy4)
			{
					Area_Flux = ( y_0 - ((del - XVeldelT)*tanT) + y_del )*XVeldelT*0.5;  //A TRAPEZIUM LEAVES
			}
		  else  if(!strcmp(Shape,"Trapezium") && Theta > PiBy4)
			{
				if( (del - XVeldelT) < x_del )   //A TRAPEZIUM LEAVES
				{
					Area_Flux = (x_del - (2.0*(del - XVeldelT)) + x_0)*delBy2;
				}
				else if(  (del - XVeldelT) >= x_del && (del - XVeldelT) < x_0)    //A TRIANGLE LEAVES
				{
					Area_Flux = 0.5*pow( (y_0 - (del - XVeldelT)*tanT) ,2)/tanT;
				}
				else if( (del - XVeldelT) >= x_0)  //NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
			}
		  else  if(!strcmp(Shape,"Triangle_Complement"))
			{
				if( (del - XVeldelT) < x_del)   //A 5-SIDED FIGURE LEAVES
				{
					Area_Flux = (del*(x_del - del + XVeldelT)) + ((del + y_del)*(del - x_del)*0.5);  //RECTANGLE + TRAPEZIUM
				}
				else if((del - XVeldelT) >= x_del)   //A TRAPEZIUM LEAVES
				{
					Area_Flux = ( y_del + y_0 - ((del - XVeldelT)*tanT) )*XVeldelT*0.5;
				}
			}
	  }
	 //temp = Area_Flux/delTdel;

	  //return temp;
	  return Area_Flux;
}

/******************************************************************************
NAME:	     CalculateY_Flux
DESCRIPTION: Calculates the fluxes for the horizontal walls
LIMITATIONS:
******************************************************************************/
  double CalculateY_Flux(char Wall[20], char Shape[20],   double Theta,   double XVeldelT,   double YVeldelT,   double x_0,   double y_0,   double x_del,   double y_del,   double x_YVeldelT,   double y_XVeldelT)
{
	/***LOCAL VARIABLES***/
	  double Area_Flux, temp;
	  double tanT;
	/********************/

	/*****INITIALISE***/
	Area_Flux = 0.0;
	temp      = 0.0;
	tanT      = tan(Theta);
	/******************/

	  	if(!strcmp(Wall,"Lower"))    //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
		{	if(!strcmp(Shape,"Rectangle"))
				{
					//A rectangle always leaves
					Area_Flux = (x_0*YVeldelT);

				}
			else if(!strcmp(Shape,"Triangle"))
				{
					if(YVeldelT >= y_0)  //ENTIRE TRIANGLE LEAVES
					{
						Area_Flux = 0.5*x_0*y_0;
					}
					else if(YVeldelT < y_0)  //A PARALLELOGRAM LEAVES
					{
						Area_Flux = (x_0 + x_YVeldelT)*YVeldelT*0.5;
					}
				}
			  else if(!strcmp(Shape,"Trapezium") && Theta < PiBy4)
				{
					if(YVeldelT >= y_0)   //ENTIRE TRAPEZIUM LEAVES
					{
						Area_Flux = (y_0 + y_del)*delBy2;
					}
					else if(YVeldelT > y_del && YVeldelT < y_0)  //A 5-SIDED FIGURE LEAVES
					{
						Area_Flux = (delBy2*(y_0 + y_del)) - ( (y_0 - YVeldelT)*x_YVeldelT*0.5 );  //AREA OF TRAPEZIUM - AREA OF TRIANGLE
					}
					else if(YVeldelT <= y_del) //A RECTANGLE LEAVES
					{
						Area_Flux = del*YVeldelT;
					}
				}
			  else  if(!strcmp(Shape,"Trapezium") && Theta > PiBy4)
				{
						Area_Flux = (x_0 + x_YVeldelT)*YVeldelT*0.5;
				}
			  else  if(!strcmp(Shape,"Triangle_Complement"))
				{
					if(YVeldelT > y_del)   //A 5-SIDED FIGURE LEAVES
					{
						Area_Flux = (del*y_del) + ( (del + x_YVeldelT)*(YVeldelT - y_del)*0.5 );  //AREA OF RECTANGLE + AREA OF TRAPEZIUM
					}
					else if(YVeldelT <= y_del)  //A RECTANGLE LEAVES
					{
						Area_Flux = del*YVeldelT;
					}
				}
		}
		else if(!strcmp(Wall,"Upper"))   //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
		{	if(!strcmp(Shape,"Rectangle"))
				{
					//A rectangle always leaves
					Area_Flux = (x_0*YVeldelT);

				}
			else if(!strcmp(Shape,"Triangle"))
				{
					if(YVeldelT <= (del - y_0))   //NOTHING LEAVES
					{
						Area_Flux = 0.0;
					}
					else if(YVeldelT > (del - y_0))  //A TRIANGLE LEAVES
					{
						Area_Flux = 0.5*pow(y_0 - del + YVeldelT,2)/tanT;
					}
				}
			  else if(!strcmp(Shape,"Trapezium") && Theta < PiBy4)
				{
					if(YVeldelT <= (del - y_0))  // NOTHING LEAVES
					{
						Area_Flux = 0.0;
					}
					else if(YVeldelT > (del - y_0) && YVeldelT <= (del - y_del))  //A TRIANGLE LEAVES
					{
						Area_Flux = 0.5*pow(y_0 - del + YVeldelT,2)/tanT;
					}
					else if(YVeldelT > (del - y_del)) //A TRAPEZIUM LEAVES
					{
						Area_Flux = ((y_0 + y_del)*delBy2) - ((del - YVeldelT)*del);
					}
				}
			  else  if(!strcmp(Shape,"Trapezium") && Theta > PI/4.0)  //A TRAPEZIUM LEAVES
				{
						Area_Flux = ( x_del + ((y_0 - del + YVeldelT)/tanT) )*YVeldelT*0.5;
				}
			  else  if(!strcmp(Shape,"Triangle_Complement"))
				{
					if(YVeldelT <= (del - y_del))    //A TAPEZIUM LEAVES
					{
						Area_Flux = ( x_del + ((y_0 - del + YVeldelT)/tanT) )*YVeldelT*0.5;
					}
					else if(YVeldelT > (del - y_del))  //A 5-SIDED FIGURE LEAVES
					{
						Area_Flux = ( (y_del - del + YVeldelT)*del) + ((x_del + del)*(del - y_del)*0.5);  //AREA OF RECTANGLE + AREA OF TRAPEZIUM
					}
				}
		}

		//temp = Area_Flux/delTdel;
		//return temp;
		return Area_Flux;
}

/******************************************************************************
NAME:		 AssignVal_FluxVariables
DESCRIPTION: Calculates the interface orientation details, for the cell whose identifier is passed.
			 The calculated values are written into the addresses passed.
LIMITATIONS:
******************************************************************************/
void AssignVal_FluxVariables(int r, int c,   double *ptr2,   double *ptr3,   double *ptr4,   double *ptr5,   double *ptr6,   double *ptr7,   double *ptr8,   double *ptr9,   double *ptr10,   double *ptr11,   double *ptr12)
{
  /*****LOOKUP FOR ARGUMENTS****
     double *ptr2  - Theta
     double *ptr3  - XVeldelT
     double *ptr4  - YVeldelT
     double *ptr5  - x_0
     double *ptr6  - y_0
     double *ptr7  - x_del
     double *ptr8  - y_del
     double *ptr9  - x_YVeldelT
     double *ptr10 - y_XVeldelT
     double *ptr11 - XVel
     double *ptr12 - YVel
  /****************************/

  /*********DECLARATIONS***********/
    double P, XVel, YVel;
    double Theta;
    double sinT,cosT,tanT;
  /********************************/

  /*************INITIALIZE*************/
  P				= Cellattrib[r][c].P;
  XVel          = *ptr11;                   //DO NOT RETRIEVE VELOCITY FROM CELL WALL .RETRIEVE ONLY FROM THE PASSED ADDRESS
  YVel          = *ptr12;                   //DO NOT RETRIEVE VELOCITY FROM CELL. RETRIEVE ONLY FROM THE PASSED ADDRESS SINCE CELL WALL IDENTIFIER IS DIFFRERENT FROM THE IDENTIFIER PASSED
  Theta         = Cellattrib[r][c].Theta;
  sinT			= sin(Theta);
  cosT			= cos(Theta);
  tanT			= tan(Theta);
  /************************************/


  /*********WRITING TO THE ADDRESSES PASSED************/
  *ptr2			= Cellattrib[r][c].Theta;
  *ptr3			= XVel*delT;
  *ptr4			= YVel*delT;
  *ptr5			= P/sinT;
  *ptr6			= P/cosT;
  *ptr7			= (*ptr6 - del)/tanT;
  *ptr8			= (*ptr6 - (del*tanT));
  *ptr9			= (*ptr6 - *ptr4)/tanT;
  *ptr10		= (*ptr6) - ((*ptr3)*tanT);
  /****************************************************/
  //FLAG: the following block is written to add the rectangle case in flux calculation

  if (!strcmp(Cellattrib[r][c].Shape,"Rectangle"))
  {
			 *ptr2			= Cellattrib[r][c].Theta;
			  *ptr5			= P;
			 // *ptr6			= P/cosT;
			  *ptr7			= P;
			 // *ptr8			= (*ptr6 - (del*tanT));
			  *ptr9			= P;
			  //*ptr10		= (*ptr6) - ((*ptr3)*tanT);


  }
}

/**************************************************************************
NAME       : BookKeeping
DESCRIPTION:
LIMITATIONS:
***************************************************************************/
void BookKeeping(int Flag)
{
    /*********DECLARATIONS***********/
	int r, c;
	  double VolFrac;
	/*******************************/

	//ASSIGN 0 AND 1 TO CELLS WHICH ARE OUT OF BOUND
	for(r=1;r <= NoOfRows; r++)
	{
	   for(c=1;c <= NoOfCols; c++)
		{
			if(Cellattrib[r][c].VolFrac < LOWER_LIMIT)
			  {
				//printf("\nCame here\n");
				VolFrac = 0.0;
				Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000);
			  }
			if(Cellattrib[r][c].VolFrac > UPPER_LIMIT)
			  {
				VolFrac = 1.0;
				Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000);
			  }
	   }
	}
	/***/
}



/*********************************************************************************************************************************************************************************************************************************************
THIS IS A PRESSURE-VELOCITY NAVIER STOKES SOLVER FOR TWO PHASE FLOW
AUTHOR: PALAS KUMAR FARSOIYA

*******************
DESCRIPTION:
1. Finite Volume Approach
2. Pressure-Velocity Formulation
3. Explicit for Velocity and Implicit for pressure
4. First order upwind for advection term

/****************
REFERENCES:
1. Direct numerical simulations of gas-liquid multiphase flows
2. A Navier-Stokes solver for single and two phase flow, Kim Motooyoshi Kalland, ( Masters Thesis), University of Oslo
3. A Front tracking/Finite-Volume Navier-Stokes Solver for Direct Numerical Simulations of Multiphase flows, Gretar Tryggvason ( Tutorial document at http://www3.nd.edu/~gtryggva/MultiphaseDNS/DNS-Solver.pdf )
4. Numerical Heat transfer and Fluid Flow, Suhas V Patankar
************************************************************************************************************************************************************************************************************************************************/


void Advance_Velocity_Field()

{

	int r,c;


	{

			

			// Let this be below, it will be needed to test lid driven cavity

			for (r=0; r<=NoOfRows+1; r++)
			{
				for (c=0; c<=NoOfCols+1; c++)
				{
					X_Velocity_old[r][c] = X_Velocity[r][c];
					Y_Velocity_old[r][c]	=	Y_Velocity[r][c];
				}
			}		
				
					//Calculate_Surface_Tension_MAC();
					if(Sigma>1e-7)
					{
					Calculate_Surface_Tension_Height_Function_DAC();
						
					}
					
					// X momentum equation
					Calculate_X_Velocity_star();
			

					// Y momentum equation
					Calculate_Y_Velocity_star();
				


				//printf("delT = %lf",delT);
			switch(Linear_Solver)
			{
				case 'S' :
					Poissons_Solver_SOR();
					break;
				case 'M' :
					Poissons_Solver_Multigrid();
					break;
			}
				//exit(0);
	
				X_Velocity_Correction();
				Y_Velocity_Correction();

		//~ // Let this be below, it will be needed to test lid driven cavity
		//~ // Calculate residue
		U_residue = 0;			//reset residue to zero only here, dont move up or down the loop
		V_residue =0;
		for (r=0; r<=NoOfRows+1; r++)
		{
			for (c=0; c<=NoOfCols+1; c++)
			{
				U_residue =  max( U_residue , fabs( X_Velocity[r][c] - X_Velocity_old[r][c] ) );
				V_residue =  max( V_residue , fabs( Y_Velocity[r][c] - Y_Velocity_old[r][c] ) );
			}
		}

	//	printf("\t%e\t%e",U_residue,V_residue);

			//printf("%d Time Step \n",StepT);

				//StepT++;
	}



}





/********************
Name: Ad_Flux_Calc
Description: Switch to the advection scheme specified in Global_Variables.h
**************************/
  double Ad_Flux_Calc(double Vel_left,   double Vel_right,   double phi_UUU,  double phi_UU, double phi_U,   double phi_D,  double phi_DD , double phi_DDD)
{
	/*********Conventional Schemes Variables********/
	 double temp,Vel_Wall,phi_Wall_p,phi_Wall_n;
	double w1,w2,w3;
	
	/******************References for WENO***********************
	Essentially Non-Oscillatory and Weighted Essentially
	Non-Oscillatory Schemes for Hyperbolic
	Conservation Laws
	Author: Chi-Wang Shu
	Affiliation: Brown University
	/*****************************************************************/
	
	/********WENO variables*********/
	double phi_0,phi_1,phi_2;
	double w_0,w_1,w_2;
	double w_0_tilde,w_1_tilde,w_2_tilde;
	double beta_0,beta_1,beta_2;
	double epsilon = 1e-7;
	/*************************************/
	
	Vel_Wall = (Vel_right+Vel_left)/2;	// face-centered velocity

	 switch(Advection_Scheme ) 
	{

		case 1  :
      
			w1=0; w2=1; w3=0;
   
			break;
	
		case 2  :
	   
			w1=0.5 ; w2=0.5; w3=0;   
			break; 
  
		case 3 :
			
			w1=0; w2=1.5; w3= -0.5;
			break;
		
		case 4 :
			
			w1=0.375; w2=0.75; w3= -0.125;
			break;
		
		case 5 :
			
		{	/*****************Left stencils biasing********************************************/
			phi_0 = 1.0/3*phi_UUU -7.0/6*phi_UU + 11.0/6*phi_U ;
			phi_1 = -1.0/6*phi_UU +5.0/6*phi_U + 1.0/3*phi_D ;
			phi_2 = 1.0/3*phi_U +5.0/6*phi_D - 1.0/6*phi_DD ;
			
			beta_0 = 13.0/12*pow((phi_UUU-2*phi_UU+phi_U),2) + 1.0/4*pow((phi_UUU - 4*phi_UU +3*phi_U),2);
			
			beta_1 = 13.0/12*pow((phi_UU-2*phi_U+phi_D),2) + 1.0/4*pow((phi_UU - phi_D),2);
			
			beta_2 = 13.0/12*pow((phi_U-2*phi_D+phi_DD),2) + 1.0/4*pow((3*phi_U- 4*phi_D +phi_DD),2);

			


			w_0_tilde = 0.1/pow((epsilon+beta_0),2);
			w_1_tilde = 0.6/pow((epsilon+beta_1),2);
			w_2_tilde = 0.3/pow((epsilon+beta_2),2);


			w_0 = w_0_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_1 = w_1_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_2 = w_2_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			
			phi_Wall_n = w_0*phi_0 + w_1*phi_1 + w_2*phi_2;
			/****************************************************************************************/
			
			/*****************Right stencils biasing********************************************/
			phi_0 = 1.0/3*phi_DDD -7.0/6*phi_DD + 11.0/6*phi_D ;
			phi_1 = -1.0/6*phi_DD +5.0/6*phi_D + 1.0/3*phi_U ;
			phi_2 = 1.0/3*phi_D +5.0/6*phi_U -1.0/6*phi_UU ;
			
			beta_0 = 13.0/12*pow((phi_DDD-2*phi_DD+phi_D),2) + 1.0/4*pow((phi_DDD - 4*phi_DD +3*phi_D),2);
			
			beta_1 = 13.0/12*pow((phi_DD-2*phi_D+phi_U),2) + 1.0/4*pow((phi_DD - phi_U),2);
			
			beta_2 = 13.0/12*pow((phi_D-2*phi_U+phi_UU),2) + 1.0/4*pow((3*phi_D- 4*phi_U +phi_UU),2);

			


			w_0_tilde = 0.1/pow((epsilon+beta_0),2);
			w_1_tilde = 0.6/pow((epsilon+beta_1),2);
			w_2_tilde = 0.3/pow((epsilon+beta_2),2);


			w_0 = w_0_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_1 = w_1_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			w_2 = w_2_tilde/(w_0_tilde+w_1_tilde+w_2_tilde);
			
			phi_Wall_p = w_0*phi_0 + w_1*phi_1 + w_2*phi_2;
			/****************************************************************************************/
			temp = (max(Vel_Wall,0))*phi_Wall_n + (min(Vel_Wall,0))*phi_Wall_p;
			//printf("%lf\n",temp);
			return temp;
			
		}
		
		default : 
		
		printf("\n ERROR: Specify the advection scheme in global_Variables.h properly\n");
		exit(0);
	}
	
		
		phi_Wall_n = w1*phi_D + w2*phi_U + w3*phi_UU;
		phi_Wall_p = w1*phi_U + w2*phi_D + w3*phi_DD;
		temp = (max(Vel_Wall,0))*phi_Wall_n + (min(Vel_Wall,0))*phi_Wall_p;
		//printf("%lf\n",temp);
	return temp;
}

//~ /********************
//~ Name: Ad_FOU
//~ Description: Calculates the momentum fluxes due to advection on LHS or Bottom of the control volume or cell
//~ Scheme: centeral difference
//~ **************************/
  //~ double Ad_FOU(  double Vel_left,   double Vel_right,   double phi_left,   double phi_right)
//~ {
	  //~ double temp,Vel_Wall,phi_Wall;

	//~ Vel_Wall = (Vel_right+Vel_left)/2;
	//~ phi_Wall = (phi_left + phi_right)/2;
	//~ //temp = (fmax(Vel_Wall,0))*phi_left + (fmin(Vel_Wall,0))*phi_right;
	//~ temp = (Vel_Wall*phi_Wall);
	//~ return temp;
//~ }
/*******************
Name: Calculate_X_Velocity_star
Description: Calculates the temporary velocities without accounting pressure
Limitations:
*******************/
void Calculate_X_Velocity_star()
{
	int r,c;
	
		
	    
	int stage; // for TVD WENO
	  double mu_bottom, rho_r;
		//~ double  Ad_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2], Ad_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	  //~ double Diff_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2],Diff_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	//~ //double X_Vel_UU,X_Vel_U, X_Vel_D,X_Vel_DD;
	int i;
	double **Ad_Flux_Vertical_Faces,**Ad_Flux_Horizontal_Faces;
	double **Diff_Flux_Vertical_Faces,**Diff_Flux_Horizontal_Faces;
	
	Ad_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Ad_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Diff_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	Diff_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
	
			for (i=0; i<NoOfRows+2; i++)
			{
				Ad_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Ad_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}
	//Copying the updated values to starred variables, necessary for WENO TVD
	for(r=0;r<=NoOfRows+1;r++)
	{
		for(c=0;c<=NoOfCols+1;c++)
		{
			X_Velocity_star[r][c] = X_Velocity[r][c];
		}
	}

/***********************************************ADVECTION FLUXES***********************************************/
	if (Advection_term==1) //Switch "Advection term" from global_Variables.h don't touch here
	{	
		//Calculate Advection fluxes on the four faces of the cell
			// Boundary Control Volumes special treatment, linear extrapolation of super-ghost values
		for (stage=1;stage<=3;stage++)
		{
			for(r=1;r<=NoOfRows;r++)
			{
				for(c=2;c<=NoOfCols+1;c++)
				{
					if (c==2)
					{	
						
						
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity_star[r][c],X_Velocity_star[r][c-1], X_Velocity_star[r][c-1], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );	
					}
					else if (c==3)
					{
						
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-2], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );
					}
					else if(c==NoOfCols)
					{
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+1] );	
					}
					else if(c==NoOfCols+1)
					{
									
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c], X_Velocity_star[r][c]);	
					}
					else
					{	
						Ad_Flux_Vertical_Faces[r][c] = Ad_Flux_Calc(X_Velocity[r][c-1],	X_Velocity[r][c],X_Velocity_star[r][c-3], X_Velocity_star[r][c-2], X_Velocity_star[r][c-1], X_Velocity_star[r][c], 	X_Velocity_star[r][c+1], X_Velocity_star[r][c+2] );	
					}
				}
			}
			// Horizontal Faces
			for(r=1;r<=NoOfRows+1;r++)
			{
				for(c=2;c<=NoOfCols;c++)
				{
					if (r==1)
					{	
						
						
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], 	X_Velocity_star[r-1][c],	X_Velocity_star[r-1][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
					else if (r==2)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-2][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
					else if (r==NoOfRows)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+1][c]);
					}
						
					else if(r==NoOfRows+1)
					{
						
						
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r][c], X_Velocity_star[r][c]);
					}
					else
					{	
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r][c-1], 	Y_Velocity[r][c], X_Velocity_star[r-3][c], X_Velocity_star[r-2][c],	X_Velocity_star[r-1][c], 	X_Velocity_star[r][c], X_Velocity_star[r+1][c], X_Velocity_star[r+2][c]);
					}
				}
			}
			//TVD stages
			if (stage==1)
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=2;c<=NoOfCols;c++)
					{
						X_Velocity_star[r][c] = X_Velocity[r][c]	-	delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] );	
						//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
			}
			else if (stage==2 && Advection_Scheme==5 )
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=2;c<=NoOfCols;c++)
					{
						X_Velocity_star[r][c] = 0.75*X_Velocity[r][c]	+	0.25*(X_Velocity_star[r][c] - delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
			}
			
			else if (stage==3 && Advection_Scheme==5 )
			{
				for(r=1;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						X_Velocity_star[r][c] = 1.0/3*X_Velocity[r][c]	+	2.0/3*(X_Velocity_star[r][c] - delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						//printf("\n%e", (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] )/del);
					}
				}
				
			}
			
			else	// if not WENO don't follow stages exit the loop
			{
				break;
			}
		
		
		}
	}
				//exit(0);
	// Net advection Flux
	//Net_Ad_Flux 		=	((Ad_Flux_Vertical_Faces[r][c] - Ad_Flux_left) + (Ad_Flux_top - Ad_Flux_bottom))/del ;


	//~ // Calculate viscosities at unknown points, top and bottom face centres of the cell

	//~ mu_top		=		0.25*( mu[r+1][c] 	+ 	mu[r+1][c-1] 	+ 	mu[r][c-1] 	+ 	mu[r][c] );
	//~ mu_bottom	=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
	//~ rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);

	//~ //Calculate the Diffusion fluxes on the four faces of wall

	//~ Diff_Flux_right		=	2*(	mu[r][c]*( X_Velocity[r][c+1]	-	X_Velocity[r][c] )/del	);
	//~ Diff_Flux_left		=	2*(	mu[r][c-1]*( X_Velocity[r][c]	-	X_Velocity[r][c-1] )/del	);
	//~ Diff_Flux_top		=	(	mu_top*( ( X_Velocity[r+1][c]	-	X_Velocity[r][c] ) 	+ ( Y_Velocity[r+1][c]	-	Y_Velocity[r+1][c-1])	) /del	);
	//~ Diff_Flux_bottom	=	(	mu_bottom*( ( X_Velocity[r][c]	-	X_Velocity[r-1][c] ) 	+ ( Y_Velocity[r][c]	-	Y_Velocity[r][c-1])	) /del	);

	//~ //Net Diffusion Flux
	//~ //
	//~ Net_Diff_Flux	=	(1.0/rho_r)*(1.0/del)*((Diff_Flux_right	-	Diff_Flux_left)	+	(Diff_Flux_top	-	Diff_Flux_bottom));
	//~ //printf("\n%d\t%d\t%lf",r,c,Net_Diff_Flux);
	//~ //Calulate the temp velocity
	if (Diffusion_term==1)	//Switch of diffusion term
	{
		//vertical faces
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols+1;c++)
			{
				Diff_Flux_Vertical_Faces[r][c]		=	2*(	mu[r][c-1]*( X_Velocity[r][c]	-	X_Velocity[r][c-1] )/del	);
			}
		}
		//Horizontal Faces
		for(r=1;r<=NoOfRows+1;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				mu_bottom	=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
				Diff_Flux_Horizontal_Faces[r][c]	=	(	mu_bottom*( ( X_Velocity[r][c]	-	X_Velocity[r-1][c] ) 	+ ( Y_Velocity[r][c]	-	Y_Velocity[r][c-1])	) /del	);
			}
		}
		
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
				
				X_Velocity_star[r][c] = X_Velocity_star[r][c]	+	(1.0/rho_r)*delTBydel* (Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] );	
				//printf("\n%e", (1.0/del)*(Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] ));
				//printf("\n%e", X_Velocity_star[r][c]);
				
			}
		}
		//~ for(r=Refine+1;r<=NoOfRows;r++)
		//~ {
			//~ for(c=2;c<=NoOfCols;c++)
			//~ {	
				//~ rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
				
				//~ X_Velocity_star[r][c] = X_Velocity_star[r][c]	+	(1.0/rho_r)*delTBydel* (Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] );	
				//~ //printf("\n%e", (1.0/del)*(Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] ));
				//~ //printf("\n%e", X_Velocity_star[r][c]);
				
			//~ }
		//~ }
	}
	//exit(0);
	if(SourceU!=0)	// if there is a non-zero source term
	{
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				X_Velocity_star[r][c] = X_Velocity_star[r][c] + delT*SourceU;
			}
		}
		
		
	}

	if(Sigma!=0)	// if there is a non-zero surface tension
	{
		for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{	
				rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
				X_Velocity_star[r][c] = X_Velocity_star[r][c] + (1.0/rho_r)*delT*ST_Fx[r][c];
			}
		}
		
	}
	
	
	
			freeArray(Ad_Flux_Vertical_Faces);
			freeArray(Ad_Flux_Horizontal_Faces);	
			freeArray(Diff_Flux_Vertical_Faces);	
			freeArray(Diff_Flux_Horizontal_Faces);
	//X_Velocity_star[r][c]	=	X_Velocity[r][c]	+	delT* (	 );		//no body force in X-direction

}


/*******************
Name: Calculate_Y_Velocity_star
Description: Calculates the temporary velocities without accounting pressure
Limitations:
*******************/
void Calculate_Y_Velocity_star()
{	
			int r,c;
			int stage; // for TVD WENO
			double mu_left, rho_r;
		//~ double  Ad_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2], Ad_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
	  //~ double Diff_Flux_Vertical_Faces[NoOfRows+2][NoOfCols+2],Diff_Flux_Horizontal_Faces[NoOfRows+2][NoOfCols+2];
		int i;
		double **Ad_Flux_Vertical_Faces,**Ad_Flux_Horizontal_Faces;
		double **Diff_Flux_Vertical_Faces,**Diff_Flux_Horizontal_Faces;
	
		Ad_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Ad_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Diff_Flux_Vertical_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
		Diff_Flux_Horizontal_Faces = (double **) calloc(NoOfRows+2,sizeof(double *));
			
			for (i=0; i<NoOfRows+2; i++)
			{
				Ad_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Ad_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Vertical_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Diff_Flux_Horizontal_Faces[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}
	
	//Copying the updated values to starred variables, necessary for WENO TVD
	for(r=0;r<=NoOfRows+1;r++)
	{
		for(c=0;c<=NoOfCols+1;c++)
		{
			Y_Velocity_star[r][c] = Y_Velocity[r][c];
		}
	}
/***********************************************ADVECTION FLUXES***********************************************/
	if (Advection_term==1) //Switch "Advection term" from global_Variables.h don't touch here
	{	
		for (stage=1;stage<=3;stage++)
		{
			//Vertical faces
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols+1;c++)
				{
					if (c==1)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-1],Y_Velocity_star[r][c-1], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1], Y_Velocity_star[r][c+2]);
					}
					else if (c==2)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1],Y_Velocity_star[r][c+2]);
					}
					else if(c==NoOfCols)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1], Y_Velocity_star[r][c+1]);
					}
					else if(c==NoOfCols+1)
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c], Y_Velocity_star[r][c]);
					}	
					else
					{
						Ad_Flux_Vertical_Faces[r][c] 		=	Ad_Flux_Calc(X_Velocity[r-1][c], 	X_Velocity[r][c], 	Y_Velocity_star[r][c-3], Y_Velocity_star[r][c-2], Y_Velocity_star[r][c-1], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c+1],Y_Velocity_star[r][c+2]);
					}
				}
			}
			
			// Horizontal Faces
			for(r=2;r<=NoOfRows+1;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					if (r==2)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					else if(r==3)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-2][c], Y_Velocity_star[r-2][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					else if(r==NoOfRows)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+1][c]);
					}
					else if (r==NoOfRows+1)
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-1][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r][c],Y_Velocity_star[r][c]);
					}
					else
					{
						Ad_Flux_Horizontal_Faces[r][c] = Ad_Flux_Calc(Y_Velocity[r-1][c], 	Y_Velocity[r][c], 	Y_Velocity_star[r-3][c], Y_Velocity_star[r-2][c], Y_Velocity_star[r-1][c], 	Y_Velocity_star[r][c], Y_Velocity_star[r+1][c],Y_Velocity_star[r+2][c]);
					}
					
				}
			}
			//TVD stages
			if (stage==1)
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						Y_Velocity_star[r][c] = Y_Velocity[r][c]	-	delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] );	
						//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
					}
				}
			}
			else if (stage==2 && Advection_Scheme==5 )
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						Y_Velocity_star[r][c] = 0.75*Y_Velocity[r][c] + 0.25*(Y_Velocity_star[r][c]	-	delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
					}
				}
			}
			else if (stage==3 && Advection_Scheme==5 )
			{
				for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
						Y_Velocity_star[r][c] = 1.0/3*Y_Velocity[r][c] + 2.0/3*(Y_Velocity_star[r][c]	-	delTBydel* (Ad_Flux_Vertical_Faces[r][c+1] - Ad_Flux_Vertical_Faces[r][c] + Ad_Flux_Horizontal_Faces[r+1][c] - Ad_Flux_Horizontal_Faces[r][c] ));	
						//printf("\n%d\t%d\t%e",r,c,Y_Velocity_star[r][c]);
					}
				}
				
			}
			
		}
	}
/*************************Diffusion Fluxes****************************/
		if (Diffusion_term==1)	//Switch "Diffusion term" from global_Variables.h don't touch here
		{	
			//Vertical faces
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols+1;c++)
				{	mu_left		=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
					Diff_Flux_Vertical_Faces[r][c] = (	mu_left*( ( X_Velocity[r][c]	-	X_Velocity[r-1][c] ) 	+ ( Y_Velocity[r][c]	-	Y_Velocity[r][c-1])	) /del	);
					
				}
			}
			
			//Horizontal Faces
			for(r=2;r<=NoOfRows+1;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					Diff_Flux_Horizontal_Faces[r][c]		=	2.0*(	mu[r-1][c]*( Y_Velocity[r][c]	-	Y_Velocity[r-1][c] )/del	);
				}
			}
			
			for(r=2;r<=NoOfRows;r++)
				{
					for(c=1;c<=NoOfCols;c++)
					{
					rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c]	+	(1.0/rho_r)*delTBydel* (Diff_Flux_Vertical_Faces[r][c+1] - Diff_Flux_Vertical_Faces[r][c] + Diff_Flux_Horizontal_Faces[r+1][c] - Diff_Flux_Horizontal_Faces[r][c] );	
					//printf("\n%e",Y_Velocity_star[r][c]);
					//printf("\n%d\t%d\t%e",r,c,Diff_Flux_Horizontal_Faces[r][c]);
				}
			}
			
		}
		//exit(0)	;
		if (SourceV!=0)
		{
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c] + delT*SourceV;
					//printf("\n sourceV = %e",SourceV);
					//exit(0);
				}
			}
			
		}

		if (Sigma!=0)
		{
			for(r=2;r<=NoOfRows;r++)
			{
				for(c=1;c<=NoOfCols;c++)
				{
					rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
					Y_Velocity_star[r][c] = Y_Velocity_star[r][c] + (1.0/rho_r)*delT*ST_Fy[r][c] ;
					//printf("\n sourceV = %e",SourceV);
					//exit(0);
				}
			}
			
			
		}
			freeArray(Ad_Flux_Vertical_Faces);
			freeArray(Ad_Flux_Horizontal_Faces);	
			freeArray(Diff_Flux_Vertical_Faces);	
			freeArray(Diff_Flux_Horizontal_Faces);				
			
	}


void Calculate_Surface_Tension_Height_Function_DAC()
{
			int i;

int r,c, r_temp, c_temp;	
double HF_0, HF_1, HF_2, Fx, Fxx, Y_RC, Y_Rp1C, X_RC, X_RCp1;
//double err_new, norm;
		int sign_normal=1;
	
	double **kappa, Grad_VolFrac_X, Grad_VolFrac_Y;
		double **kappa_f_x, **kappa_f_y;
		kappa = (double **) calloc(NoOfRows+2,sizeof(double *));
		kappa_f_x = (double **) calloc(NoOfRows+2,sizeof(double *));
		kappa_f_y = (double **) calloc(NoOfRows+2,sizeof(double *));

			for (i=0; i<NoOfRows+2; i++)
			{
				kappa[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa_f_x[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa_f_y[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}

	
// FILE *ptrWrite;
//   char LineFormat[] = "./FError/kappa_128x128-%d.dat";
//   char LineFileName[20];
//   sprintf(LineFileName,LineFormat,0);
//   *******************

//   /***********INITIALISE**************/
//   ptrWrite =fopen(LineFileName,"w");
//   /***********************************/
		 double maxK = -1.0e30;
		double minK = 1.0e30;	
	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{
			//Grad_VolFrac_X = (Cellattrib[r][c].VolFrac - Cellattrib[r][c-1].VolFrac)/del;		//gradient of VolFrac in x direction
			//Grad_VolFrac_Y = (Cellattrib[r][c].VolFrac - Cellattrib[r-1][c].VolFrac)/del;	
			if (Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac<UPPER_LIMIT)
			//if (fabs(Grad_VolFrac_X)>0 || fabs(Grad_VolFrac_Y)>0)
			{
				HF_0 = 0; HF_1 = 0; HF_2 = 0; 

				// Case 1, when the normal is mainly pointing upwards
				if ((Cellattrib[r][c].Ny)>=fabs(Cellattrib[r][c].Nx))
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						r_temp = r_temp + 1; // go up 
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while (r_temp<= NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						r_temp = r_temp - 1; // go up 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT);	//exit for completely filled cell or end of domain 

					
					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						r_temp = r_temp + 1; // go up 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT);	//exit for empty cell or end of domain 

					r_temp = r-1; c_temp = c+1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						r_temp = r_temp - 1; // go up 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT);	//exit for completely filled cell or end of domain 


						
				}
				
				//	Case 2, when the normal is mainly pointing downwards
				else if ((Cellattrib[r][c].Ny)<=-fabs(Cellattrib[r][c].Nx))
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c+1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}


				/*************Case 3 When Normal is mainly pointing right****************************/

				else if ((Cellattrib[r][c].Nx)>=fabs(Cellattrib[r][c].Ny))
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); :w

					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						c_temp = c_temp + 1; // go up 
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r; c_temp = c-1;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						c_temp = c_temp - 1; // go up 
					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						c_temp = c_temp + 1; // go up 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r+1; c_temp = c-1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						c_temp = c_temp - 1; // go up 
					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}	
						
				//	Case 4, when the normal is mainly pointing left
				else if ((Cellattrib[r][c].Nx)<=-fabs(Cellattrib[r][c].Ny))
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r+1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}

						Fx = (HF_2 - HF_0)*del/(2*del);
						Fxx = (HF_2 - 2*HF_1 + HF_0)*del/pow(del,2);
						
						kappa[r][c] = sign_normal*Fxx/pow((1+pow(Fx,2)),1.5);
					
									
									if (kappa[r][c]> maxK)
										maxK =kappa[r][c];
									else if (kappa[r][c]< minK)
										minK =kappa[r][c];
						
						//printf("\n%d\t%d\t%lf\t%lf",r,c,kappa[r][c],Cellattrib[r][c].VolFrac);	
			}
			
			 
			// else
				// {
				// 	HF_0 = 	(Cellattrib[r-1][c+3].VolFrac + Cellattrib[r-1][c+2].VolFrac + Cellattrib[r-1][c+1].VolFrac + 
				// 			Cellattrib[r-1][c].VolFrac + Cellattrib[r-1][c-1].VolFrac + Cellattrib[r-1][c-2].VolFrac + Cellattrib[r-1][c-3].VolFrac)*del;
				// 	HF_1 = 	(Cellattrib[r][c+3].VolFrac + Cellattrib[r][c+2].VolFrac + Cellattrib[r][c+1].VolFrac + 
				// 			Cellattrib[r][c].VolFrac + Cellattrib[r][c-1].VolFrac + Cellattrib[r][c-2].VolFrac + Cellattrib[r][c-3].VolFrac)*del;
				// 	HF_2 = 	(Cellattrib[r+1][c+3].VolFrac + Cellattrib[r+1][c+2].VolFrac + Cellattrib[r+1][c+1].VolFrac + 
				// 			Cellattrib[r+1][c].VolFrac + Cellattrib[r+1][c-1].VolFrac + Cellattrib[r+1][c-2].VolFrac + Cellattrib[r+1][c-3].VolFrac)*del;
				
				
				// 	// X_RC = del*3; X_RCp1 = del*4;

				// 	//  if (HF_1>X_RC && HF_1<X_RCp1)
				// 	//  {
				// 	 	Fx = (HF_2 - HF_0)/(2*del);
				// 	 	Fxx = (HF_2 - 2*HF_1 + HF_0)/pow(del,2);
				// 		//sign_normal = (Cellattrib[r][c].Nx > 0) ? 1 : ((Cellattrib[r][c].Nx < 0) ? -1 : 0);
				// 		kappa[r][c] = sign_normal*Fxx/pow((1+pow(Fx,2)),1.5);
				// 	 // }
				// 	// else
				// 	// {
				// 	// 	kappa[r][c] = -100000; // a junk value which later has to be updated
				// 	// 	//kappa[r][c] = 0.5*(kappa[r][c-1] + kappa[r][c+1]);

				// 	// }
					
				// 		//printf("\n%d\t%d\t%lf\t%lf\n",r,c,kappa[r][c],Cellattrib[r][c].VolFrac );
					

				// }
			
			 // if (r==49 && c==36)
			 // 	printf("\n%d\t%d\t%lf\t%lf\t%lf\n",r,c,kappa[r][c],Cellattrib[r][c].VolFrac,Fxx );
				   //fprintf(ptrWrite,"%d\t%d\t%e\n",r,c,kappa[r][c]);
		}
	}
			//~ printf("\nCurvature is between %e and %e ", minK,maxK);
				
			//~ exit(0);

	 // norm=0;
		// for (r=1;r<=NoOfRows;r++)
		// {
		// 	for (c=1;c<=NoOfCols;c++)
		// 	{
		// 			 if (kappa[r][c]!=0)
		// 			 {
		// 			 	err_new = fabs(fabs(kappa[r][c]) - 4.0);
		// 			 	if (err_new> norm)
		// 			 	{
		// 			 		norm = err_new;
		// 			 	}	

		// 			 }
		// 	}
		// }
		
		//  printf("\n norm = %lf\n", norm);

	// fclose(ptrWrite);
	 //exit(0);
	//calculate kappa on face centers
	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{

			
			/*****************************at horizontal faces ******************************************/

			if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
			{

				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{

					kappa_f_y[r][c] = 0.5*(kappa[r][c] + kappa[r-1][c]);
				}
				else
				{
					kappa_f_y[r][c] = kappa[r-1][c]; // assign the face left kappa
				}


			}


				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{
					if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
					{

						// do nothing
					}
					else
					{
						kappa_f_y[r][c] = kappa[r][c]; // assign the face left kappa
					}


				}
			/******************************* kappa at vertical faces ***************************************/
				if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
				{

					if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
					{

						kappa_f_x[r][c] = 0.5*(kappa[r][c] + kappa[r][c-1]);
					}
					else
					{
						kappa_f_x[r][c] = kappa[r][c-1]; // assign the face left kappa
					}


				}


				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{
					if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
					{

						// do nothing
					}
					else
					{
						kappa_f_x[r][c] = kappa[r][c]; // assign the face left kappa
					}


				}

			   // if (kappa_f_x[r][c]!=0)
			   // printf("%d\t%d\t%lf\t%lf\t%lf\t%lf\n",r,c,kappa_f_x[r][c],Cellattrib[r][c].VolFrac,kappa[r][c],kappa[r][c-1]);	
		}
	}

	

	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{	
			 ST_Fx[r][c] = -Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r][c-1].VolFrac)*(kappa_f_x[r][c]);
			 ST_Fy[r][c] = -Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r-1][c].VolFrac)*(kappa_f_y[r][c]);
			 // ST_Fx[r][c] = -Sigma*(0.5/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r][c-1].VolFrac)*(-8.0);
			 // ST_Fy[r][c] = -Sigma*(0.5/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r-1][c].VolFrac)*(-8.0);
		}
	}


	freeArray(kappa);
	freeArray(kappa_f_x);
	freeArray(kappa_f_y);
	//exit(0);
}
/******************
Name: Poisson_Solver_SOR
Description: Solves poisson equations for pressure
Scheme: Successive Over Relaxation method (SOR)
******************/
void Poissons_Solver_SOR()
{
	int r,c,Step;
	  double  Coeff_Poissons_solver, Relaxation_param, Res_inf_norm ;
	  double temp1,temp2,temp3,temp4, sum_Press, mean_Press, NoOfCells;
	  //~ double Source_Poisson[NoOfRows+2][NoOfCols+2];
	//~ double Poissons_residue[NoOfRows+2][NoOfCols+2];
	double sum_poisson =0.00 ;
	Step=0;
	Res_inf_norm = 1.0;
	Relaxation_param = 1.0;
	
	
	double **Source_Poisson,**Poissons_residue;
		
	
		Source_Poisson = (double **) calloc(NoOfRows+2,sizeof(double *));
		Poissons_residue = (double **) calloc(NoOfRows+2,sizeof(double *));
		int i;
			for (i=0; i<NoOfRows+2; i++)
			{
				Source_Poisson[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Poissons_residue[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
			}
			

    // Francais: calculer le terme source de l'equation de Poisson
    // English: Calculate the source term for Poisson's equation
	
    for (r=1; r<=NoOfRows; r++)
		{
			for (c=1; c<=NoOfCols; c++)
			{

			Source_Poisson[r][c] = 0.5*((X_Velocity_star[r][c+1]	-	X_Velocity_star[r][c]) + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
				sum_poisson = sum_poisson + Source_Poisson[r][c];


            }
        } 
	
	//printf("\n\n*********old**************************************************\n\n");
	//~ for (r=0; r<=NoOfRows+1; r++)
            //~ {
                //~ for (c=0; c<=NoOfCols+1; c++)
                //~ {
                      //~ printf("\n%lf",Press[r][c]);
			
                //~ }
		
		//~ //printf("\n");
            //~ }	
	  // printf("\n\n***********************************************************\n\n"); 
	
	//~ exit(0);	
	
	//printf("\n sum poisson = %e\n",sum_poisson); exit(0);
	
							//~ for (c=1;c<=NoOfCols;c++)
							//~ {
								//~ Source_Poisson[0][c] = Source_Poisson[1][c];                      //bottom wall
								//~ Source_Poisson[NoOfRows+1][c] = Source_Poisson[NoOfRows][c];      // top wall
							//~ }
							//~ for (r=1;r<=NoOfRows;r++)
							//~ {
								
								//~ Source_Poisson[r][0] = Source_Poisson[r][1];                      //left wall
								//~ Source_Poisson[r][NoOfCols+1] = Source_Poisson[r][NoOfCols];      //right wall
							//~ }


    /* worrying about compatibility condition, well when we are using conservative form of equations
    with staggered grid we are implicitly making the compatibility conditions true. However, even the compatibility conditions fail here
    the crucial issue is not to find pressure but to ensure the solenoidality(zero divergence) of velocity field accurately. Still not satisfied,
    read this DOI: 10.1007/BFb0107103 */

	while(Res_inf_norm>tolerance)		// if greater than the maximum allowed residue
	{


                        	sum_Press = 0; NoOfCells = 0; mean_Press = 0;
							// Set Boundary Conditions
							for (c=1;c<=NoOfCols;c++)
							{
								Press[0][c] = Press[1][c];                      //bottom wall outflow
								Press[NoOfRows+1][c] = Press[NoOfRows][c];      // top wall
							}
								for (r=1;r<=NoOfRows;r++)
							{
								
								
								Press[r][0] = Press[r][1];                      //left wall
								Press[r][NoOfCols+1] = - Press[r][NoOfCols];      //right wall 
							}
							

                    // Successive-overrelaxation Iteration
                    for (r=1; r<=NoOfRows; r++)
                    {
                        for (c=1; c<=NoOfCols; c++)
                        {
                            Coeff_Poissons_solver = (1.0/(rho[r][c]+rho[r][c+1]) + 1.0/(rho[r][c]+rho[r][c-1]) + 1.0/(rho[r+1][c]+rho[r][c]) +1.0/(rho[r][c]+rho[r-1][c]) ) / del_sq;
							temp1 = Press[r][c+1]/(rho[r][c]+rho[r][c+1])		+	Press[r][c-1]/(rho[r][c]+rho[r][c-1]) ;
							temp2 = Press[r+1][c]/(rho[r+1][c]+rho[r][c])		+	Press[r-1][c]/(rho[r][c]+rho[r-1][c]);


							//Now Pressure
							Press[r][c] 	=	Relaxation_param/Coeff_Poissons_solver * ((temp1+temp2)/del_sq	-	(Source_Poisson[r][c]))	+	(1.0-Relaxation_param)*Press[r][c] ;
							sum_Press = sum_Press + Press[r][c];
							NoOfCells = NoOfCells + 1;      //why to calculate this if we know this apriori? we may want to work with other domains where we want to make some cells dead.
			}
                    }
			
		  
		    
		    
		      //~ for (r=0; r<=NoOfRows+1; r++)
                    //~ {
                        //~ for (c=0; c<=NoOfCols+1; c++)
                        //~ {
				//~ printf("%lf\n",Press[r][c]);
			//~ }
		//~ }
		//~ exit(0);
                    // calculate the mean pressure : Why? Read the case of Neumann conditions for Poisson's equation
                    //~ mean_Press = sum_Press/NoOfCells;
	
	//NOTE:- Switch on below only if Pressure BC on all walls are Neumann, else don't use below
		    
            //~ // select the zero mean pressure : Why? again read the case of Neumann conditions for Poisson's equation
            //~ for (r=1; r<=NoOfRows; r++)
            //~ {
                //~ for (c=1; c<=NoOfCols; c++)
                //~ {
                    //~ Press[r][c] = Press[r][c] - mean_Press;
                //~ }
            //~ }

							//~ for (c=1;c<=NoOfCols;c++)
							//~ {
								//~ Press[0][c] = Press[1][c];                      //bottom wall
								//~ Press[NoOfRows+1][c] = Press[NoOfRows][c];      // top wall
							//~ }
								//~ for (r=1;r<=NoOfRows;r++)
							//~ {
								
								
								//~ Press[r][0] = Press[r][1];                      //left wall
								//~ Press[r][NoOfCols+1] = -1.0* Press[r][NoOfCols];      //right wall Outflow condition, Pressure is zero on wall, so update the ghost cell accordingly
							//~ }
							
							
							// Set Boundary Conditions
							for (c=1;c<=NoOfCols;c++)
							{
								Press[0][c] = Press[1][c];                      //bottom wall outflow
								Press[NoOfRows+1][c] = Press[NoOfRows][c];      // top wall
							}
								for (r=1;r<=NoOfRows;r++)
							{
								
								
								Press[r][0] = Press[r][1];                      //left wall
								Press[r][NoOfCols+1] =  -Press[r][NoOfCols];      //right wall 
							}
						
							
		// Calculate residue
		Res_inf_norm = 0;			//reset residue norm to zero only here, dont move up or down the loop

		 for (r=1; r<=NoOfRows; r++)
		{
                        for (c=1; c<=NoOfCols; c++)
                        {
				temp1 = (Press[r][c+1]-Press[r][c])/(rho[r][c]+rho[r][c+1]);
				temp2 = (Press[r][c]-Press[r][c-1])/(rho[r][c]+rho[r][c-1]);
				temp3 = (Press[r+1][c]-Press[r][c])/(rho[r][c]+rho[r+1][c]);
				temp4 = (Press[r][c]-Press[r-1][c])/(rho[r][c]+rho[r-1][c]);
				Poissons_residue[r][c] = Source_Poisson[r][c] - (temp1 - temp2 + temp3 - temp4)/del_sq;
				if (fabs(Poissons_residue[r][c])>Res_inf_norm)
				{
						Res_inf_norm=fabs(Poissons_residue[r][c]);
				}
			}
		}
		    
		
		Res_inf_norm = 2*delT*Res_inf_norm;
		
		Step++ ; // Step counter
		//printf("\n%e",Res_inf_norm);
		
		if (Step>1e8)
		{
			printf("\n\n*************Maximum Iterations Reached**********************************************\n\n");
			printf("\nERROR IN SUBROUTINE Poissons_Solver \n");
			printf("\n\n***********************************************************\n\n");
			return;

		}
	}
	
	
	//~ printf("\niter = %d",Step);
	printf("sor "); fflush(stdout);
	freeArray(Source_Poisson);
	freeArray(Poissons_residue);
	//~ exit(0);
		
	
}
/****************************
Name: Velocity_Correction
Description:  Adds pressure correction in the temporary velocities
Limitations:
***************************/

void X_Velocity_Correction()
{
	int r,c;
	for(r=1;r<=NoOfRows;r++)
		{
			for(c=2;c<=NoOfCols;c++)
			{
			X_Velocity[r][c] = X_Velocity_star[r][c] - 2*delTBydel*(Press[r][c] - Press[r][c-1])/(rho[r][c] + rho[r][c-1]); // the 2 appears from 0.5 in denominator for mean rho
			
		}
	}
	
	
	//~ if(StepT==1)
	//~ {
		//~ for (r=0; r<=NoOfRows+1; r++)
		//~ {
			//~ for (c=0;c<=NoOfCols+1; c++)
			//~ {
				//~ printf("\n%e",X_Velocity_star[r][c]);
			//~ }
		//~ }
		//~ exit(0);
	//~ }
	
}

void Y_Velocity_Correction()
{
	int r,c;

	for(r=2;r<=NoOfRows;r++)
	{
		for(c=1;c<=NoOfCols;c++)
		{
			Y_Velocity[r][c] = Y_Velocity_star[r][c] - 2*delTBydel*(Press[r][c] - Press[r-1][c])/(rho[r-1][c] + rho[r][c]);
		}
	}
	
	
	

}

void WriteToFile_NS(double Step)	
{
 int r,c;
 FILE *ptrWrite;
  double X_vel, Y_vel;
	char filename[50];
  sprintf(filename, "./NS_Data/NS%g.dat", Step);
 ptrWrite =fopen(filename,"w");
 for(r=0;r <= NoOfRows+1; r++)
 {
      for(c=0;c <=NoOfCols+1 ; c++)
      {
	      //  X_vel = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
	     //   Y_vel = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
		// I have not taken the averaged data output because I need the last file such that I can restart the simulation from there, averaged 
	      // data will lose some information, gerris don't have staggered grid, all data at cell centers, so gerris don't have this problem
	      //Note:- Ghost cells data also taken out, take care while post processing
	      
		fprintf(ptrWrite,"%e",Time);			// exact time of the simulation
		fprintf(ptrWrite,"\t%e",del*(c-1));		// This is not the x-coordinate at the center of the cell, calculate the averge while post processing (x coordinate of left face of CV)
		fprintf(ptrWrite,"\t%e",del*(r-1));			// This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
		//fprintf(ptrWrite,"\t%e",X_vel);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
		//fprintf(ptrWrite,"\t%e",Y_vel);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
		
	       fprintf(ptrWrite,"\t%e",X_Velocity[r][c]);	// This is not the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
		 fprintf(ptrWrite,"\t%e",Y_Velocity[r][c]);	// This is not the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
		fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
		fprintf(ptrWrite,"\t%e",Cellattrib[r][c].VolFrac);	// this is at cell center
		fprintf(ptrWrite,"\n");
       }

  }

  fclose(ptrWrite);

}


		
	
		
void freeArray(double **a) {
    int i;
    for (i = 0; i < NoOfRows+2; ++i) {
        free(a[i]);
    }
    free(a);
}	
	
		
/**************************************************
Description: 2D Poisson Multigrid Solver D2u = f

Limitation: Refinement allowed only in powers of 2
		  and nows of cells in horizontal and vertical
		  direction must be equal

Author: Palas Kumar Farsoiya, IITB
********************************************************/



void Poissons_Solver_Multigrid(){	
	
	int r,c;
	
	int GS_iter = 4;
	
	//~ n = NoOfRows+2;		// must be odd number	//no of rows and columns should be equal
	//~ h = 1.0/(n-2);	
	//~ h_sq = h*h;
	double pi = 3.14159265359;
	double  *f;	//Press -  is updated u, f is RHS of poisson's equation, ut is exact solution
	double temp1,temp2,temp3,temp4;

	double *u, *Source_Poisson_ptr,*rho_ptr;	// pointers for updated u and RHS,  used in recursive V cycle
	

	
	//~ char filename[50];
	//~ FILE *ptrWrite1,*ptrWrite2;
	
	//~ sprintf(filename, "Residue.dat");
	//~ ptrWrite1 =fopen(filename,"w");
	
	//~ sprintf(filename, "Result_multi.dat");
	//~ ptrWrite2 =fopen(filename,"w");
	
		rho_ptr  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		u  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		  f  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		//u  = ( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
	
	//init guess on the finest grid is the zero vector 
	
	int k=3; int l=3; double h=del;
	for(r=0; r<=NoOfRows+1; r++)	
	{
		for(c=0;c<=NoOfCols+1;c++)
		{
			 u[INDEX(r,c,NoOfCols)] = Press[r][c] ;
			rho_ptr[INDEX(r,c,NoOfCols)] = rho[r][c];
			
			
			if (r>0 && r<NoOfRows+1 && c>0 && c<NoOfCols+1)
			{
				f[INDEX(r,c,NoOfCols)] = 0.5*((X_Velocity_star[r][c+1]	-	X_Velocity_star[r][c]) + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
			}
			
			
			//~ f[INDEX(r,c,NoOfCols)] = -(pow(k,2)+pow(l,2))*pow(pi,2)*sin(k*pi*(-0.5+(c-0.5)*h)) * sin(l*pi * (-0.5+(r-0.5)*h));	
				
			//~ rho_ptr[INDEX(r,c,NoOfCols)] = 0.5;
			
		}
	}
	//exit(0);
	
	int flag=1;
	double Max_Res=10;
	int step_vcycle=0;
	
			while ( Max_Res>tolerance )
			  {
				
				
				u =  Vcycle( NoOfRows, NoOfCols, u,f, GS_iter,flag,rho_ptr,del);
				
				flag=1;
						
				Max_Res = Calculate_Residue ( f , u, NoOfRows, NoOfCols,rho_ptr,del) ;
				  Max_Res = 2*delT*Max_Res;
				  
					   
					    //~ if (StepT==16)
					    //~ {
						//~ printf ( "\n%e", Max_Res );
					    //~ }
						    
					    
			
				//~ fflush(stdout);  
				//~ if (step_vcycle>=10)
					//~ break;
			    
				++step_vcycle; 
				  //~ printf ( "\n iter=%d\tMaxRes = %e\n", step_vcycle,Max_Res );
			  } 
			  
			
			  
			    //~ exit(0);
			 
		
			  
			  double sum =0; 
			  double mean_Press =0;
			  
			  for(r=0; r<=NoOfRows+1; r++)	
			{
				for(c=0;c<=NoOfCols+1;c++)
				{
					 Press[r][c] = u[INDEX(r,c,NoOfCols)]  ;
					
					
				}
			}
			
			free(u); 
			free(f); 
			free(rho_ptr);
			
			
			printf("\tMG "); fflush(stdout);
			printf ( "\t%e", Max_Res );
			
	

	
}
	
	
void Initialise_VolFrac()
{

int r,c;
  //  const double radius1 = 0.1; // in x direction semi-major
   // const double radius2 = 0.09; // in y direction semi-minor
    //~ const double dx = 1.0 / width;
    //~ const double dy = 1.0 / height;

    //Buffer f(width, height);
    

    // Calculate volume fractions.
    for ( r = 1;r <= NoOfRows; ++r) {
        double y0 = ((double)(r-1) * del ) ;
        double y1 = ((double)(r ) * del ) ;
        for ( c = 1; c <= NoOfCols; ++c) {
            double x0 = ((double)(c-1) * del ) ;
            double x1 = ((double)(c) * del ) ;
            double area = volumeFraction1(x0, y0, x1, y1, 12);
            Cellattrib[r][c].VolFrac = area;
            //std::cout << (int)(0.5 + 9.0 * std::max(0.0, std::min(1.0, f(i, j))));
		//std::cout <<  std::setprecision (5) << (f(i,j));
		
        }
        //std::cout << std::endl;
    }
	

	
}	

double volumeFraction1(double x0, double y0, double x1, double y1, int recursion)
{
    bool inside00 = interface1(x0,y0) < 0;
    bool inside01 =   interface1(x1,y0) < 0;
    bool inside10 =   interface1(x0,y1) < 0;
    bool inside11 =  interface1(x1,y1) < 0;

    if (inside00 && inside01 && inside10 && inside11)
        return 1.0;

    if (!inside00 && !inside01 && !inside10 && !inside11)
        return 0.0;

    if (recursion == 0)
        return 0.5;

    double xm = 0.5 * (x0 + x1);
    double ym = 0.5 * (y0 + y1);

    --recursion;
    return 0.25 * (volumeFraction1(x0, y0, xm, ym, recursion)	//0.25 is because 
                 + volumeFraction1(xm, y0, x1, ym, recursion)
                 + volumeFraction1(x0, ym, xm, y1, recursion)
                 + volumeFraction1(xm, ym, x1, y1, recursion));
}
// Calculates the volume fraction of the unit circle within the given rectangle.
double volumeFraction2(double x0, double y0, double x1, double y1, int recursion)
{
    bool inside00 = interface2(x0,y0) < 0;
    bool inside01 =   interface2(x1,y0) < 0;
    bool inside10 =   interface2(x0,y1) < 0;
    bool inside11 =  interface2(x1,y1) < 0;

    if (inside00 && inside01 && inside10 && inside11)
        return 1.0;

    if (!inside00 && !inside01 && !inside10 && !inside11)
        return 0.0;

    if (recursion == 0)
        return 0.5;

    double xm = 0.5 * (x0 + x1);
    double ym = 0.5 * (y0 + y1);

    --recursion;
    return 0.25 * (volumeFraction2(x0, y0, xm, ym, recursion)	//0.25 is because 
                 + volumeFraction2(xm, y0, x1, ym, recursion)
                 + volumeFraction2(x0, ym, xm, y1, recursion)
                 + volumeFraction2(xm, ym, x1, y1, recursion));
}
