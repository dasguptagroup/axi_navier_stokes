/***********START OF USER INPUT***********************************/


//multigrid
/*****************GRID INPUT**********************************************/

#define NoOfRows 	(int)	64
#define NoOfCols 	(int) 64
/*******************************************************************/

/******************LVIRA INPUT****/
#define VOF_Module		(int)		1	// 0 for OFF, 1 for ON
#define Rotation_StepSize 	(float)0.00031
/*********************************/

/**************************/
#define PI        			(float)  	3.14159265358
#define UPPER_LIMIT 		(float)		0.999
#define LOWER_LIMIT 		(float)		0.001
#define CFL			(float)		0.8
#define delT_Tolerance 		(float)		0.0156250


/**************************/

/**************NAVIER STOKES INPUT********************/

/***************Switches************************************/
#define Advection_term	(int)		0	// 0 to OFF, 1 to ON
#define Diffusion_term	(int)		0	// 0 to OFF, 1 to ON
#define Linear_Solver	(char)		'M'		// M for multigrid(only for square domains with even powered grids) and S for GS 
#define Advection_Scheme (int)	5	// 1 for FOU, 2 for CD,  3 for SOU, 4 for QUICK
								//5 for WENO
/******************************************************/
/*********************GfsGlobal*************************************/
			#define theta (0.0/180.)*M_PI
			#define Umax 0.5
			#define H 0.1
			#define Q (2.0*Umax*H/3.0)
			#define Re 125.0
			#define Fr 6.0
			#define Scale 1.875
			#define H0 H*1.
			#define Uav Q/H
			#define Bw 6.
			#define rol 1.
			#define Weber 50.
/********************************************************************/			

//Physical Params
#define SourceU   	(float)	0 //(Uav*Uav*sin(theta))/(Fr*Fr*H)
#define SourceV	(float)	 0
#define rho_G	(float)	1.0e-2
#define rho_L	(float)	1.0
#define mu_G	(float)	0
#define mu_L	(float)	0
#define Sigma	(float)	1.0 //rol*Q*Q/(H*Weber)
//Numerical Params for Poisson solver
#define tolerance		(float) 	1e-3		// divergence of velocity 
#define alpha				(float)	1.0		//don't forget this, 
										//it effects th SOR poissons solver

//~ #define semi_minor (float) 0.25
//~ #define semi_major (float) 0.25
//~ #define center_x (float) 0.5
//~ #define center_y (float) 0.5
//~ #define interface(x,y) ((pow((x-center_x)/semi_minor,2))+(pow((y-center_y)/semi_major,2))-1.0)


#define DIAMETER 0.2
#define EPSILON 0.01

#define RADIUS(x,y)      	(DIAMETER/2.*(1. + EPSILON*cos (2.*atan2 (y, x))))
#define interface1(x,y) 	 (pow(x-0.5,2) + pow(y-0.5,2) - pow(RADIUS(x-0.5,y-0.5),2)) 
#define interface2(x,y) 	0.0 //(x-(Scale*24+0.1)) // (pow(x-0.5,2) + pow(y-0.5,2) - pow(RADIUS(x-0.5,y-0.5),2)) 

/**************************************************/



/*****************END OF USER INPUT**********************************/


/*------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------*/


/*############################THE CODE BELOW DOES NOT REQUIRE ANY USER ENTRY#################################*/

typedef struct				/*2D STRUCT STORES THE ATTRIBUTES FOR A CELL*/
   {
     /***************************CELL ATTRIBUTES - LVIRA******************************************/
       double  VolFrac;			//VOLUME FRACTION FOR EACH CELL      
       double  Area;			//THE AREA CALCULATED FOR EACH CELL DURING INTERFACE RECONSTRUCTION
       double  Nx;				//X-COMPONENT OF THE UNIT NORMAL TO THE LINE
       double  Ny;				//Y-COMPONENT OF THE UNIT NORMAL TO THE LINE
       double  P;				//PERPENDICULAR DISTANCE OF THE LINE FROM THE CENTER OF THE CELL
      char         Shape[30];       //SHAPE OF THE AREA BASED ON FITIING A STRAIGHT LINE
       double  Theta;			//ANGLE MADE BY THE LINE WITH THE NEGATIVE DIRECTION OF X-AXIS
	  int          Type;            //TYPE OF CELL - 1. MIXED 2. ACTIVE 3. ISOLATED
	   double  delV;            //VOLUME OF THE CELL - REQUIRED FOR BOOKKEPING DURING ADVECTION
     /********************************************************************************************/  

     /*****CELL ATTRIBUTES - MOMENTUM EQUATION***********/ 
       double Press;
     /***************************************************/    

   } STRUCT_CELL_ATTRIB;

//STRUCT_CELL_ATTRIB Cellattrib[NoOfRows+2][NoOfCols+2]; /*CREATING AN ARRAY OF STRUCTURES WHERE EACH ELEMENT OF THE ARRAY IS OF THE TYPE STRUCT_CELL_ATTRIB*/
STRUCT_CELL_ATTRIB **Cellattrib; /*CREATING AN ARRAY OF STRUCTURES WHERE EACH ELEMENT OF THE ARRAY IS OF THE TYPE STRUCT_CELL_ATTRIB*/


/*X_VELOCITY[2][3] IS THE HORIZONTAL VELOCITY THROUGH THE LHS WALL OF THE CELL[2][3]
  Y_FLUX[2][3] IS THE VERTICAL FLUX THROUGH THE BOTTOM WALL OF THE CELL[2][3]

//~ /****************WALL ATTRIBUTES***************************************************************/
 //~ double  X_Velocity[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Velocity_star[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF TEMPORARY VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity_star[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF TEMPORARY VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Flux[NoOfRows+2][NoOfCols+2];      //THE X FLUX AT THE LHS OF A CELL
 //~ double  Y_Flux[NoOfRows+2][NoOfCols+2];      //THE Y FLUX AT THE BOTTOM OF A CELL
//~ /************************************************************************************************/

    //~ double mu[NoOfRows+2][NoOfCols+2]; 		// VISCOSITY AT CENTER OF THE CELL
 //~ double rho[NoOfRows+2][NoOfCols+2]; 		// DENSITY AT CENTER OF THE CELL
 //~ double Press[NoOfRows+2][NoOfCols+2]; 	// PRESSURE AT CENTER OF THE CELL
   
   /****************WALL ATTRIBUTES***************************************************************/
double **X_Velocity;
double  **Y_Velocity;  //Y-COMPONENT OF VELOCITY AT THE BOTTOM OF A CELL
 double  **X_Velocity_star;  //X-COMPONENT OF TEMPORARY VELOCITY AT THE LHS OF A CELL
 double  **Y_Velocity_star;  //Y-COMPONENT OF TEMPORARY VELOCITY AT THE BOTTOM OF A CELL
 double  **X_Flux;      //THE X FLUX AT THE LHS OF A CELL
 double  **Y_Flux;      //THE Y FLUX AT THE BOTTOM OF A CELL

 double **ST_Fx,**ST_Fy;
 double **X_int_1,**Y_int_1,**X_int_2,**Y_int_2;
// double  **X_Velocity[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Velocity_star[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF TEMPORARY VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity_star[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF TEMPORARY VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Flux[NoOfRows+2][NoOfCols+2];      //THE X FLUX AT THE LHS OF A CELL
 //~ double  Y_Flux[NoOfRows+2][NoOfCols+2];      //THE Y FLUX AT THE BOTTOM OF A CELL
//~ /************************************************************************************************/

  double **mu; 		// VISCOSITY AT CENTER OF THE CELL
 double **rho; 		// DENSITY AT CENTER OF THE CELL
 double **Press; 	// PRESSURE AT CENTER OF THE CELL
/****************GLOBAL VARIABLES****************/
 double del_sq;
 double del_sqBy2;
 double del2;
 double del3;
 double delBy2;
 double PiBy2;
 double PiBy4;
 double ThreePiBy2;
 double delTBydel;
 double delTdel;
 //~ double BoxLength=Scale;
 double del= 1.0/NoOfCols;
 double delT;

 double Time;	// current actual time for StepT
int StepT=0;	// current computational time 
int istep = 1;		//gerris like computational time step
double step = 0;		// gerris like acutal time step
int Write_Count;  // counting the number of files writing
double Write_Time;
double end = 3000.0;		// gerris like actual end time 

	
/***********************************************/


/***********FOR NS ********************/
	
	double	 X_Velocity_old[NoOfRows+2][NoOfCols+2],Y_Velocity_old[NoOfRows+2][NoOfCols+2] ;
	double U_residue=1.0, V_residue=1.0;
	/****************************************/
	
	
//NOTE:-	To switch off one particular condition kindly put the value as 1e+10, this will 
// 		make sure that it wont be used, any other finite value will keep it on
/***************Boundary Conditions Dirichlet*********************/
//LEFT Boundary


double BcDirichlet_U_left=0;

double BcDirichlet_V_left=0;

// Right Boundary
double BcDirichlet_U_right=0;

double BcDirichlet_V_right=0;

//Bottom
double BcDirichlet_U_bottom=0;

double BcDirichlet_V_bottom=0;

//Top boundary
double BcDirichlet_U_top=1.0;

double BcDirichlet_V_top=0;

/***************Boundary Conditions Neumann*********************/

//LEFT Boundary


double BcNeumann_U_left=0;

double BcNeumann_V_left=0;

// Right Boundary
double BcNeumann_U_right=0;

double BcNeumann_V_right=0;

//Bottom
double BcNeumann_U_bottom=0;

double BcNeumann_V_bottom=0;

//Top boundary
double BcNeumann_U_top=0;

double BcNeumann_V_top=0;

/*********************functions***********************************************/


/**FUNCTION DECLARATIONS*/
void        Pass_main(int, int);
void 	CFL_Check();
void		InterfaceRecon_main();
void		Recon_Interface(int, int);
void		Write_struct_Cellattrib(int, int,   double,   double,   double,   double,   double, char[],   double, int);
void		Init_GlobalVars();
void		Calculate_InitialSlope();
void		Calculate_distance(int, int);
void		Identify_Shape(int, int);
void		Extrapolate_Line(int, int);
void		IdentifyCell_AssignArea(int, int, int, char[],   double);
  double Calculate_SumOfDiffSq(int, int);
int		Find_Quad(int, int);
void		Calculate_Theta(int, int);
void		Rotate_Line(int, int, char[]);
void		WriteToFile(double);
void		AdvanceF_field(int, int, int, int);
void		CalculateF_Flux(int);
  double CalculateX_Flux(char[], char[],   double,   double,   double,   double,   double,   double,   double,   double,   double);
  double CalculateY_Flux(char[], char[],   double,   double,   double,   double,   double,   double,   double,   double,   double);
void		AssignVal_FluxVariables(int, int,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *);
void        BookKeeping();
/*************************/
// Navier Stokes Functions
void Advance_Velocity_Field();
void Update_Fluid_Properties();

void Apply_Extra_BoundaryConditions();
  double Ad_Flux_Calc(  double ,   double ,   double ,   double, double,double,double,double );

void Calculate_X_Velocity_star();
void Calculate_Y_Velocity_star();
void Poissons_Solver_SOR();
void X_Velocity_Correction();
void Y_Velocity_Correction();
void WriteToFile_NS(double);
void freeArray(double **) ;
// multigrid routines 
void Poissons_Solver_Multigrid();

 
	double *Vcycle_Multigrid(int  , double * , double *, double * ,int ) ;

// Surface tension 

void Calculate_Surface_Tension_Height_Function_DAC();

// max min functions
double max(double, double);
double min(double, double);
// VolFrac Generation routines

double volumeFraction1(double , double , double , double , int );
double volumeFraction2(double , double , double , double , int );

void Initialise_VolFrac();

double max(double a, double b)
{
	if (a>=b)
	{
		return a;
	}
	else if (b>a)
	{
		return b;
	}
	else
	{
		printf("\nError in max a = %e, b = %e\n",a,b);
		exit(0);
	}
}	
double min(double a, double b)
{
	if (a<=b)
	{
		return a;
	}
	else if (b<a)
	{
		return b;
	}
	else
	{
		printf("\nError in min  a = %e, b = %e\n",a,b);
		exit(0);
	}
}	
