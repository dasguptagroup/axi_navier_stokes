

/********************Multigrid Dependencies***********************/

#define INDEX(r,c,NC) ((NC+2)*(r)+(c))


/******************************************************************************/
double *jacobi ( double *f, double *u_ptr, int NR, int NC,double *rho_private, double h ) 
{
  double h_sq;
	double Coeff_Poissons_solver;
  int r,c;
 

  h_sq = h*h;


	   // Successive-overrelaxation Iteration
                    //~ for (r=1; r<=Refine; r++)
                    //~ {
                        //~ for (c=(Refine*24+1); c<=NoOfCols; c++)
                        //~ {
                            //~ Coeff_Poissons_solver = (1.0/(rho[r][c]+rho[r][c+1]) + 1.0/(rho[r][c]+rho[r][c-1]) + 1.0/(rho[r+1][c]+rho[r][c]) +1.0/(rho[r][c]+rho[r-1][c]) ) / del_sq;
							//~ temp1 = Press[r][c+1]/(rho[r][c]+rho[r][c+1])		+	Press[r][c-1]/(rho[r][c]+rho[r][c-1]) ;
							//~ temp2 = Press[r+1][c]/(rho[r+1][c]+rho[r][c])		+	Press[r-1][c]/(rho[r][c]+rho[r-1][c]);


							//~ //Now Pressure
							//~ Press[r][c] 	=	Relaxation_param/Coeff_Poissons_solver * ((temp1+temp2)/del_sq	-	(Source_Poisson[r][c]))	+	(1.0-Relaxation_param)*Press[r][c] ;
							//~ sum_Press = sum_Press + Press[r][c];
							//~ NoOfCells = NoOfCells + 1;      //why to calculate this if we know this apriori? we may want to work with other domains where we want to make some cells dead.
			//~ }
                    //~ }
			
		      //~ for (r=(Refine+1); r<=NoOfRows; r++)
                    //~ {
                        //~ for (c=1; c<=NoOfCols; c++)
                        //~ {
                            //~ Coeff_Poissons_solver = (1.0/(rho[r][c]+rho[r][c+1]) + 1.0/(rho[r][c]+rho[r][c-1]) + 1.0/(rho[r+1][c]+rho[r][c]) +1.0/(rho[r][c]+rho[r-1][c]) ) / del_sq;
							//~ temp1 = Press[r][c+1]/(rho[r][c]+rho[r][c+1])		+	Press[r][c-1]/(rho[r][c]+rho[r][c-1]) ;
							//~ temp2 = Press[r+1][c]/(rho[r+1][c]+rho[r][c])		+	Press[r-1][c]/(rho[r][c]+rho[r-1][c]);


							//~ //Now Pressure
							//~ Press[r][c] 	=	Relaxation_param/Coeff_Poissons_solver * ((temp1+temp2)/del_sq	-	(Source_Poisson[r][c]))	+	(1.0-Relaxation_param)*Press[r][c] ;
				
							
			//~ }
                    //~ }						

   	
	r = 1;  
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] = u_ptr[INDEX(r,c,NC)];			//bottom neumann 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

							
							

	r = NR;  
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] = u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}


	
 for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = u_ptr[INDEX(r,1,NC)] ;				//left neumann
	 u_ptr[INDEX(r,NC+1,NC)] = u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
  
  double temp1,temp2;   


double Relaxation_param = alpha;
	//~ printf("\n");
		for (r=1; r<=NR; r++)
		{
                        for (c=1; c<=NC; c++)
                        {
				
				  Coeff_Poissons_solver = (1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
				temp1 = u_ptr[INDEX(r,c+1,NC)]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + u_ptr[INDEX(r,c-1,NC)]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) ;
				temp2 = u_ptr[INDEX(r+1,c,NC)]/ (rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) + u_ptr[INDEX(r-1,c,NC)]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ;
	    
	    
				u_ptr[INDEX(r,c,NC)] = Relaxation_param/Coeff_Poissons_solver * ( (temp1+temp2)/h_sq - f[INDEX(r,c,NC)] ) + (1.0-Relaxation_param)*u_ptr[INDEX(r,c,NC)];
				
				
				temp1 = 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) ;//+ 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)])  ;
				//~ if (NR==4)
				//~ {printf("%lf\t",u_ptr[INDEX(r,c,NC)]);}
			}
			//~ printf("\n");
		}
		//~ printf("*****uptr****");
		//~ exit(0);

 
 	
	r = 1;  
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] = u_ptr[INDEX(r,c,NC)];			//bottom outflow 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

							
							

	r = NR;  
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] = u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}


	
 for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = u_ptr[INDEX(r,1,NC)] ;				//left neumann
	 u_ptr[INDEX(r,NC+1,NC)] =  u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
  
  



  return u_ptr;
}
/*************************************************************************/
 void Residue_grid ( double *f, double *u_ptr,double *Residue, int NR,int NC, double *rho_private, double h ) 
{
	 //~ Residue_grid (info_array,f_ptr,u_ptr,Res,NR,NC,rho,h); 

	
	

  int r,c;
double temp1,temp2,temp3,temp4;	

	


  //~ printf("\n");
	for ( r = 1; r <= NR; r++ )
	{
	      for ( c = 1; c <= NC; c++ )
	      {
		      
				temp1 = (u_ptr[INDEX(r,c+1,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]);
				temp2 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r,c-1,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]);
				temp3 = (u_ptr[INDEX(r+1,c,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]);
				temp4 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r-1,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]);
		      
		       //(1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
	    
			Residue[INDEX(r,c,NC)] =
			
			f[INDEX(r,c,NC)] -  ( temp1 - temp2 + temp3 - temp4)/pow(h,2);      
			  //~ if (my_rank == 0)
			//~ {
				//~ printf("%lf\t",Residue[INDEX(r,c,NC)]);
			//~ }
		      //~ if (fabs(Residue[INDEX(i,j)])>Send_Residue[0])
		      //~ {
			      //~ Send_Residue[0] = fabs(Residue[INDEX(i,j)]);
		      //~ }
				//printf("%lf\t",Residue[INDEX(i,j,NC)] );
		}
		
		//~ printf("\n");
	}
	//~ printf("*****Res******");
//~ exit(0);

}

/*************************************************************************/
 double Calculate_Residue ( double *f , double *u_ptr, int NR,int NC,double *rho_private,double h) 
{
	

	double temp1,temp2,temp3,temp4;	
int r,c;
double  Residue;	




	double  Res_inf_norm=0;
	
	
	
	
	
	
	for (r=1; r<=NR; r++)
	{
		for (c=1; c<=NC; c++)
		{
		      
				temp1 = (u_ptr[INDEX(r,c+1,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]);
				temp2 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r,c-1,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]);
				temp3 = (u_ptr[INDEX(r+1,c,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]);
				temp4 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r-1,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]);
		      
		       //(1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
	    
			Residue =
			
			f[INDEX(r,c,NC)] -  ( temp1 - temp2 + temp3 - temp4)/pow(h,2);      
		      
		          if (fabs(Residue)>Res_inf_norm)
			{
			      Res_inf_norm = fabs(Residue);
			}
			  //~ if (my_rank == 0)
			//~ {
				//~ printf("\n%lf",Residue[INDEX(i,j)]);
			//~ }
		      //~ if (fabs(Residue[INDEX(i,j)])>Send_Residue[0])
		      //~ {
			      //~ Send_Residue[0] = fabs(Residue[INDEX(i,j)]);
		      //~ }
				//printf("%lf\t",Residue[INDEX(i,j,NC)] );
		}
		
		//printf("\n");
	}
	


	
	
	return Res_inf_norm;
}


void Restrictor ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
	

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							Res_hat[INDEX(r,c,NC)] = 0.25*(Res[INDEX(2*r-1,2*c-1,2*(NC))] + Res[INDEX(2*r-1,2*c,2*(NC))] + Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ Res[INDEX(2*r,2*c,2*(NC))]);
				
							//~ rho_hat[INDEX(r,c,NC)] = 0.25*(rho[INDEX(2*r-1,2*c-1,2*(NC))] + rho[INDEX(2*r-1,2*c,2*(NC))] + rho[INDEX(2*r,2*c-1,2*(NC))] 
										//~ + rho[INDEX(2*r,2*c,2*(NC))]);
				//~ if (my_rank==1)
				//~ printf("%lf\t",Res_hat[INDEX(r,c,NC)] );
							
			}
			//~ if (my_rank==1)
			//~ printf("\n");
		}
		//~ if (my_rank==1)
		//~ printf("************************************\n");
		
		//~ exit(0);
		
		
}

void Restrictor_rho ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
		
	double temp;

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							temp = 0.25*(1./Res[INDEX(2*r-1,2*c-1,2*(NC))] + 1./Res[INDEX(2*r-1,2*c,2*(NC))] + 1./Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ 1./Res[INDEX(2*r,2*c,2*(NC))]);
							
							Res_hat[INDEX(r,c,NC)] = 1./temp;
							//~ rho_hat[INDEX(r,c,NC)] = 0.25*(rho[INDEX(2*r-1,2*c-1,2*(NC))] + rho[INDEX(2*r-1,2*c,2*(NC))] + rho[INDEX(2*r,2*c-1,2*(NC))] 
										//~ + rho[INDEX(2*r,2*c,2*(NC))]);
				//~ if (my_rank==1)
				//~ printf("%lf\t",Res_hat[INDEX(r,c,NC)] );
							
			}
			//~ if (my_rank==1)
			//~ printf("\n");
		}
		//~ if (my_rank==1)
		//~ printf("************************************\n");
		
		//~ exit(0);
		
		
}

double *prolongator( double *u_hat, double *uCorr, double *u_ptr, int NR, int NC)
{
	
	int i,r,c;
	

		

	  for (r=0; r<NR; r++)
                    {
                        for (c=0; c<NC; c++)
                        {
				uCorr[INDEX(2*r+1,2*c+1,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r,c+1,NC)] + 3*u_hat[INDEX(r+1,c,NC)] + u_hat[INDEX(r,c,NC)]);
				
				
				uCorr[INDEX(2*r+1,2*c+2,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+1,c+2,NC)] + 3*u_hat[INDEX(r,c+1,NC)] + u_hat[INDEX(r,c+2,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+1,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+2,c+1,NC)] + 3*u_hat[INDEX(r+1,c,NC)] + u_hat[INDEX(r+2,c,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+2,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+1,c+2,NC)] + 3*u_hat[INDEX(r+2,c+1,NC)] + u_hat[INDEX(r+2,c+2,NC)]);
				//uCorr[2*r+1][2*c+2] =  0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+1][c+2] + 3*u_hat[r][c+1] + u_hat[r][c+2]);
				//uCorr[2*r+2][2*c+1] = 0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+2][c+1] + 3*u_hat[r+1][c] + u_hat[r+2][c]); 
				//uCorr[2*r+2][2*c+2] = 0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+1][c+2] + 3*u_hat[r+2][c+1] + u_hat[r+2][c+2]);
				
					
				
			}
		}
		
		

		   for (r=1; r<=2*NR; r++)
                    {
				for (c=1; c<=2*NC; c++)
				{	
				
					u_ptr[INDEX(r,c,2*NC)] = uCorr[INDEX(r,c,2*NC)] + u_ptr[INDEX(r,c,2*NC)];
					
								
				}
			
		}
	
	return u_ptr;	
		
}


	 	 
/********************************V-Cycle**********************************************/
double *Vcycle(int NR,int NC, double *u_init, double *f_ptr, int iter, int flag,double *rho,double h)
{
	double *Res,*Res_hat,*u_corr_ptr,*u_ptr,*uCorr,*u_hat,*rho_hat;
	//int NoOfRows_coarser,NoOfCols_coarser;
	int i; 
	int r,c;
	
	
	
	if (flag==1)
	{
		u_ptr = u_init;
			
		//rho_hat = rho;
		flag=0;
		//printf("haha");
		//fflush(stdout);
		//exit(0);
		
		//printf("flag = %d\n",flag);
	}
	else
		{
		
		u_ptr  =  (double *)calloc((NR+2) * (NC+2),sizeof(double*));   //( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
			
		//flag=0;
			
	}
	
	
	for (i=0;i<iter;i++)
	{
		
			
			u_ptr = jacobi (f_ptr,u_ptr,NR,NC,rho,h );			//void jacobi ( int num_procs, double f[], double *u_ptr, double *u_new_ptr, int NR, int NC ) 
		
		
		
	}
	
	

		
	
			
	
	
	if(NR<=1 || NC<=1 )
	{
		
		return u_ptr;
	}
	
	Res  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
		
	uCorr  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );	// correction from coarser grids		
	
	
	 Residue_grid (f_ptr,u_ptr,Res,NR,NC,rho,h);  // double *Residue_grid (int num_procs, double *f, double *u_ptr ) 
	
	
	
			NR = (NR/2); //keep this here
			NC =  (NC/2);
			h=2*h;
			
			
			Res_hat = (double *)calloc((NR+2)*(NC+2),sizeof(double));
		
			rho_hat  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
			
	
			 Restrictor(Res,Res_hat,NR,NC);
			
			 Restrictor_rho(rho,rho_hat,NR,NC);
			 
			 
			
	
	
			
			u_hat = Vcycle( NR, NC, u_init, Res_hat, iter,flag,rho_hat,h);
		
			
			
			
			
			u_ptr = prolongator( u_hat, uCorr, u_ptr, NR, NC);
			
		
			
			
		
		for (i=0;i<iter;i++)
		{
			u_ptr = jacobi (f_ptr,u_ptr,2*NR,2*NC,rho,h/2 );
			
		}
	
	
	
		
			free(u_hat);
			free(rho_hat); 
		
		
			free(Res);
			free(uCorr); 
			
			free(Res_hat);
			
			
			
			
			
			
			
			
			
			return u_ptr;
}


/**************/
