/***********START OF USER INPUT***********************************/
#define nodata (double) 1.0e+30
#define HUGE (double) 1.0e+30
#define implicity (double)  0.5
//multigrid
/*****************GRID INPUT**********************************************/
int NoOfRows  = -1;
int NoOfCols 	= -1;
/*******************************************************************/
int cells_prost_not=0;
/******************LVIRA INPUT****/
int Initial_guess_normal=1;  // 0 for green-guass and 1 for mixed heights 2 for afkami 
bool LVIRA=1;		
bool VOF_Module =1;	// 0 for OFF, 1 for ON // Needed for one phase flows
bool weymouth =1; // 0 for lorstad (failed oscillating droplet test) and 1 for weymouth
bool Remove_droplets = 1; // avoid find quad error if an isolated interfacial cell is found
bool create_restore_point=1; // restore simulation default on, will create a restore point
bool restore=0; // default zero, if one it will look for restore point file and will restore 
#define Rotation_StepSize 	(float) 0.00031
bool volfrac_gen=1; // keep default one, older volfrac has some bugs (viscous planar droplet test fail)
bool contact=1;
double contact_angle=M_PI/6;	// radians
/*********************************/

/**************************/
#define PI        			(float)  	3.14159265358
double  UPPER_LIMIT = 	1.0-1.0e-10;
double LOWER_LIMIT  = 	1.0e-10;
double CFL		 = 0.25;		// Do not raise above 0.5, VOF fluxing will not be conserve in Weymouth implementation
#define delT_Tolerance 		(float)		0.0156250


/**************************/

/**************NAVIER STOKES INPUT********************/
bool NS_Module =1;	// Needed for running VOF only tests
/***************Switches************************************/
bool Advection_term =1;	// 0 to OFF, 1 to ON
bool Diffusion_term	 =1;	// 0 to OFF, 1 to ON
bool Diffusion_explicit	 =0;	// 0 to OFF, 1 to ON



int Linear_Solver = 1;		// 1 for multigrid(only for domains with even powered grids) and2 for GS, 3 for hypre 
int inc_roll_back=1;
int roll_back=pow(2,1); 		// used in multigrid V cycle, roll_back is the coarsest grid you want to go default is one
int Advection_Scheme =	6;	// 1 for FOU, 2 for CD,  3 for SOU, 4 for QUICK
								//5 for WENO
int axi = 0;				//axisymmetric	mode - 1 for ON, 0 for OFF(Planar) default is zero
bool Progress=0; // No simulation progress on terminal
int ST_Module=1; // 1 for DAC and 2 for basilisk surface tension
double prost_tolerance=0.15;
//~ bool LROST=0;  // switch on only one of them 
/******************************************************/
		

//Physical Params
//~ double SourceU =0;
//~ double SourceV=0;
double rho_G =1.0;
double rho_L	=	1.0;
double mu_G	=	0;
double mu_L	=	0;
double Sigma =	0;
//Numerical Params for Poisson solver
double tolerance	= 	1e-7;
// divergence of velocity 
#define alpha_bcs				(float)	1.0		//don't forget this, 
										//it effects th SOR poissons solver




/*############################THE CODE BELOW DOES NOT REQUIRE ANY USER ENTRY#################################*/

typedef struct				/*2D STRUCT STORES THE ATTRIBUTES FOR A CELL*/
   {
     /***************************CELL ATTRIBUTES - LVIRA******************************************/
       double  VolFrac;			//VOLUME FRACTION FOR EACH CELL      
       double  Area;			//THE AREA CALCULATED FOR EACH CELL DURING INTERFACE RECONSTRUCTION
       double  Nx;				//X-COMPONENT OF THE UNIT NORMAL TO THE LINE
       double  Ny;				//Y-COMPONENT OF THE UNIT NORMAL TO THE LINE
       double  P;				//PERPENDICULAR DISTANCE OF THE LINE FROM THE CENTER OF THE CELL
      char         Shape[30];       //SHAPE OF THE AREA BASED ON FITIING A STRAIGHT LINE
       double  Theta;			//ANGLE MADE BY THE LINE WITH THE NEGATIVE DIRECTION OF X-AXIS
	  //~ int          Type;            //TYPE OF CELL - 1. MIXED 2. ACTIVE 3. ISOLATED
	   double  delV;            //VOLUME OF THE CELL - REQUIRED FOR BOOKKEPING DURING ADVECTION
	   double  al;  			// alpha variable forward and backward relations
	   double ext_volfrac;
	   //~ double para_k;
	   //~ double para_theta;
	   
     /********************************************************************************************/  

     /*****CELL ATTRIBUTES - MOMENTUM EQUATION***********/ 
       //~ double Press;		Not needed 
     /***************************************************/    

   } STRUCT_CELL_ATTRIB;

//STRUCT_CELL_ATTRIB Cellattrib[NoOfRows+2][NoOfCols+2]; /*CREATING AN ARRAY OF STRUCTURES WHERE EACH ELEMENT OF THE ARRAY IS OF THE TYPE STRUCT_CELL_ATTRIB*/
STRUCT_CELL_ATTRIB **Cellattrib; /*CREATING AN ARRAY OF STRUCTURES WHERE EACH ELEMENT OF THE ARRAY IS OF THE TYPE STRUCT_CELL_ATTRIB*/
STRUCT_CELL_ATTRIB **solid; /*CREATING AN ARRAY OF STRUCTURES WHERE EACH ELEMENT OF THE ARRAY IS OF THE TYPE STRUCT_CELL_ATTRIB*/


/*X_VELOCITY[2][3] IS THE HORIZONTAL VELOCITY THROUGH THE LHS WALL OF THE CELL[2][3]
  Y_FLUX[2][3] IS THE VERTICAL FLUX THROUGH THE BOTTOM WALL OF THE CELL[2][3]

//~ /****************WALL ATTRIBUTES***************************************************************/
 //~ double  X_Velocity[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Velocity_star[NoOfRows+2][NoOfCols+2];  //X-COMPONENT OF TEMPORARY VELOCITY AT THE LHS OF A CELL
 //~ double  Y_Velocity_star[NoOfRows+2][NoOfCols+2];  //Y-COMPONENT OF TEMPORARY VELOCITY AT THE BOTTOM OF A CELL
 //~ double  X_Flux[NoOfRows+2][NoOfCols+2];      //THE X FLUX AT THE LHS OF A CELL
 //~ double  Y_Flux[NoOfRows+2][NoOfCols+2];      //THE Y FLUX AT THE BOTTOM OF A CELL
//~ /************************************************************************************************/

    //~ double mu[NoOfRows+2][NoOfCols+2]; 		// VISCOSITY AT CENTER OF THE CELL
 //~ double rho[NoOfRows+2][NoOfCols+2]; 		// DENSITY AT CENTER OF THE CELL
 //~ double Press[NoOfRows+2][NoOfCols+2]; 	// PRESSURE AT CENTER OF THE CELL
   
   /****************WALL ATTRIBUTES***************************************************************/
double **X_Velocity;
double  **Y_Velocity;  //Y-COMPONENT OF VELOCITY AT THE BOTTOM OF A CELL
double **X_VelC;	// X velocity at the center of the cell
double  **Y_VelC;  //Y velocity at the center of the cell
double  **X_Velocity_imp,**Y_Velocity_imp; 
 double  **X_Velocity_star;  //X-COMPONENT OF TEMPORARY VELOCITY AT THE LHS OF A CELL
 double  **Y_Velocity_star;  //Y-COMPONENT OF TEMPORARY VELOCITY AT THE BOTTOM OF A CELL
 double  **X_Flux;      //THE X FLUX AT THE LHS OF A CELL
 double  **Y_Flux;      //THE Y FLUX AT THE BOTTOM OF A CELL

 double **ST_Fx,**ST_Fy; // surface tension forces in the x and y directions 
 
 double **Xp1,**Yp1,**Xp2,**Yp2; // coordinates of lines of the interface
 
 double **x,**y,**ht_x,**ht_y;
 double **SourceU, **SourceV;
 

 
 

//~ /************************************************************************************************/

  double **mu; 		// VISCOSITY AT CENTER OF THE CELL
 double **rho; 		// DENSITY AT CENTER OF THE CELL
 double **Press; 	// PRESSURE AT CENTER OF THE CELL
 double **vorticity,**streamfunction,**vel_potential;	// vorticity,streamfunction(Miles, not Stokes) at the center of the cell, only computed for output if needed 
 double *RF;
  double *eta; 	// PRESSURE AT CENTER OF THE CELL
	double **phi ; //AN INTERFACE FIELD VARIABLE
	double **cc ; //Weymouth variable
	double **ccs ; //Weymouth variable
	double **kappa;
/****************GLOBAL VARIABLES****************/
 double del_sq;
 double del_sqBy2;
 double del2;
 double del3;
 double delBy2;
 double PiBy2;
 double PiBy4;
 double ThreePiBy2;
 double delTBydel;
 double delTdel;
 double del=-1; 
 double Forced_delT=-1;
 
 //flagg
// put this line in init_vars del = (double)1./NoOfCols;
 double delT=nodata;

 double Time;	// current actual time for StepT
int StepT=0;	// current computational time 
int istep = 1;		//gerris like computational time step
double step = -1;		// gerris like acutal time step
int Write_Count;  // counting the number of files writing
double Write_Time;
double end = -1;		// gerris like actual end time 
double Start_T=0;
char passname[100]; // pass filename to restart the simulation
double alpha_max=0.96; 	



/**FUNCTION DECLARATIONS*/
void        Pass_main(int, int);
void 	CFL_Check();
void		InterfaceRecon_main();
void		Recon_Interface(int, int);
//~ void		Write_struct_Cellattrib(int, int,   double,   double,   double,   double,   double, char[],   double, int, double);
void		Init_GlobalVars();
void		Calculate_InitialSlope();
void		Calculate_distance(int, int);
//~ void		Identify_Shape(int, int);
void		Extrapolate_Line(int, int);
void		IdentifyCell_AssignArea(int, int, int, char[],   double);
  double Calculate_SumOfDiffSq(int, int);
int		Find_Quad(int, int);
void		Calculate_Theta(int, int);
void		Rotate_Line(int, int, char[]);
void 	plot_lines();
void		AdvanceF_field(int, int, int, int);
void		CalculateF_Flux(int);
  double CalculateX_Flux(char[], char[],   double,   double,   double,   double,   double,   double,   double,   double,   double, double, double, double);
  double CalculateY_Flux(char[], char[],   double,   double,   double,   double,   double,   double,   double,   double,   double, double, double, double);
void		AssignVal_FluxVariables(int, int,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *,   double *);
 double Calculate_Area(double, double, double, double);
void        BookKeeping();
/*************************/
// Navier Stokes Functions
void Advance_Velocity_Field();
void Update_Fluid_Properties();

void Apply_Default_BoundaryConditions();
  double Ad_Flux_Calc(  double ,   double ,   double ,   double, double,double,double,double );

void Calculate_X_Velocity_star();
void Calculate_Y_Velocity_star();
void Poissons_Solver_SOR();
void Poissons_Solver_hypre();
void X_Velocity_Correction();
void Y_Velocity_Correction();

void freeArray(double **) ;
// multigrid routines 
void Poissons_Solver_Multigrid();

 
	double *Vcycle_Multigrid(int  , double * , double *, double * ,int ) ;

// Surface tension 
void Calculate_Surface_Tension_Height_Function_DAC();
double CLROST(int ,int );


// max min functions
double max(double, double);
double min(double, double);
// VolFrac Generation routines

double volumeFraction(double , double , double , double ,double , double , double , double , int );
double volumeFraction_old(double , double , double , double , int, bool );

double Init_F(double ,double ) ;
double Init_solid(double ,double ) ;
double Init_U(double , double );
double Init_V(double , double );
double Init_P(double , double );
void Pressure_BC();
void Initialise_VolFrac(bool);
void Initialise_VolFrac_old(bool);
void event_phy(double);
void event_init();
void event_bc();
void event_cp();	
void Calculate_XY_Velocity_star_implicit();
double Get_distance(int, int, int , char) ;
/************************************output functions*****************************************/
void	 interface_data(double);
void tecplot_data(double);
void field_data(double);
double mycs(int,int,bool);
double max(double a, double b)
{
	if (a>=b)
	{
		return a;
	}
	else if (b>a)
	{
		return b;
	}
	else
	{
		printf("\nError in max a = %e, b = %e\n",a,b);
		exit(0);
	}
}	
double min(double a, double b)
{
	if (a<=b)
	{
		return a;
	}
	else if (b<a)
	{
		return b;
	}
	else
	{
		printf("\nError in min  a = %e, b = %e\n",a,b);
		exit(0);
	}
}	
double sign(double a)
{
	if (a<0)
		return -1;
	else if (a>0)
		return 1;
	else
		return 0;
}

double absolute(double a)
{
	if (a<0)
		return -a;
	else if (a>0)
		return a;
	else
		return 0;
}
void check_input();
void vorticity_to_velocity();

/*******************Boundary Conditions variables**********************************************/
// Note: nothing is default, provide BC explicitly

bool BcDirichlet_P_left=0, BcDirichlet_P_right=0,BcDirichlet_P_top=0,BcDirichlet_P_bottom=0;
double press_left=0,press_right=0,press_bottom=0,press_top=0;
bool BcNeumann_P_left = 0,  BcNeumann_P_right = 0,  BcNeumann_P_top = 0,  BcNeumann_P_bottom = 0;
//~ bool BcNeumann_F_left = 0,  BcNeumann_F_right = 0,  BcNeumann_F_top = 0,  BcNeumann_F_bottom = 0;
bool BcPeriodic_P_X=0, BcPeriodic_P_Y=0;
bool BC_Dirichlet_U_left=0, BC_Dirichlet_U_right=0, BC_Dirichlet_U_top=0, BC_Dirichlet_U_bottom=0; 
bool BC_Dirichlet_V_left=0, BC_Dirichlet_V_right=0, BC_Dirichlet_V_top=0, BC_Dirichlet_V_bottom=0; 
bool BC_Neumann_U_left=0, BC_Neumann_U_right=0, BC_Neumann_U_top=0, BC_Neumann_U_bottom=0; 
bool BC_Neumann_V_left=0, BC_Neumann_V_right=0, BC_Neumann_V_top=0, BC_Neumann_V_bottom=0; 
double v_vel_bottom=0, v_vel_top=0,v_vel_right=0, v_vel_left=0;
double u_vel_bottom=0, u_vel_top=0,u_vel_right=0, u_vel_left=0;


 
 
/*******************Macros*********************************************************/
#define interface(r,c) (Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
#define inter_length(r,c) pow( pow(Xp1[r][c] - Xp2[r][c],2) + pow(Yp1[r][c] - Yp2[r][c],2),0.5)
#define sq(x) (x*x)
#define sqrt(x) pow(x,0.5)
#define clamp(x,a,b) ((x) < (a) ? (a) : (x) > (b) ? (b) : (x))
#define swap(type,a,b) { type tmp = a; a = b; b = tmp; }