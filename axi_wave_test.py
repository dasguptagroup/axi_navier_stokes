import subprocess
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from math import factorial
import pylab



#~ subprocess.call(["gcc", "axi_wave.c","-I","/home/palas/Dropbox/bcsaxi/", "-lm"])
#~ tmp=subprocess.call("./a.out")

fig, ax = plt.subplots()
line, = ax.plot([], [], lw=2)
ax.grid()
xdata, ydata = [], []

#~ def run(time_step):
    # update the data
plt.cla()
#~ time_step = time_step/100
#~ filename = 'Plots/PlotLine_%g.dat'% time_step




filename = 'sim_axi_wave.dat'
xdata,ydata = np.loadtxt(filename,delimiter='\t',unpack=True)
ydata = ydata/0.01;
plt.plot(xdata, ydata,'r',label='BCS (DNS)')

filename = 'analytical_axi_wave.dat'
xAdata,yAdata = np.loadtxt(filename,delimiter='\t',unpack=True)
plt.plot(xAdata[1::2], yAdata[1::2],'k*',label='Farsoiya et. al.(2017)(Analytical)')

ax.set_xlabel('t',FontSize=20)
ax.set_ylabel(r'$\frac{a_k(t)}{a_k(0)}$',rotation=0,FontSize=20)
#~ plt.show()
ax.set_ylim(-1.2, 1.2)
ax.set_xlim(0, 30)

legend=plt.legend()
pylab.savefig('axi_capillary_wave.eps')



norm =np.absolute(yAdata-ydata)
norm = np.max(norm)

f = open('TestReports', 'w')

if (norm<=0.04):
	print('Axi Capillary Wave: PASS\n')
	f.write('Axi Capillary Wave: PASS')
else:
	print('Axi Capillary Wave: FAIL')
	f.write('Axi Capillary Wave: FAIL')