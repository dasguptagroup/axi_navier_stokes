#include "navier-stokes-planar.h"
double **XVel_old, **YVel_old;

int main(int argc,char *argv[])
{	
	rho_G 	=	1.0;
	mu_G	=	0.001;

	//~ rho_L	=	1.0;
	//~ mu_L	=	0.01;


	 //~ SourceV = -10.0;
	//Sigma = 1.0;
	
	Diffusion_term=1;
	//advection on or off
	Advection_term=1;
	//WENO
	Advection_Scheme = 5;
	//multigrid
	Linear_Solver=1;
	
	//no VOF module
	VOF_Module = 0;
	
	//switch off axi
	axi=0;
	//double k = 2*M_PI;
	//double omega = pow((pow(2*M_PI,3)/(rho_G+rho_L)),0.5);

	//~ end = 30;
	step=0.1;
	Progress=1;
			
	NoOfRows  = 128;
	NoOfCols 	= 128;
	del = 1./NoOfCols;		

			
/************User input above, no parameter should be below run_ns***********/	
	run_ns();
	
	return 0;	
	
}



// provide initial conditions below
double Init_F(double x, double y)
{	
	return -1;	
}

void event_phy(double Step)
{
	
	//~ FILE *ptrW;
	//~ if(StepT==0)
	//~ {
		//~ ptrW = fopen("amp.dat","w");
	//~ }
	//~ else
	//~ {	
		//~ ptrW = fopen("amp.dat","a");
	//~ }
	
	
	//~ int r=1,c;
	//~ double x0,y0;
        
	//~ for ( r = 1; r <= NoOfRows; ++r) {
		//~ for ( c = 1; c <= NoOfCols; ++c) {
			//~ if(interface(r,c))
			//~ {
				//~ if (absolute(Xp1[r][c]-0.)<1.e-3)
				//~ {
					//~ fprintf(ptrW,"%e\t%e\n",Time,Yp1[r][c]-bessel_prime_zero*0.5);
					//~ break;
				//~ }
				//~ else if (absolute(Xp2[r][c]-0.)<1.e-3)
				//~ {
					//~ fprintf(ptrW,"%e\t%e\n",Time,Yp2[r][c]-bessel_prime_zero*0.5);
					//~ break;
				//~ }
			//~ }
		//~ }
	//~ }
     
	//~ fclose(ptrW);	
	
}

void event_init()
{
	
			XVel_old = (double **)calloc(NoOfRows+2,sizeof(double*));
			YVel_old = (double **)calloc(NoOfRows+2,sizeof(double*));
			for (int i=0; i<NoOfRows+2; i++)
			{
				XVel_old[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				YVel_old[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}
			
		for(int r=0;r<NoOfRows+2;r++){
		for(int c=0;c<NoOfCols+2;c++){
			XVel_old[r][c]  = 1234.0;
			YVel_old[r][c]  = 1234.0;
		}		
	}	
}
	
void event_bc()
{
	//left 
	BcDirichlet_U("left",0);
	BcDirichlet_V("left",0);
	//BcNeumann_F("left");
	BcNeumann_P("left");
	
	//right
	BcDirichlet_U("right",0);
	BcDirichlet_V("right",0);
	//BcNeumann_F("right");
	BcNeumann_P("right");
	
	//bottom
	BcDirichlet_V("bottom",0);
	BcDirichlet_U("bottom",0);
	//BcNeumann_F("bottom");
	BcNeumann_P("bottom");
	
	//top
	BcDirichlet_V("top",0);
	BcDirichlet_U("top",1);
	//BcNeumann_F("top");
	BcNeumann_P("top");

	
		
}


void event_cp()
{
	double velNormXMax,velNormYMax,velNormX,velNormY, maxVel;
	int r,c;
	
	velNormXMax = 0.0;
	velNormYMax = 0.0;
	for(r=0;r<NoOfRows+2;r++){
		for(c=0;c<NoOfCols+2;c++){
			velNormX = fabs(XVel_old[r][c] - X_Velocity[r][c]);
			velNormY = fabs(YVel_old[r][c] - Y_Velocity[r][c]);
			
			if(velNormX > velNormXMax)
				velNormXMax = velNormX;
			
			if(velNormY > velNormYMax)
				velNormYMax = velNormY;
			
		}
	}
	
	for(r=0;r<NoOfRows+2;r++){
		for(c=0;c<NoOfCols+2;c++){
			XVel_old[r][c] = X_Velocity[r][c];
			YVel_old[r][c] = Y_Velocity[r][c];
		}
	}	
	
	maxVel = max(velNormXMax,velNormYMax);
	printf("\n%e",maxVel);
	
	if(maxVel < 1.0e-7){
		
		FILE *ptrW;
		 ptrW = fopen("sim_ldc_u_Re1000.dat","w");
		
		for (r=1;r<NoOfRows+1;r++)
		{
			fprintf(ptrW,"%e\t%e\n",(r-0.5)*del,X_Velocity[r][NoOfCols/2+1]); // X_Velocity is defined at the centerline faces
		}
		fclose(ptrW);
		
		ptrW = fopen("sim_ldc_v_Re1000.dat","w");
		
		for (c=1;c<NoOfCols+1;c++)
		{
			fprintf(ptrW,"%e\t%e\n",(c-0.5)*del,0.5*(Y_Velocity[NoOfRows/2+1][c] + Y_Velocity[NoOfRows/2][c] ));
		}
		fclose(ptrW);
		//~ field_data(Time);
		exit(0);
	}
}
	
double Init_U(double x, double y)
{
return 0;
	
}

double Init_V(double x, double y)
{
	return 0;
	
}

double Init_P(double x, double y)
{
	return 0;
	
}


	