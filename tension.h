#include "prost_popinet.h"
#include "height_function_popinet.h"
#include "height_function.h"


void Calculate_Surface_Tension_Height_Function_DAC()
{
	
	

int r,c; // r_temp, c_temp;	
//~ double HF_0, HF_1, HF_2, Fx, Fxx, Y_RC, Y_Rp1C, X_RC, X_RCp1;
//double err_new, norm;
		int sign_normal=1;
	
		

	double  Grad_VolFrac_X, Grad_VolFrac_Y;
		double **kappa_f_x, **kappa_f_y;
		
		kappa_f_x = (double **) calloc(NoOfRows+2,sizeof(double *));
		kappa_f_y = (double **) calloc(NoOfRows+2,sizeof(double *));

			for (int i=0; i<NoOfRows+2; i++)
			{
				
				kappa_f_x[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa_f_y[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}

	

		for (r=0;r<=NoOfRows+1;r++)
		{
			for (c=0;c<=NoOfCols+1;c++)
			{	

				//~ kappa[r][c] = 0;
				kappa[r][c] = nodata;
				ht_x[r][c] = nodata;
				ht_y[r][c] = nodata;
				
			}
		}
//   *******************

		if (ST_Module==1)
		{ 	
			for (r=1;r<=NoOfRows;r++)
			{
				for (c=1;c<=NoOfCols;c++)
				{
					
					//~ if (contact && contact_cell(r,c) )
					//~ {
						//~ if (Cellattrib[r][c].Nx>0){
							
							//~ kappa[r][c]=calculate_kappa_DAC(r,c,2);
						//~ }
						//~ else if (Cellattrib[r][c].Nx<0){
							//~ kappa[r][c]=calculate_kappa_DAC(r,c,4);
						//~ }
					//~ }
					//~ else if (contact && adjacent_cell(r,c) )
					//~ {
						//~ if (Cellattrib[r][c].Nx>0){
							
							//~ kappa[r][c]=calculate_kappa_DAC(r,c,2);
						//~ }
						//~ else if (Cellattrib[r][c].Nx<0){
							//~ kappa[r][c]=calculate_kappa_DAC(r,c,4);
						//~ }
					//~ }
					
					//if (absolute(Grad_VolFrac_X)>0 || absolute(Grad_VolFrac_Y)>0)
					 if (interface(r,c))
					{
						int orient=0;
						int orient_up=0,orient_down=0,orient_right=0,orient_left=0;
						
						orient = find_orientation(r,c);  // find orientation of the normal 
						orient_up = find_orientation(r+1,c);  // find orientation of the normal 
						orient_down = find_orientation(r-1,c);  // find orientation of the normal 
						orient_right = find_orientation(r,c+1);  // find orientation of the normal 
						orient_left = find_orientation(r,c-1);  // find orientation of the normal 
						
						if (orient==0){
							kappa[r][c]=nodata;
						}
						if (orient_up!=0 && orient!=orient_up) 
						{
							kappa[r][c]=0.5*(calculate_kappa_DAC(r,c,orient) + calculate_kappa_DAC(r,c,orient_up) );
							
							if (absolute(kappa[r][c])>0.1*nodata){
								kappa[r][c]=nodata;
							}
								
						}	
						else if (orient_down!=0 && orient!=orient_down)
						{
							kappa[r][c]=0.5*(calculate_kappa_DAC(r,c,orient) + calculate_kappa_DAC(r,c,orient_down) );
							if (absolute(kappa[r][c])>0.1*nodata){
								kappa[r][c]=nodata;
							}
						}							
						else if (orient_right!=0 && orient!=orient_right)
						{
							kappa[r][c]=0.5*(calculate_kappa_DAC(r,c,orient) + calculate_kappa_DAC(r,c,orient_right) );
							if (absolute(kappa[r][c])>0.1*nodata){
								kappa[r][c]=nodata;
							}
						}							
						else if (orient_left!=0 && orient!=orient_left)
						{
							kappa[r][c]=0.5*(calculate_kappa_DAC(r,c,orient) + calculate_kappa_DAC(r,c,orient_left) );
							if (absolute(kappa[r][c])>0.1*nodata){
								kappa[r][c]=nodata;
							}
						}
						
						else
						{							
							kappa[r][c]=calculate_kappa_DAC(r,c,orient);
							if (absolute(kappa[r][c])>0.1*nodata){
								kappa[r][c]=nodata;
							}
						}
					
					
					}
					
					 
					
				}
				
				
			}
	
		}
		else if(ST_Module==2)
		{
			 calculate_kappa_popinet();
			
			
			
		}
		
	//calculate kappa on face centers
	//~ for (r=1;r<=NoOfRows;r++)
	//~ {
		//~ for (c=1;c<=NoOfCols;c++)
		//~ {
			//~ if (interface(r,c) && kappa[r][c]==nodata)
			//~ {
				//~ double kf=0; int b=0;
				//~ for (int j=-1;j<=1;j++)
				//~ {
					//~ for (int i=-1;i<=1;i++)
					//~ {
						//~ if (kappa[r+j][c+i]<nodata)
						//~ {
							//~ kf+= kappa[r+j][c+i];
							//~ b++;
						//~ }
					//~ }
				//~ }
				//~ if (b>0)
				//~ { 
					//~ kappa[r][c]=kf/b;
				//~ }
				
				//~ if (kappa[r][c]==nodata)
				//~ {
					//~ kappa[r][c]=centroids_curvature_fit (r, c);
				//~ }
					
			//~ }
		//~ }
	//~ }
			
	//~ fclose(file);
//~ exit(0);


	//Symmetry for kappa at boundaries
	for (r=1;r<=NoOfRows;r++)
	{
		kappa[r][0]=kappa[r][1];
		kappa[r][NoOfCols+1]=kappa[r][NoOfCols];
	}
	for (c=1;c<=NoOfCols;c++)
	{
		kappa[0][c]=kappa[1][c];
		kappa[NoOfRows+1][c]=kappa[NoOfRows][c];
	}
	//calculate kappa on face centers
	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols+1;c++)
		{
			kappa_f_y[r][c] = (kappa[r][c]<nodata && kappa[r-1][c]<nodata) ? 0.5*(kappa[r][c] + kappa[r-1][c]) :
							kappa[r][c] < nodata ? kappa[r][c] : kappa[r-1][c] < nodata ? kappa[r-1][c] : 0.;
			
			
			kappa_f_x[r][c] = (kappa[r][c]<nodata && kappa[r][c-1]<nodata) ? 0.5*(kappa[r][c] + kappa[r][c-1]) :
							kappa[r][c] < nodata ? kappa[r][c] : kappa[r][c-1] < nodata ? kappa[r][c-1] : 0. ;
			//~ if (interface(r,c))
			//~ double kf=0; int b=0;
			//~ for (int j=-1;j<=0;j++)
			//~ {
				//~ for (int i=-1;i<=1;i++)
				//~ {
					//~ if (kappa[r+j][c+i]<nodata)
					//~ {
						//~ kf+= kappa[r+j][c+i];
						//~ b++;
					//~ }
				//~ }
			//~ }
			
			//~ kappa_f_y[r][c] = kf/b;
			
			
			 //~ kf=0;  b=0;
			//~ for (int j=-1;j<=1;j++)
			//~ {
				//~ for (int i=-1;i<=0;i++)
				//~ {
					//~ if (kappa[r+j][c+i]<nodata)
					//~ {
						//~ kf+= kappa[r+j][c+i];
						//~ b++;
					//~ }
				//~ }
			//~ }
			
			//~ kappa_f_x[r][c] = kf/b;
			
			//~ /*****************************at horizontal faces ******************************************/

			//~ if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
			//~ {

				//~ if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				//~ {

					//~ kappa_f_y[r][c] = 0.5*(kappa[r][c] + kappa[r-1][c]);
				//~ }
				//~ else
				//~ {
					//~ kappa_f_y[r][c] = kappa[r-1][c]; // assign the face left kappa
				//~ }


			//~ }


				//~ if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				//~ {
					//~ if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
					//~ {

						//~ // do nothing
					//~ }
					//~ else
					//~ {
						//~ kappa_f_y[r][c] = kappa[r][c]; // assign the face left kappa
					//~ }


				//~ }
			//~ /******************************* kappa at vertical faces ***************************************/
				//~ if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
				//~ {

					//~ if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
					//~ {

						//~ kappa_f_x[r][c] = 0.5*(kappa[r][c] + kappa[r][c-1]);
					//~ }
					//~ else
					//~ {
						//~ kappa_f_x[r][c] = kappa[r][c-1]; // assign the face left kappa
					//~ }


				//~ }


				//~ if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				//~ {
					//~ if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
					//~ {

						//~ // do nothing
					//~ }
					//~ else
					//~ {
						//~ kappa_f_x[r][c] = kappa[r][c]; // assign the face left kappa
					//~ }


				//~ }

			   //~ // if (kappa_f_x[r][c]!=0)
			   // printf("%d\t%d\t%lf\t%lf\t%lf\t%lf\n",r,c,kappa_f_x[r][c],Cellattrib[r][c].VolFrac,kappa[r][c],kappa[r][c-1]);	
		}
	}

	

	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{
			//~ if(Cellattrib[r][c].VolFrac!=Cellattrib[r][c-1].VolFrac)
			//~ {
				ST_Fx[r][c] = Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r][c-1].VolFrac)*(kappa_f_x[r][c]);
			//~ }
			//~ if (Cellattrib[r][c].VolFrac!=Cellattrib[r-1][c].VolFrac)
			//~ {
				ST_Fy[r][c] = Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r-1][c].VolFrac)*(kappa_f_y[r][c]);
			//~ }

		}
	}


	//~ freeArray(kappa);
	freeArray(kappa_f_x);
	freeArray(kappa_f_y);
	//exit(0);
}

