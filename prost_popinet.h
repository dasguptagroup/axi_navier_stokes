double mycs(int r,int c,bool normal)
{
	if (r==0)
	{
		return mycs(r+1,c,normal);
	}
	if (r==NoOfRows+1)
	{
		return mycs(r-1,c,normal);
	}
	if (c==0)
	{
		return mycs(r,c+1,normal);
	}
	if (c==NoOfCols+1)
	{
		return mycs(r,c-1,normal);
	}
	
					int ix;
					
					  double c_t,c_b,c_r,c_l;
					  double mx0,my0,mx1,my1,mm1,mm2;
					  
					  /* top, bottom, right and left sums of c values */
					  c_t =  Cellattrib[r+1][c-1].VolFrac +  Cellattrib[r+1][c].VolFrac +  Cellattrib[r+1][c+1].VolFrac;
					  c_b = Cellattrib[r-1][c-1].VolFrac +  Cellattrib[r-1][c].VolFrac +  Cellattrib[r-1][c+1].VolFrac;
					  c_r = Cellattrib[r-1][c+1].VolFrac +  Cellattrib[r][c+1].VolFrac +  Cellattrib[r+1][c+1].VolFrac;
					  c_l = Cellattrib[r-1][c-1].VolFrac +  Cellattrib[r][c-1].VolFrac +  Cellattrib[r+1][c-1].VolFrac;

					  /* consider two lines: sgn(my) Y =  mx0 X + alpha,
					     and: sgn(mx) X =  my0 Y + alpha */ 
					  mx0 = 0.5*(c_l-c_r);
					  my0 = 0.5*(c_b-c_t);

					  /* minimum coefficient between mx0 and my0 wins */
					  if (fabs(mx0) <= fabs(my0)) {
					    my0 = my0 > 0. ? 1. : -1.;
					    ix = 1;
					  }
					  else {
					    mx0 = mx0 > 0. ? 1. : -1.;
					    ix = 0;
					  }

					  /* Youngs' normal to the interface */
					  mm1 =  Cellattrib[r+1][c-1].VolFrac + (2.0*Cellattrib[r][c-1].VolFrac) + Cellattrib[r-1][c-1].VolFrac;
					  mm2 = Cellattrib[r+1][c+1].VolFrac + (2.0*Cellattrib[r][c+1].VolFrac) + Cellattrib[r-1][c+1].VolFrac;
					  mx1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception
					  mm1 =  Cellattrib[r-1][c+1].VolFrac + (2.0*Cellattrib[r-1][c].VolFrac) + Cellattrib[r-1][c-1].VolFrac;
					  mm2 =Cellattrib[r+1][c+1].VolFrac + (2.0*Cellattrib[r+1][c].VolFrac) + Cellattrib[r+1][c-1].VolFrac;
					  my1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception

					  /* choose between the best central and Youngs' scheme */ 
					  if (ix) {
					    mm1 = fabs(my1);
					    mm1 = fabs(mx1)/mm1;
					    if (mm1 > fabs(mx0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
					  else {
					    mm1 = fabs(mx1);
					    mm1 = fabs(my1)/mm1;
					    if (mm1 > fabs(my0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
						
					  /* normalize the set (mx0,my0): |mx0|+|my0|=1 and
					     write the two components of the normal vector  */
						mm1 = absolute(mx0) + absolute(my0);
					  if(normal)
						return mx0/mm1;
					  else
						return my0/mm1;
					  
}
#define HDRIFT 20.0

static inline double height (double a) {
  return a > HDRIFT/2. ? a - HDRIFT : a < -HDRIFT/2. ? a + HDRIFT : a;
}

static inline int orientation (double a) {
  return fabs(a) > HDRIFT/2.;
}
typedef struct {
  double x, y, z;
} coord;

#include "parabola.h"

static int independents (coord * p, int n)
{
  if (n < 2)
    return n;
  int ni = 1;
  for (int j = 1; j < n; j++) {
    bool depends = false;
    for (int i = 0; i < j && !depends; i++) {
      double d2 = 0.;
      
	d2 = pow(p[i].x - p[j].x,2)+pow(p[i].y - p[j].y,2);
	
	    
      depends = (d2 < sq(0.5));
    }
    ni += !depends;
  }
  return ni;
}

/**
Given a volume fraction field *c* and a height function field *h*,
this function returns the "mixed heights" parabola-fitted curvature
(or *nodata* if the curvature cannot be computed). */

static double height_curvature_fit ( int r, int c)
{

  /**
  The coordinates of the interface points and the number of
  interface points. */
  
  coord ip[6];
  int n = 0;

 // y direction


    /**
    We don't want to mix heights with different orientations. We first
    find the "dominant" orientation *ori*. */
    
    int n1 = 0, n2 = 0;

    for (int i = -1; i <= 1; i++)
      if (ht_y[r][c+i] != nodata) {
	if (orientation(ht_y[r][c+i])) n1++; else n2++;
      }

    int ori = (n1 > n2);

    /**
    We look for height-functions with the dominant orientation and
    store the corresponding interface coordinates (relative to the
    center of the cell and normalised by the cell size). */


    for (int i = -1; i <= 1; i++)
      if (ht_y[r][c+i] != nodata && orientation(ht_y[r][c+i]) == ori)
	ip[n].x = i, ip[n++].y = height(ht_y[r][c+i]);

 // x direction
           n1 = 0; n2 = 0;

    for (int i = -1; i <= 1; i++)
      if (ht_x[r+i][c] != nodata) {
	if (orientation(ht_x[r+i][c])) n1++; else n2++;
      }

     ori = (n1 > n2);

    /**
    We look for height-functions with the dominant orientation and
    store the corresponding interface coordinates (relative to the
    center of the cell and normalised by the cell size). */


    for (int i = -1; i <= 1; i++)
      if (ht_x[r+i][c] != nodata && orientation(ht_x[r+i][c]) == ori)
	ip[n].y = i, ip[n++].x = height(ht_x[r+i][c]);
  
  /**
  If we don't have enough independent points, we cannot do the
  parabolic fit. */
  
  if (independents (ip, n) < 3)
    return nodata;

  /**
  We recover the interface normal and the centroid of the interface
  fragment and initialize the parabolic fit. */
  
  coord m , fc;
  m.x = mycs(r,c,1);
  m.y = mycs(r,c,0);
  m.z=0;
  
  double alp = plane_alpha (Cellattrib[r][c].VolFrac, m);
  
  double area = line_length_center (m, alp, &fc);
  
  ParabolaFit fit;
  parabola_fit_init (&fit, fc, m);

  //~ NOT_UNUSED(area);
  parabola_fit_add (&fit, fc, PARABOLA_FIT_CENTER_WEIGHT);

  
  /**
  We add the collected interface positions and compute the
  curvature. */

  for (int i = 0; i < n; i++)
    parabola_fit_add (&fit, ip[i], 1.);
  parabola_fit_solve (&fit);
  double kappa = parabola_fit_curvature (&fit, 2., NULL)/del;

if (axi==1)
{
	parabola_fit_axi_curvature (&fit, x[r][c] + fc.x*del, del, &kappa, NULL);
}

  return kappa;
  //~ return nodata;
}

static double centroids_curvature_fit (int r,int c)
{

  /**
  We recover the interface normal and the centroid of the interface
  fragment and initialize the parabolic fit. */
  
  coord m, fc;
 
	m.x = mycs(r,c,1);
	m.y = mycs(r,c,0);
	m.z=0;
	
  double alp =plane_alpha (Cellattrib[r][c].VolFrac, m);
	
  line_length_center (m, alp, &fc);
  ParabolaFit fit;
  parabola_fit_init (&fit, fc, m);

  /**
  We add the interface centroids in a $3^d$ neighborhood and compute
  the curvature. */

  coord rk = {x[r][c],y[r][c],0};
  for (int j=-1;j<=1;j++)
  {
	for(int i=-1;i<=1;i++)
	{	if (interface(r+j,c+i)) {
      coord m , fc;
	m.x = mycs(r+j,c+i,1);
	m.y = mycs(r+j,c+i,1); //Cellattrib[r+j][c+i].Ny;
	m.z=0;
      double alp =plane_alpha (Cellattrib[r+j][c+i].VolFrac, m);
      double area = line_length_center (m, alp, &fc);
      coord rn = {x[r+j][c+i],y[r+j][c+i],0};
     // foreach_dimension()
	fc.x += (rn.x - rk.x)/del;
	fc.y += (rn.y - rk.y)/del;
      parabola_fit_add (&fit, fc, area);
    }
    }
    }
  parabola_fit_solve (&fit);
  double kappa = parabola_fit_curvature (&fit, 2., NULL)/del;
    if (axi==1)
    {
	parabola_fit_axi_curvature (&fit, x[r][c] + fc.x*del, del, &kappa, NULL);
    }

    //~ printf("FLAG:  centroid fit used\n");
//~ exit(0);
  return kappa;
  //~ return nodata;
}
