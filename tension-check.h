
void Calculate_Surface_Tension_Height_Function_DAC()
{
	
int r,c, r_temp, c_temp;	
double HF_0, HF_1, HF_2, Fx, Fxx, Y_RC, Y_Rp1C, X_RC, X_RCp1;
//double err_new, norm;
		int sign_normal=1;
	
		

	double **kappa, Grad_VolFrac_X, Grad_VolFrac_Y;
		double **kappa_f_x, **kappa_f_y;
		kappa = (double **) calloc(NoOfRows+2,sizeof(double *));
		kappa_f_x = (double **) calloc(NoOfRows+2,sizeof(double *));
		kappa_f_y = (double **) calloc(NoOfRows+2,sizeof(double *));

			for (int i=0; i<NoOfRows+2; i++)
			{
				kappa[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa_f_x[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				kappa_f_y[i] = (double *) calloc (NoOfCols+2, sizeof (double));
			}

	

		for (r=0;r<=NoOfRows+1;r++)
		{
			for (c=0;c<=NoOfCols+1;c++)
			{	

				kappa[r][c] = nodata;
				
			}
		}
//   *******************
int tag=0;

	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{
			Grad_VolFrac_X = (Cellattrib[r][c+1].VolFrac - Cellattrib[r][c-1].VolFrac)/del;		//gradient of VolFrac in x direction
			Grad_VolFrac_Y = (Cellattrib[r+1][c].VolFrac - Cellattrib[r-1][c].VolFrac)/del;	
			if (Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac<UPPER_LIMIT)
			//if (absolute(Grad_VolFrac_X)>0 || absolute(Grad_VolFrac_Y)>0)
			{	
				HF_0 = 0; HF_1 = 0; HF_2 = 0; 
		
				// Case 1, when the normal is mainly pointing upwards
				if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny>0 )
				//~ if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y>0)	
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1; tag=1;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						r_temp = r_temp + 1; // go up 
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						r_temp = r_temp - 1; // go up 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						r_temp = r_temp + 1; // go up 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r-1; c_temp = c+1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						r_temp = r_temp - 1; // go up 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}
				
				//	Case 2, when the normal is mainly pointing downwards
				//~ else if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny<0)
				else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx>0)
				//~ else	if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y<0)	
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1; tag=3;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c+1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}


				/*************Case 3 When Normal is mainly pointing right****************************/
				 else if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny<0)
				//~ else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx>0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X>0)
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c; tag=2;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						c_temp = c_temp + 1; // go up 
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r; c_temp = c-1;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						c_temp = c_temp - 1; // go up 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						c_temp = c_temp + 1; // go up 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r+1; c_temp = c-1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						c_temp = c_temp - 1; // go up 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}	
						
				//	Case 4, when the normal is mainly pointing left
				else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx<0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X<0)
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c; tag=4;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_1;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_1;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_2;
						c_temp = c_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols && Cellattrib[r_temp][c_temp].VolFrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r+1; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].VolFrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].VolFrac + HF_2;
						c_temp = c_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=1 && Cellattrib[r_temp][c_temp].VolFrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}

						Fx = (HF_2 - HF_0)*del/(2*del);
						Fxx = (HF_2 - 2*HF_1 + HF_0)*del/pow(del,2);
						
						kappa[r][c] = -Fxx/pow((1+pow(Fx,2)),1.5) ;
				
						double pl_kappa=kappa[r][c];
						double axi_kappa = 777;
				
						double rad_dist=0;
						//~ printf("\n %lf\t%lf\t%lf",(c-0.5)*del,(r-0.5)*del,kappa[r][c]);

				
						if(absolute(kappa[r][c])>1.0/del)
						{
							kappa[r][c] = sign(kappa[r][c])/del;
							//~ kappa[r][c] = 0;
						}
						//~ int nr=Find_Quad(r,c);
				
						if (axi==1 && tag==1)
						{	
							rad_dist = RF[c];	// radius at the center of the cell
							kappa[r][c] = -Fxx/pow((1+pow(Fx,2)),1.5) - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							axi_kappa = - Fx/(RF[c]*pow((1+pow(Fx,2)),0.5));
							
						}
						else if (axi==1 && tag==3)
						{
							rad_dist = RF[c]; // radius at the center of the cell
							kappa[r][c] = -Fxx/pow((1+pow(Fx,2)),1.5)  - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							axi_kappa =   - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
						}
						else if(axi==1 && tag==2)
						{
							rad_dist=eta[c]+HF_1*del; // radius at bottom of cell plus the height from the bottom of the cell
							axi_kappa = +1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							kappa[r][c] = -Fxx/pow((1+pow(Fx,2)),1.5) +1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							
						}
						else if(axi==1 && tag==4)
						{
							rad_dist=eta[c]+HF_1*del;  // radius at bottom of cell plus the height from the bottom of the cell
							axi_kappa =  -1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							kappa[r][c] = -Fxx/pow((1+pow(Fx,2)),1.5) -1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							
						}
						else if(axi==1 && tag==0)  // no tag found for axi, show error
						{
							printf("no tag found");
							printf("\n %lf\t%lf\t%lf\t%lf\t%lf\t%d\t%lf",(c-0.5)*del,(r-0.5)*del,pl_kappa,axi_kappa,kappa[r][c],tag,Cellattrib[r][c].VolFrac);
							exit(0);
						}
						
						//~ kappa[r][c]=2.0;  //just to check
						//~ double an_kappa= (2.0/1.0 - 2.5/((c-0.5)*del));
						
						//~ double error = absolute(absolute(an_kappa)-absolute(kappa[r][c]));
						
						
						
	
						//~ if (absolute((c-0.5)*del-3.5)<1e-1 )
						//~ if ((c-0.5)*del<2.
						//~ {
							//~ fprintf(file,"\n %lf\t%lf\t%lf\t%lf\t%lf\t%d\t%lf\t%lf\t%lf",(c-0.5)*del,(r-0.5)*del,rad_dist,Fx,kappa[r][c],tag,axi_kappa,Cellattrib[r][c].VolFrac,rad_dist-RF[c]);
						//~ }
						//~ else
							
						//~ {
							//~ printf("\n %lf\t%lf\t%lf\t%d\t%lf\t%lf\t%lf",(c-0.5)*del,(r-0.5)*del,Fx,tag,kappa[r][c],an_kappa,0.0);
							printf("\n %lf\t%lf\t%lf\t%d\t%lf",(c-0.5)*del,(r-0.5)*del,Fx,tag,HF_2);
						//~ }
					
							//~ exit(0);
						//~ }
						
						
									
			}
			
			 
			
		}
		
		
	}
	//~ fclose(file);
exit(0);
	//Symmetry for kappa at boundaries
	for (r=1;r<=NoOfRows;r++)
	{
		kappa[r][0]=kappa[r][1];
		kappa[r][NoOfCols+1]=kappa[r][NoOfCols];
	}
	for (c=1;c<=NoOfCols;c++)
	{
		kappa[0][c]=kappa[1][c];
		kappa[NoOfRows+1][c]=kappa[NoOfRows][c];
	}
	//calculate kappa on face centers
	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols+1;c++)
		{
			//~ kappa_f_y[r][c] = (kappa[r][c]<nodata && kappa[r-1][c]<nodata) ? 0.5*(kappa[r][c] + kappa[r-1][c]) :
							//~ kappa[r][c] < nodata ? kappa[r][c] : kappa[r-1][c] < nodata ? kappa[r-1][c] : 0.;
			
			
			//~ kappa_f_x[r][c] = (kappa[r][c]<nodata && kappa[r][c-1]<nodata) ? 0.5*(kappa[r][c] + kappa[r][c-1]) :
							//~ kappa[r][c] < nodata ? kappa[r][c] : kappa[r][c-1] < nodata ? kappa[r][c-1] : 0. ;
			
			
			/*****************************at horizontal faces ******************************************/

			if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
			{

				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{

					kappa_f_y[r][c] = 0.5*(kappa[r][c] + kappa[r-1][c]);
				}
				else
				{
					kappa_f_y[r][c] = kappa[r-1][c]; // assign the face left kappa
				}


			}


				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{
					if(Cellattrib[r-1][c].VolFrac>LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
					{

						// do nothing
					}
					else
					{
						kappa_f_y[r][c] = kappa[r][c]; // assign the face left kappa
					}


				}
			//~ /******************************* kappa at vertical faces ***************************************/
				if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
				{

					if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
					{

						kappa_f_x[r][c] = 0.5*(kappa[r][c] + kappa[r][c-1]);
					}
					else
					{
						kappa_f_x[r][c] = kappa[r][c-1]; // assign the face left kappa
					}


				}


				if(Cellattrib[r][c].VolFrac>LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
				{
					if(Cellattrib[r][c-1].VolFrac>LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT)
					{

						// do nothing
					}
					else
					{
						kappa_f_x[r][c] = kappa[r][c]; // assign the face left kappa
					}


				}

			   // if (kappa_f_x[r][c]!=0)
			   // printf("%d\t%d\t%lf\t%lf\t%lf\t%lf\n",r,c,kappa_f_x[r][c],Cellattrib[r][c].VolFrac,kappa[r][c],kappa[r][c-1]);	
		}
	}

	

	for (r=1;r<=NoOfRows;r++)
	{
		for (c=1;c<=NoOfCols;c++)
		{
			//~ if(Cellattrib[r][c].VolFrac!=Cellattrib[r][c-1].VolFrac)
			//~ {
				ST_Fx[r][c] = Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r][c-1].VolFrac)*(kappa_f_x[r][c]);
			//~ }
			//~ if (Cellattrib[r][c].VolFrac!=Cellattrib[r-1][c].VolFrac)
			//~ {
				ST_Fy[r][c] = Sigma*(1.0/del)*(Cellattrib[r][c].VolFrac-Cellattrib[r-1][c].VolFrac)*(kappa_f_y[r][c]);
			//~ }

		}
	}


	freeArray(kappa);
	freeArray(kappa_f_x);
	freeArray(kappa_f_y);
	//exit(0);
}

