
double mycs_solid(int r,int c,bool normal)
{
	if (r==0)
	{
		return mycs_solid(r+1,c,normal);
	}
	if (r==NoOfRows+1)
	{
		return mycs_solid(r-1,c,normal);
	}
	if (c==0)
	{
		return mycs_solid(r,c+1,normal);
	}
	if (c==NoOfCols+1)
	{
		return mycs_solid(r,c-1,normal);
	}
	
					int ix;
					
					  double c_t,c_b,c_r,c_l;
					  double mx0,my0,mx1,my1,mm1,mm2;
					  
					  /* top, bottom, right and left sums of c values */
					  c_t =  solid[r+1][c-1].VolFrac +  solid[r+1][c].VolFrac +  solid[r+1][c+1].VolFrac;
					  c_b = solid[r-1][c-1].VolFrac +  solid[r-1][c].VolFrac +  solid[r-1][c+1].VolFrac;
					  c_r = solid[r-1][c+1].VolFrac +  solid[r][c+1].VolFrac +  solid[r+1][c+1].VolFrac;
					  c_l = solid[r-1][c-1].VolFrac +  solid[r][c-1].VolFrac +  solid[r+1][c-1].VolFrac;

					  /* consider two lines: sgn(my) Y =  mx0 X + alpha,
					     and: sgn(mx) X =  my0 Y + alpha */ 
					  mx0 = 0.5*(c_l-c_r);
					  my0 = 0.5*(c_b-c_t);

					  /* minimum coefficient between mx0 and my0 wins */
					  if (fabs(mx0) <= fabs(my0)) {
					    my0 = my0 > 0. ? 1. : -1.;
					    ix = 1;
					  }
					  else {
					    mx0 = mx0 > 0. ? 1. : -1.;
					    ix = 0;
					  }

					  /* Youngs' normal to the interface */
					  mm1 =  solid[r+1][c-1].VolFrac + (2.0*solid[r][c-1].VolFrac) + solid[r-1][c-1].VolFrac;
					  mm2 = solid[r+1][c+1].VolFrac + (2.0*solid[r][c+1].VolFrac) + solid[r-1][c+1].VolFrac;
					  mx1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception
					  mm1 =  solid[r-1][c+1].VolFrac + (2.0*solid[r-1][c].VolFrac) + solid[r-1][c-1].VolFrac;
					  mm2 =solid[r+1][c+1].VolFrac + (2.0*solid[r+1][c].VolFrac) + solid[r+1][c-1].VolFrac;
					  my1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception

					  /* choose between the best central and Youngs' scheme */ 
					  if (ix) {
					    mm1 = fabs(my1);
					    mm1 = fabs(mx1)/mm1;
					    if (mm1 > fabs(mx0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
					  else {
					    mm1 = fabs(mx1);
					    mm1 = fabs(my1)/mm1;
					    if (mm1 > fabs(my0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
						
					  /* normalize the set (mx0,my0): |mx0|+|my0|=1 and
					     write the two components of the normal vector  */
						mm1 = absolute(mx0) + absolute(my0);
					  if(normal)
						return mx0/mm1;
					  else
						return my0/mm1;
					  
}

#include "solid_recon.h"

#include "solid_advect.h"


double Get_distance(int r, int c, int Quad, char direction) {

	if ((r < 0 || r >= NoOfRows+2) || (c < 0 || c >= NoOfCols+2)) {
		printf("Arguments to the subroutine Get_distance are out of bounds @ %d %d\n", r, c);
		exit(EXIT_FAILURE);
	}
	//printf("Going for r: %d and c: %d,\n", r, c);
	if (r == 0) {
		if (direction == 'u')
			direction = 'd';
		else if (direction == 'd')
			direction = 'u';
		return Get_distance(1, c, Quad, direction);
	}
	if (c == 0) {
		if (direction == 'l')
			direction = 'r';
		else if (direction == 'r')
			direction = 'l';
		return Get_distance(r, 1, Quad, direction);
	}
	if (r == NoOfRows+1) {
		if (direction == 'u')
			direction = 'd';
		else if (direction == 'd')
			direction = 'u';
		return Get_distance(NoOfRows, c, Quad, direction);
	}
	if (c == NoOfCols+1) {
		if (direction == 'l')
			direction = 'r';
		else if (direction == 'r')
			direction = 'l';
		return Get_distance(r, NoOfCols, Quad, direction);
	}

	if (solid[r][c].VolFrac < LOWER_LIMIT) {
		//printf("Empty cell\n");
		return 0;
	}
	else if (solid[r][c].VolFrac > UPPER_LIMIT) {
		//printf("Full cell\n");
		return delBy2;
	}
	//printf("Going for r: %d and c: %d,\n", r, c);
	//getchar();

	if (Quad == -1)
		Quad = Find_Quad(r, c);
	//printf("With Quad: %d and direction: ", Quad);
	//putchar(direction);
	//getchar();
	switch (direction) {
		case 'l':
			if (Quad == 1) {
				// local variables
				// assuming positive perpendicular and 0 <= theta <= Pi/2
				double perpendicular = solid[r][c].P;
				double theta = solid[r][c].Theta;
				double x0;
				// REVIEW:
				// added tolerance
				if (absolute(theta - PiBy2) <= 0.001) {	// vertical interface
					return fmin(perpendicular, delBy2);
				}
				else if (absolute(theta) <= 0.001) {	// horizontal interface
					if (perpendicular < delBy2) 	// interface below the centroid
						return 0;
					else							// interface above the centroid
						return delBy2;
				}
				else {
					x0 = perpendicular/sin(theta) - delBy2/tan(theta);
					if (x0 <= 0)
						return 0;
					else if (x0 < delBy2)
						return x0;
					else
						return delBy2;
				}
			}
			else if (Quad == 2) {
				return Get_distance(r, c, 1, 'u');
			}
			else if (Quad == 3) {
				return Get_distance(r, c, 1, 'r');
			}
			else if (Quad == 4) {
				return Get_distance(r, c, 1, 'd');
			}
			else {
				printf("Quad error in subroutine Get_distance\n");
				exit(EXIT_FAILURE);
			}
			break;
		case 'r':
			if (Quad == 1) {
				// local variables
				// assuming positive perpendicular and 0 <= theta <= Pi/2
				double perpendicular = solid[r][c].P;
				double theta = solid[r][c].Theta;
				double x0;

				if (absolute(theta - PiBy2) <= 0.001) {	// vertical interface
					return perpendicular - fmin(perpendicular, delBy2);
				}
				else if (absolute(theta) <= 0.001) {	// horizontal interface
					if (perpendicular < delBy2) 	// interface below the centroid
						return 0;
					else							// interface above the centroid
						return delBy2;
				}
				else {
					x0 = perpendicular/sin(theta) - delBy2/tan(theta);
					if (x0 <= delBy2)
						return 0;
					else if (x0 < del)
						return x0 - delBy2;
					else
						return delBy2;
				}
			}
			else if (Quad == 2) {
				return Get_distance(r, c, 1, 'd');
			}
			else if (Quad == 3) {
				return Get_distance(r, c, 1, 'l');
			}
			else if (Quad == 4) {
				return Get_distance(r, c, 1, 'u');
			}
			else {
				printf("Quad error in subroutine Get_distance\n");
				exit(EXIT_FAILURE);
			}
			break;
		case 'u':
		if (Quad == 1) {
				// local variables
				// assuming positive perpendicular and 0 <= theta <= Pi/2
				double perpendicular = solid[r][c].P;
				double theta = solid[r][c].Theta;
				double y0;

				if (absolute(theta - PiBy2) <= 0.001) {	// vertical interface
					if (perpendicular < delBy2)		// interface on left of centroid
						return 0;
					else							// interface on right of centroid
						return delBy2;
				}
				else if (absolute(theta) <= 0.001) {	// horizontal interface
					return perpendicular - fmin(perpendicular, delBy2);
				}
				else {
					y0 = perpendicular/cos(theta) - delBy2*tan(theta);
					if (y0 <= delBy2)
						return 0;
					else if (y0 < del)
						return y0 - delBy2;
					else
						return delBy2;
				}
			}
			else if (Quad == 2) {
				return Get_distance(r, c, 1, 'r');
			}
			else if (Quad == 3) {
				return Get_distance(r, c, 1, 'd');
			}
			else if (Quad == 4) {
				return Get_distance(r, c, 1, 'l');
			}
			else {
				printf("Quad error in subroutine Get_distance\n");
				exit(EXIT_FAILURE);
			}
			break;
		case 'd':
		if (Quad == 1) {
				// local variables
				// assuming positive perpendicular and 0 <= theta <= Pi/2
				double perpendicular = solid[r][c].P;
				double theta = solid[r][c].Theta;
				double y0;

				if (absolute(theta - PiBy2) <= 0.001) {	// vertical interface
					if (perpendicular < delBy2)		// interface on left of centroid
						return 0;
					else							// interface on right of centroid
						return delBy2;
				}
				else if (absolute(theta) <= 0.001) {	// horizontal interface
					return fmin(perpendicular, delBy2);
				}
				else {
					y0 = perpendicular/cos(theta) - delBy2*tan(theta);
					if (y0 <= 0)
						return 0;
					else if (y0 < delBy2)
						return y0;
					else
						return delBy2;
				}
			}
			else if (Quad == 2) {
				return Get_distance(r, c, 1, 'l');
			}
			else if (Quad == 3) {
				return Get_distance(r, c, 1, 'u');
			}
			else if (Quad == 4) {
				return Get_distance(r, c, 1, 'r');
			}
			else {
				printf("Quad error in subroutine Get_distance\n");
				exit(EXIT_FAILURE);
			}
			break;
		default:
			printf("Direction argument to subroutine Get_distance is wrong\n");
			exit(EXIT_FAILURE);
	}
}


