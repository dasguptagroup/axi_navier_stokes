
#include "navier-stokes-planar.h"
//~ double bessel_prime_zero = 7.0155866698156; 
double bessel_prime_zero =3.831705970207512; 
//~ double bessel_prime_zero =2.0*M_PI; 


int main(int argc,char *argv[])
{
	
	rho_G =0.01;
	mu_G	=	0.0001;

	rho_L	=	1.0;
	mu_L	=	1.0;


	 
	Sigma = 1.0;
	volfrac_gen=1;
		Diffusion_term=1;
		Advection_term=1;
		Advection_Scheme = 5;
	
	Linear_Solver=1;
	axi=1;
	double k = 2*M_PI;

	double omega = pow((pow(2*M_PI,3)/(rho_G+rho_L)),0.5);
	//~ ST_Module=2;
		end = 5;
	step=0.01;
	Progress=1;
	//~ CFL=0.1;	
	implicity=1.0;
			 NoOfRows  =64*2;
			NoOfCols 	= 64;
			del = bessel_prime_zero/NoOfCols;
				

			
/************User input above, no parameter should be below run_ns***********/	
	run_ns();
	
	return 0;	
	
}



// provide initial conditions below
double Init_F(double x, double y)
{	double a0 = 0.01;
	
	//~ return -((y -M_PI*4.0)-( a0*cos (x-M_PI))) ;
    //~ return -((sq(x)+sq(y)) * sqrt(sq(x)+sq(y)) - (sq(x)+sq(y)) * R0 - a0 * 0.5 * (2*sq(x)-sq(y)));
	//~ return (sq(x)+sq(y/0.8) -1.0);
	
	//~ double a0 = 1.0;
	

	
  
	double bessels,integrand_i,integrand_ip1,theta_i,theta_ip1;
		double area=0;
		int i; 
	        int k=1;
		int n=1000;
	
	
		
		
			area = 0;
			for (i=0;i<n;i++)
			{
				theta_i = i*M_PI/n;
				theta_ip1 = (i+1)* M_PI/n;
				
				integrand_i = cos(k*(x)*sin(theta_i));
				integrand_ip1 = cos(k*(x)*sin(theta_ip1));
				
				area = 0.5*(integrand_i + integrand_ip1)*(M_PI/n) + area;
				
			
			}




			bessels = (area/M_PI)  ;

			

			
		
		

	
	
	return -(y - a0*bessels - bessel_prime_zero*1.0 );
	
	
}

void event_phy(double Step)
{
	interface_data(Step);
	double n = Step*100;
	vti_data(n);
	//~ FILE *ptrW;
	//~ if(StepT==0)
	//~ {
		//~ ptrW = fopen("amp.dat","w");
	//~ }
	//~ else
	//~ {	
		//~ ptrW = fopen("amp.dat","a");
	//~ }
	
	
	//~ int r=1,c;
	//~ double x0,y0;
        
	//~ for ( r = 1; r <= NoOfRows; ++r) {
		//~ for ( c = 1; c <= NoOfCols; ++c) {
			//~ if(interface(r,c))
			//~ {
				//~ if (absolute(Xp1[r][c]-0.)<1.e-3)
				//~ {
					//~ fprintf(ptrW,"%e\t%e\n",Time,Yp1[r][c]-bessel_prime_zero*0.5);
					//~ break;
				//~ }
				//~ else if (absolute(Xp2[r][c]-0.)<1.e-3)
				//~ {
					//~ fprintf(ptrW,"%e\t%e\n",Time,Yp2[r][c]-bessel_prime_zero*0.5);
					//~ break;
				//~ }
			//~ }
		//~ }
	//~ }
     
	//~ fclose(ptrW);	
	
}
void event_init()
{}
void event_bc()
{
	//left 
	BcDirichlet_U("left",0);
	BcNeumann_V("left");
	BcNeumann_F("left");
	BcNeumann_P("left");
	
	//right
	BcDirichlet_U("right",0);
	BcNeumann_V("right");
	BcNeumann_F("right");
	BcNeumann_P("right");
	
	//bottom
	BcDirichlet_V("bottom",0);
	BcNeumann_U("bottom");
	BcNeumann_F("bottom");
	BcNeumann_P("bottom");
	//top
	BcDirichlet_V("top",0);
	BcNeumann_U("top");
	BcNeumann_F("top");
	BcNeumann_P("top");
	
	}

double Init_U(double x, double y)
{
return 0;
	
}

double Init_V(double x, double y)
{
	return 0;
	
}

double Init_P(double x, double y)
{
	return 0;
	
}
void event_cp()
{
	for (int r=0;r<=NoOfRows+1;r++){
		for(int c=0; c<=NoOfCols+1;c++){
			SourceV[r][c]=-10.0;
		}
	}
	
	
	}

	