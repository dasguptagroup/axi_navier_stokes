typedef struct{
	double *uptr;
	double *vptr;
} UVptr;

/********************Multigrid Dependencies***********************/

#define INDEX(r,c,NC) ((NC+2)*(r)+(c))


/******************************************************************************/
UVptr jacobi_implicit ( double *f_u,double *f_v, double *u_ptr,double *v_ptr, int NR, int NC,double *rho_private,double *mu_private, double h ) 
{
  double h_sq;
  double del_sq;
        h_sq = h*h;
	del_sq = h_sq;
  int r,c;
 
	UVptr UV;




/* Dirichlet Boundary Conditions for outflow in genenal where P = 0 at the boundary */	
// NOTE: VolFrac or rho always remain neumann in general 

	// U left
			for (r=1; r<=NR; r++)
			{	if (BC_Dirichlet_U_left==1)				
				{
					u_ptr[INDEX(r,1,NC)] = u_vel_left  ;
				}
				if (BC_Neumann_U_left==1)
				{
					u_ptr[INDEX(r,1,NC)]  = u_ptr[INDEX(r,2,NC)]   ;
				}
			rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
			mu_private[INDEX(r,0,NC)] = mu_private[INDEX(r,1,NC)] ;
	
			}
	
	//U right
			for (r=1; r<=NR; r++)
			{	if (BC_Dirichlet_U_right==1)
				{
					 u_ptr[INDEX(r,NC+1,NC)] = u_vel_right ; 
				}
				if (BC_Neumann_U_right==1)
				{
					 u_ptr[INDEX(r,NC+1,NC)] =  u_ptr[INDEX(r,NC,NC)] ;
				}
	
			rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
			mu_private[INDEX(r,NC+1,NC)] = mu_private[INDEX(r,NC,NC)] ;
				
			}	
	
	//U top
			
			for (c=1;c<=NC; c++)
			{	if (BC_Dirichlet_U_top==1)
				{
					u_ptr[INDEX(NR+1,c,NC)] = 2.0*u_vel_top - u_ptr[INDEX(NR,c,NC)]  ; 
				}
				if (BC_Neumann_U_top==1)
				{
					u_ptr[INDEX(NR+1,c,NC)] = u_ptr[INDEX(NR,c,NC)]  ;
				}
				
				
			rho_private[INDEX(NR+1,c,NC)] = rho_private[INDEX(NR,c,NC)];
			mu_private[INDEX(NR+1,c,NC)] = mu_private[INDEX(NR,c,NC)];

			}			
			
	//U bottom

			for (c=1;c<=NC; c++)
			{	if (BC_Dirichlet_U_bottom==1)
				{
					u_ptr[INDEX(0,c,NC)] =2.0*u_vel_bottom -u_ptr[INDEX(1,c,NC)] ;
				}
				if (BC_Neumann_U_bottom==1)
				{
					u_ptr[INDEX(0,c,NC)] = u_ptr[INDEX(1,c,NC)]  ;
				}
	
		rho_private[INDEX(0,c,NC)] = rho_private[INDEX(1,c,NC)];
		mu_private[INDEX(0,c,NC)] = mu_private[INDEX(1,c,NC)];
				
			}	
			
	//V left
			
			for (r=1; r<=NR; r++)
			{
				if (BC_Dirichlet_V_left==1)
				{
					v_ptr[INDEX(r,0,NC)] = 2.0*v_vel_left- v_ptr[INDEX(r,1,NC)] ;
				}
				if (BC_Neumann_V_left==1)
				{
					v_ptr[INDEX(r,0,NC)] =v_ptr[INDEX(r,1,NC)] ;
				}
			}			

								
	// V right
			
			for (r=1; r<=NR; r++)
			{
				if (BC_Dirichlet_V_right==1)
				{
					v_ptr[INDEX(r,NC+1,NC)] = 2.0* v_vel_right - v_ptr[INDEX(r,NC,NC)] ;
				}
				if (BC_Neumann_V_right==1)
				{
					v_ptr[INDEX(r,NC+1,NC)] = v_ptr[INDEX(r,NC,NC)] ;
				}
					
			}

	 // V top
			for (c=1;c<=NC; c++)
			{
				if (BC_Dirichlet_V_top==1)
				{
					v_ptr[INDEX(NR+1,c,NC)]  = v_vel_top;
				}
				if (BC_Neumann_V_top==1)
				{
					v_ptr[INDEX(NR+1,c,NC)]  = v_ptr[INDEX(NR,c,NC)] ;
				}
			}
			
	//V bottom
			
			for (c=1;c<=NC; c++)
			{
				if (BC_Dirichlet_V_bottom==1)
				{
					v_ptr[INDEX(1,c,NC)] = v_vel_bottom ;
				}
				if (BC_Neumann_V_bottom==1)
				{
					v_ptr[INDEX(1,c,NC)] = v_ptr[INDEX(2,c,NC)] ;
				}
			}			



		

/****************************************END BC******************************************/

  
	double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
 
double Relaxation_param = 1.0;
	
double mu_left, mu_right, rho_r,mu_bottom,mu_up;
	//~ printf("\n");
		for (r=1; r<=NR; r++)
		{
                        for (c=1; c<=NC; c++)
                        {
								
				if (c!=1){
					rho_r		=	0.5*( rho_private[INDEX(r,c-1,NC)]	+ rho_private[INDEX(r,c,NC)]);
					mu_bottom	=	0.25*( mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_up 		=   0.25*( mu_private[INDEX(r+1,c,NC)] 	+	mu_private[INDEX(r+1,c-1,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r,c,NC)]);
					
					
					//basilisk
					double denominator = 1.0 + delT/rho_r*implicity * (mu_private[INDEX(r,c,NC)]+mu_private[INDEX(r,c-1,NC)])/pow(eta_p[c],2)*axi + delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]*RF_p[c]/eta_p[c]  + 2.0*mu_private[INDEX(r,c-1,NC)]*RF_p[c-1]/eta_p[c]  + mu_up + mu_bottom);
					
					
					double numerator = delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]* u_ptr[INDEX(r,c+1,NC)] *RF_p[c]/eta_p[c]  + 2.0* mu_private[INDEX(r,c-1,NC)]*u_ptr[INDEX(r,c-1,NC)] *RF_p[c-1]/eta_p[c] + mu_up * (u_ptr[INDEX(r+1,c,NC)] + (v_ptr[INDEX(r+1,c,NC)] - v_ptr[INDEX(r+1,c-1,NC)])  ) 
												- mu_bottom* ( - u_ptr[INDEX(r-1,c,NC)] + v_ptr[INDEX(r,c,NC)] - v_ptr[INDEX(r,c-1,NC)] ));
					
					
					//~ X_Velocity_star[r][c] = numerator/denominator;
					u_ptr[INDEX(r,c,NC)] = Relaxation_param*((f_u[INDEX(r,c,NC)] + numerator ) / denominator) + (1.0-Relaxation_param)*u_ptr[INDEX(r,c,NC)]; 
					
				}
					
					
				if (r!=1){
					mu_left		=	0.25*( mu_private[INDEX(r,c,NC)] 	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_right		=	0.25*( mu_private[INDEX(r,c+1,NC)] 	+	mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r-1,c,NC)]	+	mu_private[INDEX(r-1,c+1,NC)]);
					rho_r		=	0.5*(rho_private[INDEX(r-1,c,NC)]	+ rho_private[INDEX(r,c,NC)] );
					
					
					double denominator = 1.0 + delT/rho_r/del_sq*implicity* (2.0*mu_private[INDEX(r,c,NC)]  + 2.0*mu_private[INDEX(r-1,c,NC)]  + mu_right*eta_p[c+1]/RF_p[c]  + mu_left*eta_p[c]/RF_p[c]  );
					
					double numerator =  delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]  * v_ptr[INDEX(r+1,c,NC)] + 2.0*mu_private[INDEX(r-1,c,NC)] *v_ptr[INDEX(r-1,c,NC)] + mu_right * (v_ptr[INDEX(r,c+1,NC)] + u_ptr[INDEX(r,c+1,NC)] - u_ptr[INDEX(r-1,c+1,NC)]) *eta_p[c+1]/RF_p[c] 
									- mu_left* ( -v_ptr[INDEX(r,c-1,NC)] + u_ptr[INDEX(r,c,NC)] - u_ptr[INDEX(r-1,c,NC)])*eta_p[c]/RF_p[c]  );
					
					//~ Y_Velocity_star[r][c] = numerator/denominator;
					v_ptr[INDEX(r,c,NC)] = Relaxation_param*(( f_v[INDEX(r,c,NC)] + numerator ) /denominator) + (1.0-Relaxation_param)*v_ptr[INDEX(r,c,NC)]; 
				}				
				
			}
			
		}



 



/* Dirichlet Boundary Conditions for outflow in genenal where P = 0 at the boundary */	
// NOTE: VolFrac or rho always remain neumann in general 
	// U left
			for (r=1; r<=NR; r++)
			{	if (BC_Dirichlet_U_left==1)				
				{
					u_ptr[INDEX(r,1,NC)] = u_vel_left  ;
				}
				if (BC_Neumann_U_left==1)
				{
					u_ptr[INDEX(r,1,NC)]  = u_ptr[INDEX(r,2,NC)]   ;
				}
			rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
			mu_private[INDEX(r,0,NC)] = mu_private[INDEX(r,1,NC)] ;
	
			}
	
	//U right
			for (r=1; r<=NR; r++)
			{	if (BC_Dirichlet_U_right==1)
				{
					 u_ptr[INDEX(r,NC+1,NC)] = u_vel_right ; 
				}
				if (BC_Neumann_U_right==1)
				{
					 u_ptr[INDEX(r,NC+1,NC)] =  u_ptr[INDEX(r,NC,NC)] ;
				}
	
			rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
			mu_private[INDEX(r,NC+1,NC)] = mu_private[INDEX(r,NC,NC)] ;
				
			}	
	
	//U top
			
			for (c=1;c<=NC; c++)
			{	if (BC_Dirichlet_U_top==1)
				{
					u_ptr[INDEX(NR+1,c,NC)] = 2.0*u_vel_top - u_ptr[INDEX(NR,c,NC)]  ; 
				}
				if (BC_Neumann_U_top==1)
				{
					u_ptr[INDEX(NR+1,c,NC)] = u_ptr[INDEX(NR,c,NC)]  ;
				}
				
				
			rho_private[INDEX(NR+1,c,NC)] = rho_private[INDEX(NR,c,NC)];
			mu_private[INDEX(NR+1,c,NC)] = mu_private[INDEX(NR,c,NC)];

			}			
			
	//U bottom

			for (c=1;c<=NC; c++)
			{	if (BC_Dirichlet_U_bottom==1)
				{
					u_ptr[INDEX(0,c,NC)] =2.0*u_vel_bottom -u_ptr[INDEX(1,c,NC)] ;
				}
				if (BC_Neumann_U_bottom==1)
				{
					u_ptr[INDEX(0,c,NC)] = u_ptr[INDEX(1,c,NC)]  ;
				}
	
		rho_private[INDEX(0,c,NC)] = rho_private[INDEX(1,c,NC)];
		mu_private[INDEX(0,c,NC)] = mu_private[INDEX(1,c,NC)];
				
			}	
			
	//V left
			
			for (r=1; r<=NR; r++)
			{
				if (BC_Dirichlet_V_left==1)
				{
					v_ptr[INDEX(r,0,NC)] = 2.0*v_vel_left- v_ptr[INDEX(r,1,NC)] ;
				}
				if (BC_Neumann_V_left==1)
				{
					v_ptr[INDEX(r,0,NC)] =v_ptr[INDEX(r,1,NC)] ;
				}
			}			

								
	// V right
			
			for (r=1; r<=NR; r++)
			{
				if (BC_Dirichlet_V_right==1)
				{
					v_ptr[INDEX(r,NC+1,NC)] = 2.0* v_vel_right - v_ptr[INDEX(r,NC,NC)] ;
				}
				if (BC_Neumann_V_right==1)
				{
					v_ptr[INDEX(r,NC+1,NC)] = v_ptr[INDEX(r,NC,NC)] ;
				}
					
			}

	 // V top
			for (c=1;c<=NC; c++)
			{
				if (BC_Dirichlet_V_top==1)
				{
					v_ptr[INDEX(NR+1,c,NC)]  = v_vel_top;
				}
				if (BC_Neumann_V_top==1)
				{
					v_ptr[INDEX(NR+1,c,NC)]  = v_ptr[INDEX(NR,c,NC)] ;
				}
			}
			
	//V bottom
			
			for (c=1;c<=NC; c++)
			{
				if (BC_Dirichlet_V_bottom==1)
				{
					v_ptr[INDEX(1,c,NC)] = v_vel_bottom ;
				}
				if (BC_Neumann_V_bottom==1)
				{
					v_ptr[INDEX(1,c,NC)] = v_ptr[INDEX(2,c,NC)] ;
				}
			}			





/****************************************END BC******************************************/

 
  UV.uptr = u_ptr;
  UV.vptr = v_ptr;	

  return UV;
 
 
}
/*************************************************************************/
 void Residue_grid_implicit ( double *f_u,double *f_v, double *u_ptr,double *v_ptr,double *Residue_u,double *Residue_v, int NR,int NC, double *rho_private,double *mu_private, double h ) 
{
	
	int r,c;
	double h_sq;
        double del_sq;
        h_sq = h*h;
	del_sq = h_sq;

	double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
	

double mu_left, mu_right, rho_r,mu_bottom,mu_up;

		for (r=1; r<=NR; r++)
		{
                        for (c=1; c<=NC; c++)
                        {
								
				if (c!=1){
					rho_r		=	0.5*( rho_private[INDEX(r,c-1,NC)]	+ rho_private[INDEX(r,c,NC)]);
					mu_bottom	=	0.25*( mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_up 		=   0.25*( mu_private[INDEX(r+1,c,NC)] 	+	mu_private[INDEX(r+1,c-1,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r,c,NC)]);
					
					
					//basilisk
					double denominator = 1.0 + delT/rho_r*implicity * (mu_private[INDEX(r,c,NC)]+mu_private[INDEX(r,c-1,NC)])/pow(eta_p[c],2)*axi + delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]*RF_p[c]/eta_p[c]  + 2.0*mu_private[INDEX(r,c-1,NC)]*RF_p[c-1]/eta_p[c]  + mu_up + mu_bottom);
					
					
					double numerator = delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]* u_ptr[INDEX(r,c+1,NC)] *RF_p[c]/eta_p[c]  + 2.0* mu_private[INDEX(r,c-1,NC)]*u_ptr[INDEX(r,c-1,NC)] *RF_p[c-1]/eta_p[c] + mu_up * (u_ptr[INDEX(r+1,c,NC)] + (v_ptr[INDEX(r+1,c,NC)] - v_ptr[INDEX(r+1,c-1,NC)])  ) 
												- mu_bottom* ( - u_ptr[INDEX(r-1,c,NC)] + v_ptr[INDEX(r,c,NC)] - v_ptr[INDEX(r,c-1,NC)] ));
					
					
					//~ X_Velocity_star[r][c] = numerator/denominator;
					Residue_u[INDEX(r,c,NC)] = u_ptr[INDEX(r,c,NC)] - ((f_u[INDEX(r,c,NC)] + numerator ) / denominator) ; 
					
				}
					
					
				if (r!=1){
					mu_left		=	0.25*( mu_private[INDEX(r,c,NC)] 	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_right		=	0.25*( mu_private[INDEX(r,c+1,NC)] 	+	mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r-1,c,NC)]	+	mu_private[INDEX(r-1,c+1,NC)]);
					rho_r		=	0.5*(rho_private[INDEX(r-1,c,NC)]	+ rho_private[INDEX(r,c,NC)] );
					
					
					double denominator = 1.0 + delT/rho_r/del_sq*implicity* (2.0*mu_private[INDEX(r,c,NC)]  + 2.0*mu_private[INDEX(r-1,c,NC)]  + mu_right*eta_p[c+1]/RF_p[c]  + mu_left*eta_p[c]/RF_p[c]  );
					
					double numerator =  delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]  * v_ptr[INDEX(r+1,c,NC)] + 2.0*mu_private[INDEX(r-1,c,NC)] *v_ptr[INDEX(r-1,c,NC)] + mu_right * (v_ptr[INDEX(r,c+1,NC)] + u_ptr[INDEX(r,c+1,NC)] - u_ptr[INDEX(r-1,c+1,NC)]) *eta_p[c+1]/RF_p[c] 
									- mu_left* ( -v_ptr[INDEX(r,c-1,NC)] + u_ptr[INDEX(r,c,NC)] - u_ptr[INDEX(r-1,c,NC)])*eta_p[c]/RF_p[c]  );
					
					//~ Y_Velocity_star[r][c] = numerator/denominator;
					Residue_v[INDEX(r,c,NC)]  = v_ptr[INDEX(r,c,NC)] - (( f_v[INDEX(r,c,NC)] + numerator ) /denominator); 
				}				
				
			}
			
		}
	


}

/*************************************************************************/
 double Calculate_Residue_implicit (  double *f_u,double *f_v, double *u_ptr,double *v_ptr, int NR,int NC, double *rho_private,double *mu_private, double h ) 
{
	
  double h_sq;
  double del_sq;
        h_sq = h*h;
	del_sq = h_sq;
	int r,c;
	double  Residue = 0.0;	

	double  Res_inf_norm=0.0;
	
	
	double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
	
	
	double mu_left, mu_right, rho_r,mu_bottom,mu_up;

		for (r=1; r<=NR; r++)
		{
                        for (c=1; c<=NC; c++)
                        {
								
				if (c!=1){
					rho_r		=	0.5*( rho_private[INDEX(r,c-1,NC)]	+ rho_private[INDEX(r,c,NC)]);
					mu_bottom	=	0.25*( mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_up 		=   0.25*( mu_private[INDEX(r+1,c,NC)] 	+	mu_private[INDEX(r+1,c-1,NC)]	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r,c,NC)]);
					
					
					//basilisk
					double denominator = 1.0 + delT/rho_r*implicity * (mu_private[INDEX(r,c,NC)]+mu_private[INDEX(r,c-1,NC)])/pow(eta_p[c],2)*axi + delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]*RF_p[c]/eta[c]  + 2.0*mu_private[INDEX(r,c-1,NC)]*RF_p[c-1]/eta_p[c]  + mu_up + mu_bottom);
					
					
					double numerator = delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]* u_ptr[INDEX(r,c+1,NC)] *RF_p[c]/eta_p[c]  + 2.0* mu_private[INDEX(r,c-1,NC)]*u_ptr[INDEX(r,c-1,NC)] *RF_p[c-1]/eta_p[c] + mu_up * (u_ptr[INDEX(r+1,c,NC)] + (v_ptr[INDEX(r+1,c,NC)] - v_ptr[INDEX(r+1,c-1,NC)])  ) 
												- mu_bottom* ( - u_ptr[INDEX(r-1,c,NC)] + v_ptr[INDEX(r,c,NC)] - v_ptr[INDEX(r,c-1,NC)] ));
					
					
					//~ X_Velocity_star[r][c] = numerator/denominator;
					Residue = u_ptr[INDEX(r,c,NC)] - ((f_u[INDEX(r,c,NC)] + numerator ) / denominator ); 
					
				}
				
				if (fabs(Residue)>Res_inf_norm)
				{
				      Res_inf_norm = fabs(Residue);
				}
					
					
				if (r!=1){
					mu_left		=	0.25*( mu_private[INDEX(r,c,NC)] 	+	mu_private[INDEX(r,c-1,NC)]	+	mu_private[INDEX(r-1,c-1,NC)]	+	mu_private[INDEX(r-1,c,NC)] );
					mu_right		=	0.25*( mu_private[INDEX(r,c+1,NC)] 	+	mu_private[INDEX(r,c,NC)]	+	mu_private[INDEX(r-1,c,NC)]	+	mu_private[INDEX(r-1,c+1,NC)]);
					rho_r		=	0.5*(rho_private[INDEX(r-1,c,NC)]	+ rho_private[INDEX(r,c,NC)] );
					
					
					double denominator = 1.0 + delT/rho_r/del_sq*implicity* (2.0*mu_private[INDEX(r,c,NC)]  + 2.0*mu_private[INDEX(r-1,c,NC)]  + mu_right*eta_p[c+1]/RF_p[c]  + mu_left*eta_p[c]/RF_p[c]  );
					
					double numerator =  delT/rho_r/del_sq*implicity * (2.0*mu_private[INDEX(r,c,NC)]  * v_ptr[INDEX(r+1,c,NC)] + 2.0*mu_private[INDEX(r-1,c,NC)] *v_ptr[INDEX(r-1,c,NC)] + mu_right * (v_ptr[INDEX(r,c+1,NC)] + u_ptr[INDEX(r,c+1,NC)] - u_ptr[INDEX(r-1,c+1,NC)]) *eta_p[c+1]/RF_p[c] 
									- mu_left* ( -v_ptr[INDEX(r,c-1,NC)] + u_ptr[INDEX(r,c,NC)] - u_ptr[INDEX(r-1,c,NC)])*eta_p[c]/RF_p[c]  );
					
					//~ Y_Velocity_star[r][c] = numerator/denominator;
					Residue = v_ptr[INDEX(r,c,NC)] - (( f_v[INDEX(r,c,NC)] + numerator ) /denominator); 
				}


				if (fabs(Residue)>Res_inf_norm)
				{
				      Res_inf_norm = fabs(Residue);
				}


				
				
			}
			
		}

	
	return Res_inf_norm;
}


void Restrictor_implicit ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
	

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							Res_hat[INDEX(r,c,NC)] = 0.25*(Res[INDEX(2*r-1,2*c-1,2*(NC))] + Res[INDEX(2*r-1,2*c,2*(NC))] + Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ Res[INDEX(2*r,2*c,2*(NC))]);
							
			}

		}
	
		
		
}

void Restrictor_rho_implicit( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
		
	double temp;

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							temp = 0.25*(1./Res[INDEX(2*r-1,2*c-1,2*(NC))] + 1./Res[INDEX(2*r-1,2*c,2*(NC))] + 1./Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ 1./Res[INDEX(2*r,2*c,2*(NC))]);
							
							Res_hat[INDEX(r,c,NC)] = 1./temp;
							
			}

		}

		
}

void Restrictor_mu_implicit ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
		
	double temp;

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							temp = 0.25*(1./Res[INDEX(2*r-1,2*c-1,2*(NC))] + 1./Res[INDEX(2*r-1,2*c,2*(NC))] + 1./Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ 1./Res[INDEX(2*r,2*c,2*(NC))]);
							
							Res_hat[INDEX(r,c,NC)] = 1./temp;
							
			}

		}

		
		
}

UVptr prolongator_implicit(UVptr UV_hat, double *uCorr,double *vCorr, UVptr UV, int NR, int NC)
{
	
	int i,r,c;
	

		

	  for (r=0; r<NR; r++)
                    {
                        for (c=0; c<NC; c++)
                        {
				uCorr[INDEX(2*r+1,2*c+1,2*NC)] = 0.0625*(9*UV_hat.uptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.uptr[INDEX(r,c+1,NC)] + 3*UV_hat.uptr[INDEX(r+1,c,NC)] + UV_hat.uptr[INDEX(r,c,NC)]);
				
				uCorr[INDEX(2*r+1,2*c+2,2*NC)] = 0.0625*(9*UV_hat.uptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.uptr[INDEX(r+1,c+2,NC)] + 3*UV_hat.uptr[INDEX(r,c+1,NC)] + UV_hat.uptr[INDEX(r,c+2,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+1,2*NC)] = 0.0625*(9*UV_hat.uptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.uptr[INDEX(r+2,c+1,NC)] + 3*UV_hat.uptr[INDEX(r+1,c,NC)] + UV_hat.uptr[INDEX(r+2,c,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+2,2*NC)] = 0.0625*(9*UV_hat.uptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.uptr[INDEX(r+1,c+2,NC)] + 3*UV_hat.uptr[INDEX(r+2,c+1,NC)] + UV_hat.uptr[INDEX(r+2,c+2,NC)]);

				
				vCorr[INDEX(2*r+1,2*c+1,2*NC)] = 0.0625*(9*UV_hat.vptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.vptr[INDEX(r,c+1,NC)] + 3*UV_hat.vptr[INDEX(r+1,c,NC)] + UV_hat.vptr[INDEX(r,c,NC)]);
				
				vCorr[INDEX(2*r+1,2*c+2,2*NC)] = 0.0625*(9*UV_hat.vptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.vptr[INDEX(r+1,c+2,NC)] + 3*UV_hat.vptr[INDEX(r,c+1,NC)] + UV_hat.vptr[INDEX(r,c+2,NC)]);
				
				vCorr[INDEX(2*r+2,2*c+1,2*NC)] = 0.0625*(9*UV_hat.vptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.vptr[INDEX(r+2,c+1,NC)] + 3*UV_hat.vptr[INDEX(r+1,c,NC)] + UV_hat.vptr[INDEX(r+2,c,NC)]);
				
				vCorr[INDEX(2*r+2,2*c+2,2*NC)] = 0.0625*(9*UV_hat.vptr[INDEX(r+1,c+1,NC)] + 3*UV_hat.vptr[INDEX(r+1,c+2,NC)] + 3*UV_hat.vptr[INDEX(r+2,c+1,NC)] + UV_hat.vptr[INDEX(r+2,c+2,NC)]);
				
					
				
			}
		}
		
		
		int nNR=2*NR,nNC=2*NC;	// for the loop below
		
		   for (r=1; r<=nNR; r++)
			{
				for (c=1; c<=nNC; c++)
				{	
				
					UV.uptr[INDEX(r,c,2*NC)] = uCorr[INDEX(r,c,2*NC)] + UV.uptr[INDEX(r,c,2*NC)];
					UV.vptr[INDEX(r,c,2*NC)] = vCorr[INDEX(r,c,2*NC)] + UV.vptr[INDEX(r,c,2*NC)];
					
								
				}
			
			}
			

	
	return UV;	
		
}


	 	 
/********************************V-Cycle**********************************************/
UVptr Vcycle_implicit(int NR,int NC, double *u_init, double *v_init, double *f_uptr, double *f_vptr, int iter, int flag,double *rho,double *mu,double h)
{
	double *Res_u,*Res_hat_u,*u_ptr,*uCorr,*rho_hat,*mu_hat;
	double *Res_v,*Res_hat_v,*v_ptr,*vCorr;
	
	UVptr UV,UV_hat;

	int i; 
	int r,c;
	
	
	
	if (flag==1)
	{
		u_ptr = u_init;
		v_ptr = v_init;
			
		//rho_hat = rho;
		flag=0;
		//printf("haha");
		//fflush(stdout);
		//exit(0);
		
		//printf("flag = %d\n",flag);
	}
	else
		{
		
		u_ptr  =  (double *)calloc((NR+2) * (NC+2),sizeof(double));   //( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
		v_ptr  =  (double *)calloc((NR+2) * (NC+2),sizeof(double));   //( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
		
			
		//flag=0;
			
	}
	
	
	for (i=0;i<iter;i++)
	{
		
			
			UV = jacobi_implicit(f_uptr,f_vptr,u_ptr,v_ptr,NR,NC,rho,mu,h );			//void jacobi ( int num_procs, double f[], double *u_ptr, double *u_new_ptr, int NR, int NC ) 
	
			
		
		
		
	}


	
	if(NR<=roll_back || NC<=roll_back )
	{
		
		return UV;
	}
	
	Res_u  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
	Res_v  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
		
	uCorr  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );	// correction from coarser grids		
	vCorr  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );	
	
	 Residue_grid_implicit (f_uptr,f_vptr,u_ptr,v_ptr,Res_u,Res_v,NR,NC,rho,mu,h);  // double *Residue_grid (int num_procs, double *f, double *u_ptr ) 
	
 
	
			NR = (NR/2); //keep this here
			NC =  (NC/2);
			h=2*h;
			
			
			Res_hat_u = (double *)calloc((NR+2)*(NC+2),sizeof(double));
			Res_hat_v = (double *)calloc((NR+2)*(NC+2),sizeof(double));
		
			rho_hat  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
			mu_hat  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
	
			 Restrictor_implicit(Res_u,Res_hat_u,NR,NC);
			 
			 Restrictor_implicit(Res_v,Res_hat_v,NR,NC);
			
			 Restrictor_rho_implicit(rho,rho_hat,NR,NC);
			 
			 Restrictor_mu_implicit(mu,mu_hat,NR,NC);
			 
			
	
	
			
			 UV_hat = Vcycle_implicit( NR, NC, u_init,v_init, Res_hat_u,Res_hat_v, iter,flag,rho_hat,mu_hat,h);
		
			

			
			
			UV = prolongator_implicit( UV_hat, uCorr,vCorr, UV, NR, NC);
			

			
			
			u_ptr = UV.uptr;
			v_ptr = UV.vptr;
			
		
		for (i=0;i<iter;i++)
		{
			UV = jacobi_implicit (f_uptr,f_vptr,u_ptr,v_ptr,2*NR,2*NC,rho,mu,h/2 );
			
		}
	
	
	
		
			free(UV_hat.uptr);
			free(UV_hat.vptr);
		
			free(rho_hat); 
			free(mu_hat); 
		
		
			free(Res_u);
			free(Res_v);
			free(uCorr); 
			free(vCorr); 	

			free(Res_hat_u);
			free(Res_hat_v);
			

			return UV;
			
			
}


/**************/

		
/**************************************************
Description: 2D Poisson Multigrid Solver D2u = f

Limitation: Refinement allowed only in powers of 2
		  and nows of cells in horizontal and vertical
		  direction must be equal

Author: Palas Kumar Farsoiya, IITB
********************************************************/



void Poissons_Solver_Multigrid_implicit(){	
	
	//~ printf(" PS: MG "); fflush(stdout);
	
	int r,c;
	
	int GS_iter = 10;
	
	//~ n = NoOfRows+2;		// must be odd number	//no of rows and columns should be equal
	//~ h = 1.0/(n-2);	
	//~ h_sq = h*h;
	double pi = 3.14159265359;
	double  *f_u, *f_v;	//Press -  is updated u, f is RHS of poisson's equation, ut is exact solution


	double *u,*v,*rho_ptr,*mu_ptr;	// pointers for updated u and RHS,  used in recursive V cycle
	

		rho_ptr  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		mu_ptr  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		u  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		v  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );

		f_u  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		f_v  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );

		//u  = ( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
	
	//init guess on the finest grid is the zero vector 
	BC_implicit();  // keep this here to be updated for any time dependent BC
		
	double h=del;
	double mu_left, mu_right, rho_r,mu_bottom,mu_up;
	for(r=0; r<=NoOfRows+1; r++)	
	{	
		for(c=0;c<=NoOfCols+1;c++)
		{
			u[INDEX(r,c,NoOfCols)] = X_Velocity_imp[r][c] ;
			v[INDEX(r,c,NoOfCols)] = Y_Velocity_imp[r][c] ;
			rho_ptr[INDEX(r,c,NoOfCols)] = rho[r][c];
			mu_ptr[INDEX(r,c,NoOfCols)] = mu[r][c];
			
			if (r>0 && r<NoOfRows+1 && c>0 && c<NoOfCols+1)
			{
				if (c!=1){
					
					rho_r		=	0.5*( rho[r][c-1]	+ rho[r][c]);
					mu_bottom	=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
					mu_up =   0.25*( mu[r+1][c] 	+	mu[r+1][c-1]	+	mu[r][c-1]	+	mu[r][c] );
				
					f_u[INDEX(r,c,NoOfCols)] = X_Velocity_star[r][c] + delT/rho_r/del_sq*(1.0-implicity) * (2.0*mu[r][c]* (X_Velocity[r][c+1] - X_Velocity[r][c])*RF[c]/eta[c] - 2.0* mu[r][c-1]*(X_Velocity[r][c] - X_Velocity[r][c-1])*RF[c-1]/eta[c]
								       + mu_up *( X_Velocity[r+1][c] - X_Velocity[r][c] + (Y_Velocity[r+1][c] - Y_Velocity[r+1][c-1])  ) - mu_bottom* (X_Velocity[r][c] - X_Velocity[r-1][c] + Y_Velocity[r][c] - Y_Velocity[r][c-1])) - delT/rho_r*(1.0-implicity)*(mu[r][c]+mu[r][c-1]) * ( X_Velocity[r][c]/pow(eta[c],2) )*axi;
				}
				
				if (r!=1){
					mu_left		=	0.25*( mu[r][c] 	+	mu[r][c-1]	+	mu[r-1][c-1]	+	mu[r-1][c] );
					mu_right	=	0.25*( mu[r][c+1] 	+	mu[r][c]	+	mu[r-1][c]	+	mu[r-1][c+1] );
					rho_r		=	0.5*( rho[r-1][c]	+ rho[r][c]);
					
					f_v[INDEX(r,c,NoOfCols)]  = Y_Velocity_star[r][c] + delT/rho_r/del_sq*(1.0-implicity) * (2.0*mu[r][c] * (Y_Velocity[r+1][c] - Y_Velocity[r][c] )- 2.0*mu[r-1][c]*(Y_Velocity[r][c] -Y_Velocity[r-1][c] )+ mu_right * (Y_Velocity[r][c+1]  - Y_Velocity[r][c] + X_Velocity[r][c+1] - X_Velocity[r-1][c+1])*eta[c+1]/RF[c] 
									- mu_left* (Y_Velocity[r][c]  - Y_Velocity[r][c-1] + X_Velocity[r][c] - X_Velocity[r-1][c] )*eta[c]/RF[c] );
				
				}
			}
			
			
		}
		
	}
	//~ exit(0);

	

	
	
	
	int flag=1;
	double Max_Res_old=10;
	double Max_Res_new=10;
	int step_vcycle=0;
	
	UVptr UV; 	// create struct variable
		
	
			while ( Max_Res_new>tolerance )
			  {
				
				
				UV =  Vcycle_implicit( NoOfRows, NoOfCols, u, v, f_u, f_v, GS_iter,flag,rho_ptr,mu_ptr,del);
				
				flag=1;
				  
				 u = UV.uptr;
				 v = UV.vptr;



				Max_Res_new = Calculate_Residue_implicit ( f_u ,f_v, u,v, NoOfRows, NoOfCols,rho_ptr,mu_ptr, del) ;
				//Max_Res_new = 2*delT*Max_Res_new;
				  
					   	
					if (step_vcycle>1000)
					{
						printf("\t MG viscous iter Failed -- roll_back increased "); fflush(stdout);
						roll_back = roll_back+1;
						Poissons_Solver_Multigrid_implicit();
						return ;
					}

					

				++step_vcycle; 
				//~ printf ( "\n iter=%d\tMaxRes = %e\n", step_vcycle,Max_Res_new );

				for(r=0;r<=NoOfRows+1;r++)
				{
					for(c=0;c<=NoOfCols+1;c++)
					{
						X_Velocity_imp[r][c] = u[INDEX(r,c,NoOfCols)];
						Y_Velocity_imp[r][c] = v[INDEX(r,c,NoOfCols)];
					}
				}



			  }


			for(r=0;r<=NoOfRows+1;r++)
			{
				for(c=0;c<=NoOfCols+1;c++)
				{
					X_Velocity_star[r][c] = u[INDEX(r,c,NoOfCols)];
					Y_Velocity_star[r][c] = v[INDEX(r,c,NoOfCols)];
				}
			}			  
			  
			
			
			  

			 //~ if (Max_Res_new<(tolerance*1.0e-5))	// sometimes zero comes out of inf division, then sor might be required
			 //~ {
				//~ printf("\t MG Failed -- Switched to SOR "); fflush(stdout);
				 //~ Poissons_Solver_SOR();
				 //~ return ;
			 //~ }
		

			
			

		
		BC_implicit();  // keep this here to be updated for any time dependent BC
		
			free(u); 
			free(v); 
			free(f_u);
			free(f_v);
			free(rho_ptr);
			free(mu_ptr);
			
			
			
			//~ printf ( " Div: %e", Max_Res_new );
			
	

	
}
	
