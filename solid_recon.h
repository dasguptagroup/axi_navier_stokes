/*#################################################################################
| FILE NAME:	HF implementation																						 |
|																											 |
| INTERFACE_SOLVER                                                                                                                       								 |
|																											 |
| DESCRIPTION:																								 |
| 1. Reconstructs a piecewise-linear interface from a given distribution of volume fractions using the LVIRA algorithm [1,2]			 |
| 2. Given a velocity field and an F distribution, it calculates the F distribution at the next time-step.								 |
|															              											 |
| LIMITATIONS:																								 |
| 1. Presently works only for an uniform grid.																		 |
| 2. Presently works only for 2D																					 |
| 3. Has to be modified for an open domain. See function CalculateF_Flux()													 |
|                                                                                                                                         										 |
| REFERENCES:                                                                                                                             									 |
| 1. A higher order projection method for tracking fluid interfaces in variable-density flows - JCP (130), 269-282 (1997), Puckett et al       |
| 2. Second-order volume-of-fluid algorithms for tracking material interfaces - JCP(199), 465-502 (2004), Pilliod et al                  		 |
| 3. Comparison of volume-of-fluid methods for surface-tension dominant two-phase flows - IJHMT(49), 740-754(2006), Gerlach et al       	 |
| 4. High-order surface-tension VOF model for 3D bubble flows with high density ratio - JCP (200), 153-176 (2004), Lorstad et al              |
##################################################################################


/*****************************************************************************************************************
NAME       : Find_Quad
DESCRIPTIOM: TAKES r AND c AS INPUT AND RETURNS THE QUADRANT BASED ON THE SIGNS OF Nx AND Ny
LIMITATIONS: None
*****************************************************************************************************************/
int Find_Quad_solid(int r, int c)
{
      double Nx, Ny;
	int Quad;
if (LVIRA==0){
    Nx =  mycs_solid(r,c,1);//solid[r][c].Nx;
  Ny =  mycs_solid(r,c,0); //solid[r][c].Ny;
}
else{
	Nx =  solid[r][c].Nx;
	Ny =  solid[r][c].Ny;
}

	
	
    if (Nx > 0 && Ny >= 0)   //FIRST QUADRANT
      {
	Quad = 1;
      }
    else if (Nx <= 0 && Ny > 0) //SECOND QUADRANT
      {
	Quad = 2;
      }
    else if (Nx < 0 && Ny <= 0) //THIRD QUADRANT
      {
	Quad = 3;
      }
    else if (Nx >= 0 && Ny < 0) //FOURTH QUADRANT
      {
	Quad = 4;
      }
     else if (c==0)
     {
	     Quad = Find_Quad_solid(r,1);
     }
     else if (c==NoOfCols+1)
     {
	     Quad = Find_Quad_solid(r,NoOfCols);
     }
       else if (r==0)
     {
	     Quad = Find_Quad_solid(1,c);
     }
     else if (r==NoOfRows+1)
     {
	     Quad = Find_Quad_solid(NoOfRows,r);
     }
	else if (Remove_droplets==1)
	{
		solid[r][c].VolFrac = 0;
	}

	else //ERROR HANDLING
    {
      printf("\n\n***********************************************************\n\n");
	  printf("\nERROR IN SUBROUTINE Find_Quad\n");
	  printf("\n\n***********************************************************\n\n");
	  printf("%d\t%d\t%e\t%e\t%e",r,c,solid[r][c].Nx,solid[r][c].Ny,solid[r][c].VolFrac);
	    exit(0);
    }
    return Quad;
}


/******************************************************************************************************************
METHOD NAME       : Calculate_InitialSlope()
DESCRIPTION       : CALCULATES INITIAL SLOPE FROM [4,5]
KNOWN LIMITATIONS : CANNOT WORK FOR THE BOUNDARY CELLS. THIS HAS TO BE CORRECTED.
******************************************************************************************************************/
void Calculate_InitialSlope_solid()
{
  /*LOCAL VARIABLES**********/
  int r, c;
    double temp1, temp2, Nx, Ny;
  /**************************/

	// Green-gauss gradient
	if (Initial_guess_normal==0)
	{
		
		  for(r=1;r <= NoOfRows ; r++)
		   {
		      for(c=1;c <= NoOfCols ; c++)
		       {
				 if(solid[r][c].VolFrac > 0 && solid[r][c].VolFrac < 1)
				   {
						  temp1 = solid[r+1][c+1].VolFrac + (2.0*solid[r][c+1].VolFrac) + solid[r-1][c+1].VolFrac;
						  temp2 = solid[r+1][c-1].VolFrac + (2.0*solid[r][c-1].VolFrac) + solid[r-1][c-1].VolFrac;

						  Nx =  (temp1 - temp2)/del;

						  if(Nx != 0.0)
							{
							  Nx = -Nx; //THIS IS REQUIRED BECAUSE REF[3] FROM WHERE THE FORMULA(Nx,Ny) IS TAKEN, HAS THE DEFN OPPOSITE
							}

						  temp1 = solid[r+1][c+1].VolFrac + (2.0*solid[r+1][c].VolFrac) + solid[r+1][c-1].VolFrac;
						  temp2 = solid[r-1][c+1].VolFrac + (2.0*solid[r-1][c].VolFrac) + solid[r-1][c-1].VolFrac;
						  Ny    = (temp1 - temp2)/del;    //CHANGE POINT

						  if(Ny != 0.0)
							{
							  Ny    = -Ny; //CHECK THIS
							}
							if (Remove_droplets==1 && Nx==0 && Ny==0)  // nothing around an interfacial cell, 
							{
								solid[r][c].VolFrac = 0; // remove volfrac dirty fix, not a good idea, the velocities and pressures in such cells are quite high
							}

						  //~ Write_struct_solid(r,c,-100000,-100000,Nx,Ny,-100000,"-100000",-100000,-100000,-100000);
							
							solid[r][c].Nx = Nx;
							solid[r][c].Ny = Ny;
							
				   }
		      }
		   }
	}
	   
   //"Mixed-Youngs-Centered" normal approximation of Ruben Scardovelli.
   if (Initial_guess_normal==1)
   {
	     for(r=1;r <= NoOfRows ; r++)
		   {
		      for(c=1;c <= NoOfCols ; c++)
		       {
				 if(interface(r,c))
				{
					int ix;
					  double c_t,c_b,c_r,c_l;
					  double mx0,my0,mx1,my1,mm1,mm2;
					  
					  /* top, bottom, right and left sums of c values */
					  c_t =  solid[r+1][c-1].VolFrac +  solid[r+1][c].VolFrac +  solid[r+1][c+1].VolFrac;
					  c_b = solid[r-1][c-1].VolFrac +  solid[r-1][c].VolFrac +  solid[r-1][c+1].VolFrac;
					  c_r = solid[r-1][c+1].VolFrac +  solid[r][c+1].VolFrac +  solid[r+1][c+1].VolFrac;
					  c_l = solid[r-1][c-1].VolFrac +  solid[r][c-1].VolFrac +  solid[r+1][c-1].VolFrac;

					  /* consider two lines: sgn(my) Y =  mx0 X + alpha,
					     and: sgn(mx) X =  my0 Y + alpha */ 
					  mx0 = 0.5*(c_l-c_r);
					  my0 = 0.5*(c_b-c_t);

					  /* minimum coefficient between mx0 and my0 wins */
					  if (fabs(mx0) <= fabs(my0)) {
					    my0 = my0 > 0. ? 1. : -1.;
					    ix = 1;
					  }
					  else {
					    mx0 = mx0 > 0. ? 1. : -1.;
					    ix = 0;
					  }

					  /* Youngs' normal to the interface */
					  mm1 =  solid[r+1][c-1].VolFrac + (2.0*solid[r][c-1].VolFrac) + solid[r-1][c-1].VolFrac;
					  mm2 = solid[r+1][c+1].VolFrac + (2.0*solid[r][c+1].VolFrac) + solid[r-1][c+1].VolFrac;
					  mx1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception
					  mm1 =  solid[r-1][c+1].VolFrac + (2.0*solid[r-1][c].VolFrac) + solid[r-1][c-1].VolFrac;
					  mm2 =solid[r+1][c+1].VolFrac + (2.0*solid[r+1][c].VolFrac) + solid[r+1][c-1].VolFrac;
					  my1 = mm1 - mm2 + 1.0e-30;  // avoiding floating point exception

					  /* choose between the best central and Youngs' scheme */ 
					  if (ix) {
					    mm1 = fabs(my1);
					    mm1 = fabs(mx1)/mm1;
					    if (mm1 > fabs(mx0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
					  else {
					    mm1 = fabs(mx1);
					    mm1 = fabs(my1)/mm1;
					    if (mm1 > fabs(my0)) {
					      mx0 = mx1;
					      my0 = my1;
					    }
					  }
						
					  /* normalize the set (mx0,my0): |mx0|+|my0|=1 and
					     write the two components of the normal vector  */
						mm1 = absolute(mx0) + absolute(my0);
						Nx = mx0/mm1;
						Ny = my0/mm1;
					  
					  solid[r][c].Nx=Nx;
					  solid[r][c].Ny=Ny;
					  
					    //~ Write_struct_solid(r,c,-100000,-100000,Nx,Ny,-100000,"-100000",-100000,-100000,-100000);
				}
			  }
		  }
	  }
  
}


/***********************************************************************************************************************************************************
METHOD NAME: Calculate_distance
DESCRIPTION: BASED ON THE SHAPE OF THE AREA, CALCULATES THE PERPENDICULAR DISTANCE. ASSUMES EVERYTHING TO BE IN THE 1ST QUADRANT
LIMITATIONS :NO CHECKS FOR BOUNDARY VALUES AS YET / NO CHECKS FOR NX, NY = 0   
             NO CHECKS FOR SEEING IF THE STRING SHAPE IS NULL IN STRUCTsolid
***********************************************************************************************************************************************************/
void Calculate_distance_solid(int r, int c)
{
/*LOCAL VARIABLES*/
  double Theta;
  double VolFrac;
  char   Shape[30];
  double P;
  double al;
  double m;
  double A;
  double M;
  /****************/

  /*INITIALIZE*/
  Theta   = 0.0;
  VolFrac = 0.0;
  P       = 0.0;
  al = 0.0;
  m = 0.0;
  A = 0.0;
  M= 0.0;
  /****************************/
 
  /*FETCH THE VALUES INTO THE LOCAL VARIABLES*/
  Theta   =  solid[r][c].Theta;
  VolFrac =  solid[r][c].VolFrac;
   M = absolute(tan(Theta));
   m = M/(M+1);
   A = VolFrac*del_sq;
  /****************************************/	
  if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
	{
	 P = VolFrac*del;
	 al = P*m/del;	
	 //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,P,"-100000",-100000,-100000,al);
		
		solid[r][c].P = P;
		solid[r][c].al = al;
         return;
	}
	if (m <= 0.5 && 1-m >= 0.5)
	{
		if (A >= 0  &&  A <= ( del_sq*m/(2*(1-m)) )  )
		{
		al = sqrt(2*A*m*(1-m)/del_sq);
		//strcpy(Shape,"Triangle");
		P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));		
		}
                else if ( A > (del_sq*m/(2*(1-m)))  &&  A <= 0.5*del_sq)
		{       
		 al =  (A*(1-m)/del_sq) + m/2;
		 //strcpy(Shape,"Trapezium");  //theta<pi/4
		 P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
		}
		else if ( A > 0.5*del_sq  &&  A <= (del_sq*((2-3*m)/(2*(1-m)))) )
		{
		 al = (A*(1-m)/del_sq) + m/2;
		// strcpy(Shape,"Trapezium");  //theta<pi/4
		 P = ((al*del)/m)*sin(Theta);
		}
		else if ( A > (del_sq*((2-3*m)/(2*(1-m)))) &&  A < 1*del_sq)
		{
		 al = 1 - sqrt(2*m*(1-(A/del_sq))*(1-m));
		// strcpy(Shape,"Triangle_Complement");
		 P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
		}
         //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,P,"-100000",-100000,-100000,al);		
			solid[r][c].P = P;
		solid[r][c].al = al;
	}
	else if ( m > 0.5 && 1-m < 0.5)
	{
		if ( A >= 0 && A <= (del_sq*(1-m)/(2*m)) )
		{
		 al = sqrt(2*A*m*(1-m)/del_sq); 
		// strcpy(Shape,"Triangle");
	         P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
		}
		else if ( A> (del_sq*(1-m)/(2*m))  &&  A <= 0.5*del_sq)
		{
		 al = ((2*A*m/del_sq)-m+1)/2;
		// strcpy(Shape,"Trapezium");          //theta>pi/4
		 P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
		}
		else if ( A > 0.5*del_sq && A <= (del_sq*((3*m)-1)/(2*m)) ) 
		{
		 al = (2*m*A/del_sq-m+1)/2;
		 //strcpy(Shape,"Trapezium");     //theta>pi/4
		 P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
		}
		else if (A > (del_sq*((3*m)-1)/(2*m))  &&  A < 1*del_sq)
		{
	         al = 1 - sqrt((1-(A/del_sq))*2*m*(1-m));
	        // strcpy(Shape,"Triangle_Complement");
	         P = absolute(al*(M+1)*del/(sqrt((M*M)+1)));
	        }
	 //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,P,"-100000",-100000,-100000,al);	
		solid[r][c].P = P;
		solid[r][c].al = al;		
     	}
}
/************************************************************************************************************************************************************
METHOD NAME: IdentifyCell_AssignArea
DESCRIPTION: TAKES THE CELL NAME EG. NORTHWEST AND LOCATES THE POSITION OF THE CELL, BASED ON THE QUADRANT. ASSIGNS THE PASSED VALUE OF THE AREA TO THE CELL.
LIMITATIONS: NONE
*************************************************************************************************************************************************************/

void IdentifyCell_AssignArea_solid(int r, int c,int Quad, char CellName[30],  double Area)
{
  int R, C;
  if(!strcmp(CellName,"NorthWest") )
    {
       switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c-1;
               break;

               case 2:
                 R = r-1;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c+1;
               break;

               case 4:
                 R = r+1;
                 C = c+1;
               break;
        }
    }
  else if(!strcmp(CellName,"North") )
    {
      switch (Quad)
        {
               case 1:
                 R = r+1;
                 C = c;
               break;

               case 2:
                 R = r;
                 C = c-1;
               break;

               case 3:
                 R = r-1;
                 C = c;
               break;

               case 4:
				 R =r;
				 C =c+1;
			   break;
		}
    }
  else if(!strcmp(CellName,"NorthEast") )
    {
        switch (Quad)
		{
			   case 1:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 2:
				R = r+1;
				C = c-1;
			   break;

       		   case 3:
				 R = r-1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c+1;
			   break;
		}
    }
  else if( !strcmp(CellName,"West") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c-1;
			   break;

       		   case 2:
				R = r-1;
				C = c;
			   break;

       		   case 3:
			    R = r;
				C = c+1;
			   break;

       		   case 4:
				R = r+1;
				C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"East") )
    {
		switch (Quad)
		{
			   case 1:
				R = r;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c;
			   break;

       		   case 3:
				 R = r;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthWest") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c-1;
			   break;

       		   case 2:
				 R = r-1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 4:
				 R = r+1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"South") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c;
			   break;

       		   case 2:
				 R = r;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c;
			   break;

       		   case 4:
				 R = r;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"SouthEast") )
    {
		switch (Quad)
		{
			   case 1:
				R = r-1;
				C = c+1;
			   break;

       		   case 2:
				 R = r+1;
				 C = c+1;
			   break;

       		   case 3:
				 R = r+1;
				 C = c-1;
			   break;

       		   case 4:
				 R = r-1;
				 C = c-1;
			   break;
		}
    }
  else if(!strcmp(CellName,"Central") )
    {
      R = r;
      C = c;
    }
  else //ERROR HANDLING
    {
      printf("\n\n***********************************************************\n\n");
	  printf("\nERROR IN SUBROUTINE IDENTIFYCELL_ASSIGNAREA\n");
	  printf("\n\n***********************************************************\n\n");
	  exit(0);
    }

  //~ Write_struct_solid(R,C,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
     solid[R][C].Area=Area;
}

/*************************************************************************************************************************************************
METHOD NAME: Extrapolate_Line
DESCRIPTION: EXTRAPOLATES THE LINE IN THE "CENTRAL" CELL TO THE 3X3 BLOCK CENTRED ON IT.
             ASSIGNS AREAS BASED ON CELL NAMES EG.NORTHWEST CELL'S COORDINATES ARE [r+1,c-1] FOR 1ST QUADRANT, [r-1,c-1] FOR 2ND QUADRANT ETC.
             ROTATES THE 3X3 BLOCK CLOCKWISE TO REDUCE EVERY QUADRANT TO THE FIRST QUADRANT.
LIMITATIONS:
**************************************************************************************************************************************************
 _______________________________________________________
|                 	|                    	|       	         |
|                 	|                    	|               	 |
|                 	|	             	|       	       	 |
| NorthWest	|      North     	|    NorthEast   |
|                 	|                    	|                        |
|                 	|                    	|                        |
|_______________	|_______________ |________________|
|                 	|                       |                |
|                 	|                       |                |
|                 	|                       |                |
|    West         	|      Central     |    East        |
|                 	|                       |                |
|                 	|                       |                |
|_______________|_______________ |________________|
|                 |                    |                |
|                 |                    |                |
|                 |                    |                |
|    SouthWest    |      South         |    SouthEast   |
|                 |                    |                |
|                 |                    |                |
|                 |                    |                |
|_________________|____________________|________________|
*/

void Extrapolate_Line_solid(int r, int c)
{
  /****LOCAL VARIABLES****/
  int    Quad;
  long double y_0, y_del, y_2del, y_3del;   //the value of y at x = 0 , x = del , x = 2*del and x = 3*del
  long double x_0, x_del, x_2del, x_3del ;  //the value of x at y = 0 , y = del , y = 2*del and y = 3*del
  long double Area;
  long double Theta; 
  long double Slope;      
  long double P;
  char   Shape[30];
  long double al;
  long double m;
  /**********************/

  //~ strcpy(Shape,solid[r][c].Shape);
  Quad        =  Find_Quad_solid(r,c);
  P           =  solid[r][c].P;
  Theta  =  solid[r][c].Theta;
  al = solid[r][c].al;
 
  IdentifyCell_AssignArea(r,c,Quad,"Central",solid[r][c].VolFrac);   //for the central cell, assign the same VolFrac

  //if rectangle, 
  if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
    {
      Area = P*del;
      if(Quad == 1)
        {
            //~ Write_struct_solid(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r  ,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
	
		solid[r+1][c+1].Area = 0;
		solid[r][c+1].Area = 0;
		solid[r-1][c+1].Area = 0;
		
            //~ Write_struct_solid(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r+1][c].Area = Area;
		solid[r-1][c].Area = Area;

            //~ Write_struct_solid(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r  ,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r+1][c-1].Area = 1.0;
		solid[r][c-1].Area = 1.0;
		solid[r-1][c-1].Area = 1.0;
		
        }
      else if(Quad == 2)
        {
            //~ Write_struct_solid(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r+1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r+1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		solid[r+1][c-1].Area = 0;
		solid[r+1][c].Area = 0;
		solid[r+1][c+1].Area = 0;
		
            //~ Write_struct_solid(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r][c-1].Area = Area;
		solid[r][c+1].Area = Area;

            //~ Write_struct_solid(r-1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r-1][c-1].Area = 1.0;
		solid[r-1][c].Area = 1.0;
		solid[r-1][c+1].Area = 1.0;
        }
      else if(Quad == 3)
        {
            //~ Write_struct_solid(r+1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r  ,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		solid[r+1][c-1].Area = 0;
		solid[r][c].Area = 0;
		solid[r-1][c-1].Area = 0;
		
            //~ Write_struct_solid(r+1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
	
		solid[r+1][c].Area = Area;
		solid[r-1][c].Area = Area;

		//~ Write_struct_solid(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r  ,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r+1][c+1].Area = 1.0;
		solid[r][c+1].Area = 1.0;
		solid[r-1][c+1].Area = 1.0;
		
		
        }
      else if(Quad == 4)
        {
            //~ Write_struct_solid(r-1,c-1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c  ,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r-1,c+1,-100000,0.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            
		solid[r-1][c-1].Area = 0;
		solid[r-1][c].Area = 0;
		solid[r-1][c+1].Area = 0;
		
            //~ Write_struct_solid(r,c-1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r,c+1,-100000,Area,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r][c-1].Area = Area;
		solid[r][c+1].Area = Area;
		
            //~ Write_struct_solid(r+1,c-1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r+1,c  ,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
            //~ Write_struct_solid(r+1,c+1,-100000,1.0,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		
		solid[r+1][c-1].Area = 1.0;
		solid[r+1][c].Area = 1.0;
		solid[r+1][c+1].Area = 1.0;
		
        } 
      return;
    }
 
         Theta       =  solid[r][c].Theta;
         Slope       =  tan(Theta); 
	 m            =   Slope/(Slope+1);
         /// ALL CONDITIONS ARE CHECKED USING THE FOLLOWING EQUATION
         /// y = tan(theta)[h + p*cosec(theta) - x] + h 
         /// P IS MEASURED FROM THE BOTTOM LHS CORNER

         y_0   =  Slope*( del + (P/sin(Theta))           ) + del;
         y_del =  Slope*( del + (P/sin(Theta)) - del     ) + del;
         y_2del = Slope*(       (P/sin(Theta)) - del     ) + del;
         y_3del = Slope*(       (P/sin(Theta)) - del2    ) + del;

         x_0    = del + (P/sin(Theta)) + (del/Slope);
         x_del  = del + (P/sin(Theta))              ;
         x_2del = del + (P/sin(Theta)) - (del/Slope);
         x_3del = del + (P/sin(Theta)) - (del2/Slope);

      //SHADED AREA IS A TRIANGLE

         if( al <= m && al <= (1-m) )    //both for Theta <> PI/4.0     
          {
                //ASSIGN VOLFRAC TO CELLS WHICH ARE 0 OR 1
                IdentifyCell_AssignArea_solid(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"East",0.0);
                /******************************************/

                /*****************************CALCULATE AREAS FOR NORTHWEST & WEST CELL*****************************/
                 if (y_0 <= del2)  //IF NORTHWEST IS 0
                   {
                     IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea_solid(r,c,Quad,"West",Area);
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //IF NORTHWEST IS A TRIANGLE
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment.for NorthWest
                     IdentifyCell_AssignArea_solid(r,c,Quad,"West",Area);
                   }
                 else if( y_0 > del3 )   //IF NORTHWEST IS A TRAPEZIUM
                   {
                     Area = ( x_2del + x_3del )*delBy2;                       // Calculate the area of the trapezium for NorthWest
                     IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea_solid(r,c,Quad,"West",Area);
                   }
                 /***********************END*****************************************************************************/

                 /********CALCULATE AREAS FOR South AND SOUTHEAST CELLS (exchange x and y in all expressions above)***********/
                  if( x_0 <= del2 )    //IF SOUTHEAST IS 0
                  {
                    IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",0.0);

                    Area = ( (x_0 - del) + (x_del - del) )*delBy2;       //calculate area of trapezium for South
                    IdentifyCell_AssignArea_solid(r,c,Quad,"South",Area);
                  }
                 else if ( x_0 > del2 && x_0 <= del3 )                  //IF SOUTHEAST IS A TRIANGLE
                   {
                     Area = 0.5*(x_0 - del2)*y_2del;                       //Calculate area of the triangle for SouthEast
                     IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea_solid(r,c,Quad,"South",Area);
                   }
                 else if( x_0 > del3 )      //IF SOUTHEAST IS A TRAPEZIUM
                   {
                     Area = (y_2del + y_3del)*delBy2;                       // Calculate the area of the trapezium for SouthEast
                     IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);

                     Area = del_sq - ( 0.5*(del - y_2del)*(del2 - x_del) ); //Calculate area of triangle and take complement for South
                     IdentifyCell_AssignArea_solid(r,c,Quad,"South",Area);
                   }
                 /***********************END*********************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA < PI / 4
         else if ( al > m && al <= (1-m) )  // for Theta < PI / 4.0
        {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea_solid(r,c,Quad,"North",0.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"NorthEast",0.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"South",1.0);
                /******************************************/
                                
                /*****************************CALCULATE AREAS FOR NORTHWEST AND WEST CELLS*****************************/
                 if (y_0 <= del2)  //if NorthWest is 0
                   {
                     IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",0.0);

                     Area = ( (y_0 - del) + (y_del - del) )*delBy2;          //Calculate area of trapezium for West
                     IdentifyCell_AssignArea_solid(r,c,Quad,"West",Area);                  
                   }
                 else if ( y_0 > del2 && y_0 <= del3 )                  //if NorthWest is a triangle
                   {
                     Area = 0.5*(y_0 - del2)*x_2del;                       //Calculate area of the triangle for NorthWest
                     IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);                     

                     Area = del_sq - ( 0.5*(del - x_2del)*(del2 - y_del) ); //Calculate area of triangle and take compliment for West
                     IdentifyCell_AssignArea_solid(r,c,Quad,"West",Area);                  
                   }            
                 /***********************END*****************************************************************************/

                 /*****************************CALCULATE AREAS FOR SOUTHEAST AND EAST CELLS*****************************/
                if(y_3del >= del)    //if SouthEast is 1
                  {
                     IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",1.0);
                     
                     Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
                     IdentifyCell_AssignArea_solid(r,c,Quad,"East",Area);
                  }
                else if(y_3del > 0 && y_3del < del)   //if SouthEast is a complement of a triangle
                  {
                    Area = del_sq  - ( 0.5*(del3 - x_del)*(del-y_3del) ); //calculate area of the triangle and then take complement for SouthEast
                    IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);

                    Area = 0.5*(y_2del - del)*(x_del - del2);     //calculate area of area of the trinagle for East
                    IdentifyCell_AssignArea_solid(r,c,Quad,"East",Area);
                  }
                 /***********************END****************************************************************************/
        }

      //SHADED AREA IS A TRAPEZIUM WITH THETA > PI / 4
         else if ( al> (1-m) &&  al <= m )   // for Theta > PI / 4.0
         {
                //Assign VolFRac to cells which are 0 or 1
                IdentifyCell_AssignArea_solid(r,c,Quad,"West",1.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"SouthWest",1.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"East",0.0);
                IdentifyCell_AssignArea_solid(r,c,Quad,"NorthEast",0.0);
                /******************************************/
                
                /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST*****************************/
                if(y_del >= del3)   //if NorthWest is 1
                  {
                    IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",1.0);
                    //printf("\nNW is 1\n");

                    Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
                    IdentifyCell_AssignArea_solid(r,c,Quad,"North",Area);
                    // printf("\nN %f \n",Area);
                  }
                else if(y_del < del3) //if NorthWest is complement of a triangle
                  {
                    Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest 
                    IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);

                    Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
                    IdentifyCell_AssignArea_solid(r,c,Quad,"North",Area);                 
                  }             
                /***********************END**************************************************************************/

                /*****************************CALCULATE AREAS FOR SOUTH & SOUTHEAST**********************************/
                if(x_0 <= del2)   //if SouthEast is 0
                  {
                    //printf("\nSE is 0\n");
                    IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",0.0);             

                    Area = ( (x_del - del) + (x_0 - del) )*delBy2;   //calculate area of the trapezium for South
                    IdentifyCell_AssignArea_solid(r,c,Quad,"South",Area);                 
                    //printf("\nN %f \n",Area);
                  }
                else if(x_0 >  del2)    //if SouthEast is a triangle
                  {
                    Area = 0.5*y_2del*(x_0 - del2);    //calculate area of the triangle for SouthEast
                    IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);            

                    Area = del_sq - (del - y_2del)*(del2 - x_del);      //calculate area of the triangle and take complement for South
                    IdentifyCell_AssignArea_solid(r,c,Quad,"South",Area);                 
                  }             
                /***********************END**************************************************************************/
        }

      //SHADED AREA IS A 5 SIDED FIGURE WHICH IS THE COMPLEMENT OF A TRIANGLE
         else if( al > m && al > (1-m) )  //both for Theta <> PI/4
        {
            //Assign VolFRac to cells which are 0 or 1
            IdentifyCell_AssignArea_solid(r,c,Quad,"West",1.0);
            IdentifyCell_AssignArea_solid(r,c,Quad,"SouthWest",1.0);
            IdentifyCell_AssignArea_solid(r,c,Quad,"South",1.0);
            IdentifyCell_AssignArea_solid(r,c,Quad,"NorthEast",0.0);
            /******************************************/

          /*****************************CALCULATE AREAS FOR NORTH & NORTHWEST**********************************/
          if(x_3del >= del)   //if NorthWest is 1
           {
             //printf("NW is 1\n");
              IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",1.0);

              Area = ( (x_2del - del) + (x_3del - del) )*delBy2;   //calculate area of the trapezium for North
              IdentifyCell_AssignArea_solid(r,c,Quad,"North",Area);
              //printf("\nN is %f",Area);
           }
          else if(x_3del < del  && x_3del > 0) //if NorthWest is complement of a triangle
           {
            Area = del_sq - ( 0.5*(del3 - y_del)*(del - x_3del) );   //calculate area of the triangle and take complement for NorthWest 
            IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);
            //printf("NW is %f",Area);

            Area = 0.5*(y_del - del2)*(x_2del - del);          //calculate area of the triangle for North
            IdentifyCell_AssignArea_solid(r,c,Quad,"North",Area);                 
           }            
          else if(x_3del  < 0)    //if NorthWest is a trapezium
           {
             Area = ( (y_0 - del2) + (y_del - del2) )*delBy2;    //calculate area of the trapezium for NorthWest
             IdentifyCell_AssignArea_solid(r,c,Quad,"NorthWest",Area);

             Area =0.5*(y_del - del2)*(x_2del - del);   //calculate area of the triangle for North
             IdentifyCell_AssignArea_solid(r,c,Quad,"North",Area);
           }
          /****************************************END**********************************************************/

          /*****************************CALCULATE AREAS FOR EAST & SOUTHEAST**********************************/
           if(y_3del >= del)       //if SouthEast is 1
            {
              IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",1.0);

              Area = ( (y_2del - del) + (y_3del - del) )*delBy2;   //calculate area of the trapezium for East
              IdentifyCell_AssignArea_solid(r,c,Quad,"East",Area);
            }
          else if(y_3del < del && y_3del > 0)   //if SouthEast is complement of a triangle
            {
              Area = del_sq - ( 0.5*(del3 - x_del)*(del - y_3del) );   //calculate area of the triangle and take complement for SouthEast 
              IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);

              Area = 0.5*(x_del - del2)*(y_2del - del);          //calculate area of the triangle for East
              IdentifyCell_AssignArea_solid(r,c,Quad,"East",Area);
            }
          else if(y_3del < 0)    //if SouthEast is a trapezium
            {         
              Area = ( (x_0 - del2) + (x_del - del2) )*delBy2;    //calculate area of the trapezium for SouthEast
              IdentifyCell_AssignArea_solid(r,c,Quad,"SouthEast",Area);

              Area =0.5*(x_del - del2)*(y_2del - del);   //calculate area of the triangle for East
              IdentifyCell_AssignArea_solid(r,c,Quad,"East",Area);
            }
          /****************************************END**********************************************************/
        }
	 else //ERROR HANDLING
        {
	  printf("\n***********************************************************\n");
	  printf("\nERROR - ERROR IN SUBROUTINE EXTRAPOLATE_LINE\n");
	  printf("\n***********************************************************\n");
	  //exit(0);
        }
}





/*************************************************************************************************************************
NAME       : Calculate_SumOfDiffSq
DESCRIPTION: CALCULATES THE SUM OF SQUARE OF THE DIFFERENCE BETWEEN THE ACTUAL AREA AND CALCULATED AREA FOR THE 3X3 BLOCK
LIMITATIONS: NONE
**************************************************************************************************************************/
  double Calculate_SumOfDiffSq_solid(int r, int c)
{
    double North_Area,South_Area,West_Area,East_Area,NorthWest_Area,NorthEast_Area,SouthWest_Area,SouthEast_Area;
    double North_VolFrac,South_VolFrac,West_VolFrac,East_VolFrac,NorthWest_VolFrac,NorthEast_VolFrac,SouthWest_VolFrac,SouthEast_VolFrac;
    double Square1, Square2, Square3, Square4, Square5, Square6, Square7, Square8;
    double Sum_Squares;

  North_Area    = solid[r+1][c].Area;
  North_VolFrac = solid[r+1][c].VolFrac*del_sq;

  South_Area    = solid[r-1][c].Area;
  South_VolFrac = solid[r-1][c].VolFrac*del_sq;

  West_Area     = solid[r][c-1].Area;
  West_VolFrac  = solid[r][c-1].VolFrac*del_sq;

  East_Area     = solid[r][c+1].Area;
  East_VolFrac  = solid[r][c+1].VolFrac*del_sq;

  NorthWest_Area     = solid[r+1][c-1].Area;
  NorthWest_VolFrac  = solid[r+1][c-1].VolFrac*del_sq;

  NorthEast_Area     = solid[r+1][c+1].Area;
  NorthEast_VolFrac  = solid[r+1][c+1].VolFrac*del_sq;

  SouthWest_Area     = solid[r-1][c-1].Area;
  SouthWest_VolFrac  = solid[r-1][c-1].VolFrac*del_sq;

  SouthEast_Area     = solid[r-1][c+1].Area;
  SouthEast_VolFrac  = solid[r-1][c+1].VolFrac*del_sq;

  Square1 = pow( North_Area      - North_VolFrac,2);
  Square2 = pow( South_Area      - South_VolFrac,2);
  Square3 = pow( West_Area       - West_VolFrac,2);
  Square4 = pow( East_Area       - East_VolFrac,2);
  Square5 = pow( NorthWest_Area  - NorthWest_VolFrac,2);
  Square6 = pow( SouthWest_Area  - SouthWest_VolFrac,2);
  Square7 = pow( NorthEast_Area  - NorthEast_VolFrac,2);
  Square8 = pow( SouthEast_Area  - SouthEast_VolFrac,2);

  Sum_Squares = Square1 + Square2 + Square3 + Square4 + Square5 + Square6 + Square7 + Square8;

  return Sum_Squares;
}


/*******************************************************************************************************************
NAME       : Calculate_Theta
DESCRIPTIOM: RETURNS THE ANGLE MADE BY THE LINE WITH THE NEGATIVE DIRECTION OF THE X-AXIS, IN AN ANTICLOCKWISE SENSE
LIMITATIONS:
********************************************************************************************************************/
void Calculate_Theta_solid(int r, int c)
{
  /*LOCAL VARIABLES*/
    double Nx, Ny, Theta,Quad;
  /*****************/
if (LVIRA==0){
  Nx =  mycs_solid(r,c,1);//solid[r][c].Nx;
  Ny =  mycs_solid(r,c,0); //solid[r][c].Ny;
}
else{
	Nx = solid[r][c].Nx;
	Ny =  solid[r][c].Ny;
}
  Quad = Find_Quad_solid(r,c);

  //FLAG --  IS THERE A BUG? FIND IT (corrected the theta values it looks like they are the angle for normal, but they should be the angle with interface with horizontal axis - Palas )
  if( Ny == 0.0 && Nx > 0.0 )
    {
      //Theta = 0.0; //this should be pi/2 because Theta is the  positive acute angle of the INTERFACE(not normal) made with the horizontal axis
	    Theta = PI/2;
      //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta,-100000,-100000);
	    solid[r][c].Theta=Theta;
      return;
    }
  else if ( Nx == 0.0 && Ny > 0.0 )
    {
      Theta = PI/2.0;		// this is the second quad if you rotate the normal to quad 1, the interface will make pi/2 with x-axis

      //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000,-100000);
	     solid[r][c].Theta=Theta;
      return;
    }
  else if( Ny == 0.0 && Nx < 0.0 )
    {
      //Theta = PI;   //this should be zero because Theta is the  positive acute angle of the INTERFACE(not normal) made with the horizontal axis
	  Theta = PI/2;   // third quadrant, if you rotate the normal to quad 1 angle will remain the same
      //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000,-100000);
	     solid[r][c].Theta=Theta;
      return;
    }
  else if( (Nx == 0.0 && Ny < 0.0) )
    {
      //Theta = 3.0*PiBy2;		//
	    Theta = PI/2;		//this is the fourth quad therefore it is pi/2, if you rotate the normal to 1
      //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000,-100000);
	     solid[r][c].Theta=Theta;
      return;
    }
    else
    {
	    Theta  = (PiBy2) - absolute(atan(Ny/Nx));
	    if(Quad == 2 || Quad == 4)
	    {
	      Theta = PiBy2 - Theta;
	    }
    }

   /****/

    // remember that theta has to be calculated in a way that if we rotate the cell to make the normal in the first quadrant, then what angle the interface makes with the x-axis that too positive acute angle.  --------Palas


Theta = Theta + 1.e-10; // This is a quick fix for theta==0,Pi/2,3Pi/2,2Pi cases, to avoid uneccessary coding effort
    
  //~ Write_struct_solid(r,c,-100000,-100000,-100000,-100000,-100000,"-100000",Theta, -100000,-100000);
     solid[r][c].Theta=Theta;
}

/*****************************************************************************************************************
NAME       : Rotate_Line
DESCRIPTIOM: ROTATES THE LINE CLOCKWISE / COUNTERCLOCKWISE
LIMITATIONS:
*****************************************************************************************************************/
void Rotate_Line_solid(int r, int c, char sense[20])
{
    double Nx, Ny;
  int Quad;

  Nx   =  solid[r][c].Nx;
  Ny   =  solid[r][c].Ny;
  Quad =  Find_Quad_solid(r,c);

  if(!strcmp(sense,"Clockwise"))
    {
		 if(Quad == 1)
		 {
		  Nx = Nx + Nx/100;
		  Ny = Ny - Ny/100;
		 }
		 else if(Quad == 2 )
		 {
		  Nx = Nx + Nx/100;
		  Ny = Ny + Ny/100;
		 }
		 else if(Quad == 3)
		 {
		  Nx = Nx - Nx/100;
		  Ny = Ny + Ny/100;
		 }
		 else if(Quad == 4)
		 {
		  Nx = Nx - Nx/100;
		  Ny = Ny - Ny/100;
		 }
    }
  else if(!strcmp(sense,"Counterclockwise"))
    {
		 if(Quad == 1)
		 {
		  Nx = Nx - Nx/100;
		  Ny = Ny + Ny/100;
		 }
		 else if(Quad == 2)
		 {
		  Nx = Nx - Nx/100;
		  Ny = Ny - Ny/100;
		 }
		 else if(Quad == 3)
		 {
  		  Nx = Nx + Nx/100;
		  Ny = Ny - Ny/100;
		 }
		 else if(Quad == 4)
		 {
  		  Nx = Nx + Nx/100;
		  Ny = Ny + Ny/100;
		 }
    }
 //~ Write_struct_solid(r,c,-100000,-100000,Nx,Ny,-100000,"-100000",-100000,-100000,-100000);
     solid[r][c].Nx=Nx;
     solid[r][c].Ny=Ny;
}
/*******END************/

/****************************************************************************************************************
METHOD NAME: Recon_Interface
DESCRIPTION: GIVEN A CELL, THIS FUNCTION FINDS OUT ST. LINES TO REPRESENT THE INTERFACE IN THAT CELL. THE OUTPUT
			 IS WRITTEN TO A FILE NAMED PLOTLINE_{STEP}.DAT. HERE {STEP} REPRESENTS AN INTEGER, CORRESPONDING
			 TO THE ACTUAL NO. OF TIME-STEPS FOR WHICH THE CALCULATION IS PERFORMED.
LIMITATIONS:
****************************************************************************************************************/
void Recon_Interface_solid(int r, int c)
{
     double Sum_OfDiff_Sq_clock, Sum_OfDiff_Sq_anticlock;
     double Sum_OfDiff_Sq_old, Sum_OfDiff_Sq_current;
   char sense1[20], sense2[20];
   char sense[20], Shape_current[20];
   int flag = 1;
     double Nx_current, Ny_current, Theta_current, P_current, Nx_clock, Ny_clock, Nx_anticlock, Ny_anticlock;

	strcpy(sense1,"Clockwise");
	strcpy(sense2,"Counterclockwise");
	
		//~ if (StepT>=221)
			//~ if(r==39 && c==2)
				//~ printf("debug");
 	if(solid[r][c].VolFrac > LOWER_LIMIT && solid[r][c].VolFrac < UPPER_LIMIT)
	{
		
			
			
		 Calculate_Theta_solid(r,c);

		 //~ Nx_current = mycs(r,c,1);   //preserve the old value
		 //~ Ny_current = mycs(r,c,0);   //preserve the old value
		
		  Nx_current = solid[r][c].Nx;   //preserve the old value
		Ny_current = solid[r][c].Ny;   //preserve the old value
		
		//~ if(r==32 && c==26)
		//~ {	printf("\n %lf\t%lf\t%lf\t%d",solid[r][c].Theta,Nx_current,Ny_current,Find_Quad(r,c));
		//~ exit(0); }
		 //Identify_Shape(r,c);       //find the shape depending on VolFrac and Slope
		 Calculate_distance_solid(r,c);   //find the perp. distance depending on shape
		Theta_current  = solid[r][c].Theta; //preserve the old value
		 P_current = solid[r][c].P;
		 //~ strcpy(Shape_current,solid[r][c].Shape);
		
		if (LVIRA==0)
		{
			return;
		}
		 Extrapolate_Line_solid(r,c);
		 Sum_OfDiff_Sq_current = Calculate_SumOfDiffSq_solid(r,c);
		

		 /*ROTATE CLOCKWISE AND CALCULATE SUM OF SQUARES*/
		 Rotate_Line_solid(r,c,sense1);   //change the Nx and Ny values
		 Nx_clock = solid[r][c].Nx;   //preserve the clockwise value
		 Ny_clock = solid[r][c].Ny;   //preserve the clockwise value
		 Calculate_Theta_solid(r,c);
		 //printf("\nAfter clockwise rotation the value of Theta is %f\n", solid[r][c].Theta);
		 //Identify_Shape(r,c);
		 Calculate_distance_solid(r,c);
		 Extrapolate_Line_solid(r,c);
		 Sum_OfDiff_Sq_clock = Calculate_SumOfDiffSq_solid(r,c);
		 /************************************************/
		//~ printf("\n%e",Nx_current);
		//~ exit(0);

		 //Write_struct_solid(r,c,-100000,-100000,Nx_current,Ny_current,-100000,"-100000",-100000,-100000);  //restore the current values
		 //~ Write_struct_solid(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000,-100000);  //restore the current values
		
		solid[r][c].Nx = Nx_current;
		solid[r][c].Ny = Ny_current;
		solid[r][c].P = P_current;
		solid[r][c].Theta = Theta_current;
		
		 /*ROTATE COUNTER-CLOCKWISE AND CALCULATE SUM OF SQUARES*/
		 Rotate_Line_solid(r,c,sense2);  //change the Nx and Ny values
		 Nx_anticlock = solid[r][c].Nx;   //preserve the clockwise value
		 Ny_anticlock = solid[r][c].Ny;   //preserve the clockwise value
		 Calculate_Theta_solid(r,c);
		 //printf("\nAfter clockwise counterclockwise rotation the value of Theta is %f\n", solid[r][c].Theta);
		 //Identify_Shape(r,c);
		 Calculate_distance_solid(r,c);
		 Extrapolate_Line_solid(r,c);
		 Sum_OfDiff_Sq_anticlock = Calculate_SumOfDiffSq_solid(r,c);
		 /************************************************/

		 //Write_struct_solid(r,c,-100000,-100000,Nx_current,Ny_current,-100000,"-100000",-100000,-100000);  //restore the current values
		 //~ Write_struct_solid(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000,-100000);  //restore the current values
		 
		 solid[r][c].Nx = Nx_current;
		solid[r][c].Ny = Ny_current;
		solid[r][c].P = P_current;
		solid[r][c].Theta = Theta_current;
		 
		 if( Sum_OfDiff_Sq_current >= Sum_OfDiff_Sq_clock)
		   {
			 strcpy(sense,sense1);
			   
			 //~ Write_struct_solid(r,c,-100000,-100000,Nx_clock,Ny_clock,-100000,"-100000",-100000,-100000,-100000);  //make the clockwise value as current
			   
			    solid[r][c].Nx = Nx_clock;
			solid[r][c].Ny = Ny_clock;
			 //printf("\n THE LINE SHOULD BE ROTATED %s\n", sense);
		   }
		 else if(Sum_OfDiff_Sq_current >= Sum_OfDiff_Sq_anticlock)
		   {
			 strcpy(sense,sense2);
			 //~ Write_struct_solid(r,c,-100000,-100000,Nx_anticlock,Ny_anticlock,-100000,"-100000",-100000,-100000,-100000);  //make the clockwise value as current
			   
			    solid[r][c].Nx = Nx_anticlock;
			solid[r][c].Ny = Ny_anticlock;
			 //printf("\n THE LINE SHOULD BE ROTATED %s\n", sense);
		   }
		 else //CURRENT VALUE IS THE MINIMUM - NO NEED FOR ITERATIONS
		   {
			 return;
		   }

		 flag = 1;
		 while(flag)
		   {		
				 Calculate_Theta_solid(r,c);
				Theta_current  = solid[r][c].Theta; //preserve the old value
				 P_current = solid[r][c].P;
				 strcpy(Shape_current,solid[r][c].Shape);
				  Nx_current = solid[r][c].Nx;   //preserve the old value
					 Ny_current = solid[r][c].Ny;   //preserve the old value
			   
			   /***********************************/
			 Rotate_Line_solid(r,c,sense);
			 Calculate_Theta_solid(r,c);
			 //Identify_Shape(r,c);       //Find the shape depending on VolFrac and Slope
			 Calculate_distance_solid(r,c);   //Find the perp. distance depending on shape
			 Extrapolate_Line_solid(r,c);

			 Sum_OfDiff_Sq_old = Sum_OfDiff_Sq_current;
			 Sum_OfDiff_Sq_current = Calculate_SumOfDiffSq_solid(r,c);
			   //~ printf("\n%lf",Sum_OfDiff_Sq_current);
				/***********************************/
			 if (Sum_OfDiff_Sq_old <= Sum_OfDiff_Sq_current)
			{
				 flag = 0;
				//~ Write_struct_solid(r,c,-100000,-100000,Nx_current,Ny_current,P_current,Shape_current,Theta_current,-100000,-100000);  //restore the old values
				
				solid[r][c].Nx = Nx_current;
				solid[r][c].Ny = Ny_current;
				solid[r][c].P = P_current;
				solid[r][c].Theta = Theta_current;
				
			}
		}
		//~ exit(0);
	}
}
/*********************************************************************************
NAME       : InterfaceRecon_main()
DESCRIPTION: THIS IS A DUMY FUNCTION WHICH IS USED TO REMOVE CODE FROM main()
LIMITATIONS: NONE
/********************************************************************************/
void InterfaceRecon_main_solid()
{
	int r,c;

	Calculate_InitialSlope();
	for(r=1;r <= NoOfRows ; r++)
	{
	   for(c=1;c <= NoOfCols ; c++)
		{
		  Recon_Interface_solid(r,c);
		}
	}
}

/*****************************************************************************************************************
NAME       :plot_lines
DESCRIPTIOM: Writes the (x1,y1) and (x2,y2) values for each interfacial cell into variables
LIMITATIONS:
*****************************************************************************************************************/
void plot_lines_solid()
{

  /*DECLARE LOCAL VARIABLES*/
  int r,c;
  int Quad;
    double Theta;
    double P;
  char   Shape[30];
    double X1, Y1, X2, Y2;

		double al;
			double m;
  /*********************/


  for(r=1;r <= NoOfRows ; r++)
   {
     for(c=1;c <= NoOfCols ; c++)
       {		
		 if(solid[r][c].VolFrac > LOWER_LIMIT && solid[r][c].VolFrac < UPPER_LIMIT)
		   {
			 strcpy(Shape,solid[r][c].Shape);
			 Quad        =  Find_Quad_solid(r,c);
			 P           =  solid[r][c].P;
			 Theta       =  solid[r][c].Theta;
			 al    =  solid[r][c].al;
                         m       =    tan(Theta)/(1+tan(Theta));   

			 //~ if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
			   //~ {
				 //~ if(Quad == 1 || Quad == 3)
				   //~ {
					  //~ X1 = P + ((c-1)*del);
					  //~ Y1 = (r-1)*del;

					  //~ X2 = P + ((c-1)*del);
					  //~ Y2 = (r)*del;
				   //~ }
				 //~ else if (Quad == 2 || Quad == 4)
				   //~ {
					  //~ X1 = ((c-1)*del);
					  //~ Y1 = P + ((r-1)*del);

					  //~ X2 = (c)*del;
					  //~ Y2 = P + ((r-1)*del);
				   //~ }
			   //~ }
			  if( al <=m && al <= (1-m))
			 {
				 if(Quad == 1)
				   {
					 X1 = (P/sin(Theta)) + ((c-1)*del);
					 Y1 =  (r-1)*del;

					 X2 = (c-1)*del;
					 Y2 = (P/cos(Theta)) + ((r-1)*del);
				   }
				 else if (Quad == 2)
				   {
					 X1 = del - (P/cos(Theta)) + ((c-1)*del);
					 Y1 = (r-1)*del;

					 X2 = (c)*del;
					 Y2 = (P/sin(Theta)) + ((r-1)*del);
				   }
				 else if (Quad == 3)
				   {
					 X1 = (c)*del - (P/sin(Theta));
					 Y1 = (r)*del;

					 X2 = (c)*del;
					 Y2 = (r)*del - (P/cos(Theta));
				   }
				 else if(Quad == 4)
				   {
					 X1 = (c-1)*del;
					 Y1 = (r)*del - (P/sin(Theta));

					 X2 = (c-1)*del + (P/cos(Theta));
					 Y2 = (r)*del;
				   }
				}
				else if ( al > m && al <= (1-m) )  // for Theta < PI / 4.0
				{
					 if(Quad == 1)
					   {
						 X1 = (c-1)*del;
						 Y1 = (r-1)*del + (P/cos(Theta));

						 X2 = (c)*del;
						 Y2 = (P/cos(Theta)) - (del*tan(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c)*del - (P/cos(Theta));
						 Y1 =  (r-1)*del;

						 X2= (1+tan(Theta))*del - (P/cos(Theta)) + ((c-1)*del);
						 Y2= (r)*del;
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c-1)*del;
						 Y1 = (1+tan(Theta))*del - (P/cos(Theta)) + ((r-1)*del);

						 X2 = (c)*del;
						 Y2 = (r)*del - (P/cos(Theta));
					   }
					 else if(Quad == 4)
					   {
						 X1 = (P/cos(Theta)) - (del*tan(Theta)) + ((c-1)*del);
						 Y1 = (r-1)*del;

						 X2 = (P/cos(Theta)) + ((c-1)*del);
						 Y2 = (r)*del;
					   }
				   }
				 else if ( al > (1-m) && al <= m )  // for Theta > PI / 4.0
				   {
					 if(Quad == 1)
					   {
						 X1 = (P/sin(Theta)) - (del/tan(Theta)) + ((c-1)*del);
						 Y1 = (r)*del;

						 X2 = (P/sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c-1)*del;
						 Y1 = (P/sin(Theta)) - (del/tan(Theta)) + ((r-1)*del);

						 X2 = (c)*del;
						 Y2 = (P/sin(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c)*del - (P/sin(Theta));
						 Y1 = (r)*del;

						 X2 = (1+(1/tan(Theta)))*del - (P/sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if(Quad == 4)
					   {
						 X1 = (c-1)*del;
						 Y1 = (r)*del - (P/sin(Theta));

						 X2 = (c)*del;
						 Y2 = (1+ (1/tan(Theta)))*del - (P/sin(Theta)) + ((r-1)*del);
					   }
				   }
				 else if( al > (1-m) && al > m )  //both for Theta <> PI/4
				   {
					 if(Quad == 1)
					   {
						 X1 = -(del/tan(Theta)) + (P/sin(Theta)) + ((c-1)*del);
						 Y1 = (r)*del;

						 X2 = (c)*del;
						 Y2 = (P/cos(Theta)) - (del*tan(Theta)) + ((r-1)*del);
					   }
					 else if (Quad == 2)
					   {
						 X1 = (c-1)*del;
						 Y1 = (P*sin(Theta)) - (del/tan(Theta)) + (P*pow(cos(Theta),2)/sin(Theta)) + ((r-1)*del);

						 X2 = (1+tan(Theta))*del - (P*cos(Theta)) - (P*pow(sin(Theta),2)/cos(Theta)) + ((c-1)*del);
						 Y2 = (r)*del;
					   }
					 else if (Quad == 3)
					   {
						 X1 = (c-1)*del;
						 Y1 = (1+tan(Theta))*del - (P*cos(Theta)) - (P*pow(sin(Theta),2)/cos(Theta)) + ((r-1)*del);

						 X2 = (1+(1/tan(Theta)))*del - (P*pow(cos(Theta),2)/sin(Theta)) - (P*sin(Theta)) + ((c-1)*del);
						 Y2 = (r-1)*del;
					   }
					 else if(Quad == 4)
					   {
						 X1 = (P/cos(Theta)) - (del*tan(Theta)) + ((c-1)*del);
						 Y1 = (r-1)*del;

						 X2 = (c)*del;
						 Y2 = (del/tan(Theta)) - (P/sin(Theta)) + (r)*del;
					   }
					}


				   Xp1[r][c] = X1;
				    Yp1[r][c] = Y1;
				    Xp2[r][c] = X2;
				    Yp2[r][c] = Y2;



			   }
       }
  }


}