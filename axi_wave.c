
#include "navier-stokes-planar.h"
double bessel_prime_zero = 7.0155866698156; 


int main(int argc,char *argv[])
{
	
	rho_G =0.001;
	mu_G	=	0.0001;

	rho_L	=	1.0;
	mu_L	=	0.01;


	 SourceV = -981.0;
	Sigma = 72.0;
	CROST=1;
		Diffusion_term=1;
		Advection_term=1;
		Advection_Scheme = 5;
	Linear_Solver=1;
	axi=1;


		end = 1;
	step=0.001;
	Progress=1;
			
			 NoOfRows  = 256;
			NoOfCols 	= 256;
			del = bessel_prime_zero/NoOfCols;
				

			
/************User input above, no parameter should be below run_ns***********/	
	run_ns();
	
	return 0;	
	
}



// provide initial conditions below
double Init_F(double x, double y)
{	double a0 = 0.01;
	
	//~ return -((y -0.5)-(1.0+ a0*cos (2.*M_PI*(x-0.5)))) ;
    //~ return -((sq(x)+sq(y)) * sqrt(sq(x)+sq(y)) - (sq(x)+sq(y)) * R0 - a0 * 0.5 * (2*sq(x)-sq(y)));
	//~ return (sq(x)+sq(y/0.8) -1.0);
	
	//~ double a0 = 1.0;
	

	
  
	double bessels,integrand_i,integrand_ip1,theta_i,theta_ip1;
		double area=0;
		int i; 
	        int k=1;
		int n=1000;
	
	
		
		
			area = 0;
			for (i=0;i<n;i++)
			{
				theta_i = i*M_PI/n;
				theta_ip1 = (i+1)* M_PI/n;
				
				integrand_i = cos(k*(x)*sin(theta_i));
				integrand_ip1 = cos(k*(x)*sin(theta_ip1));
				
				area = 0.5*(integrand_i + integrand_ip1)*(M_PI/n) + area;
				
			
			}




			bessels = (area/M_PI)  ;

			

			
		
		

	
	
	return (y - a0*bessels - bessel_prime_zero*0.5 );
	
	
}

void event_phy(double Step)
{
	interface_data(Step);
	tecplot_data(Step);
	
}
void event_init()
{}
void event_bc()
{
	//left 
	BcDirichlet_U("left",0);
	BcNeumann_V("left");
	BcNeumann_F("left");
	BcNeumann_P("left");
	
	//right
	BcDirichlet_U("right",0);
	BcNeumann_V("right");
	BcNeumann_F("right");
	BcNeumann_P("right");
	
	//bottom
	BcDirichlet_V("bottom",0);
	BcNeumann_U("bottom");
	BcNeumann_F("bottom");
	BcNeumann_P("bottom");
	//top
	BcDirichlet_V("top",0);
	BcNeumann_U("top");
	BcNeumann_F("top");
	BcNeumann_P("top");
	
	}

double Init_U(double x, double y)
{
return 0;
	
}

double Init_V(double x, double y)
{
	return 0;
	
}

double Init_P(double x, double y)
{
	return 0;
	
}
void event_cp()
{}

	