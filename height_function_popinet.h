


void calculate_kappa_popinet()
{ 
	int r,c,r_temp;
	int r_bottom_empty_cell=0;
	int r_top_filled_cell=0;
	int r_bottom_filled_cell=0;
	int r_top_empty_cell=0;
	int r_bottom_interfacial_cell=0;
	int r_top_interfacial_cell=NoOfRows;
	
		int tag_l=-1;	// reset tags for each column
			int tag_u=-1;
	
		// columwise heights in y direction
		for(c=1;c<=NoOfCols;c++)
		{	
		
			
			for (r=1;r<=NoOfRows;r++)	
			{
					 r_bottom_interfacial_cell=0;
					r_top_interfacial_cell=NoOfRows;			
					
					if (interface(r,c) )
					{
						for ( r_temp=r-1; r_temp>=1; r_temp--) // go down
						{
							if  (Cellattrib[r_temp][c].VolFrac>=UPPER_LIMIT)
							{
								tag_l=1;
								r_bottom_filled_cell=r_temp;
								break;
							}
							if  (Cellattrib[r_temp][c].VolFrac<=LOWER_LIMIT)
							{
								tag_l=0;
								r_bottom_empty_cell=r_temp;
								break;
							}
							
							
						}
						
						for ( r_temp=r+1; r_temp<=NoOfRows; r_temp++) // go up
						{
							if  (Cellattrib[r_temp][c].VolFrac>=UPPER_LIMIT)
							{
								tag_u=1;
								r_top_filled_cell=r_temp;
								break;
							}
							if  (Cellattrib[r_temp][c].VolFrac<=LOWER_LIMIT)
							{
								tag_u=0;
								r_top_empty_cell=r_temp;
								break;
							}
							
							
						}
						
						for ( r_temp=(tag_l==1?r_bottom_filled_cell:r_bottom_empty_cell); r_temp>=1; r_temp--) // go down look for interfacial cell
						{
							if (interface(r_temp,c) )
							{
								r_bottom_interfacial_cell=r_temp;
								break;
							}
						}
						for ( r_temp=(tag_u==1?r_top_filled_cell:r_top_empty_cell) ; r_temp<=NoOfRows; r_temp++) // go up
						{
							if (interface(r_temp,c))
							{
								r_top_interfacial_cell=r_temp;
								break;
							}
						}
						
						if (tag_l ==-1 || tag_u==-1 || (tag_l ==0 && tag_u==0) || (tag_l ==1 && tag_u==1) ) 	// not a suitable column to find heights, in fact it is not needed remove this later
						{
							
							ht_y[r][c]=nodata;
							 tag_l=-1;	// reset tags for each column
							tag_u=-1;
						}
						
						else if (tag_l==1 && tag_u==0) // bottom column is unity
						{
								double H=0;
								
								for ( r_temp=r_top_empty_cell-1; r_temp>=r-(r-r_bottom_interfacial_cell)/2; r_temp--) // downward integration
								{
									//~ if (r_temp<r_bottom_filled_cell && Cellattrib[r_temp][c].VolFrac<UPPER_LIMIT)
									//~ {
										//~ break;
									//~ }
										H += Cellattrib[r_temp][c].VolFrac;
									
										if (H>5.5)
										{
											r_temp=r_temp-1;
											break;
										}
									
								}
								
								ht_y[r_temp+1][c] = H-0.5;
								//~ printf("\n%lf",ht_y[r][c]);
								
									for ( r_temp=r_temp+2; r_temp<=r+(r_top_interfacial_cell-r)/2; r_temp++)
									{
										ht_y[r_temp][c] = ht_y[r_temp-1][c] -1.0;
										//~ printf("\n%d\t%d\t%lf",r_temp,c,ht_y[r_temp][c]);
										if (ht_y[r_temp][c]<-5.5)
										{	ht_y[r_temp][c]=nodata;
											r=r_top_empty_cell;
											break;
										}
									}
								
								
						}
						
						else if (tag_l==0 && tag_u==1) // bottom is empty cell
						{
							double H=0;
								
								for ( r_temp=r_bottom_empty_cell+1; r_temp<=r+(r_top_interfacial_cell-r)/2; r_temp++) // upward integration
								{
									//~ if (r_temp>r_top_filled_cell && Cellattrib[r_temp][c].VolFrac<UPPER_LIMIT)
									//~ {
										//~ break;
									//~ }										
									
										H += Cellattrib[r_temp][c].VolFrac;
									
										if (H>6.5)
										{
											r_temp=r_temp+1; // needed to compensate the next executable line counter(rtemp-1)
											break;
										}
									
								}
								
								ht_y[r_temp-1][c] = HDRIFT-(H-0.5);
								//~ printf("\n%lf",ht_y[r_temp][c]);
								
									for ( r_temp=r_temp-2; r_temp>=r-(r-r_bottom_interfacial_cell)/2; r_temp--)
									{
										ht_y[r_temp][c] = ht_y[r_temp+1][c] +1.0;
										//~ printf("\n%d\t%d\t%lf",r_temp,c,ht_y[r_temp][c]);
										if (ht_y[r_temp][c]>HDRIFT+5.5)
										{	ht_y[r_temp][c]=nodata;
											r=r_top_filled_cell;
											break;
										}
									}
									
										
						}									
						
						
						 tag_l=-1;	// reset tags for each column
						 tag_u=-1;	
						
					}
					
						
						
					
					
					
					
			
					
					
				
			}
		}

	

	// rowwise heights in x direction
	for (r=1;r<=NoOfRows;r++)	
		{	
			
			
			
			for(c=1;c<=NoOfCols;c++)
			{	
				 r_bottom_interfacial_cell=0;
				r_top_interfacial_cell=NoOfCols;

					if (interface(r,c) )
					{
						for ( r_temp=c-1; r_temp>=1; r_temp--) // go down
						{
							if  (Cellattrib[r][r_temp].VolFrac>=UPPER_LIMIT)
							{
								tag_l=1;
								r_bottom_filled_cell=r_temp;
								break;
							}
							if  (Cellattrib[r][r_temp].VolFrac<=LOWER_LIMIT)
							{
								tag_l=0;
								r_bottom_empty_cell=r_temp;
								break;
							}
							
							
						}
						
						for ( r_temp=c+1; r_temp<=NoOfCols; r_temp++) // go up
						{
							if  (Cellattrib[r][r_temp].VolFrac>=UPPER_LIMIT)
							{
								tag_u=1;
								r_top_filled_cell=r_temp;
								break;
							}
							if  (Cellattrib[r][r_temp].VolFrac<=LOWER_LIMIT)
							{
								tag_u=0;
								r_top_empty_cell=r_temp;
								break;
							}
							
							
						}
						for ( r_temp=(tag_l==1?r_bottom_filled_cell:r_bottom_empty_cell); r_temp>=1; r_temp--) // go down look for interfacial cell
						{
							if (interface(r,r_temp) )
							{
								r_bottom_interfacial_cell=r_temp;
								break;
							}
						}
						for ( r_temp=(tag_u==1?r_top_filled_cell:r_top_empty_cell) ; r_temp<=NoOfCols; r_temp++) // go up
						{
							if (interface(r,r_temp))
							{
								r_top_interfacial_cell=r_temp;
								break;
							}
						}
						
						if (tag_l==1 && tag_u==0) // bottom column is unity
						{
								double H=0;
								
								for ( r_temp=r_top_empty_cell-1; r_temp>=c-(c-r_bottom_interfacial_cell)/2; r_temp--) // downward integration
								{
									//~ if (r_temp<r_bottom_filled_cell && Cellattrib[r][r_temp].VolFrac<UPPER_LIMIT)
									//~ {
										//~ break;
									//~ }
										H += Cellattrib[r][r_temp].VolFrac;
									
										if (H>5.5)
										{
											r_temp=r_temp-1;
											break;
										}
									
								}
								
								ht_x[r][r_temp+1] = H-0.5;
								//~ printf("\n%lf",ht_y[r][c]);
								
									for ( r_temp=r_temp+2; r_temp<=c+(r_top_interfacial_cell-c)/2; r_temp++)
									{
										ht_x[r][r_temp]= ht_x[r][r_temp-1]-1.0;
										//~ printf("\n%d\t%d\t%lf",r_temp,c,ht_y[r_temp][c]);
										if (ht_x[r][r_temp]<-5.5)
										{	ht_x[r][r_temp]=nodata;
											c=r_top_empty_cell;
											break;
										}
									}
								
								
						}
						
						else if (tag_l==0 && tag_u==1)
						{
							double H=0;
								
								for ( r_temp=r_bottom_empty_cell+1; r_temp<=c+(r_top_interfacial_cell-c)/2; r_temp++) // upward integration
								{
									//~ if (r_temp>r_top_filled_cell && Cellattrib[r][r_temp].VolFrac<UPPER_LIMIT)
									//~ {
										//~ break;
									//~ }										
									
										H += Cellattrib[r][r_temp].VolFrac;
									
										if (H>6.5)
										{
											r_temp=r_temp+1; // needed to compensate the next executable line counter(rtemp-1)
											break;
										}
									
								}
								
								ht_x[r][r_temp-1] = HDRIFT-(H-0.5);
								//~ printf("\n%lf",ht_y[r_temp][c]);
								
									for ( r_temp=r_temp-2; r_temp>=c-(c-r_bottom_interfacial_cell)/2; r_temp--)
									{
										ht_x[r][r_temp] = ht_x[r][r_temp+1] +1.0;
										//~ printf("\n%d\t%d\t%lf",r_temp,c,ht_y[r_temp][c]);
										if (ht_x[r][r_temp]>HDRIFT+5.5)
										{	ht_x[r][r_temp]=nodata;
											c=r_top_filled_cell;
											break;
										}
									}
									
										
						}									
						
						
						 tag_l=-1;	// reset tags for each column
						 tag_u=-1;	
						
					}
					
						
				
				
			}
		}			

	//Symmetry for heights at boundaries
	for (r=1;r<=NoOfRows;r++)
	{
		ht_x[r][0]=ht_x[r][1];
		ht_x[r][NoOfCols+1]=ht_x[r][NoOfCols];
		
		ht_y[r][0]=ht_y[r][1];
		ht_y[r][NoOfCols+1]=ht_y[r][NoOfCols];
	}
	for (c=1;c<=NoOfCols;c++)
	{
		ht_x[0][c]=ht_x[1][c];
		ht_x[NoOfRows+1][c]=ht_x[NoOfRows][c];
		
		ht_y[0][c]=ht_y[1][c];
		ht_y[NoOfRows+1][c]=ht_y[NoOfRows][c];
	}

	bool skip_calc=0;

	for (r=1;r<=NoOfRows;r++)	
		{		
		for(c=1;c<=NoOfCols;c++)
			{	skip_calc=0;
				if (interface(r,c))
				{	
					double nx = (Cellattrib[r][c+1].VolFrac - Cellattrib[r][c-1].VolFrac);
					double ny = ( Cellattrib[r+1][c].VolFrac - Cellattrib[r-1][c].VolFrac );
					if (absolute(ny)>absolute(nx))
					{
						int ori = orientation(ht_y[r][c]);
						for (int i = -1; i <= 1; i++)
							if (ht_y[r][c+i] == nodata || orientation(ht_y[r][c+i]) != ori)
							{	kappa[r][c] = nodata; 
								skip_calc=1;
								break;
								
							}
						
						if (skip_calc==0)		
						{
							//~ kappa[r][c]=12;
							 double hx = (ht_y[r][c+1] - ht_y[r][c-1])/2.;
							double hxx = (ht_y[r][c+1] + ht_y[r][c-1] - 2.*ht_y[r][c])/del;
							kappa[r][c] = sign(ny)*hxx/pow(1. + sq(hx), 3/2.);
							
							if (axi==1)
							{
								
								
								double nr = hx*(orientation(ht_y[r][c]) ? 1 : -1);
								
								kappa[r][c] += nr/max (sqrt(1. + sq(hx))*x[r][c], del/2.);
							}
							
						}
					}
					else if (absolute(nx)>=absolute(ny))
					{
						int ori = orientation(ht_x[r][c]);
						for (int i = -1; i <= 1; i++)
							if (ht_x[r+i][c] == nodata || orientation(ht_x[r+i][c]) != ori)
							{	kappa[r][c] = nodata; 
								skip_calc=1;
								break;
								
							}
						
						if (skip_calc==0)		
						{
							//~ kappa[r][c]=12;
							 double hx = (ht_x[r+1][c] - ht_x[r-1][c])/2.;
							double hxx = (ht_x[r+1][c] + ht_x[r-1][c] - 2.*ht_x[r][c])/del;
							kappa[r][c] = sign(nx)*hxx/pow(1. + sq(hx), 3/2.);
							
							if (axi==1)
							{
								double  r_dist =  x[r][c] + height(ht_x[r][c])*del;
								
								double nr = orientation(ht_x[r][c]) ? -1 : 1;
								
								kappa[r][c] += nr/max (sqrt(1. + sq(hx))*r_dist, del/2.);
							}
							
						}
					}
					else
					{
						//~ kappa[r][c]=sum_kappa/num_kappa;
						kappa[r][c]=nodata;
					}
					if (kappa[r][c]==nodata) //use popinet height curvature fit
					{
						kappa[r][c]=height_curvature_fit(r,c);
						
						
						
					}
					
					//~ printf("\n%d\t%d\t\%e",r,c,kappa[r][c]);
				}
				
				//~ kappa[r][c] = ht_y[r][c];	
			}
		}
			
			

				
		//~ exit(0);				
				
					
		
		
	
	
}