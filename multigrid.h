

/********************Multigrid Dependencies***********************/

#define INDEX(r,c,NC) ((NC+2)*(r)+(c))


/******************************************************************************/
double *jacobi ( double *f, double *u_ptr, int NR, int NC,double *rho_private, double h ) 
{
  double h_sq;
	double Coeff_Poissons_solver;
  int r,c;
 

  h_sq = h*h;


   	// Neumann Boundary conditions (In general there are only three boundary conditions)
	// possible for pressure : neumann,periodic or dirichlet


 if (BcNeumann_P_bottom==1)
{ 	r = 1; 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] =  u_ptr[INDEX(r,c,NC)];			//bottom outflow 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcNeumann_P_top==1)
{ r = NR;
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] = u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}

}
	

	if (BcNeumann_P_right==1)
{
 for ( r = 1 ; r <=NR ; r++ )
  {
	 u_ptr[INDEX(r,NC+1,NC)] = u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  

	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
 } 

if (BcNeumann_P_left==1)
{
	for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = u_ptr[INDEX(r,1,NC)] ;				//left neumann

	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
  }
 }


/* Periodic Boundary Conditions */
 if (BcPeriodic_P_Y==1)
{ 	
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(0,c,NC)] =  u_ptr[INDEX(NR,c,NC)];			//bottom periodic 
			rho_private[INDEX(0,c,NC)] = rho_private[INDEX(NR,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcPeriodic_P_X==1)
{ 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(NR+1,c,NC)] = u_ptr[INDEX(1,c,NC)];			// top neumann
			rho_private[INDEX(NR+1,c,NC)] = rho_private[INDEX(1,c,NC)];
		}

}

/* Dirichlet Boundary Conditions for outflow in genenal where P = 0 at the boundary */	
// NOTE: VolFrac or rho always remain neumann in general 
  if (BcDirichlet_P_bottom==1)
{ 	r = 1; 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] = 2.0*press_bottom -  u_ptr[INDEX(r,c,NC)];			//bottom outflow 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcDirichlet_P_top==1)
{ r = NR;
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] =2.0*press_top -  u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}

}
	

	if (BcDirichlet_P_right==1)
{
 for ( r = 1 ; r <=NR ; r++ )
  {
	 u_ptr[INDEX(r,NC+1,NC)] =2.0*press_right -  u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  

	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
 } 

if (BcDirichlet_P_left==1)
{
	for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = 2.0*press_left -  u_ptr[INDEX(r,1,NC)] ;				//left neumann

	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
  }
 }

/****************************************END BC******************************************/

  double temp1,temp2;   
	double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
 
double Relaxation_param = alpha_bcs;
	//~ printf("\n");
		for (r=1; r<=NR; r++)
		{
                        for (c=1; c<=NC; c++)
                        {
				
				  Coeff_Poissons_solver = (1.0*eta_p[c+1]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0*eta_p[c]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
				temp1 = u_ptr[INDEX(r,c+1,NC)]*eta_p[c+1]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + u_ptr[INDEX(r,c-1,NC)]*eta_p[c]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) ;
				temp2 = u_ptr[INDEX(r+1,c,NC)]/ (rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) + u_ptr[INDEX(r-1,c,NC)]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ;
	    
	    
				u_ptr[INDEX(r,c,NC)] = Relaxation_param/Coeff_Poissons_solver * ( (temp1+temp2)/h_sq - f[INDEX(r,c,NC)] ) + (1.0-Relaxation_param)*u_ptr[INDEX(r,c,NC)];
				
				
				//temp1 = 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) ;//+ 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)])  ;
				
				
			}
			
		}
	//~ if (NR==1){
	//~ for (r=0; r<=NR+1; r++)
		//~ {	printf("\n");
                        //~ for (c=0; c<=NC+1; c++)
                        //~ {
				//~ printf("%lf\t",rho_private[INDEX(r,c,NC)]);
			//~ }
		//~ }
		
		//~ exit(0);
	//~ }
 
   	// Neumann Boundary conditions (In general there are only three boundary conditions)
	// possible for pressure : neumann,periodic or dirichlet


 if (BcNeumann_P_bottom==1)
{ 	r = 1; 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] =  u_ptr[INDEX(r,c,NC)];			//bottom outflow 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcNeumann_P_top==1)
{ r = NR;
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] = u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}

}
	

	if (BcNeumann_P_right==1)
{
 for ( r = 1 ; r <=NR ; r++ )
  {
	 u_ptr[INDEX(r,NC+1,NC)] = u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  

	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
 } 

if (BcNeumann_P_left==1)
{
	for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = u_ptr[INDEX(r,1,NC)] ;				//left neumann

	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
  }
 }


/* Periodic Boundary Conditions */
 if (BcPeriodic_P_Y==1)
{ 	
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(0,c,NC)] =  u_ptr[INDEX(NR,c,NC)];			//bottom periodic 
			rho_private[INDEX(0,c,NC)] = rho_private[INDEX(NR,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcPeriodic_P_X==1)
{ 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(NR+1,c,NC)] = u_ptr[INDEX(1,c,NC)];			// top neumann
			rho_private[INDEX(NR+1,c,NC)] = rho_private[INDEX(1,c,NC)];
		}

}

/* Dirichlet Boundary Conditions for outflow in genenal where P = 0 at the boundary */	
// NOTE: VolFrac or rho always remain neumann in general 
  if (BcDirichlet_P_bottom==1)
{ 	r = 1; 
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r-1,c,NC)] = 2.0*press_bottom  -  u_ptr[INDEX(r,c,NC)];			//bottom outflow 
			rho_private[INDEX(r-1,c,NC)] = rho_private[INDEX(r,c,NC)];	
			//printf("-");
		}

}								
							

  
if (BcDirichlet_P_top==1)
{ r = NR;
	 for ( c = 1; c <= NC; c++ )
		{	
			u_ptr[INDEX(r+1,c,NC)] =2.0*press_top  -  u_ptr[INDEX(r,c,NC)];			// top neumann
			rho_private[INDEX(r+1,c,NC)] = rho_private[INDEX(r,c,NC)];
		}

}
	

	if (BcDirichlet_P_right==1)
{
 for ( r = 1 ; r <=NR ; r++ )
  {
	 u_ptr[INDEX(r,NC+1,NC)] =2.0*press_right -  u_ptr[INDEX(r,NC,NC)] ;			//right neumann
	  
	 
	  

	  rho_private[INDEX(r,NC+1,NC)] = rho_private[INDEX(r,NC,NC)] ;
  }
 } 

if (BcDirichlet_P_left==1)
{
	for ( r = 1 ; r <=NR ; r++ )
  {
	  u_ptr[INDEX(r,0,NC)] = 2.0*press_left  -  u_ptr[INDEX(r,1,NC)] ;				//left neumann

	  
	 
	  
	    rho_private[INDEX(r,0,NC)] = rho_private[INDEX(r,1,NC)] ;
  }
 }

/****************************************END BC******************************************/

 
  



  return u_ptr;
}
/*************************************************************************/
 void Residue_grid ( double *f, double *u_ptr,double *Residue, int NR,int NC, double *rho_private, double h ) 
{
	 //~ Residue_grid (info_array,f_ptr,u_ptr,Res,NR,NC,rho,h); 

	
	

  int r,c;
double temp1,temp2,temp3,temp4;	

	
	double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
	


  //~ printf("\n");
	for ( r = 1; r <= NR; r++ )
	{
	      for ( c = 1; c <= NC; c++ )
	      {
		      
				temp1 = (u_ptr[INDEX(r,c+1,NC)]-u_ptr[INDEX(r,c,NC)])*eta_p[c+1]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]);
				temp2 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r,c-1,NC)])*eta_p[c]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]);
				temp3 = (u_ptr[INDEX(r+1,c,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]);
				temp4 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r-1,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]);
		      
		       //(1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
	    
			Residue[INDEX(r,c,NC)] = f[INDEX(r,c,NC)] -  ( temp1 - temp2 + temp3 - temp4)/pow(h,2);      
			  //~ if (my_rank == 0)
			//~ {
				//~ printf("%lf\t",Residue[INDEX(r,c,NC)]);
			//~ }
		      //~ if (fabs(Residue[INDEX(i,j)])>Send_Residue[0])
		      //~ {
			      //~ Send_Residue[0] = fabs(Residue[INDEX(i,j)]);
		      //~ }
				//printf("%lf\t",Residue[INDEX(i,j,NC)] );
		}
		
		//~ printf("\n");
	}
	
	//~ printf("*****Res******");
//~ exit(0);

}

/*************************************************************************/
 double Calculate_Residue ( double *f , double *u_ptr, int NR,int NC,double *rho_private,double h) 
{
	

	double temp1,temp2,temp3,temp4;	
int r,c;
double  Residue;	




	double  Res_inf_norm=0;
	
	
		double eta_p[NC+2],RF_p[NC+2];
 
	for (c=1;c<=NC+2;c++)
	{
		if (axi ==0)
		{
			eta_p[c] = 1;
			RF_p[c]=1;
		}
		else if (axi ==1)
		{
			eta_p[c] = (c-1)*h;
			RF_p[c] = (c-0.5)*h;
		}
	}
	
	
	
	for (r=1; r<=NR; r++)
	{
		for (c=1; c<=NC; c++)
		{
		      
				temp1 = (u_ptr[INDEX(r,c+1,NC)]-u_ptr[INDEX(r,c,NC)])*eta_p[c+1]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]);
				temp2 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r,c-1,NC)])*eta_p[c]/RF_p[c]/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]);
				temp3 = (u_ptr[INDEX(r+1,c,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]);
				temp4 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r-1,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]);
		      
		       //(1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
	    
			Residue =
			
			f[INDEX(r,c,NC)] -  ( temp1 - temp2 + temp3 - temp4)/pow(h,2);      
		      
		          if (fabs(Residue)>Res_inf_norm)
			{
			      Res_inf_norm = fabs(Residue);
			}
			  //~ if (my_rank == 0)
			//~ {
				//~ printf("\n%d\t%d\t%lf",r,c,Residue);
			//~ }
		      //~ if (fabs(Residue[INDEX(i,j)])>Send_Residue[0])
		      //~ {
			      //~ Send_Residue[0] = fabs(Residue[INDEX(i,j)]);
		      //~ }
				//printf("%lf\t",Residue[INDEX(i,j,NC)] );
		}
		
		//~ printf("\n");
	}
	//~ exit(0);
	//~ for (r=1; r<=NR; r++)
	//~ {
		//~ for (c=1; c<=NC; c++)
		//~ {
		      
				//~ temp1 = (u_ptr[INDEX(r,c+1,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]);
				//~ temp2 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r,c-1,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]);
				//~ temp3 = (u_ptr[INDEX(r+1,c,NC)]-u_ptr[INDEX(r,c,NC)])/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]);
				//~ temp4 = (u_ptr[INDEX(r,c,NC)]-u_ptr[INDEX(r-1,c,NC)])/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]);
		      
		       //~ //(1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c+1,NC)]) + 1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r,c-1,NC)]) + 1.0/(rho_private[INDEX(r+1,c,NC)]+rho_private[INDEX(r,c,NC)]) +1.0/(rho_private[INDEX(r,c,NC)]+rho_private[INDEX(r-1,c,NC)]) ) / h_sq;
	    
			//~ Residue =
			
			//~ f[INDEX(r,c,NC)] -  ( temp1 - temp2 + temp3 - temp4)/pow(h,2);      
		      
		          //~ if (fabs(Residue)>Res_inf_norm)
			//~ {
			      //~ Res_inf_norm = fabs(Residue);
			//~ }
			  //~ if (my_rank == 0)
			//~ {
				//~ printf("\n%lf",Residue[INDEX(i,j)]);
			//~ }
		      //~ if (fabs(Residue[INDEX(i,j)])>Send_Residue[0])
		      //~ {
			      //~ Send_Residue[0] = fabs(Residue[INDEX(i,j)]);
		      //~ }
				//~ //printf("%lf\t",Residue[INDEX(i,j,NC)] );
		//~ }
		
		//~ //printf("\n");
	//~ }

	
	
	return Res_inf_norm;
}


void Restrictor ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
	

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							Res_hat[INDEX(r,c,NC)] = 0.25*(Res[INDEX(2*r-1,2*c-1,2*(NC))] + Res[INDEX(2*r-1,2*c,2*(NC))] + Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ Res[INDEX(2*r,2*c,2*(NC))]);
				
							//~ rho_hat[INDEX(r,c,NC)] = 0.25*(rho[INDEX(2*r-1,2*c-1,2*(NC))] + rho[INDEX(2*r-1,2*c,2*(NC))] + rho[INDEX(2*r,2*c-1,2*(NC))] 
										//~ + rho[INDEX(2*r,2*c,2*(NC))]);
				//~ if (my_rank==1)
				//~ printf("%lf\t",Res_hat[INDEX(r,c,NC)] );
							
			}
			//~ if (my_rank==1)
			//~ printf("\n");
		}
		//~ if (my_rank==1)
		//~ printf("************************************\n");
		
		//~ exit(0);
		
		
}

void Restrictor_rho ( double *Res, double *Res_hat, int NR, int NC ) 
{
	
	int r,c;
		
	double temp;

	
		for ( r =1; r <(NR+1); r++ )
		{
			for(c=1;c<(NC+1);c++){
							temp = 0.25*(1./Res[INDEX(2*r-1,2*c-1,2*(NC))] + 1./Res[INDEX(2*r-1,2*c,2*(NC))] + 1./Res[INDEX(2*r,2*c-1,2*(NC))] 
										+ 1./Res[INDEX(2*r,2*c,2*(NC))]);
							
							Res_hat[INDEX(r,c,NC)] = 1./temp;
							//~ rho_hat[INDEX(r,c,NC)] = 0.25*(rho[INDEX(2*r-1,2*c-1,2*(NC))] + rho[INDEX(2*r-1,2*c,2*(NC))] + rho[INDEX(2*r,2*c-1,2*(NC))] 
										//~ + rho[INDEX(2*r,2*c,2*(NC))]);
				//~ if (my_rank==1)
				//~ printf("%lf\t",Res_hat[INDEX(r,c,NC)] );
							
			}
			//~ if (my_rank==1)
			//~ printf("\n");
		}
		//~ if (my_rank==1)
		//~ printf("************************************\n");
		//~ printf("\n");
		//~ exit(0);
		
		
}

double *prolongator( double *u_hat, double *uCorr, double *u_ptr, int NR, int NC)
{
	
	int i,r,c;
	

		

	  for (r=0; r<NR; r++)
                    {
                        for (c=0; c<NC; c++)
                        {
				uCorr[INDEX(2*r+1,2*c+1,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r,c+1,NC)] + 3*u_hat[INDEX(r+1,c,NC)] + u_hat[INDEX(r,c,NC)]);
				
				
				uCorr[INDEX(2*r+1,2*c+2,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+1,c+2,NC)] + 3*u_hat[INDEX(r,c+1,NC)] + u_hat[INDEX(r,c+2,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+1,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+2,c+1,NC)] + 3*u_hat[INDEX(r+1,c,NC)] + u_hat[INDEX(r+2,c,NC)]);
				
				uCorr[INDEX(2*r+2,2*c+2,2*NC)] = 0.0625*(9*u_hat[INDEX(r+1,c+1,NC)] + 3*u_hat[INDEX(r+1,c+2,NC)] + 3*u_hat[INDEX(r+2,c+1,NC)] + u_hat[INDEX(r+2,c+2,NC)]);
				//uCorr[2*r+1][2*c+2] =  0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+1][c+2] + 3*u_hat[r][c+1] + u_hat[r][c+2]);
				//uCorr[2*r+2][2*c+1] = 0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+2][c+1] + 3*u_hat[r+1][c] + u_hat[r+2][c]); 
				//uCorr[2*r+2][2*c+2] = 0.0625*(9*u_hat[r+1][c+1] + 3*u_hat[r+1][c+2] + 3*u_hat[r+2][c+1] + u_hat[r+2][c+2]);
				
					
				
			}
		}
		
		
		int nNR=2*NR,nNC=2*NC;	// for the loop below
		
		   for (r=1; r<=nNR; r++)
			{
				for (c=1; c<=nNC; c++)
				{	
				
					u_ptr[INDEX(r,c,2*NC)] = uCorr[INDEX(r,c,2*NC)] + u_ptr[INDEX(r,c,2*NC)];
					
								
				}
			
			}
			
			//~ for (r=nNR/BoxRows+1; r<=nNR; r++)
			//~ {
				//~ for (c=1; c<=nNC; c++)
				//~ {	
				
					//~ u_ptr[INDEX(r,c,2*NC)] = uCorr[INDEX(r,c,2*NC)] + u_ptr[INDEX(r,c,2*NC)];
					
								
				//~ }
			
			//~ }
	
	return u_ptr;	
		
}


	 	 
/********************************V-Cycle**********************************************/
double *Vcycle(int NR,int NC, double *u_init, double *f_ptr, int iter, int flag,double *rho,double h)
{
	double *Res,*Res_hat,*u_corr_ptr,*u_ptr,*uCorr,*u_hat,*rho_hat;
	//int NoOfRows_coarser,NoOfCols_coarser;
	int i; 
	int r,c;
	
	
	
	if (flag==1)
	{
		u_ptr = u_init;
			
		//rho_hat = rho;
		flag=0;
		//printf("haha");
		//fflush(stdout);
		//exit(0);
		
		//printf("flag = %d\n",flag);
	}
	else
		{
		
		u_ptr  =  (double *)calloc((NR+2) * (NC+2),sizeof(double*));   //( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
			
		//flag=0;
			
	}
	
	
	for (i=0;i<iter;i++)
	{
		
			
			u_ptr = jacobi (f_ptr,u_ptr,NR,NC,rho,h );			//void jacobi ( int num_procs, double f[], double *u_ptr, double *u_new_ptr, int NR, int NC ) 
		
		
		
	}
	
	

		//~ return u_ptr;
	
			
	
	
	if(NR<=roll_back || NC<=roll_back )
	{
		
		return u_ptr;
	}
	
	Res  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
		
	uCorr  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );	// correction from coarser grids		
	
	
	 Residue_grid (f_ptr,u_ptr,Res,NR,NC,rho,h);  // double *Residue_grid (int num_procs, double *f, double *u_ptr ) 
	
	
	
			NR = (NR/2); //keep this here
			NC =  (NC/2);
			h=2*h;
			
			
			Res_hat = (double *)calloc((NR+2)*(NC+2),sizeof(double));
		
			rho_hat  = ( double * ) calloc ( (NR+2) * (NC+2),sizeof ( double ) );
			
	
			 Restrictor(Res,Res_hat,NR,NC);
			
			 Restrictor_rho(rho,rho_hat,NR,NC);
			 
			 
			
	
	
			
			u_hat = Vcycle( NR, NC, u_init, Res_hat, iter,flag,rho_hat,h);
		
			
			//~ exit(0);
			
			
			u_ptr = prolongator( u_hat, uCorr, u_ptr, NR, NC);
			
		
			
			
		
		for (i=0;i<iter;i++)
		{
			u_ptr = jacobi (f_ptr,u_ptr,2*NR,2*NC,rho,h/2 );
			
		}
	
	
	
		
			free(u_hat);
			free(rho_hat); 
		
		
			free(Res);
			free(uCorr); 
			
			free(Res_hat);
			
			
			
			
			
			
			
			
			
			return u_ptr;
}


/**************/

		
/**************************************************
Description: 2D Poisson Multigrid Solver D2u = f

Limitation: Refinement allowed only in powers of 2
		  and nows of cells in horizontal and vertical
		  direction must be equal

Author: Palas Kumar Farsoiya, IITB
********************************************************/



void Poissons_Solver_Multigrid(){	
	
	printf(" PS: MG "); fflush(stdout);
	
	int r,c;
	
	int GS_iter = 4;
	
	//~ n = NoOfRows+2;		// must be odd number	//no of rows and columns should be equal
	//~ h = 1.0/(n-2);	
	//~ h_sq = h*h;
	double pi = 3.14159265359;
	double  *f;	//Press -  is updated u, f is RHS of poisson's equation, ut is exact solution
	double temp1,temp2,temp3,temp4;

	double *u, *Source_Poisson_ptr,*rho_ptr;	// pointers for updated u and RHS,  used in recursive V cycle
	

	
	//~ char filename[50];
	//~ FILE *ptrWrite1,*ptrWrite2;
	
	//~ sprintf(filename, "Residue.dat");
	//~ ptrWrite1 =fopen(filename,"w");
	
	//~ sprintf(filename, "Result_multi.dat");
	//~ ptrWrite2 =fopen(filename,"w");
	
		rho_ptr  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		u  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		  f  = ( double * ) calloc ( (NoOfRows+2) * (NoOfCols+2),sizeof ( double ) );
		//u  = ( double * ) malloc ( (NR+2) * (NC+2)*sizeof ( double ) );
	
	//init guess on the finest grid is the zero vector 
	Pressure_BC();  // keep this here to be updated for any time dependent BC
		
	int k=3; int l=3; double h=del;
	for(r=0; r<=NoOfRows+1; r++)	
	{	
		for(c=0;c<=NoOfCols+1;c++)
		{
			 u[INDEX(r,c,NoOfCols)] = Press[r][c] ;
			rho_ptr[INDEX(r,c,NoOfCols)] = rho[r][c];
			
			
			if (r>0 && r<NoOfRows+1 && c>0 && c<NoOfCols+1)
			{
				f[INDEX(r,c,NoOfCols)] = 0.5*((X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c] + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
			}
			
			
			//~ f[INDEX(r,c,NoOfCols)] = -(pow(k,2)+pow(l,2))*pow(pi,2)*sin(k*pi*(-0.5+(c-0.5)*h)) * sin(l*pi * (-0.5+(r-0.5)*h));	
		//~ printf("%lf\t",rho_ptr[INDEX(r,c,NoOfCols)]); 		
		//~ printf("%lf\t",rho[r][c]); 		
			
			
		}
		
	}
	//~ exit(0);
	
	int flag=1;
	double Max_Res_old=10;
	double Max_Res_new=10;
	int step_vcycle=0;
	
		
	
			while ( Max_Res_new>tolerance )
			  {
				
				
				u =  Vcycle( NoOfRows, NoOfCols, u,f, GS_iter,flag,rho_ptr,del);
				
				flag=1;
						
				Max_Res_new = Calculate_Residue ( f , u, NoOfRows, NoOfCols,rho_ptr,del) ;
				  Max_Res_new = 2*delT*Max_Res_new;
				  
					   
					    //~ if (StepT==16)
					    //~ {
						//~ printf ( "\n%e", Max_Res );
					    //~ }
						    
					    
			
				//~ fflush(stdout);  
				//~ if (step_vcycle%100==0)
				//~ {	
					if (step_vcycle>1000)
					{
						printf("\t MG iter Failed -- roll_back increased "); fflush(stdout);
						inc_roll_back = inc_roll_back+1;
						roll_back = pow(2,inc_roll_back);
						Poissons_Solver_Multigrid();
						return ;
					}
					//~ Max_Res_old = Max_Res_new;
					
				//~ }
				++step_vcycle; 
				  //~ printf ( "\n iter=%d\tMaxRes = %e\n", step_vcycle,Max_Res_new );
			  } 
			  
				double sum =0; 
				double mean_Press =0;
			  
			    //~ exit(0);
			 if (Max_Res_new<(tolerance*1.0e-5))	// sometimes zero comes out of inf division, then sor might be required
			 {
				printf("\t MG Failed -- Switched to SOR "); fflush(stdout);
				 Poissons_Solver_SOR();
				 return ;
			 }
		
			else
			{	
			 	
			  
				for(r=0; r<=NoOfRows+1; r++)	
				{
					for(c=0;c<=NoOfCols+1;c++)
					{
					 Press[r][c] = u[INDEX(r,c,NoOfCols)]  ;
					sum = sum + Press[r][c];
						
					}
				}
			}
			
			
		if (BcNeumann_P_left==1 && BcNeumann_P_right==1 && BcNeumann_P_bottom==1 && BcNeumann_P_top==1)	
		{
			
			mean_Press = sum/(NoOfCols*NoOfRows);
			
			  for(r=0; r<=NoOfRows+1; r++)	
			{
				for(c=0;c<=NoOfCols+1;c++)
				{
					 Press[r][c] = Press[r][c] - mean_Press;
					
						
				}
			}
		}
		
		Pressure_BC();  // keep this here to be updated for any time dependent BC
		
			free(u); 
			free(f); 
			free(rho_ptr);
			
			
			
			printf ( " Div: %e", Max_Res_new );
			fflush(stdout);
	

	
}
	
