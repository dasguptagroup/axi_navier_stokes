import subprocess
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from math import factorial
import pylab



subprocess.call(["gcc", "ldc.c","-lm"])
tmp=subprocess.call("./a.out")

# clear figure
plt.cla()

filename = 'ghia_ldc_u_Re1000.dat'
xAdata,yAdata = np.loadtxt(filename,delimiter=' ',unpack=True)
xAdata=xAdata+0.5 # shifting the x coordinate in reference file
plt.plot(xAdata, yAdata,'k*')

filename = 'sim_ldc_u_Re1000.dat'
xdata,ydata = np.loadtxt(filename,delimiter='\t',unpack=True)
plt.plot(xdata, ydata,'r')

plt.xlabel('y')
plt.ylabel('u')
plt.title('u velocity profile for Re=1000')

pylab.savefig('xprof_ldc_Re1000.png')

# clear figure
plt.cla() 

filename = 'ghia_ldc_v_Re1000.dat'
xAdata,yAdata = np.loadtxt(filename,delimiter=' ',unpack=True)
xAdata=xAdata+0.5 # shifting the x coordinate in reference file
plt.plot(xAdata, yAdata,'k*')

filename = 'sim_ldc_v_Re1000.dat'
xdata,ydata = np.loadtxt(filename,delimiter='\t',unpack=True)
plt.plot(xdata, ydata,'r')

plt.xlabel('x')
plt.ylabel('v')
plt.title('v velocity profile for Re=1000')

pylab.savefig('yprof_ldc_Re1000.png')


subprocess.call(["rm","a.out"])
# Norm we have to define later because data points are limited

#~ norm =np.absolute(yAdata-ydata)
#~ norm = np.max(norm)

#~ f = open('TestReports', 'w')

#~ if (norm<=0.04):
	#~ print('Lid Driven Cavity: PASS\n')
	#~ f.write('Lid Driven Cavity: PASS')
#~ else:
	#~ print('Lid Driven Cavity: FAIL')
	#~ f.write('Lid Driven Cavity: FAIL')