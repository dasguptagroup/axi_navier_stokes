#define PARABOLA_FIT_CENTER_WEIGHT .1
void normalize (coord * n)
{
  double norm = 0.;
  
    norm += sq(n->x);
    norm += sq(n->y);
	
  norm = sqrt(norm);
  
    n->x /= norm;
    n->y /= norm;
}
double plane_alpha (double c, coord n)
{
  double alp;
  coord n1;
  
  n1.x = fabs (n.x); n1.y = fabs (n.y); n1.z = fabs (n.z);

  double m1, m2, m3;
  m1 = min(n1.x, n1.y);
  m3 = max(n1.x, n1.y);
  m2 = n1.z;
  if (m2 < m1) {
    double tmp = m1;
    m1 = m2;
    m2 = tmp;
  }
  else if (m2 > m3) {
    double tmp = m3;
    m3 = m2;
    m2 = tmp;
  }
  double m12 = m1 + m2;
  double pr = max(6.*m1*m2*m3, 1e-50);
  double V1 = m1*m1*m1/pr;
  double V2 = V1 + (m2 - m1)/(2.*m3), V3;
  double mm;
  if (m3 < m12) {
    mm = m3;
    V3 = (m3*m3*(3.*m12 - m3) + m1*m1*(m1 - 3.*m3) + m2*m2*(m2 - 3.*m3))/pr;
  }
  else {
    mm = m12;
    V3 = mm/(2.*m3);
  }

  double ch = min(c, 1. - c);
  if (ch < V1)
    alp = pow (pr*ch, 1./3.);
  else if (ch < V2)
    alp = (m1 + sqrt(m1*m1 + 8.*m2*m3*(ch - V1)))/2.;
  else if (ch < V3) {
    double p12 = sqrt (2.*m1*m2);
    double q = 3.*(m12 - 2.*m3*ch)/(4.*p12);
    double teta = acos(clamp(q,-1.,1.))/3.;
    double cs = cos(teta);
    alp = p12*(sqrt(3.*(1. - cs*cs)) - cs) + m12;
  }
  else if (m12 < m3)
    alp = m3*ch + mm/2.;
  else {
    double p = m1*(m2 + m3) + m2*m3 - 1./4., p12 = sqrt(p);
    double q = 3.*m1*m2*m3*(1./2. - ch)/(2.*p*p12);
    double teta = acos(clamp(q,-1.,1.))/3.;
    double cs = cos(teta);
    alp = p12*(sqrt(3.*(1. - cs*cs)) - cs) + 1./2.;
  }
  if (c > 1./2.) alp = 1. - alp;

  if (n.x < 0.)
    alp += n.x;
  if (n.y < 0.)
    alp += n.y;
  if (n.z < 0.)
    alp += n.z;

  return alp - (n.x + n.y + n.z)/2.;;
}
double line_length_center (coord m, double alp, coord * p)
{
  alp += (m.x + m.y)/2.;

  coord n = m;
  if (n.x < 0.) {
    alp -= n.x;
    n.x = - n.x;
  }
  if (n.y < 0.) {
    alp -= n.y;
    n.y = - n.y;
  }

  p->x = p->y = 0.;

  if (alp <= 0. || alp >= n.x + n.y)
    return 0.;
  // FLAG: sochenge iske bare me
 //x-direction
    if (n.x < 1e-4) {
      p->x = 0.;
      p->y = (m.y < 0. ? 1. - alp : alp) - 0.5;
      return 1.;
    }
    // y-direction
   if (n.y < 1e-4) {
      p->y = 0.;
      p->x = (m.x < 0. ? 1. - alp : alp) - 0.5;
      return 1.;
    }
    
  if (alp >= n.x) {
    p->x += 1.;
    p->y += (alp - n.x)/n.y;
  }
  else
    p->x += alp/n.x;

  double ax = p->x, ay = p->y;
  if (alp >= n.y) {
    p->y += 1.;
    ay -= 1.;
    p->x += (alp - n.y)/n.x;
    ax -= (alp - n.y)/n.x;
  }
  else {
    p->y += alp/n.y;
    ay -= alp/n.y;
  }

  //x dir
    p->x /= 2.;
    p->x = clamp (p->x, 0., 1.);
    if (m.x < 0.)
      p->x = 1. - p->x;
    p->x -= 0.5;
   //y dir
    p->y /= 2.;
    p->y = clamp (p->y, 0., 1.);
    if (m.y < 0.)
      p->y = 1. - p->y;
    p->y -= 0.5;

  return sqrt (ax*ax + ay*ay);
}

typedef struct {
  coord o;
 /* y = a[0]*x^2 + a[1]*x + a[2] */
  coord m;
  double ** M, rhs[3], a[3];

} ParabolaFit;


void * matrix_new (int n, int p, size_t size)
{
  void ** m = malloc (n*sizeof (void *));
  char * a = malloc (n*p*size);
  for (int i = 0; i < n; i++)
    m[i] = a + i*p*size;
  return m;
}
static void parabola_fit_init (ParabolaFit * p, coord o, coord m)
{
  
    p->o.x = o.x;
    p->o.y = o.y;


    p->m.x = m.x;
    p->m.y = m.y;
 
	normalize (&p->m);
	
  int n = 3;

  p->M = matrix_new (n, n, sizeof(double));  
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++)
      p->M[i][j] = 0.;
    p->rhs[i] = 0.;
  }
}
void matrix_free (void * m)
{
  free (((void **) m)[0]);
  free (m);
}

double matrix_inverse (double ** m, int n, double pivmin)
{
  int indxc[n], indxr[n], ipiv[n];
  int i, icol = 0, irow = 0, j, k, l, ll;
  double big, dum, pivinv, minpiv = HUGE;

  for (j = 0; j < n; j++)
    ipiv[j] = -1;

  for (i = 0; i < n; i++) {
    big = 0.0;
    for (j = 0; j < n; j++)
      if (ipiv[j] != 0)
	for (k = 0; k < n; k++) {
	  if (ipiv[k] == -1) {
	    if (fabs (m[j][k]) >= big) {
	      big = fabs (m[j][k]);
	      irow = j;
	      icol = k;
	    }
	  }
	}
    ipiv[icol]++;
    if (irow != icol)
      for (l = 0; l < n; l++) 
	swap (double, m[irow][l], m[icol][l]);
    indxr[i] = irow;
    indxc[i] = icol;
    if (fabs (m[icol][icol]) <= pivmin)
      return 0.;
    if (fabs (m[icol][icol]) < minpiv)
      minpiv = fabs (m[icol][icol]);
    pivinv = 1.0/m[icol][icol];
    m[icol][icol] = 1.0;
    for (l = 0; l < n; l++) m[icol][l] *= pivinv;
    for (ll = 0; ll < n; ll++)
      if (ll != icol) {
	dum = m[ll][icol];
	m[ll][icol] = 0.0;
	for (l = 0; l < n; l++)
	  m[ll][l] -= m[icol][l]*dum;
      }
  }
  for (l = n - 1; l >= 0; l--) {
    if (indxr[l] != indxc[l])
      for (k = 0; k < n; k++)
	swap (double, m[k][indxr[l]], m[k][indxc[l]]);
  }
  return minpiv;
}

static void parabola_fit_add (ParabolaFit * p, coord m, double w)
{
	if (axi==0)
	{
	  double x1 = m.x - p->o.x, y1 = m.y - p->o.y;
	  double x = p->m.y*x1 - p->m.x*y1;
	  double y = p->m.x*x1 + p->m.y*y1;
	  double x2 = w*x*x, x3 = x2*x, x4 = x3*x;
	  p->M[0][0] += x4;
	  p->M[1][0] += x3; p->M[1][1] += x2;
	  p->M[2][1] += w*x; p->M[2][2] += w;
	  p->rhs[0] += x2*y; p->rhs[1] += w*x*y; p->rhs[2] += w*y;
	}
	else
	{
		double x1 = m.y - p->o.y, y1 = m.x - p->o.x;
		  double x = p->m.x*x1 - p->m.y*y1;
		  double y = p->m.y*x1 + p->m.x*y1;
		  double x2 = w*x*x, x3 = x2*x, x4 = x3*x;
		  p->M[0][0] += x4;
		  p->M[1][0] += x3; p->M[1][1] += x2;
		  p->M[2][1] += w*x; p->M[2][2] += w;
		  p->rhs[0] += x2*y; p->rhs[1] += w*x*y; p->rhs[2] += w*y;
	}

}

static double parabola_fit_solve (ParabolaFit * p)
{
  p->M[0][1] = p->M[1][0];
  p->M[0][2] = p->M[2][0] = p->M[1][1];
  p->M[1][2] = p->M[2][1];
  double pivmin = matrix_inverse (p->M, 3, 1e-10);
  if (pivmin) {
    p->a[0] = p->M[0][0]*p->rhs[0] + p->M[0][1]*p->rhs[1] + p->M[0][2]*p->rhs[2];
    p->a[1] = p->M[1][0]*p->rhs[0] + p->M[1][1]*p->rhs[1] + p->M[1][2]*p->rhs[2];
  }
  else /* this may be a degenerate/isolated interface fragment */
    p->a[0] = p->a[1] = 0.;

  matrix_free (p->M);
  return pivmin;
}

static double parabola_fit_curvature (ParabolaFit * p,
				      double kappamax, double * kmax)
{
  double kappa;

  double dnm = 1. + sq(p->a[1]);
  kappa = - 2.*p->a[0]/pow(dnm, 3/2.);
  if (kmax)
    *kmax = fabs (kappa);

  if (fabs (kappa) > kappamax) {
    if (kmax)
      *kmax = kappamax;
    return kappa > 0. ? kappamax : - kappamax;
  }
  return kappa;
}

static void parabola_fit_axi_curvature (const ParabolaFit * p,
					double r, double h,
					double * kappa, double * kmax)
{
  double nr = (p->m.y*p->a[1] + p->m.x)/sqrt (1. + sq(p->a[1]));
  /* limit the minimum radius to half the grid size */
  double kaxi = nr/max(r, h/2.);
  *kappa += kaxi;
  if (kmax)
    *kmax = max (*kmax, fabs (kaxi));
}