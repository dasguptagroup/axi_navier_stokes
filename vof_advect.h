

/********************************************************************************************************************************
NAME       : AdvanceF_field
DESCRIPTION:
LIMITATIONS:
*********************************************************************************************************************************/
void AdvanceF_field(int r,int c, int Flag, int Togg)
{
	  /*******LOCAL VARIABLES*******/
	    double VolFrac;
	    double Zeta, Zeta1, Zeta2;
	    double NetXFlux, NetYFlux, NetXFlux_Corr, NetYFlux_Corr;
	    double X_Flux_Out, X_Flux_In, X_Flux_Out_Corr, delV_Corr;
	    double Y_Flux_Out, Y_Flux_In, Y_Flux_Out_Corr;
	    double X_Flux_Right,X_Flux_Left, X_Flux_Right_Corr, X_Flux_Left_Corr;
	    double Y_Flux_Up, Y_Flux_Down, Y_Flux_Up_Corr, Y_Flux_Down_Corr;
	    double Ustar_Left, Ustar_Right, Vstar_Up, Vstar_Down;
	  /*****************************/
	
	/****************WEYMOUTH IMPLEMENTATION*******************************************/
	if (weymouth==1)
	{
		if (CFL>0.5)
		{
			printf("keep CFL below 0.5 to ensure conservature of volume fractions");
			exit(0);
		}
						
		
		if (Flag==1)
		{
			Cellattrib[r][c].VolFrac = Cellattrib[r][c].VolFrac + 1.0/RF[c]* ( (X_Flux[r][c]*eta[c]-X_Flux[r][c+1]*eta[c+1] )/del_sq + delT*cc[r][c]*(X_Velocity[r][c+1]*eta[c+1]-X_Velocity[r][c]*eta[c])/del );
		}
		else if(Flag==2)
		{
			Cellattrib[r][c].VolFrac = Cellattrib[r][c].VolFrac + (Y_Flux[r][c]-Y_Flux[r+1][c] )/del_sq + delT*cc[r][c]*(Y_Velocity[r+1][c]-Y_Velocity[r][c])/del ;
		}
		
		return;
	}
	/****************WEYMOUTH IMPLEMENTATION END*******************************************/
					
					
	/*  LORSTAD IMPLEMENTATION*********************/
				//~ if (StepT>=221 )
					//~ {
						//~ if(r==39 && c==4)
						//~ printf(" ");
					//~ }
	  	  if(Flag == 1)    //HORIZONTAL SWEEP
			{
				
			 //   if(X_Velocity[r][c+1] >= 0.0)     //RIGHT SIDE OF THE CELL EFFLUXES
				{
					 X_Flux_Right = X_Flux[r][c+1]/del_sq;  //fraction of volume out
					 X_Flux_Left  =  X_Flux[r][c]/del_sq;		//fraction of volume in


							Ustar_Right  = delTBydel*X_Velocity[r][c+1];

			  				Zeta1 = X_Flux_Right/Ustar_Right;
							Zeta2 = (Cellattrib[r][c].VolFrac - X_Flux_Right)/(1.0 - Ustar_Right);

							if( absolute(Zeta1 - 0.5) >= absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( absolute(Zeta1 - 0.5) < absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 0)   //HORIZONTAL PASS IS THE FIRST
								{
									 Cellattrib[r][c].delV = (1.0 - delTBydel*(X_Velocity[r][c+1]*eta[c+1] - X_Velocity[r][c]*eta[c])/RF[c]);

									 NetXFlux     = (X_Flux_Right*eta[c+1] - X_Flux_Left*eta[c])/RF[c];
									
									 //~ NetXFlux     = (X_Flux_Right*(c+1)*del - X_Flux_Left*c*del)/((c+0.5)*del);

									 VolFrac = (Cellattrib[r][c].VolFrac - NetXFlux)/Cellattrib[r][c].delV;
								}
								else if (Togg == 1) //HORIZONTAL PASS IS THE SECOND
								{

									 delV_Corr = (Cellattrib[r][c].delV + (1-Cellattrib[r][c].delV)*Zeta);

									 X_Flux_Right_Corr = Cellattrib[r][c].delV*(X_Flux_Right- Ustar_Right) + delV_Corr*Ustar_Right;

									 NetXFlux_Corr     = (X_Flux_Right_Corr*eta[c+1] - X_Flux_Left*eta[c])/RF[c];
									
									 //~ NetXFlux_Corr     = ((X_Flux_Right_Corr)*(c)*del - (X_Flux_Left)*(c-1)*del)/((c-0.5)*del);

									VolFrac = ((Cellattrib[r][c].VolFrac*Cellattrib[r][c].delV) - NetXFlux_Corr);
								}





				}


			}

		  else if(Flag == 2)   //VERTICAL SWEEP
		{

				//if(Y_Velocity[r+1][c] >= 0.0)     //UPPER PART OF THE CELL EFFLUXES
				{
					Y_Flux_Up    =Y_Flux[r+1][c]/del_sq;
					Y_Flux_Down  = Y_Flux[r][c]/del_sq;
					Vstar_Up  = delTBydel*Y_Velocity[r+1][c];

			  				Zeta1 = Y_Flux_Up/Vstar_Up;
							Zeta2 = (Cellattrib[r][c].VolFrac - Y_Flux_Up)/(1.0 - Vstar_Up);

							if( absolute(Zeta1 - 0.5) >= absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta1;
							}
							else if ( absolute(Zeta1 - 0.5) < absolute(Zeta2 - 0.5) )
							{
								Zeta = Zeta2;
							}

								if(Togg == 1)   //VERTICAL PASS IS THE FIRST
								{
									Cellattrib[r][c].delV = 1.0 - delTBydel*(Y_Velocity[r+1][c] - Y_Velocity[r][c]);
									NetYFlux     = Y_Flux_Up - Y_Flux_Down;

									VolFrac = (Cellattrib[r][c].VolFrac - NetYFlux)/Cellattrib[r][c].delV;
								}
								else if (Togg == 0) //VERTICAL PASS IS THE SECOND
								{
									delV_Corr = Cellattrib[r][c].delV + (1-Cellattrib[r][c].delV)*Zeta;
									 Y_Flux_Up_Corr = Cellattrib[r][c].delV*(Y_Flux_Up - Vstar_Up) + delV_Corr*Vstar_Up;
									NetYFlux_Corr     = Y_Flux_Up_Corr - Y_Flux_Down;

									VolFrac = (Cellattrib[r][c].VolFrac*Cellattrib[r][c].delV - NetYFlux_Corr);


								}




				}


		} //Vertical Sweep
					//~ if (StepT>=221 )
					//~ {
						//~ if(r==39 && c==4)
						//~ printf(" ");
					//~ }
		
		  //~ Write_struct_Cellattrib(r,c,VolFrac,-100000,-100000,-100000,-100000,"-100000",-100000,-100000,-100000);
		 Cellattrib[r][c].VolFrac=VolFrac;
		/*************************LORSTAD END***********/ 
		
			
			
			
		
		

}




/**********************************************************************************************************
NAME: CalculateF_Flux
DESCRIPTION:
			1. If Flag = 1 , this subroutine does a horizontal pass and calculates all horizontal fluxes
			2. If Flag = 2 , this subroutine does a vertical pass and calculates all vertical fluxes
LIMITATIONS:
**********************************************************************************************************/
void CalculateF_Flux(int flag)
{
  /*******LOCAL VARIABLES**********/
  int		  r,c, Quad;
  double Theta;
  char		  Shape[30];
  double x_0,y_0,x_del,y_del,y_XVeldelT,x_YVeldelT;
  double X_Vel, Y_Vel;
  double XVeldelT,YVeldelT;
  double P;
  double al;
  double Area;
  /********************************/

		//HORIZONTAL PASS
		if(flag == 1)
		{
		  for(r=1;r <= NoOfRows+1;r++)
		   {
			 for(c=1;c <= NoOfCols+1 ;c++)
			  {
				  
				
				/************INITIALISE**********/
				 X_Vel = X_Velocity[r][c];
				 Y_Vel = Y_Velocity[r][c];
				 /******************************/

				 if(X_Vel > 0.0)   //THE CELL TO THE LEFT OF THE BOUNDARY DECIDES THE EFFLUX AND THAT CELL WILL FLUX FROM ITS RIGHT BOUNDARY
				  {
					//~ if(c == 0)
					//~ {
						//~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
						//~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
						//~ continue;
					//~ }

					if(Cellattrib[r][c-1].VolFrac > LOWER_LIMIT && Cellattrib[r][c-1].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c-1] - INIT BLOCK
						Quad = Find_Quad(r,c-1);
						strcpy(Shape,Cellattrib[r][c-1].Shape);  //GET SHAPE FROM [r][c-1]
						P = Cellattrib[r][c-1].P;
						al = Cellattrib[r][c-1].al;
						Area = Cellattrib[r][c-1].Area;
						
						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariables() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 2)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 3)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							X_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
						else if(Quad == 4)
						{
							AssignVal_FluxVariables(r, c-1, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						}
					}
					else if(Cellattrib[r][c-1].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c-1].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = XVeldelT*del; //4-6-15

					}
				  }
				else if (X_Vel < 0.0)   //THE CELL TO THE RIGHT OF THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LEFT BOUNDARY
				  {

					//~ if(c == NoOfCols)
					//~ {
						  //~ if(r==1 && c==3)
						//~ printf("haha");
					  //~ //X_Flux[r][c] = X_Vel*delT*del/delTdel;  ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE NEIGHBOURING CELL IS FULL
					  //~ X_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = absolute(X_Vel);
					Y_Vel = absolute(Y_Vel);

					if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT) //FOR CELLS CONTAINING INTERFACE
					{
						//FETCH INTERFACE VALUES FROM [r][c] - INIT BLOCK
						Quad = Find_Quad(r,c);
						strcpy(Shape,Cellattrib[r][c].Shape);  //GET SHAPE FROM [r][c]
						P = Cellattrib[r][c].P;
						al = Cellattrib[r][c].al;
						Area = Cellattrib[r][c].Area;
						/**************************************/

						//THE PARAMETERS PASSED TO THE FUNCTION AssignVal_FluxVariables() WILL DIFFER FOR 1/3 AND 2/4 QUADRANTS. THE XVEL AND YVEL HAVE TO BE INTERCHANGED FOR 2/4 QUADRANTS
						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 					X_Flux[r][c] = -CalculateX_Flux("Left",  Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
							//if(r == 30 & c == 5)
							//	printf("\nVelocity Negative, Quad 2 %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
						    X_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
								//if(r == 15 & c == 38)
								//printf("\nVelocity Negative Quad is 3rd %d %d    %e\n",r,c,X_Flux[r][c]);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							X_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
					}
					else if(Cellattrib[r][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						X_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c].VolFrac >= UPPER_LIMIT) //CELL IS FULL - PARTIAL FLUX
					{
						XVeldelT     = X_Vel*delT;
						//X_Flux[r][c] = -XVeldelT*del/delTdel;  // KEPT AS THE SAME FORMULA ALTHOUGH NUM. & DEN. CANCEL
						X_Flux[r][c] = -XVeldelT*del;   // corrected on 4-6-15


					}
				  }
				else //NO FLUX
				  {
					X_Flux[r][c] = 0.0;
				  }

				
			}
		   }
		   

		}
		
	  //VERTICAL PASS
		else if(flag == 2)
		{
		   for(r=1;r <= NoOfRows+1;r++) 
		  {
			 for(c=1;c <= NoOfCols+1;c++)
			  {
					
					//~ if (StepT==641)
					//~ {
						//~ if(r==64 && c==4)
						//~ printf("debug");
						
					//~ }  
				  
				  
				
				X_Vel = X_Velocity[r][c];
				Y_Vel = Y_Velocity[r][c];

		  		if(Y_Vel > 0.0)   //THE CELL BELOW THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS UPPER BOUNDARY
				  {
					//~ if(r == 0)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					if(Cellattrib[r-1][c].VolFrac > LOWER_LIMIT && Cellattrib[r-1][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r-1][c] - INIT
						Quad = Find_Quad(r-1,c);
						strcpy(Shape, Cellattrib[r-1][c].Shape);  //GET SHAPE FROM [r - 1][c]
						P = Cellattrib[r-1][c].P;
						al = Cellattrib[r-1][c].al;
						Area = Cellattrib[r-1][c].Area;
						/**************************************/
					
					
						
						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
	  					    Y_Flux[r][c] = CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 4)
						 {	
							AssignVal_FluxVariables(r-1, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);

						 }
					}
					else if (Cellattrib[r-1][c].VolFrac <= LOWER_LIMIT) // NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if (Cellattrib[r-1][c].VolFrac >= UPPER_LIMIT)  //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = YVeldelT*del/delTdel;//
						Y_Flux[r][c] = YVeldelT*del;  // 4-6-15
					}

				  }
				else if (Y_Vel < 0.0)   //THE CELL ABOVE THE BOUNDARY DECIDES THE FLUX AND THAT CELL WILL FLUX FROM ITS LOWER BOUNDARY
				  {
					//~ if(r == NoOfRows)
					//~ {
					  //~ //Y_Flux[r][c] = Y_Vel*delT*del/delTdel;          ////CHANGE - CURRENT ASSUMPTION IS THAT AT THE BOUNDARY NEIGNBOURS ARE FULL
					  //~ Y_Flux[r][c] = 0.0;   //PRESENTLY THE ONLY THE INTERIOR PART OF THE INTERFACE CONTAINS FLUID AND HENCE NO FLUX AT THE BOUNDARY
					  //~ continue;
					//~ }

					X_Vel = absolute(X_Vel);
					Y_Vel = absolute(Y_Vel);

					if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
					{
						//FETCH INTERFACE DETAILS FROM [r][c] - INIT
						Quad = Find_Quad(r,c);
						strcpy(Shape, Cellattrib[r][c].Shape);  //GET SHAPE FROM [r][c]
						P = Cellattrib[r][c].P;
						al = Cellattrib[r][c].al;
						Area = Cellattrib[r][c].Area;
						/**************************************/
						
							
						if(Quad == 1)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
		 				    Y_Flux[r][c] = -CalculateY_Flux("Lower", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 2)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
							Y_Flux[r][c] = -CalculateX_Flux("Left", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 3)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &X_Vel, &Y_Vel);
							Y_Flux[r][c] = -CalculateY_Flux("Upper", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
						 else if(Quad == 4)
						 {
							AssignVal_FluxVariables(r, c, &Theta, &XVeldelT, &YVeldelT, &x_0, &y_0, &x_del, &y_del, &x_YVeldelT, &y_XVeldelT, &Y_Vel, &X_Vel);
						    Y_Flux[r][c] = -CalculateX_Flux("Right", Shape, Theta, XVeldelT, YVeldelT, x_0, y_0, x_del, y_del, x_YVeldelT, y_XVeldelT,P,al,Area);
						 }
					}
					else if(Cellattrib[r][c].VolFrac <= LOWER_LIMIT)  //NO FLUX
					{
						Y_Flux[r][c] = 0.0;
					}
					else if(Cellattrib[r][c].VolFrac >= UPPER_LIMIT)   //CELL IS FULL - PARTIAL FLUX
					{
						YVeldelT     = Y_Vel*delT;
						//Y_Flux[r][c] = -YVeldelT*del/delTdel;
						Y_Flux[r][c] = -YVeldelT*del; //4-6-15
					}
				  }
				else //NO FLUX
				  {
						Y_Flux[r][c] = 0.0;
				  }
			  }
		  }
		}
		

}

/******************************************************************************
NAME:		 CalculateX_Flux
DESCRIPTION: Calculates the fluxes for the vertical walls
LIMITATIONS:
******************************************************************************/
  double CalculateX_Flux(char Wall[20], char Shape[20],   double Theta,   double XVeldelT,   double YVeldelT,   double x_0,   double y_0,   double x_del,   double y_del,   double x_YVeldelT,   double y_XVeldelT, double P, double al, double Area)
{
	/***LOCAL VARIABLES***/
	 double Area_Flux, temp;
	 double tanT;
	 double m;
	 double P_dash;
	 double M;
	 double Area_rem;
	/********************/

	/*****INITIALISE***/
	Area_Flux = 0.0;
	temp      = 0.0;
	tanT      = tan(Theta);
	m         = tanT/(1+tanT);
	M = tan(Theta);
	P_dash = 0.0;
	Area_rem = 0.0;
	/******************/

	  if(!strcmp(Wall,"Left"))      //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
	  {	
		  
		    if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)     //RECTANGLE
			{
				 if( (XVeldelT) >= x_0 )   //ENTIRE RECTANGLE LEAVES 
				   {
					 Area_Flux = x_0*del;
				   }
				 else                      //A TRAPEZIUM LEAVES
				   {
					 Area_Flux = (XVeldelT*del);
				   }
			}
		else  if( al <= m && al <= (1-m) )       //TRIANGLE
			{
				 //~ if( (XVeldelT) >= x_0 )   //ENTIRE TRIANGLE LEAVES		 
				   //~ {
					 //~ Area_Flux = 0.5*x_0*y_0;
					//~ printf("%Lf",Area_Flux);
				   //~ }
				 //~ else                      //A TRAPEZIUM LEAVES
				   //~ {
					 //~ //Area_Flux = ( y_0 + y_XVeldelT )*(XVeldelT*0.5); 
					 Area_Flux = Calculate_Area(Theta,P,XVeldelT,del);
				   //~ }
			}
		  else if ( al > m && al <= (1-m) )     //TRAPEZIUM THETA<PI/4
			{
					 //Area_Flux = ( y_0 + y_XVeldelT)*(XVeldelT*0.5);  //A TRAPEZIUM LEAVES
					 Area_Flux = Calculate_Area(Theta,P,XVeldelT,del);
				
			}
		  else  if ( al> (1-m) &&  al <= m )          //TRAPEZIUM THETA >PI/4
			{
				 if(XVeldelT <= x_del)   //A RECTANGLE LEAVES
				   {
					 Area_Flux = XVeldelT*del;
				   }
				 else if( (XVeldelT > x_del) && (XVeldelT < x_0)  )             //A 5-SIDED FIGURE LEAVES
				   {
					 //Area_Flux = ( (x_del + x_0)*delBy2 ) - ( 0.5*(x_0 - XVeldelT)*y_XVeldelT );   //TRAPEZIUM - TRIANGLE
					 Area_Flux = Calculate_Area(Theta,P,XVeldelT,del);
				   }
				 else if( XVeldelT >= x_0 )    //ENTIRE TRAPEZIUM LEAVES
					{
					  //Area_Flux = (x_del + x_0)*delBy2;
					  Area_Flux = Calculate_Area(Theta,P,XVeldelT,del);
					}
			}
		  else  if( al > m && al > (1-m) )                              //TRIANGLE COMPLEMENT
			{
				 if(XVeldelT <= x_del)        // A RECTANGLE LEAVES
				   {
					 Area_Flux = del*XVeldelT;
				   }
				 else if(XVeldelT > x_del)   //A 5-sided figure leaves
				   {
					 //Area_Flux = (x_del*del) + ( (del + y_XVeldelT)*(XVeldelT - x_del)*0.5 );  //RECTANGLE + TRAPEZIUM
					 Area_Flux = Calculate_Area(Theta,P,XVeldelT,del);
				   }
			}
	  }
	  else if(!strcmp(Wall,"Right"))   //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
	  {						
		 if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)         //RECTANGLE
			{
				if( (del - XVeldelT) >= x_0)  // NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
				else if((del - XVeldelT) < x_0)   // A RECTANGLE LEAVES
				{
					Area_Flux = del*(x_0 - del + XVeldelT );					
				}					
			}
		  else if( al <= m && al <= (1-m) )              //TRIANGLE 
			{
				if( (del - XVeldelT) >= x_0)  // NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
				else if((del - XVeldelT) < x_0)   // A TRIANGLE LEAVES
				{
					 //Area_Flux = 0.5*pow( (x_0 - del + XVeldelT), 2 )*tanT;
					 P_dash = P - (del-XVeldelT)*sin(Theta);
					 Area_Flux = Calculate_Area(Theta,P_dash,XVeldelT,del);
				}					
			}
		  else if ( al > m && al <= (1-m) )           //trapezium theta<pi/4
			{
					 //Area_Flux = ( y_0 - ((del - XVeldelT)*tanT) + y_del )*XVeldelT*0.5;  //A TRAPEZIUM LEAVES
				         P_dash = P - (del-XVeldelT)*sin(Theta);
					 Area_Flux = Calculate_Area(Theta,P_dash,XVeldelT,del);
			}
		  else if ( al> (1-m) &&  al <= m )          //trapezium theta>pi/4
			{
				if ((del - XVeldelT) >= x_0)  //NOTHING LEAVES
				{
					Area_Flux = 0.0;
				}
				else if ( (del - XVeldelT) < x_del )   //A TRAPEZIUM LEAVES
				{
					 //Area_Flux = (x_del - (2.0*(del - XVeldelT)) + x_0)*delBy2;
					 P_dash = P - (del-XVeldelT)*sin(Theta);
					 Area_Flux = Calculate_Area(Theta,P_dash,XVeldelT,del);
				}
				else if(  (del - XVeldelT) >= x_del && (del - XVeldelT) < x_0)    //A TRIANGLE LEAVES
				{
					 //Area_Flux = 0.5*pow( (y_0 - (del - XVeldelT)*tanT) ,2)/tanT;
					 P_dash = P - (del-XVeldelT)*sin(Theta);
					 Area_Flux = Calculate_Area(Theta,P_dash,XVeldelT,del);
				}
			}
		  else  if( al > m && al > (1-m) )
			{
				//~ if( (del - XVeldelT) < x_del)   //A 5-SIDED FIGURE LEAVES
				//~ {
					 //~ Area_Flux = (del*(x_del - del + XVeldelT)) + ((del + y_del)*(del - x_del)*0.5);  //RECTANGLE + TRAPEZIUM
					 P_dash = P - (del-XVeldelT)*sin(Theta);
					 Area_Flux = Calculate_Area(Theta,P_dash,XVeldelT,del);
				//~ }
				//~ else if((del - XVeldelT) >= x_del)   //A TRAPEZIUM LEAVES
				//~ {
					//~ Area_Flux = ( y_del + y_0 - ((del - XVeldelT)*tanT) )*XVeldelT*0.5;
					//~ printf("%Lf",Area_Flux);
				//~ }
			}
	  }
	 //temp = Area_Flux/delTdel;

	  //return temp;
	  return Area_Flux;
}


/******************************************************************************
NAME:	     CalculateY_Flux
DESCRIPTION: Calculates the fluxes for the horizontal walls
LIMITATIONS:
******************************************************************************/
  double CalculateY_Flux(char Wall[20], char Shape[20],   double Theta,   double XVeldelT,   double YVeldelT,   double x_0,   double y_0,   double x_del,   double y_del,   double x_YVeldelT,   double y_XVeldelT,double P, double al, double Area)
{
	/***LOCAL VARIABLES***/
	long double Area_Flux, temp;
	long double tanT;
	long double m;
	long double P_dash;
	/********************/

	/*****INITIALISE***/
	Area_Flux = 0.0;
	temp      = 0.0;
	tanT      = tan(Theta);
	m          =   tanT/(1+tanT);
	P_dash   = 0.0;
	/******************/

	  	if(!strcmp(Wall,"Lower"))    //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
		{	if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
				{
					//A rectangle always leaves
					Area_Flux = (x_0*YVeldelT);
					
				}
			else if( al <= m && al <= (1-m) )
				{
					if(YVeldelT >= y_0)  //ENTIRE TRIANGLE LEAVES
					{
						Area_Flux = 0.5*x_0*y_0;
					}
					else if(YVeldelT < y_0)  //A PARALLELOGRAM LEAVES 
					{
						//Area_Flux = (x_0 + x_YVeldelT)*YVeldelT*0.5;
						Area_Flux = Calculate_Area(Theta,P,del,YVeldelT);
					}
				}
			  else if ( al > m && al <= (1-m) )
				{
					if(YVeldelT >= y_0)   //ENTIRE TRAPEZIUM LEAVES
					{
						//Area_Flux = (y_0 + y_del)*delBy2;
						Area_Flux = Calculate_Area(Theta,P,del,YVeldelT);
					}
					else if(YVeldelT > y_del && YVeldelT < y_0)  //A 5-SIDED FIGURE LEAVES
					{
						//Area_Flux = (delBy2*(y_0 + y_del)) - ( (y_0 - YVeldelT)*x_YVeldelT*0.5 );  //AREA OF TRAPEZIUM - AREA OF TRIANGLE
						Area_Flux = Calculate_Area(Theta,P,del,YVeldelT);
					}
					else if(YVeldelT <= y_del) //A RECTANGLE LEAVES
					{
						Area_Flux = del*YVeldelT;
					}
				}
			  else if ( al> (1-m) &&  al <= m )
				{
						//Area_Flux = (x_0 + x_YVeldelT)*YVeldelT*0.5;
						Area_Flux = Calculate_Area(Theta,P,del,YVeldelT);
				}
			  else  if( al > m && al > (1-m) )
				{
					if(YVeldelT > y_del)   //A 5-SIDED FIGURE LEAVES
					{
						///Area_Flux = (del*y_del) + ( (del + x_YVeldelT)*(YVeldelT - y_del)*0.5 );  //AREA OF RECTANGLE + AREA OF TRAPEZIUM
						Area_Flux = Calculate_Area(Theta,P,del,YVeldelT);
					}
					else if(YVeldelT <= y_del)  //A RECTANGLE LEAVES
					{
						Area_Flux = del*YVeldelT;
					}
				}
		}
		else if(!strcmp(Wall,"Upper"))   //THE WALL OF A CELL THROUGH WHICH THE FLUX LEAVES
		{	if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
				{
					//A rectangle always leaves
					Area_Flux = (x_0*YVeldelT);
					
				}
			else if( al <= m && al <= (1-m) )
				{
					if(YVeldelT <= (del - y_0))   //NOTHING LEAVES
					{
						Area_Flux = 0.0;
					}
					else if(YVeldelT > (del - y_0))  //A TRIANGLE LEAVES
					{
						///Area_Flux = 0.5*pow(y_0 - del + YVeldelT,2)/tanT;
						P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
					}
				}
			  else if ( al > m && al <= (1-m) )
				{
					if(YVeldelT <= (del - y_0))  // NOTHING LEAVES
					{
						Area_Flux = 0.0;
					}
					else if(YVeldelT > (del - y_0) && YVeldelT <= (del - y_del))  //A TRIANGLE LEAVES
					{
						//Area_Flux = 0.5*pow(y_0 - del + YVeldelT,2)/tanT;
						P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
					}
					else if(YVeldelT > (del - y_del)) //A TRAPEZIUM LEAVES
					{
						//Area_Flux = ((y_0 + y_del)*delBy2) - ((del - YVeldelT)*del);
						P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
					}
				}
			  else  if ( al> (1-m) &&  al <= m )  //A TRAPEZIUM LEAVES
				{
						//Area_Flux = ( x_del + ((y_0 - del + YVeldelT)/tanT) )*YVeldelT*0.5;
					        P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
				}
			  else  if( al > m && al > (1-m) )
				{
					if(YVeldelT <= (del - y_del))    //A TRAPEZIUM LEAVES
					{
						//Area_Flux = ( x_del + ((y_0 - del + YVeldelT)/tanT) )*YVeldelT*0.5;
						P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
					}
					else if(YVeldelT > (del - y_del))  //A 5-SIDED FIGURE LEAVES
					{
						//Area_Flux = ( (y_del - del + YVeldelT)*del) + ((x_del + del)*(del - y_del)*0.5);  //AREA OF RECTANGLE + AREA OF TRAPEZIUM
						P_dash = P - (del-YVeldelT)*cos(Theta);
					        Area_Flux = Calculate_Area(Theta,P_dash,del,YVeldelT);
					}
				}
		}

		//temp = Area_Flux/delTdel;
		//return temp;
		return Area_Flux;
}

/******************************************************************************
NAME:		 AssignVal_FluxVariables
DESCRIPTION: Calculates the interface orientation details, for the cell whose identifier is passed.
			 The calculated values are written into the addresses passed.
LIMITATIONS:
******************************************************************************/
void AssignVal_FluxVariables(int r, int c,   double *ptr2,   double *ptr3,   double *ptr4,   double *ptr5,   double *ptr6,   double *ptr7,   double *ptr8,   double *ptr9,   double *ptr10,   double *ptr11,   double *ptr12)
{
  /*****LOOKUP FOR ARGUMENTS****
     double *ptr2  - Theta
     double *ptr3  - XVeldelT
     double *ptr4  - YVeldelT
     double *ptr5  - x_0
     double *ptr6  - y_0
     double *ptr7  - x_del
     double *ptr8  - y_del
     double *ptr9  - x_YVeldelT
     double *ptr10 - y_XVeldelT
     double *ptr11 - XVel
     double *ptr12 - YVel
  /****************************/

  /*********DECLARATIONS***********/
    double P, XVel, YVel;
    double Theta;
    double sinT,cosT,tanT;
  /********************************/

  /*************INITIALIZE*************/
  P				= Cellattrib[r][c].P;
  XVel          = *ptr11;                   //DO NOT RETRIEVE VELOCITY FROM CELL WALL .RETRIEVE ONLY FROM THE PASSED ADDRESS
  YVel          = *ptr12;                   //DO NOT RETRIEVE VELOCITY FROM CELL. RETRIEVE ONLY FROM THE PASSED ADDRESS SINCE CELL WALL IDENTIFIER IS DIFFRERENT FROM THE IDENTIFIER PASSED
  Theta         = Cellattrib[r][c].Theta;
  sinT			= sin(Theta);
  cosT			= cos(Theta);
  tanT			= tan(Theta);
  /************************************/


  /*********WRITING TO THE ADDRESSES PASSED************/
  *ptr2			= Cellattrib[r][c].Theta;
  *ptr3			= XVel*delT;
  *ptr4			= YVel*delT;
  *ptr5			= P/sinT;
  *ptr6			= P/cosT;
  *ptr7			= (*ptr6 - del)/tanT;
  *ptr8			= (*ptr6 - (del*tanT));
  *ptr9			= (*ptr6 - *ptr4)/tanT;
  *ptr10		= (*ptr6) - ((*ptr3)*tanT);
  /****************************************************/
  //FLAG: the following block is written to add the rectangle case in flux calculation

  if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
  {
			 *ptr2			= Cellattrib[r][c].Theta;
			  *ptr5			= P;
			 // *ptr6			= P/cosT;
			  *ptr7			= P;
			 // *ptr8			= (*ptr6 - (del*tanT));
			  *ptr9			= P;
			  //*ptr10		= (*ptr6) - ((*ptr3)*tanT);


  }
}
/******************************************************************************
NAME:		 Calculate_Area
DESCRIPTION: Calculates the area of the cell given the Theta and perpendicular distance values
LIMITATIONS: 
******************************************************************************/
 double Calculate_Area(double Theta, double P_dash, double c1, double c2)
{
   double M;
   double m;
   double A_rem;
   double al;	
	
   M= tan(Theta);
   m= M*c1/(M*c1+c2);
   al =  P_dash*sqrt((m*m)/(c1*c1)+((1-m)*(1-m))/(c2*c2));
	
	if (al <= 0.5 && m <= 0.5)
	 {
                 if (m >= al)
		 {
			 A_rem = c1*c2*al*al/(2*m*(1-m));
		 }
                 else if (m<al)
		 {
			 A_rem = c1*c2*(2*al-m)/(2*(1-m));
		 }
	 }
	 else if (al > 0.5 && m<=0.5)
	 {
		 if( al <= (1-m) )
		 {
			 A_rem = c1*c2*(2*al-m)/(2*(1-m));
		 }
		 else if( al > (1-m))
		 {
			 A_rem = c1*c2*(1-((1-al)*(1-al)/(2*m*(1-m))));
		 }
	 }
	 else if ( al <= 0.5 && m > 0.5)
	 {
		 if( al <= (1-m) )
		 {
			 A_rem = c1*c2*al*al/(2*m*(1-m));
		 }
		 else if( al > (1-m))
		 {
			 A_rem = c1*c2*(2*al - 1 +m)/(2*m);
		 }
	 }
	 else if ( al > 0.5 && m >0.5)
	 {
		 if (m >= al)
		 {
			 A_rem = c1*c2*(2*al - 1 +m)/(2*m);
		 }
		 else if ( m< al)
		 {
			  A_rem = c1*c2*(1-((1-al)*(1-al)/(2*m*(1-m))));
		 }
	 }
	 return A_rem;
 }
