void calculate_derived_variables()
{
		 int r,c;
	
			
	
	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        X_VelC[r][c] = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
		        Y_VelC[r][c] = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
	      }
	}
	// Calculate vorticity
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        
		        vorticity[r][c] = (Y_VelC[r][c+1] - Y_VelC[r][c-1])/(2.0*del)-(X_VelC[r+1][c] - X_VelC[r-1][c])/(2.0*del);
	      }
	}
	// Calculate  streamfunction **********************
	double **intx,**inty;
	
	
	intx = (double **) calloc(NoOfRows+2,sizeof(double *));
	inty = (double **) calloc(NoOfRows+2,sizeof(double *));
	
	
			for (int i=0; i<NoOfRows+2; i++)
			{
				intx[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				inty[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
	
			}
	// Integration for streamfunction		
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        inty[r][c] = -(RF[c] * X_VelC[r][c]+RF[c] * X_VelC[r-1][c])*del/2 + inty[r-1][c];	// Kundu Cohen pp. 271
			intx[r][c] =  (RF[c] * Y_VelC[r][c]+RF[c-1] * Y_VelC[r][c-1])*del/2 + intx[r][c-1];
           
		      
		      //~ vel_potential[r][c] = (X_VelC[r+1][c] + X_VelC[r-1][c])*del + (Y_VelC[r][c+1] + Y_VelC[r][c-1])*del; // Farsoiya et. al. Eq. 1.4 supplementary material, trapezoidal rule for numerical integration
	      }
	}
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		      
           
		      
		      streamfunction[r][c] = inty[r][1] + intx[r][c]; // stokes stream function // Farsoiya et. al. Eq. 1.13 supplementary material, trapezoidal rule for numerical integration
		      //~ streamfunction[r][c] = -(inty[r][1] + intx[r][c])/RF[c]; // miles stream function // Farsoiya et. al. Eq. 1.13 supplementary material, trapezoidal rule for numerical integration
		      //~ vel_potential[r][c] = (X_VelC[r+1][c] + X_VelC[r-1][c])*del + (Y_VelC[r][c+1] + Y_VelC[r][c-1])*del; // Farsoiya et. al. Eq. 1.4 supplementary material, trapezoidal rule for numerical integration
	      }
	}
	
	freeArray(intx);
	freeArray(inty);
	/************************************************************************/
	// Calculate  vel_potential **********************
	
	
/*	
	intx = (double **) calloc(NoOfRows+2,sizeof(double *));
	inty = (double **) calloc(NoOfRows+2,sizeof(double *));
	
	
			for (int i=0; i<NoOfRows+2; i++)
			{
				intx[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				inty[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
	
			}
	// Integration for streamfunction		
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        intx[r][c] = (X_VelC[r][c]+X_VelC[r][c-1])*del/2 + intx[r][c-1];	// Kundu Cohen pp. 271
			inty[r][c] =  ( Y_VelC[r][c]+Y_VelC[r-1][c])*del/2 + inty[r-1][c];
           
		      
		      //~ vel_potential[r][c] = (X_VelC[r+1][c] + X_VelC[r-1][c])*del + (Y_VelC[r][c+1] + Y_VelC[r][c-1])*del; // Farsoiya et. al. Eq. 1.4 supplementary material, trapezoidal rule for numerical integration
	      }
	}
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		      
           
		      
		      
		      vel_potential[r][c] = (inty[r][1] + intx[r][c]); 
		      
	      }
	}
	
	freeArray(intx);
	freeArray(inty);
	*/
	/************************************************************************/
		// Boundary Cells Vorticity
			for (r=1; r<=NoOfRows; r++)
			{
				
				vorticity[r][1] = vorticity[r][2];
				vorticity[r][NoOfCols] = vorticity[r][NoOfCols-1];
				
				streamfunction[r][1] = streamfunction[r][2];
				streamfunction[r][NoOfCols] = streamfunction[r][NoOfCols-1];
				
				//~ vel_potential[r][1] = vel_potential[r][2];
				//~ vel_potential[r][NoOfCols] = vel_potential[r][NoOfCols-1];
			}
		
			for (c=1;c<=NoOfCols; c++)
			{		
					vorticity[1][c] = vorticity[2][c]; 
					vorticity[NoOfRows][c] = vorticity[NoOfRows-1][c]; 
				
					streamfunction[1][c] = streamfunction[2][c]; 
					streamfunction[NoOfRows][c] = streamfunction[NoOfRows-1][c]; 
				
					//~ vel_potential[1][c] = vel_potential[2][c]; 
					//~ vel_potential[NoOfRows][c] = vel_potential[NoOfRows-1][c]; 
			}
		
}
/*Create a file to restore the simulation*from the last restore point***********************/
void restore_point()
{	
			
		 int r,c;
		 FILE *ptrWrite;
		 
			char filename[50];
		  //~ sprintf(filename, "restore.bcs", Step);
				ptrWrite =fopen("restore.bcs","w");
			   fprintf(ptrWrite,"%e\t%e\t%d\t%d\n",Time,del,NoOfRows,NoOfCols);	
		 for(r=0;r <= NoOfRows+1; r++)
		 {
		      for(c=0;c <=NoOfCols+1 ; c++)
		      {
				
			       fprintf(ptrWrite,"%e\t%e\t%e\t%e\n",X_Velocity[r][c],Y_Velocity[r][c],Press[r][c],Cellattrib[r][c].VolFrac);	// This is not the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
				
		       }

		  }

		  fclose(ptrWrite);

	
}
void bcs_data(double Step)
{			struct stat st = {0};
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data_field", 0700);
			}
			create_restore_point=0;
		 int r,c;
		 FILE *ptrWrite;
		 
			char filename[50];
		   sprintf(filename, "./NS_Data_field/NS%g.bcs", Step);
			ptrWrite =fopen(filename,"w");
				//~ ptrWrite =fopen("restore.bcs","w");
			   fprintf(ptrWrite,"%e\t%e\t%d\t%d\n",Time,del,NoOfRows,NoOfCols);	
		 for(r=0;r <= NoOfRows+1; r++)
		 {
		      for(c=0;c <=NoOfCols+1 ; c++)
		      {
				
			       fprintf(ptrWrite,"%e\t%e\t%e\t%e\n",X_Velocity[r][c],Y_Velocity[r][c],Press[r][c],Cellattrib[r][c].VolFrac);	// This is not the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
				
		       }

		  }

		  fclose(ptrWrite);

	
}
void field_data(double Step)	
{
	calculate_derived_variables();
	
			struct stat st = {0};
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data_field", 0700);
			}
			
 int r,c;
 FILE *ptrWrite;
  //~ double X_vel, Y_vel;
	char filename[50];
  sprintf(filename, "./NS_Data_field/NS%g.dat", Step);
 ptrWrite =fopen(filename,"w");
 for(r=0;r <= NoOfRows+1; r++)
 {
      for(c=0;c <=NoOfCols+1 ; c++)
      {
	      //  X_vel = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
	     //   Y_vel = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
		// I have not taken the averaged data output because I need the last file such that I can restart the simulation from there, averaged 
	      // data will lose some information, gerris don't have staggered grid, all data at cell centers, so gerris don't have this problem
	      //Note:- Ghost cells data also taken out, take care while post processing
	      
		
		fprintf(ptrWrite,"%e",del*(c-1));		// This is not the x-coordinate at the center of the cell, calculate the averge while post processing (x coordinate of left face of CV)
		fprintf(ptrWrite,"\t%e",del*(r-1));			// This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
		//fprintf(ptrWrite,"\t%e",X_vel);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
		//fprintf(ptrWrite,"\t%e",Y_vel);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
		
	       fprintf(ptrWrite,"\t%e",X_Velocity[r][c]);	// This is not the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
		 fprintf(ptrWrite,"\t%e",Y_Velocity[r][c]);	// This is not the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
		fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
		fprintf(ptrWrite,"\t%e",Cellattrib[r][c].VolFrac);	// this is at cell center
	      fprintf(ptrWrite,"\t%e",streamfunction[r][c]);	// this is at cell center
	      fprintf(ptrWrite,"\t%e",vorticity[r][c]);	// this is at cell center
		//~ fprintf(ptrWrite,"\t%e",kappa[r][c]);	// this is at cell center
		fprintf(ptrWrite,"\n");
       }

  }

  fclose(ptrWrite);

}

void tecplot_data(double Step)	
{
			struct stat st = {0};
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data_tecplot", 0700);
			}
	 int r,c;
	 FILE *ptrWrite;
	  
		char filename[50];
	  sprintf(filename, "./NS_Data_tecplot/NS-Tecplot-%g.dat", Step);
	 ptrWrite =fopen(filename,"w");
	
	fprintf(ptrWrite,"VARIABLES = \"X\", \"Y\", \"U\", \"V\", \"P\", \"f\",\"vorticity\"");
	fprintf(ptrWrite,"\nZONE T = \"%g\" I=%d, J=%d, F=POINT",Time,NoOfCols,NoOfRows);
	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        X_VelC[r][c] = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
		        Y_VelC[r][c] = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
	      }
	}
	
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        
		        vorticity[r][c] = (X_VelC[r+1][c] - X_VelC[r-1][c])/(2.0*del) - (Y_VelC[r][c+1] - Y_VelC[r][c-1])/(2.0*del);
	      }
	}
	
		// Boundary Cells Vorticity
			for (r=1; r<=NoOfRows; r++)
			{
				
				vorticity[r][1] = vorticity[r][2];
				vorticity[r][NoOfCols] = vorticity[r][NoOfCols-1];
			}
		
			for (c=1;c<=NoOfCols; c++)
			{		
					vorticity[1][c] = vorticity[2][c]; 
					vorticity[NoOfRows][c] = vorticity[NoOfRows-1][c]; 
			}
		
			
		
		
		
			
			
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        
			
			fprintf(ptrWrite,"\n%e",x[r][c]);		// This is not the x-coordinate at the center of the cell, calculate the averge while post processing (x coordinate of left face of CV)
			fprintf(ptrWrite,"\t%e",y[r][c]);			// This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
			fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
			fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
			
		    
			fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
			fprintf(ptrWrite,"\t%e",Cellattrib[r][c].VolFrac);	// this is at cell center
		      fprintf(ptrWrite,"\t%e",kappa[r][c]);	// this is at cell center
			//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
	       }

	  }

	  fclose(ptrWrite);

}


/*****************************************************************************************************************
NAME       : interface_data()
DESCRIPTIOM: Writes the (x1,y1) and (x2,y2) values for each interfacial cell into a file corresponding to actual time
LIMITATIONS:
*****************************************************************************************************************/
void interface_data(double Step)
{
	struct stat st = {0};
			if (stat("Plots", &st) == -1)
			{
				mkdir("Plots", 0700);
			}

  /*DECLARE LOCAL VARIABLES*/
  int r,c;


  FILE *ptrWrite;
  char LineFormat[] = "./Plots/PlotLine_%g.dat";
  char LineFileName[100];
  sprintf(LineFileName,LineFormat,Step);


  /***********INITIALISE**************/
  ptrWrite =fopen(LineFileName,"w");
  /***********************************/

  for(r=1;r <= NoOfRows ; r++)
   {
     for(c=1;c <= NoOfCols ; c++)
       {		
		 if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
		   {
			 
				   fprintf(ptrWrite,"%e\t%e\n", Xp1[r][c],Yp1[r][c]);
				   fprintf(ptrWrite,"%e\t%e\n", Xp2[r][c],Yp2[r][c]);


			   }
       }
  }

	fclose(ptrWrite);
}
void interface_midpt_data(double Step)
{
	struct stat st = {0};
			if (stat("Plots", &st) == -1)
			{
				mkdir("Plots", 0700);
			}

  /*DECLARE LOCAL VARIABLES*/
  int r,c;


  FILE *ptrWrite;
  char LineFormat[] = "./Plots/PlotLine_%g.csv";
  char LineFileName[20];
  sprintf(LineFileName,LineFormat,Step);


  /***********INITIALISE**************/
  ptrWrite =fopen(LineFileName,"w");
  /***********************************/
	 fprintf(ptrWrite,"x,y\n");
  for(r=1;r <= NoOfRows ; r++)
   {
     for(c=1;c <= NoOfCols ; c++)
       {		
		 if(Cellattrib[r][c].VolFrac > LOWER_LIMIT && Cellattrib[r][c].VolFrac < UPPER_LIMIT)
		   {
			 
				   fprintf(ptrWrite,"%e,%e\n", 0.5*(Xp1[r][c]+Xp2[r][c]),0.5*(Yp1[r][c]+Yp2[r][c]));
				   


			   }
       }
  }

	fclose(ptrWrite);
}

void vtk_data(double Step)	
{
			struct stat st = {0};
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data", 0700);
			}
	 int r,c;
	
			
	
	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        X_VelC[r][c] = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
		        Y_VelC[r][c] = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
	      }
	}
	
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        
		        vorticity[r][c] = (X_VelC[r+1][c] - X_VelC[r-1][c])/(2.0*del) - (Y_VelC[r][c+1] - Y_VelC[r][c-1])/(2.0*del);
	      }
	}
	
		// Boundary Cells Vorticity
			for (r=1; r<=NoOfRows; r++)
			{
				
				vorticity[r][1] = vorticity[r][2];
				vorticity[r][NoOfCols] = vorticity[r][NoOfCols-1];
			}
		
			for (c=1;c<=NoOfCols; c++)
			{		
					vorticity[1][c] = vorticity[2][c]; 
					vorticity[NoOfRows][c] = vorticity[NoOfRows-1][c]; 
			}
		
			
	// start writing in file	
		
	 FILE *ptrWrite;
	  
		char filename[50];
	  sprintf(filename, "./NS_Data/NS-%g.vtk", Step);
	 ptrWrite =fopen(filename,"w");
	
	fputs ("# vtk DataFile Version 2.0\n"
	 "Bombay Class Solver(BCS)\n"
	 "ASCII\n"
	 "DATASET STRUCTURED_GRID\n", ptrWrite);
	fprintf (ptrWrite, "DIMENSIONS %d %d 1\n",  NoOfRows, NoOfCols);
	fprintf (ptrWrite, "POINTS %d double\n", NoOfCols*NoOfRows);	
			
			
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        
			
			fprintf(ptrWrite,"%g %g 0\n",x[r][c],y[r][c]);		// This is not the x-coordinate at the center of the cell, calculate the averge while post processing (x coordinate of left face of CV)
			//~ fprintf(ptrWrite,"\t%e",y[r][c]);
	      }
	}
	fprintf (ptrWrite, "POINT_DATA %d\n", NoOfCols*NoOfRows);	
	
	for(int i=0;i<1;i++)
	{
		 fprintf (ptrWrite, "SCALARS VOF double\n");
		fputs ("LOOKUP_TABLE default\n", ptrWrite);
		 for(r=1;r <= NoOfRows; r++)
		 {
		      for(c=1;c <=NoOfCols ; c++)
		      {
			      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
				//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
				//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
				
			    
				//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
				fprintf(ptrWrite,"%g\n",Cellattrib[r][c].VolFrac);	// this is at cell center
				//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
		       }

		  }
	}

	  fclose(ptrWrite);

}
//~ vtk_data_binary does not work currently, don't use till further notice
void vtk_data_binary(double Step)	
{
			struct stat st = {0};
			if (stat("NS_Data_vtk", &st) == -1)
			{
				mkdir("NS_Data_vtk", 0700);
			}
	 int r,c;
	
			
	
	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        X_VelC[r][c] = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
		        Y_VelC[r][c] = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
	      }
	}
	
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        
		        vorticity[r][c] = (X_VelC[r+1][c] - X_VelC[r-1][c])/(2.0*del) - (Y_VelC[r][c+1] - Y_VelC[r][c-1])/(2.0*del);
	      }
	}
	
		// Boundary Cells Vorticity
			for (r=1; r<=NoOfRows; r++)
			{
				
				vorticity[r][1] = vorticity[r][2];
				vorticity[r][NoOfCols] = vorticity[r][NoOfCols-1];
			}
		
			for (c=1;c<=NoOfCols; c++)
			{		
					vorticity[1][c] = vorticity[2][c]; 
					vorticity[NoOfRows][c] = vorticity[NoOfRows-1][c]; 
			}
		
			
	// start writing in file	
		
	 FILE *ptrWrite;
	  
		char filename[50];
	  sprintf(filename, "./NS_Data_vtk/NS-%g.vtk", Step);
	 ptrWrite =fopen(filename,"w");
	
	fputs ("# vtk DataFile Version 2.0 byte_order=LittleEndian\n"
	 "Bombay Class Solver(BCS)\n"
	 "BINARY\n"
	 "DATASET STRUCTURED_GRID\n", ptrWrite);
	fprintf (ptrWrite, "DIMENSIONS %d %d 1\n",  NoOfRows, NoOfCols);
	fprintf (ptrWrite, "POINTS %d double\n", NoOfCols*NoOfRows);	
			
		double z =0;	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        
			 fwrite(&x[r][c], sizeof(double), 1, ptrWrite); 
			 fwrite(&y[r][c], sizeof(double), 1, ptrWrite); 
			fwrite(&z, sizeof(double), 1, ptrWrite); 
			
	      }
	}
	
	 //~ for(r=1;r <= NoOfRows; r++)
	 //~ {
	      //~ for(c=1;c <=NoOfCols ; c++)
	      //~ {
		        
			 //~ fwrite(&y[r][c], sizeof(double), 1, ptrWrite); 
			
		
	      //~ }
	//~ }
	//~ double z=0;
	 //~ for(r=1;r <= NoOfRows; r++)
	 //~ {
	      //~ for(c=1;c <=NoOfCols ; c++)
	      //~ {
		        
			 //~ fwrite(&z, sizeof(double), 1, ptrWrite); 
			
	      //~ }
	//~ }
	
	
	fprintf (ptrWrite, "\nPOINT_DATA %d\n", NoOfCols*NoOfRows);	
	
	for(int i=0;i<1;i++)
	{
		 fprintf (ptrWrite, "SCALARS VOF double\n");
		fputs ("LOOKUP_TABLE default\n", ptrWrite);
		 for(r=1;r <= NoOfRows; r++)
		 {
		      for(c=1;c <=NoOfCols ; c++)
		      {
			      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
				//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
				//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
				
			    
				//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
				//~ fprintf(ptrWrite,"%u",Cellattrib[r][c].VolFrac);	// this is at cell center
			       fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
				//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
		       }

		  }
	}

	  fclose(ptrWrite);

}
void vti_data(double Step)	
{
			struct stat st = {0};
			if (stat("NS_Data_vti", &st) == -1)
			{
				mkdir("NS_Data_vti", 0700);
			}
	calculate_derived_variables();
			
	// start writing in file	
		
	 FILE *ptrWrite;
	  
		char filename[50];
	sprintf(filename, "./NS_Data_vti/NS-%g.vti", Step);

	 ptrWrite =fopen(filename,"w");
	
	fputs ("<?xml version=\"1.0\"?>\n"
	 "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n", ptrWrite);
	
	fprintf (ptrWrite, "<ImageData WholeExtent=\"0 %d 0 %d 0 0\" Origin=\"0 0 0\" Spacing=\"%g %g 0\">\n",  NoOfCols, NoOfRows,del,del);
	fprintf (ptrWrite, "<Piece Extent=\"0 %d 0 %d 0 0\">\n",  NoOfCols, NoOfRows);	
	fputs(" <CellData>\n",ptrWrite);		
	
	
	int r,c;
	
	for(int i=1;i<=6;i++)
	{
		if(i==1)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"u\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					fprintf(ptrWrite,"%g ",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==2)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"v\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					fprintf(ptrWrite,"%g ",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==3)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"p\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					fprintf(ptrWrite,"%g ",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
			       }

			  }
			   fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==4)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"f\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					//~ fprintf(ptrWrite,"\t%e",vorticity[r][c]);		// this is at cell center
			       }

			  }
			   fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==5)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"vorticity\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					fprintf(ptrWrite,"%g ",vorticity[r][c]);		// this is at cell center
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		// not working 
		if(i==6)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"streamfunction\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					fprintf(ptrWrite,"%g ",streamfunction[r][c]);		// this is at cell center
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==100)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"vel_potential\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					fprintf(ptrWrite,"%g ",vel_potential[r][c]);		// this is at cell center
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==8)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"kappa\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					if (kappa[r][c]<nodata){
						fprintf(ptrWrite,"%g ",kappa[r][c]);		
					}
					//~ else {
						//~ fprintf(ptrWrite,"%g ",0.0);
					//~ } // this is at cell center
				      
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==7)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"solid\" format=\"ascii\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      // This is not the y-coordinate at the center of the cell, calculate the averge while post processing (y coordinate of bottom face of CV)
					//~ fprintf(ptrWrite,"\t%e",X_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing (x velocity of left face of CV)
					//~ fprintf(ptrWrite,"\t%e",Y_VelC[r][c]);	// This is  the velocity at the center of the cell, calculate the averge while post processing  (y velocity of bottom face of CV)
					
				    
					//~ fprintf(ptrWrite,"\t%e",Press[r][c]);		// this is at cell center
					//~ fprintf(ptrWrite,"%g ",Cellattrib[r][c].VolFrac);	// this is at cell center
				       //~ fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					
						fprintf(ptrWrite,"%g ",solid[r][c].VolFrac);		
					
					//~ else {
						//~ fprintf(ptrWrite,"%g ",0.0);
					//~ } // this is at cell center
				      
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
	}
	
	fputs("</CellData>\n"
	"</Piece>\n"
	" </ImageData>\n"
	" </VTKFile>",ptrWrite);
	
	  fclose(ptrWrite);

}
// FLAG- vtk_data_binary_xml Does not work properly, dont use till further notice
void vtk_data_binary_xml(double Step)	
{
			struct stat st = {0};
			if (stat("NS_Data", &st) == -1)
			{
				mkdir("NS_Data", 0700);
			}
	 int r,c;
	
			
	
	
	 for(r=1;r <= NoOfRows; r++)
	 {
	      for(c=1;c <=NoOfCols ; c++)
	      {
		        X_VelC[r][c] = 0.5*(X_Velocity[r][c+1]+X_Velocity[r][c]);
		        Y_VelC[r][c] = 0.5*(Y_Velocity[r+1][c]+Y_Velocity[r][c]);
	      }
	}
	
	 for(r=2;r <= NoOfRows-1; r++)
	 {
	      for(c=2;c <=NoOfCols-1 ; c++)
	      {
		        
		        vorticity[r][c] = (X_VelC[r+1][c] - X_VelC[r-1][c])/(2.0*del) - (Y_VelC[r][c+1] - Y_VelC[r][c-1])/(2.0*del);
	      }
	}
	
		// Boundary Cells Vorticity
			for (r=1; r<=NoOfRows; r++)
			{
				
				vorticity[r][1] = vorticity[r][2];
				vorticity[r][NoOfCols] = vorticity[r][NoOfCols-1];
			}
		
			for (c=1;c<=NoOfCols; c++)
			{		
					vorticity[1][c] = vorticity[2][c]; 
					vorticity[NoOfRows][c] = vorticity[NoOfRows-1][c]; 
			}
		
			
	// start writing in file	
		
	 FILE *ptrWrite;
	  
		char filename[50];
	  sprintf(filename, "./NS_Data/NSb-%g.vti", Step);
	 ptrWrite =fopen(filename,"w");
	
	fputs ("<?xml version=\"1.0\"?>\n"
	 "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n", ptrWrite);
	
	fprintf (ptrWrite, "<ImageData WholeExtent=\"0 %d 0 %d 0 0\" Origin=\"0 0 0\" Spacing=\"%g %g 0\">\n",  NoOfCols, NoOfRows,del,del);
	fprintf (ptrWrite, "<Piece Extent=\"0 %d 0 %d 0 0\">\n",  NoOfCols, NoOfRows);	
	fputs(" <CellData>\n",ptrWrite);		
	
	
	
	
	for(int i=1;i<=5;i++)
	{
		if(i==1)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"u\" format=\"binary\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      
				       fwrite(&X_VelC[r][c], sizeof(double), 1, ptrWrite); 
					
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==2)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"v\" format=\"binary\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				    
				       fwrite(&Y_VelC[r][c], sizeof(double), 1, ptrWrite); 
					
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==3)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"p\" format=\"binary\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				    
				       fwrite(&Press[r][c], sizeof(double), 1, ptrWrite); 
				
			       }

			  }
			   fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==4)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"f\" format=\"binary\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				      
				       fwrite(&Cellattrib[r][c].VolFrac, sizeof(double), 1, ptrWrite); 
					
			       }

			  }
			   fputs("\n</DataArray>\n",ptrWrite);
		}
		if(i==5)
		{
			fputs(" <DataArray type=\"Float32\" Name=\"vorticity\" format=\"binary\">\n",ptrWrite);		
			
			 for(r=1;r <= NoOfRows; r++)
			 {
			      for(c=1;c <=NoOfCols ; c++)
			      {
				       fwrite(&vorticity[r][c], sizeof(double), 1, ptrWrite); 
				
			       }

			  }
			  fputs("\n</DataArray>\n",ptrWrite);
		}
	}
	
	fputs("</CellData>\n"
	"</Piece>\n"
	" </ImageData>\n"
	" </VTKFile>",ptrWrite);
	
	  fclose(ptrWrite);

}
