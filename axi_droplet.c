
#include "navier-stokes-planar.h"
//~ double bessel_prime_zero = 7.0155866698156; 


int main(int argc,char *argv[])
{
	
	rho_G =0.001;
	mu_G	= 0.0001;

	rho_L	=	1.0;
	mu_L	=	0.01;

	 //~ SourceV = -10.0;
	Sigma = 0.0001;

		Diffusion_term=0;
		Advection_term=0;
	Advection_Scheme = 5;
	Linear_Solver=1;
	axi=1;
	
	end = 10.0; 
	Progress=1;
	step=0.001;
	ST_Module=2;
	//~ FORCED_PROST=1;
	//~ Forced_delT = 5.e-6;
	//~ CFL=0.1;
			 NoOfRows  = 8;
			NoOfCols 	= 8;
			del = 5.0/NoOfCols;
				

			
/************User input above, no parameter should be below run_ns***********/	
	run_ns();
	
	return 0;	
	
}

double LegendreP(int l, double x)
{
	// now only for l=2

		return (3.0*pow(x,2)-1.0)*0.5;
	
	// l=4
		//~ return (3.0-30.0*pow(x,2)+35.0*pow(x,4))*0.125;
	
}

// provide initial conditions below
double Init_F(double x, double y)
{	
		y+=-2.5;
		x+=-2.5;
	
	double R0=0.1, epsilon=0,radius; 	
	
			//~ if (x==0 && y==0)
			//~ {
				//~ RADIUS = DIAMETER/2;
			//~ }
			//~ else
			//~ {
				
				 //~ RADIUS =DIAMETER/2*(1.0 + EPSILON*LegendreP(2,y/pow(pow(x,2)+pow(y,2),0.5)));
			//~ }
				//~ radius = R0*(1.0+epsilon*cos(4.0*atan2(x,y)));
			return -(pow(x,2) + pow(y,2) - pow(1.8,2)) ;

	
}

void event_phy(double Step)
{
	interface_data(Step);
	tecplot_data(Step);
	//~ double fileN = Step*1000;
	//~ vti_data(fileN);
	//~ FILE *ptrW;
	//~ ptrW = fopen("sim_axi_droplet.dat","a");
	
	//~ int r=1,c;
	//~ double x0,y0;
      
        //~ for ( c = 1; c <= NoOfCols+1; ++c) {
        
		//~ if (interface(r,c) && absolute(Yp1[r][c]-0)<1.0e-3)
		//~ {
			//~ fprintf(ptrW,"%e\t%e\n",Time,Xp1[r][c]-0.2);
		//~ }
		//~ if (interface(r,c) && absolute(Yp2[r][c]-0)<1.0e-3)
		//~ {
			//~ fprintf(ptrW,"%e\t%e\n",Time,Xp2[r][c]-0.2);
		//~ }
	
		
        //~ }
     
	//~ fclose(ptrW);	
	
	
	
}
void event_init()
{}
void event_bc()
{
	//left 
	BcDirichlet_U("left",0);
	BcNeumann_V("left");
	BcNeumann_F("left");
	BcNeumann_P("left");
	
	//right
	BcNeumann_U("right");
	BcNeumann_V("right");
	BcNeumann_F("right");
	BcDirichlet_P("right",0);
	
	//bottom
	BcDirichlet_V("bottom",0);
	BcNeumann_U("bottom");
	BcNeumann_F("bottom");
	BcNeumann_P("bottom");
	//top
	BcDirichlet_V("top",0);
	BcNeumann_U("top");
	BcNeumann_F("top");
	BcNeumann_P("top");
	
	}

double Init_U(double x, double y)
{
	return 0;
	
}

double Init_V(double x, double y)
{
	return 0;
	
}

double Init_P(double x, double y)
{
	return 0;
	
}

void event_cp()
{
		tecplot_data(StepT);
	//~ SourceU = 30.0*sin(200.0*Time);
	
	//~ interface_data((double)StepT);
	
	//~ int r,c;
	//~ FILE *ptrW2;
	
	//~ char filename[20];
	
	//~ sprintf(filename,"kappa-%d",StepT);
	
	//~ ptrW2 = fopen(filename,"w");
	
	
	
      //~ for ( r = 0; r <= NoOfRows+1; ++r){
		//~ for ( c = 0; c <= NoOfCols+1; ++c) {
        
			//~ fprintf(ptrW2,"%e\t%e\t%e\t%e\n",(c-0.5)*del,(r-0.5)*del,Cellattrib[r][c].VolFrac,kappa[r][c]);
	
		
		//~ }
	//~ }
     
	//~ fclose(ptrW2);	
	
}

	