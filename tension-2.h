
double CLROST(int r,int c)
{
	
int r_temp, c_temp;	
double HF_0, HF_1, HF_2, Fx, Fxx, HX_0, HX_1, HX_2,kappa_temp=0;

	
		

//   *******************
int tag=0;

	
				HF_0 = 0; HF_1 = 0; HF_2 = 0; 
				HX_0 = 0; HX_1 = 0; HX_2 = 0; 
				
				// Case 1, when the normal is mainly pointing upwards
				if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny>0 )
				//~ if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y>0)	
				{	/***************Left Column*******************/
					tag=1;
					// we are sure center cell has interface
					HF_1 = 0.5*(Yp1[r][c] + Yp2[r][c]);
					HX_1 = 0.5*(Xp1[r][c] + Xp2[r][c]);
					
						// Find interface in the left column
					r_temp=r;
					while (!interface(r_temp,c-1) )
					{	
						r_temp = r_temp+1; 
						
						if (r_temp-r>5)	// break if interface not found in 5 cells above
							break;
						else if(r_temp>NoOfRows) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
					}
					if (r_temp-r>5)	// down when up search fails
					{
						r_temp=r;
						while (!interface(r_temp,c-1) )
						{	
							r_temp = r_temp-1; 
							
							if (r-r_temp>5)	// break if interface not found in 5 cells below
								break;
							else if(r_temp<1) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
							
						}
					}
					
					HF_0 = 0.5*(Yp1[r_temp][c-1] + Yp2[r_temp][c-1]);
					HX_0 = 0.5*(Xp1[r_temp][c-1] + Xp2[r_temp][c-1]);
					
						
					// Find interface in the right column
					r_temp=r;
					while (!interface(r_temp,c+1) )
					{	
						r_temp = r_temp+1; 
						
						if (r_temp-r>5)	// break if interface not found in 5 cells above
							break;
						else if(r_temp>NoOfRows) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
					}
					if (r_temp-r>5)	// down when up search fails
					{
						r_temp=r;
						while (!interface(r_temp,c+1) )
						{	
							r_temp = r_temp-1; 
							
							if (r-r_temp>5)	// break if interface not found in 5 cells below
								break;
							else if(r_temp<1) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
							
						}
					}
					
					HF_2 = 0.5*(Yp1[r_temp][c+1] + Yp2[r_temp][c+1]);
					HX_2 = 0.5*(Xp1[r_temp][c+1] + Xp2[r_temp][c+1]);
					
					
				}
						
				
				
				//	Case 2, when the normal is mainly pointing downwards
				else if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny<0)
				//~ else	if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y<0)	
				{	
					tag=3;
					// we are sure center cell has interface
					HF_1 = 0.5*(Yp1[r][c] + Yp2[r][c]);
					HX_1 = 0.5*(Xp1[r][c] + Xp2[r][c]);
					
					// Find interface in the left column
					r_temp=r;
					while (!interface(r_temp,c-1) )
					{	
						r_temp = r_temp+1; 
						
						if (r_temp-r>5)	// break if interface not found in 5 cells above
							break;
						else if(r_temp>NoOfRows) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
					}
					if (r_temp-r>5)	// down when up search fails
					{
						r_temp=r;
						while (!interface(r_temp,c-1) )
						{	
							r_temp = r_temp-1; 
							
							if (r-r_temp>5)	// break if interface not found in 5 cells below
								break;
							else if(r_temp<1) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
							
						}
					}
					
					HF_0 = 0.5*(Yp1[r_temp][c-1] + Yp2[r_temp][c-1]);
					HX_0 = 0.5*(Xp1[r_temp][c-1] + Xp2[r_temp][c-1]);
					
						
					// Find interface in the right column
					r_temp=r;
					while (!interface(r_temp,c+1) )
					{	
						r_temp = r_temp+1; 
						
						if (r_temp-r>5)	// break if interface not found in 5 cells above
							break;
						else if(r_temp>NoOfRows) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
					}
					if (r_temp-r>5)	// down when up search fails
					{
						r_temp=r;
						while (!interface(r_temp,c+1) )
						{	
							r_temp = r_temp-1; 
							
							if (r-r_temp>5)	// break if interface not found in 5 cells below
								break;
						else if(r_temp<1) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
							
						}
					}
					
					HF_2 = 0.5*(Yp1[r_temp][c+1] + Yp2[r_temp][c+1]);
					HX_2 = 0.5*(Xp1[r_temp][c+1] + Xp2[r_temp][c+1]);
					
					
				}


				/*************Case 3 When Normal is mainly pointing right****************************/

				else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx>0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X>0)
				{	/***************Bottom Row*******************/
					tag=2;
					// we are sure center cell has interface
					HF_1 = 0.5*(Xp1[r][c] + Xp2[r][c]);
					HX_1 = 0.5*(Yp1[r][c] + Yp2[r][c]);
					
					
					// Find interface in the above row
					c_temp=c;
					while (!interface(r-1,c_temp) )
					{	
						c_temp = c_temp+1; 
						
						if (c_temp-c>5)	// break if interface not found in 5 cells right
							break;
						else if(c_temp>NoOfCols) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
					}
					if (c_temp-c>5)	// left when right search fails
					{
						c_temp=c;
						while (!interface(r-1,c_temp) )
						{	
							c_temp = c_temp-1; 
							
							if (c-c_temp>5)	// break if interface not found in 5 cells left
								break;
							else if(c_temp<1) // circle cannot be fitted with only two points return zero curvature
							{
								return 0;
							}
						
							
						}
					}
					
					
					HF_0 = 0.5*(Xp1[r-1][c_temp] + Xp2[r-1][c_temp]);
					HX_0 = 0.5*(Yp1[r-1][c_temp] + Yp2[r-1][c_temp]);
					
						
					// Find interface in the above row
					c_temp=c;
					while (!interface(r+1,c_temp) )
					{	
						c_temp = c_temp+1; 
						
						if (c_temp-c>5)	// break if interface not found in 5 cells right
							break;
						else if(c_temp>NoOfCols) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
						
					}
					if (c_temp-c>5)	// left when right search fails
					{
						c_temp=c;
						while (!interface(r+1,c_temp) )
						{	
							c_temp = c_temp-1; 
							
							if (c-c_temp>5)	// break if interface not found in 5 cells left
								break;
							else if(c_temp<1) // circle cannot be fitted with only two points return zero curvature
							{
								return 0;
							}
						
							
						}
					}
					
					HF_2 = 0.5*(Xp1[r+1][c_temp] + Xp2[r+1][c_temp]);
					HX_2 = 0.5*(Yp1[r+1][c_temp] + Yp2[r+1][c_temp]);
					
					
				
					
					
					

						
				}	
						
				//	Case 4, when the normal is mainly pointing left
				else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx<0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X<0)
				{	/***************Bottom Row*******************/
					
					tag=4;
					
					
					// we are sure center cell has interface
					HF_1 = 0.5*(Xp1[r][c] + Xp2[r][c]);
					HX_1 = 0.5*(Yp1[r][c] + Yp2[r][c]);
					
					
					// Find interface in the above row
					c_temp=c;
					while (!interface(r-1,c_temp) )
					{	
						c_temp = c_temp+1; 
						
						if (c_temp-c>5)	// break if interface not found in 5 cells right
							break;
						else if(c_temp>NoOfCols) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
						
					}
					if (c_temp-c>5)	// left when right search fails
					{
						c_temp=c;
						while (!interface(r-1,c_temp) )
						{	
							c_temp = c_temp-1; 
							
							if (c-c_temp>5)	// break if interface not found in 5 cells left
								break;
							else if(c_temp<1) // circle cannot be fitted with only two points return zero curvature
							{
								return 0;
							}
						
							
						}
					}
					
					
					HF_0 = 0.5*(Xp1[r-1][c_temp] + Xp2[r-1][c_temp]);
					HX_0 = 0.5*(Yp1[r-1][c_temp] + Yp2[r-1][c_temp]);
					
						
					// Find interface in the above row
					c_temp=c;
					while (!interface(r+1,c_temp) )
					{	
						c_temp = c_temp+1; 
						
						if (c_temp-c>5)	// break if interface not found in 5 cells right
							break;
						else if(c_temp>NoOfCols) // circle cannot be fitted with only two points return zero curvature
						{
							return 0;
						}
						
						
					}
					if (c_temp-c>5)	// left when right search fails
					{
						c_temp=c;
						while (!interface(r+1,c_temp) )
						{	
							c_temp = c_temp-1; 
							
							if (c-c_temp>5)	// break if interface not found in 5 cells left
								break;
							else if(c_temp<1) // circle cannot be fitted with only two points return zero curvature
							{
								return 0;
							}
						
							
						}
					}
					
					HF_2 = 0.5*(Xp1[r+1][c_temp] + Xp2[r+1][c_temp]);
					HX_2 = 0.5*(Yp1[r+1][c_temp] + Yp2[r+1][c_temp]);
					
					
				}
					
					/*********CROST********Solve Linear eq in 2 variables ****************/
				if (CROST==1)
				{
						double a1 = 2*(HX_2 - HX_1);
						double b1 = 2*(HF_2-HF_1);
						double c1 = -((sq(HX_2) - sq(HX_1)) + (sq(HF_2) - sq(HF_1)));
				
						double a2 = 2*(HX_2 - HX_0);
						double b2 = 2*(HF_2-HF_0);
						double c2 = -((sq(HX_2) - sq(HX_0)) + (sq(HF_2) - sq(HF_0)));
				
						double cx = (b1*c2 - b2*c1) / (a1*b2 - a2*b1);	
						double cy = (c1*a2 - c2*a1) / (a1*b2 - a2*b1);
				
						double cr = pow((pow(HX_1-cx,2) + pow(HF_1-cy,2)),0.5);
				
						kappa_temp = 1.0/cr;
				}
				/*********CROST********Solve Linear eq in 2 variables ****************/
				 if (LROST==1)
				{
						Fx = (HF_2 - HF_0)/(HX_2-HX_0);
				
						Fxx = ((HF_2 - HF_1)/(HX_2-HX_1) - (HF_1 - HF_0)/(HX_1-HX_0))/ (0.5*(HX_2-HX_0));
						
					
						kappa_temp = -Fxx/pow((1+pow(Fx,2)),1.5) ;
						if (tag==3 || tag==4)
						{
							kappa_temp = -kappa_temp;
						}
				}		
						
						if(absolute(kappa_temp)>1.0/del && axi==0)
						{
							kappa_temp = sign(kappa_temp)/del;
							//~ kappa[r][c] = 0;
						}
						else if (absolute(kappa_temp)>3.0/del && axi==1)
						{
							kappa_temp = sign(kappa_temp) * (3.0/del);
						}		

	return kappa_temp;			
									
	
}

