
void find_coord(double *intersects, double *guess_vector)
{	
	//~   \frac{-\sqrt{\text{ny}^2-4 a \text{nx} \left(k \text{nx}+\left(\text{nx}^2+\text{ny}^2\right) (x-\text{x0})\right)}+2 a \text{nx}^2 \text{y0}+\text{ny} (2 a \text{nx} (x-\text{x0})-1)}{2 a \text{nx}^2}
	double a = guess_vector[0] ;
	double k = guess_vector[1];
	double theta = guess_vector[2] ;
	
	
	//~ double x0=1.5*del;
	//~ double y0=1.5*del;
	
	//~ // if looking for y coordinate
	double nx=sin(theta);
	double ny=-cos(theta);
	
	//~ double a = 1.0;
	//~ double k = 0;
	//~ double theta = guess_vector[2];
	//~ del=1.0;
	double x0=1.5*del;
	double y0=1.5*del;
	
	// if looking for y coordinate
	//~ double nx=0.001;
	//~ double ny=-1.0;
	
	double x_coord,y_coord;
	
	
	int i;
	
	
	if (a==0 ) // straight line
	{
		if (ny==0) // parallel to y-axis
		{
			for (i=0; i<=3; i++)
			{
				
				intersects[2*i] =nodata;
				intersects[2*i+1] = nodata;
			}
			
			
			
			for (i=4; i<=7; i++)
			{
				
			
				intersects[2*i] =-k/nx+x0;
				intersects[2*i+1] =nodata;
			
			}
		}
			
		else if (nx==0) // parallel to x axis
		{
			for (i=0; i<=3; i++)
			{
				x_coord=del*i;
			
				
				
				
				
					intersects[2*i] =-k/ny + y0;
					
					intersects[2*i+1] = nodata;
				
				
				
			}
			
			
			
			for (i=4; i<=7; i++)
			{
			
				intersects[2*i] =nodata;
				intersects[2*i+1] =nodata;
			
		
			}
		}
		else // straight line not parallel to either axes
		{
			for (i=0; i<=3; i++)
			{
				x_coord=del*i;
	
					
				intersects[2*i] =-(k+nx*(x_coord-x0))/ny + y0;
				intersects[2*i+1] = nodata;
				
				
				
			}
			
			 ny=sin(theta);
			nx=-cos(theta);
			
			for (i=4; i<=7; i++)
			{
				y_coord=del*(i-4);
		
			
				intersects[2*i] =-(k+nx*(y_coord-y0))/ny + x0;
				
				intersects[2*i+1] =nodata;
			
			
			
			}
		}
	
		
	}
		
	else //parabola 
	{
		if (absolute(nx)<1.e-6)
		{
				for (i=0; i<=3; i++)
				{
					x_coord=del*i;
				
					
					
					
						intersects[2*i] =( - k-a*ny*ny*(x_coord-x0)*(x_coord-x0))/ny +y0 ;
						intersects[2*i+1] = nodata ;
					
				}
		}
		else{	
				for (i=0; i<=3; i++)
				{
					x_coord=del*i;
				
					double determinant = (ny*ny)-4.0*a*nx*( k*nx + (nx*nx+ny*ny)*(x_coord-x0));
					
					if (determinant<0)
					{
						intersects[2*i] = nodata; // no real root exists on the given line
						intersects[2*i+1] = nodata; // no real root exists on the given line
					}
					else
					{
						intersects[2*i] = (2.0*a*(nx*nx)*y0 + ny*(2.0*a*nx*(x_coord-x0)-1.0)  - pow(determinant,0.5))/(2.0*a*(nx*nx) ) ;
						intersects[2*i+1] = (2.0*a*(nx*nx)*y0 + ny*(2.0*a*nx*(x_coord-x0)-1.0)  + pow(determinant,0.5))/(2.0*a*(nx*nx)) ;
					}
					
					//~ printf("\n%lf\n", - determinant);
				
					//~ exit(0);
				}
			}
		
		
		
		 // if looking for x coordinate
		 ny=sin(theta);
		 nx=-cos(theta);
		
	
		if (absolute(nx)<1.0e-6)
		{
				for (i=4; i<=7; i++)
				{
					y_coord=del*(i-4);
				
					
					
					
						intersects[2*i] =( - k-a*ny*ny*(y_coord-y0)*(y_coord-y0))/ny +x0 ;
						intersects[2*i+1] = nodata ;
					
				}
		}	
		else {
			for (i=4; i<=7; i++)
			{
				y_coord=del*(i-4);
			
				double determinant = pow(ny,2)-4.0*a*nx*( k*nx + (nx*nx+ny*ny)*(y_coord-y0));
				
				if (determinant<0)
				{
					intersects[2*i] = nodata; // no real root exists on the given line
					intersects[2*i+1] = nodata; // no real root exists on the given line
				}
				else
				{
					intersects[2*i] = (2.0*a*(nx*nx)*y0 + ny*(2.0*a*nx*(y_coord-y0)-1.0)  - pow(determinant,0.5))/(2.0*a*(nx*nx)) ;
					intersects[2*i+1] = (2.0*a*(nx*nx)*y0 + ny*(2.0*a*nx*(y_coord-y0)-1.0)  + pow(determinant,0.5))/(2.0*a*(nx*nx)) ;
				}
				
				//~ printf("\n%lf\n",  determinant);
			
				//~ exit(0);
			}
		}
		//~ }
	}
		
	//~ for (i=0; i<16; i++)
	//~ {
		//~ printf("\n%d\t%e\t%e\t%e\t%e\n",i,intersects[i],a,k,nx);
	//~ }
	//~ exit(0);
	//~ exit(0);
	
}

// Calculate the volume fractions of parabola of 3 X 3 stencil
// Calcule les fractions de volume de parabole d'une 3 X 3 pochoir 
double  *calculate_volfrac_parabola_stencil_new( double *guess_vector, double *VolFrac_parabola, double *intersects)
{
	int r,c;
	//~ double sum_of_volfrac_para = 0;
	//~ double parabola_sign=1.0;
	//~ double *intersects; // maximum 16 intersections are possible , 8 in x and 8 in y grid lines, 2 per line, first 8 are for x and next 8 for y
	double area;
	
	//~ intersects  = ( double * ) calloc ( 16,sizeof ( double ) );
	
	 bool inside00, inside01, inside10, inside11;
	  bool tag[4]={0,0,0,0}; 
	int yoyo=0;
	// find all the intersections of parabola in the stencil
	 find_coord(intersects, guess_vector);
	
	 //~ printf("\nSb=%d",StepT);
	
	double cell_cut[4]; 
	
	// Calculate volume fractions.
	    for ( r = 0; r <= 2; ++r) {
		double y0 = ((double)(r) * del ) ;
		double y1 = ((double)(r+1 ) * del ) ;
		for ( c = 0; c <= 2; ++c) {
		    double x0 = ((double)(c) * del ) ;
		    double x1 = ((double)(c+1) * del ) ;
			
		    // check if all corners of the cells are inside or outside the parabola
		     inside00 = parabola(x0,y0, guess_vector) <= 1.e-7;
		     inside01 =   parabola(x1,y0,  guess_vector) <= 1.e-7;
		     inside10 =   parabola(x0,y1, guess_vector) <= 1.e-7;
		     inside11 =  parabola(x1,y1, guess_vector) <= 1.e-7;
			   
			if (inside00 && inside01 && inside10 && inside11) // inside
			{ VolFrac_parabola[index_p(r,c)] = 1.0;  //printf("\nVP1 = %lf", VolFrac_parabola[index_p(r,c)]); 
				continue;}
				

			    if (!inside00 && !inside01 && !inside10 && !inside11) // outside
			    {   VolFrac_parabola[index_p(r,c)] = 0; //printf("\nVP1 = %lf", VolFrac_parabola[index_p(r,c)]); 
				    continue; }
			
			     tag[0]=0;
			     tag[1]=0;
			     tag[2]=0;
			     tag[3]=0;
			
			if ( intersects[2*c] >= r*del && intersects[2*c] <= (r+1)*del  ) // left face first root
			{
				tag[0]=1;
				cell_cut[0]= intersects[2*c] ;
				
			}
			if (intersects[2*c+1] >= r*del && intersects[2*c+1] <= (r+1)*del) // left face second root
			{
				tag[0]=1;
				cell_cut[0]= intersects[2*c+1] ;
				
			}
			if (intersects[2*r+8] > c*del && intersects[2*r+8] <= (c+1)*del)  // bottom face first root
			{
				//~ if (!(tag[0] && absolute(intersects[2*r+8]- c*del)<1e-7))
				 	tag[1]=1;
					cell_cut[1]=intersects[2*r+8];
				
				//~ if (inside01) // right bottom corner is inside the parabola
				//~ area = (intersects[2*r+8] - x1)*y0;  // comes from Gerlach et. al. x_i y_{i+1} - x_{i+1} y_i
				
			}
			
			if (intersects[2*r+9] > c*del && intersects[2*r+9] <= (c+1)*del)  // bottom face 2nd root
			{	
				//if (!(tag[0] && absolute(intersects[2*r+9]- c*del)<1e-7))
				 	tag[1]=1;
					cell_cut[1]=intersects[2*r+9];
				
				
				
			}
			if (intersects[2*c+2] >= r*del && intersects[2*c+2] <= (r+1)*del) // right face Ist root
			{ 	if (!(tag[0] && tag[1]))
				{tag[2]=1;
					cell_cut[2]=intersects[2*c+2]; }
				
			}
			if (intersects[2*c+3] >= r*del && intersects[2*c+3] <= (r+1)*del) // right face 2nd root
			{	if (!(tag[0] && tag[1]))
				{
					tag[2]=1;
				cell_cut[2]=intersects[2*c+3]; }
				
			}
			if (intersects[2*r+10] >= c*del && intersects[2*r+10] <= (c+1)*del)  // top face first root
			{	if (!((tag[0] && tag[1]) || (tag[0] && tag[2]) || (tag[1] && tag[2])))
				{ tag[3]=1;
					cell_cut[3]=intersects[2*r+10]; }
				
				
			}
			if (intersects[2*r+11] >= c*del && intersects[2*r+11] <= (c+1)*del)  // top face 2nd root
			{	if (!((tag[0] && tag[1]) || (tag[0] && tag[2]) || (tag[1] && tag[2])))
				 {	tag[3]=1;
					cell_cut[3]=intersects[2*r+11];
				 }
				
				
			}
			
			// une fois que nous connaissons les points d'intersection il ya 4C2=6*2 combinaisons a parcourir
			if (tag[0] && tag[1]) // left and bottom cut 
			{
				area = absolute(0.5*(cell_cut[0]-y0)*(cell_cut[1]-x0)/del_sq);
				yoyo=1;
				
				VolFrac_parabola[index_p(r,c)] = inside00 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			else if (tag[1] && tag[2]) // bottom and right cut 
			{
				area = absolute(0.5*(cell_cut[1]-x1)*(cell_cut[2]-y0)/del_sq);
				yoyo=2;
				
				VolFrac_parabola[index_p(r,c)] = inside01 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			else if (tag[2] && tag[3]) // right and top cut 
			{
				area = absolute(0.5*(cell_cut[3]-x1)*(cell_cut[2]-y1)/del_sq);
				yoyo=3;
				
				VolFrac_parabola[index_p(r,c)] = inside11 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			else if (tag[3] && tag[0]) // left and top cut 
			{
				area = absolute(0.5*(cell_cut[3]-x0)*(cell_cut[0]-y1)/del_sq);
				yoyo=4;
				
				VolFrac_parabola[index_p(r,c)] = inside10 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			else if (tag[0] && tag[2]) // right and left cut 
			{
				area = absolute(0.5*((cell_cut[0]-y0)+(cell_cut[2]-y0))*del/del_sq);
				yoyo=5;
				
				VolFrac_parabola[index_p(r,c)] = inside00 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			else if (tag[1] && tag[3]) // top and bottom cut 
			{
				area = absolute(0.5*((cell_cut[1]-x0)+(cell_cut[3]-x0))*del/del_sq);
				yoyo=6;
				
				VolFrac_parabola[index_p(r,c)] = inside00 ? area : (1.0-area) ; // 2 cases handled triangle volfrac and complement of it
			}
			
			//printf("\nVP1s = %lf\t%d\t%d\t%d\t%d\t%d", VolFrac_parabola[index_p(r,c)],yoyo,tag[0],tag[1],tag[2],tag[3]);
			//~ printf("\nVP1 = %lf\t%d", area,yoyo);
			//~ if (yoyo==0)
			//~ { 
				//~ printf("bc");
				//~ }
		    
		}
	       
	    }
	      
	  //~ exit(0);
	   
	
	    return VolFrac_parabola;
	    
	    
}