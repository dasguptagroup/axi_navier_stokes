int find_orientation(int r, int c)
{	
	    double Nx, Ny;
	
			Nx =  Cellattrib[r][c].Nx;
			Ny =  Cellattrib[r][c].Ny;
	
	//~ if (contact)
	//~ {	if (r==1 || r== NoOfRows || c==1 || c== NoOfCols )
		//~ {
			//~ Nx =  mycs(r,c,1);
			//~ Ny =  mycs(r,c,0);
			
			//~ if  (Nx<0){
				//~ return 4;
			//~ }
			//~ else{
				//~ return 2;
			//~ }
		//~ }
	//~ }
	
	if (absolute(Ny)>=absolute(Nx) && Ny>0 )
	{	
		return 1;	// upwards
	}
	else if (absolute(Ny)>=absolute(Nx) && Ny<0)
	{
		return 3; // downwards
	}
	else if (absolute(Nx)>=absolute(Ny) && Nx>0)
	{	
		 return 2; // right
						
	}
	else if (absolute(Nx)>=absolute(Ny) && Nx<0)
	{	
		return 4; // left
	}
	
	//~ printf("\norient 0 found in height_function.h: debug this if this error comes up");
	return 0;
	
}
				
	

double calculate_kappa_DAC(int r,int c, int orient)
{
	int r_temp, c_temp;	

	double HF_0, HF_1, HF_2, Fx, Fxx;
	//double err_new, norm;
		int sign_normal=1;
	
	HF_0 = 0; HF_1 = 0; HF_2 = 0; 
	
		
		
		switch (orient)
		{	// Case 1, when the normal is mainly pointing upwards
				//~ if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny>0 )
				//~ if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y>0)	
				case 1:
				{	/***************Left Column*******************/
					r_temp = r; c_temp = c-1; ;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT  );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Column*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						r_temp = r_temp + 1; // go up 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						r_temp = r_temp - 1; // go down 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Right Column*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						r_temp = r_temp + 1; // go up 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r-1; c_temp = c+1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						r_temp = r_temp - 1; // go down 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}
				break;
				//	Case 2, when the normal is mainly pointing downwards
				//~ else if (absolute(Cellattrib[r][c].Ny)>=absolute(Cellattrib[r][c].Nx) && Cellattrib[r][c].Ny<0)
				//~ else	if (absolute(Grad_VolFrac_Y) >= absolute(Grad_VolFrac_X) && Grad_VolFrac_Y<0)	
				case 3:
				{	/***************Left Column*******************/
					r_temp = r+1; c_temp = c-1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						r_temp = r_temp + 1; // go up
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c-1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						r_temp = r_temp - 1; // go down
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 

					/***************Middle Column*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						r_temp = r_temp + 1; // go up
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Right Column*******************/
					r_temp = r+1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						r_temp = r_temp + 1; // go up
						//~ if (r_temp-r>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( r_temp<=NoOfRows+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						r_temp = r_temp - 1; // go down
						//~ if (r-r_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while (r_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT);	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}
				break;

				/*************Case 3 When Normal is mainly pointing right****************************/

				//~ else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx>0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X>0)
				case 2:
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c;
					do  
					{
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						c_temp = c_temp + 1; // go right
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					

					
					r_temp = r-1; c_temp = c-1;
					do  
					{
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);

					/***************Middle Row*******************/
					r_temp = r; c_temp = c;
					do  
					{
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						c_temp = c_temp + 1; // go right 
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					//	printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_1); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 
					//exit(0);
					r_temp = r; c_temp = c-1;
					do  
					{
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 

					
					/***************Top Row*******************/
					r_temp = r+1; c_temp = c;
					do  
					{
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						c_temp = c_temp + 1; // go right
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for empty cell or end of domain 

					r_temp = r+1; c_temp = c-1;
					do  
					{
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for completely filled cell or end of domain 


						
				}	
				break;		
				//	Case 4, when the normal is mainly pointing left
				//~ else if (absolute(Cellattrib[r][c].Nx)>=absolute(Cellattrib[r][c].Ny) && Cellattrib[r][c].Nx<0)
				//~ else	if (absolute(Grad_VolFrac_X) >= absolute(Grad_VolFrac_Y) && Grad_VolFrac_X<0)
				case 4:
				{	/***************Bottom Row*******************/
					r_temp = r-1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r-1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Middle Row*******************/
					r_temp = r; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_1 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_1;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_1 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_1;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 

					/***************Top Row*******************/
					r_temp = r+1; c_temp = c+1;
					do  
					{
						//HF_0 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_0;
						HF_2 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_2;
						c_temp = c_temp + 1; // go up
						//~ if (c_temp-c>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp<=NoOfCols+1 && Cellattrib[r_temp][c_temp].ext_volfrac<=UPPER_LIMIT );	//exit for empty cell or end of domain 
						
					
					r_temp = r+1; c_temp = c;
					do  
					{
						//HF_0 = 	(Cellattrib[r_temp][c_temp].ext_volfrac-1) + HF_0;
						HF_2 = 	Cellattrib[r_temp][c_temp].ext_volfrac + HF_2;
						c_temp = c_temp - 1; // go left
						//~ if (c-c_temp>5 && PROST==1)
						//~ {	
							//~ flag=1;
							//~ goto use_clrost;
						//~ }
						//printf("\n%d\t%d\t%lf",r_temp,c_temp,HF_0); 
					} while ( c_temp>=0 && Cellattrib[r_temp][c_temp].ext_volfrac>LOWER_LIMIT );	//exit for completely filled cell or end of domain 
					//exit(0);
					
					
				}
				break;
				
				default:
				
				printf("\norient 0 found in height_function.h: debug this if this error comes up");
				printf("\n%d\t%d",r,c);
				exit(0);
					
		}
						// modify heights if contact angle boundary condition is enforced
					if (contact && contact_cell(r,c)){
						if (orient==2 || orient == 4){
							HF_0 = HF_1 + 1.0/tan(contact_angle);
						}
						if (orient==1 || orient == 3){
							if (Cellattrib[r][c-1].VolFrac<LOWER_LIMIT){
								HF_0= HF_1 - 1.0*tan(contact_angle);
							}
							else{
								HF_2= HF_1 - 1.0*tan(contact_angle);
							}
						}
					}
					
						
					
						Fx = (HF_2 - HF_0)*del/(2*del);
						Fxx = (HF_2 - 2*HF_1 + HF_0)*del/pow(del,2);
						//~ if (axi==0)
						//~ {
							double kk1 = -Fxx/pow((1+pow(Fx,2)),1.5) ;
		
							if (abs(kk1)>1.0/del){
								kk1 = sign(kk1)/del;
								
								}
								
						if(axi==0){		
							return kk1;	 } // return curvature for planar simulation
							
						//~ }
						
				
										
						
						
						double rad_dist=0;
						//~ printf("\n %lf\t%lf\t%lf",(c-0.5)*del,(r-0.5)*del,kappa[r][c]);

						
						//~ int nr=Find_Quad(r,c);
				
						if (axi==1 && orient==1)
						{	
							rad_dist = RF[c];	// radius at the center of the cell
							//~ double kk = -Fxx/pow((1+pow(Fx,2)),1.5) - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							double kk2 = - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							if (abs(kk2)>del/2){
								kk2 = sign(kk2)*del/2 ; }
							
							return kk1+kk2; 

						}
						else if (axi==1 && orient==3)
						{
							rad_dist = RF[c]; // radius at the center of the cell
							//~ double kk= -Fxx/pow((1+pow(Fx,2)),1.5)  - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							double kk2= - Fx/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							if (abs(kk2)>del/2){
								kk2 = sign(kk2)*del/2 ; }
							
							return kk1+kk2; 
						}
						else if(axi==1 && orient==2)
						{
							rad_dist=eta[c]+HF_1*del; // radius at bottom of cell plus the height from the bottom of the cell
							
							//~ double kk = -Fxx/pow((1+pow(Fx,2)),1.5) +1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							double kk2 = +1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							if (abs(kk2)>del/2){
								kk2 = sign(kk2)*del/2 ; }
							
							return kk1+kk2; 
						}
						else if(axi==1 && orient==4)
						{
							rad_dist=eta[c]+HF_1*del;  // radius at bottom of cell plus the height from the bottom of the cell
							
							//~ double kk = -Fxx/pow((1+pow(Fx,2)),1.5) -1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							double kk2 = -1.0/(rad_dist*pow((1+pow(Fx,2)),0.5));
							
							if (abs(kk2)>del/2){
								kk2 = sign(kk2)*del/2 ; }
							
							return kk1+kk2; 
						}
						else if(axi==1 && orient==0)  // no tag found for axi, show error
						{
							printf("no tag found in height_function.h: debug this if this error comes up");
							//~ printf("\n %lf\t%lf\t%lf\t%lf\t%lf\t%d\t%lf",(c-0.5)*del,(r-0.5)*del,pl_kappa,axi_kappa,kappa[r][c],tag,Cellattrib[r][c].ext_volfrac);
							exit(0);
						}
						
}						