/**********Obsolete functions****************************
I wish to remove these functions gradually, these are not actually required*/

/**********************************************************************************************************************************************
METHOD NAME: Identify_Shape
DESCRIPTION: Takes [r,c] as a cell identifier and identifies the shape of the area based on the Slope and the VolFraction
LIMITATIONS: Check all greater than equal to signs
***********************************************************************************************************************************************/
//~ void Identify_Shape(int r, int c)
//~ {
  //~ /*LOCAL VARIABLES*/
    //~ double Theta;
    //~ double Slope;
    //~ double VolFrac;
    //~ double Area;
  //~ char   Shape[30];
  //~ /*****************/
       //~ VolFrac =  Cellattrib[r][c].VolFrac;
	   //~ Area    =  VolFrac*del_sq;
       //~ Theta   =  Cellattrib[r][c].Theta;

        //~ if(Theta == 0 || Theta == PiBy2 || Theta == PI || Theta == ThreePiBy2)
         //~ {
           //~ strcpy(Shape,"Rectangle");
           //~ Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,Shape,-100000,-100000,-100000);
           //~ return;
         //~ }

        //~ Slope   =  tan(Theta) ;   //DONT MOVE THIS UP OTHERWISE SLOPE WILL GO TO INFINITY FOR THETA = PI / 2.0
       //~ //SHADED AREA IS A TRIANGLE
       //~ if( (Area <= (del_sqBy2*Slope) ) && (Area <= (del_sqBy2/Slope) )  )    //both for Theta <> PI/4.0
        //~ {
           //~ strcpy(Shape,"Triangle");
        //~ }
      //~ //SHADED AREA IS A TRAPEZIUM WITH THETA < PI / 4
      //~ else if ( (Area > del_sqBy2*Slope )  && Area < (del_sq - (del_sqBy2*Slope) )  )  // for Theta < PI / 4.0
        //~ {
          //~ strcpy(Shape,"Trapezium");
        //~ }
      //~ //SHADED AREA IS A TRAPEZIUM WITH THETA > PI / 4
      //~ else if ( (Area > del_sqBy2/Slope )  && Area < (del_sq - (del_sqBy2/Slope) )  )  // for Theta > PI / 4.0
        //~ {
          //~ strcpy(Shape,"Trapezium");
        //~ }
      //~ //SHADED AREA IS A 5 SIDED FIGURE WHICH IS THE COMPLEMENT OF A TRIANGLE
      //~ else
        //~ {
          //~ strcpy(Shape,"Triangle_Complement");
        //~ }

       //~ //WRITE SHAPE TO THE STRUCT
       //~ Write_struct_Cellattrib(r,c,-100000,-100000,-100000,-100000,-100000,Shape,-100000,-100000,-100000);
//~ }




/**************************************************************************************************
METHOD NAME: Write_struct_Cellattrib
DESCRIPTION: ALL WRITE OPERATIONS TO THE STRUCTURE STRUCT_CELL_ATTRIB HAPPEN THROUGH THIS FUNCTION.
             SEND -100000 IF NOTHING IS TO BE WRITTEN FOR A GIVEN PARAMETER
LIMITATIONS: NONE
***************************************************************************************************/
//~ void Write_struct_Cellattrib(int r,int c,  double VolFraction,   double Area,   double Nx,   double Ny,   double P, char Shape[30],   double Theta, int Type, double al)
  //~ {
    //~ if (VolFraction != -100000)
      //~ {
       //~ Cellattrib[r][c].VolFrac = VolFraction;
      //~ }
    //~ if (Area != -100000)
      //~ {
       //~ Cellattrib[r][c].Area = Area;
      //~ }
    //~ if (Nx != -100000)
      //~ {
        //~ Cellattrib[r][c].Nx = Nx;
      //~ }
    //~ if (Ny != -100000)
      //~ {
        //~ Cellattrib[r][c].Ny = Ny;
      //~ }
    //~ if (P != -100000)
      //~ {
        //~ Cellattrib[r][c].P = P;
      //~ }
    //~ if (!strcmp(Shape,"-100000"))   //strcmp RETURNS 0 IF BOTH THE STRINGS MATCH
      //~ {
        //~ //DO NOTHING
      //~ }
    //~ else
      //~ {
        //~ strcpy(Cellattrib[r][c].Shape,Shape);
      //~ }
    //~ if(Theta != -100000)
      //~ {
        //~ Cellattrib[r][c].Theta = Theta;
      //~ }
	//~ if(Type != -100000)
	//~ {
		//~ Cellattrib[r][c].Type = Type;
	//~ }
	//~ if(al != -100000)
	//~ {
		//~ Cellattrib[r][c].al = al;
	//~ }
  //~ }
