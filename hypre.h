
void Poissons_Solver_hypre()
{
	printf("\t HYPRE Running"); fflush(stdout);
	
	int r,c,Step;
	
	double **Source_Poisson,**Poissons_residue;
		
	
		Source_Poisson = (double **) calloc(NoOfRows+2,sizeof(double *));
		Poissons_residue = (double **) calloc(NoOfRows+2,sizeof(double *));
		int i;
			for (i=0; i<NoOfRows+2; i++)
			{
				Source_Poisson[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				Poissons_residue[i] = (double *) calloc (NoOfCols+2, sizeof (double));
				
			}
	
//~ for (r=0; r<=NoOfRows+1; r++)
		//~ {
			//~ for (c=0; c<=NoOfCols+1; c++)
			//~ {

                //~ Source_Poisson[r][c] = 0;
                //~ Poissons_residue[r][c]=0;

            //~ }
        //~ }

    // Francais: calculer le terme source de l'equation de Poisson
    // English: Calculate the source term for Poisson's equation
	
	for (r=1; r<=NoOfRows; r++)
		{
			for (c=1; c<=NoOfCols; c++)
			{

			Source_Poisson[r][c] = 0.5*((X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c] + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
				//~ sum_poisson = sum_poisson + Source_Poisson[r][c];
				//~ sum1 =sum1+ (X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c];
				//~ sum2 = sum2+(Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c])/delTdel;
				//~ sum3 = sum3+ Y_Velocity_star[r+1][c];
				//~ sum4 = sum4+ Y_Velocity_star[r][c];
				//~ printf("\n%d\t%d\t%e", r,c,X_Velocity_star[r][c]);
				
            }
        } 		
	/*variable definitions*/
	HYPRE_StructGrid grid;
	HYPRE_StructStencil stencil;
	HYPRE_StructMatrix A;
	HYPRE_StructVector b;
	HYPRE_StructVector x;
	HYPRE_StructSolver solver;
	HYPRE_StructSolver precond;
	
	
	int j;
	int n=NoOfCols;
	int m=NoOfRows;
	
	
	/*GRID*/
	{
		/*Create empty grid*/
		HYPRE_StructGridCreate(MPI_COMM_WORLD,2,&grid);
		
		{
			/*Set lower corner and upper corner of grid*/
			int ilower[2]={1,1};
			int iupper[2]={n,m};
			HYPRE_StructGridSetExtents(grid,ilower,iupper);
		}
		
		/*Assemble grid*/
		HYPRE_StructGridAssemble(grid);
	}

	/*STENCIL*/
	{
		/*Create empty stencil*/
		HYPRE_StructStencilCreate(2,5,&stencil);
		
		{
			/*Set stencil elements. Here we choose a 5-pt stencil*/
			int entry;
			int offset[5][2]={{0,0},{-1,0},{1,0},{0,-1},{0,1}};
			for(entry=0;entry<5;entry++)
			{
				HYPRE_StructStencilSetElement(stencil,entry,offset[entry]);
			}
		}
	
	}
	
	/*MATRIX*/
	{	
		/*Create empty matrix*/
		HYPRE_StructMatrixCreate(MPI_COMM_WORLD,grid,stencil,&A);
	

			/*Initialize matrix*/
			HYPRE_StructMatrixInitialize(A);
			
			int ilower[2]={1,1};
			int iupper[2]={n,m};
			double *values;
			values=calloc(n*m*5,sizeof(double));
			
			int nentry=5;	/*Number of stencil points*/
			int nvalues=n*m*5;	/*Number of total points (stencil pts*grid points*)*/
			
			int stencil_indices[5]={0,1,2,3,4}; /*Stencil indices in an array*/
			
			/*Add values to matrix*/
			r=1;
			c=0;
			for(i=0;i<nvalues;i+=nentry)
			{
				c++;
				values[i]=-((1.0*eta[c+1]/((rho[r][c]+rho[r][c+1])*RF[c] )+ 1.0*eta[c]/((rho[r][c]+rho[r][c-1])*RF[c])+ 1.0/(rho[r+1][c]+rho[r][c]) +1.0/(rho[r][c]+rho[r-1][c])))/ del_sq;
				values[i+1]=1.0*eta[c]/((rho[r][c]+rho[r][c-1])*RF[c])/ del_sq;
				values[i+2]=1.0*eta[c+1]/((rho[r][c]+rho[r][c+1])*RF[c] )/ del_sq;
				values[i+3]=1.0/(rho[r][c]+rho[r-1][c])/ del_sq;
				values[i+4]=1.0/(rho[r+1][c]+rho[r][c])/ del_sq;
				
				if(c==NoOfCols)
				{
					c=0;
					r++;
				}
			}
			
			
			HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,nentry,stencil_indices,values);
			free(values);
			
			/*boundary conditions*/
			double *bvalues1;
			double *bvalues2;
			double *bvalues3;
			double *bvalues4;
			
			bvalues1=calloc(n*2,sizeof(double));
			bvalues2=calloc(n*2,sizeof(double));
			bvalues3=calloc(n*2,sizeof(double));
			bvalues4=calloc(n*2,sizeof(double));
			
			int c1=1;
			int c2=1;
			int r1=1;
			int r2=1;
			
			/*set stencils to emulate neumann boundaries*/
			for(i=0;i<n*2;i+=2)
			{
				/*bottom*/
				bvalues1[i]=-((1.0*eta[c1+1]/((rho[1][c1]+rho[1][c1+1])*RF[c1] )+ 1.0*eta[c1]/((rho[1][c1]+rho[1][c1-1])*RF[c1])+ 1.0/(rho[1+1][c1]+rho[1][c1])))/ del_sq;
				bvalues1[i+1]=0;
				
				c1++;

				/*top*/
				bvalues2[i]=-((1.0*eta[c2+1]/((rho[NoOfRows][c2]+rho[NoOfRows][c2+1])*RF[c2] )+ 1.0*eta[c2]/((rho[NoOfRows][c2]+rho[NoOfRows][c2-1])*RF[c2])+ 1.0/(rho[NoOfRows][c2]+rho[NoOfRows-1][c2])))/ del_sq;
				bvalues2[i+1]=0;
				
				c2++;
			}
			for(i=0;i<m*2;i+=2)
			{
				/*left*/
				bvalues3[i]=-((1.0*eta[1+1]/((rho[r1][1]+rho[r1][1+1])*RF[1] )+ 1.0/(rho[r1+1][1]+rho[r1][1]) +1.0/(rho[r1][1]+rho[r1-1][1])))/ del_sq;
				bvalues3[i+1]=0;
				
				r1++;
				
				/*right*/
				bvalues4[i]=-((1.0*eta[NoOfCols]/((rho[r2][NoOfCols]+rho[r2][NoOfCols-1])*RF[NoOfCols])+ 1.0/(rho[r2+1][NoOfCols]+rho[r2][NoOfCols]) +1.0/(rho[r2][NoOfCols]+rho[r2-1][NoOfCols])))/ del_sq;
				bvalues4[i+1]=0;
			
				r2++;
		
			}

		
				
				/*bottom*/
				{
					int ilower[2]={1,1};
					int iupper[2]={n,1};
					int stencil_indices[2]={0,3};
					HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,2,stencil_indices,bvalues1);
					
				}
				/*top*/
				{
					int ilower[2]={1,m};
					int iupper[2]={n,m};
					int stencil_indices[2]={0,4};
					HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,2,stencil_indices,bvalues2);
					
				}
				/*left*/
				{
					int ilower[2]={1,1};
					int iupper[2]={1,m};
					int stencil_indices[2]={0,1};
					HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,2,stencil_indices,bvalues3);
					
				}
				/*right*/
				{
					int ilower[2]={n,1};
					int iupper[2]={n,m};
					int stencil_indices[2]={0,2};
					HYPRE_StructMatrixSetBoxValues(A,ilower,iupper,2,stencil_indices,bvalues4);					
				}
				
				/*set corner point stencils to emulate neumann boundaries*/

				{
					
					double cvalues1[5];
					double cvalues2[5];
					double cvalues3[5];
					double cvalues4[5];
					
					/*bottom left*/
					{
						cvalues1[0]=-((1.0*eta[1+1]/((rho[1][1]+rho[1][1+1])*RF[1] )+ 1.0/(rho[1+1][1]+rho[1][1])))/ del_sq;
						cvalues1[1]=1.0*eta[1+1]/((rho[1][1]+rho[1][1+1])*RF[1] )/ del_sq;
						cvalues1[2]=1.0/(rho[1+1][1]+rho[1][1])/ del_sq;
						cvalues1[3]=0;
						cvalues1[4]=0;
						int corner[2]={1,1};
						int stencil_indices[5]={0,2,4,1,3};
						
						HYPRE_StructMatrixSetBoxValues(A,corner,corner,nentry,stencil_indices,cvalues1);
					}
					/*bottom right*/
					{
						cvalues2[0]=-((1.0*eta[NoOfCols]/((rho[1][NoOfCols]+rho[1][NoOfCols-1])*RF[NoOfCols])+ 1.0/(rho[1+1][NoOfCols]+rho[1][NoOfCols])))/ del_sq;
						cvalues2[1]=1.0*eta[NoOfCols]/((rho[1][NoOfCols]+rho[1][NoOfCols-1])*RF[NoOfCols])/ del_sq;
						cvalues2[2]=1.0/(rho[1+1][NoOfCols]+rho[1][NoOfCols])/ del_sq;
						cvalues2[3]=0;
						cvalues2[4]=0;
						int corner[2]={n,1};
						int stencil_indices[5]={0,1,4,2,3};
						HYPRE_StructMatrixSetBoxValues(A,corner,corner,nentry,stencil_indices,cvalues2);
					}
					/*top left*/
					{
						cvalues3[0]=-((1.0*eta[1+1]/((rho[NoOfRows][1]+rho[NoOfRows][1+1])*RF[1] )+1.0/(rho[NoOfRows][1]+rho[NoOfRows-1][1])))/ del_sq;
						cvalues3[1]=1.0*eta[1+1]/((rho[NoOfRows][1]+rho[NoOfRows][1+1])*RF[1] )/ del_sq;
						cvalues3[2]=1.0/(rho[NoOfRows][1]+rho[NoOfRows-1][1])/ del_sq;
						cvalues3[3]=0;
						cvalues3[4]=0;
						int corner[2]={1,m};
						int stencil_indices[5]={0,2,3,1,4};
						HYPRE_StructMatrixSetBoxValues(A,corner,corner,nentry,stencil_indices,cvalues3);
					}
					/*top right*/
					{
						cvalues4[0]=-((1.0*eta[NoOfCols]/((rho[NoOfRows][NoOfCols]+rho[NoOfRows][NoOfCols-1])*RF[NoOfCols])+ 1.0/(rho[NoOfRows][NoOfCols]+rho[NoOfRows-1][NoOfCols])))/ del_sq;
						cvalues4[1]=1.0*eta[NoOfCols]/((rho[NoOfRows][NoOfCols]+rho[NoOfRows][NoOfCols-1])*RF[NoOfCols])/ del_sq;
						cvalues4[2]=1.0/(rho[NoOfRows][NoOfCols]+rho[NoOfRows-1][NoOfCols])/ del_sq;
						cvalues4[3]=0;
						cvalues4[4]=0;
						int corner[2]={n,m};
						int stencil_indices[5]={0,1,3,2,4};
						HYPRE_StructMatrixSetBoxValues(A,corner,corner,nentry,stencil_indices,cvalues4);
					}
				free(bvalues1);
				free(bvalues2);
				free(bvalues3);
				free(bvalues4);
				}
			

		
		/*Assemble matrix*/
		HYPRE_StructMatrixAssemble(A);
	}
	
	/*VECTORS*/
	{
		/*Create empty vectors*/
		HYPRE_StructVectorCreate(MPI_COMM_WORLD,grid,&b);
		HYPRE_StructVectorCreate(MPI_COMM_WORLD,grid,&x);
		
		{
			/*Initialize vectors*/
			HYPRE_StructVectorInitialize(b);
			HYPRE_StructVectorInitialize(x);
			
			/*Add values to vectors*/
			int ilower[2]={1,1};
			int iupper[2]={n,m};
			
			/*for b vector*/
			double *values;
			values=calloc(n*m,sizeof(double));
			r=1;
			c=0;
			
			
			for(i=0;i<n*m;i++)
			{
				c++;
				values[i]=0.5*((X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c] + (Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c]))/delTdel;
				HYPRE_StructVectorSetBoxValues(b,ilower,iupper,values);
				if(c==NoOfCols)
				{
					c=0;
					r++;
				}
			}
			
			
			
		
			/*for x vector*/
			r=1;
			c=0;
			for(i=0;i<n*m;i++)
			{
				c++;
				values[i]=Press[r][c];
				HYPRE_StructVectorSetBoxValues(x,ilower,iupper,values);
				if(c==NoOfCols)
				{
					c=0;
					r++;
				}
			}
			

		free(values);	
		}
		HYPRE_StructVectorAssemble(b);
		HYPRE_StructVectorAssemble(x);
	}
	
	
	/*SOLVER*/
	{
		/*Solver 1-PCG*/
		/*Create solver*/
		HYPRE_StructPFMGCreate(MPI_COMM_WORLD,&solver);
		
		/*Set parameters for solver*/
		//~ HYPRE_StructPFMGSetTol(solver,1.0e-10);
		HYPRE_StructPFMGSetPrintLevel(solver,1);
		HYPRE_StructPFMGSetMaxIter(solver, 1000);
		
		/*Setup the solver and solve for x*/
		HYPRE_StructPFMGSetup(solver,A,b,x);
		HYPRE_StructPFMGSolve(solver,A,b,x);
		

	}
	
	/*GET RESULTANT PRESSURE MATRIX*/
	{
		double *values;
		values = (double*) calloc(n*m,sizeof(double));
		int ilower[2]={1,1};
		int iupper[2]={n,m};
		HYPRE_StructVectorGetBoxValues(x, ilower, iupper, values);
		r=1;
		c=0;
		for(i=0;i<n*m;i++)
		{
			c++;
			Press[r][c]=values[i];
			if(c==NoOfCols)
			{
				c=0;
				r++;
			}
		}
		free(values);	
	}

		//~ /*PRINT RESULT TO FILE*/
	//~ {
		//~ FILE *file=fopen("HYPREMesh","w");
		//~ int Dim = 2;
		//~ int nentry=5;
		//~ int i, j,k;
		//~ int stencil_indices[5]={0,1,2,3,4};
		
			
		//~ double *values;
		//~ values = (double*) calloc(n*m,sizeof(double));
		//~ int ilower[2]={1,1};
		//~ int iupper[2]={n,m};
		//~ HYPRE_StructVectorGetBoxValues(x, ilower, iupper, values);
			//~ for (j = 0; j < m; j++)
			//~ {
				//~ for (i = 0; i < n; i++)
				//~ {
					//~ fprintf(file, "%.5e  ", values[i + j*n]);	
				//~ }
				//~ fprintf(file,"\n");
			//~ }
			//~ fprintf(file,"\n");
			
		//~ double *mvalues;
		//~ mvalues = (double*) calloc(n*m*5,sizeof(double));
			
		//~ HYPRE_StructMatrixGetBoxValues(A,ilower,iupper,nentry,stencil_indices, mvalues);
			//~ for (j = 0; j < n*m*5; j++)
			//~ {
				
				//~ fprintf(file, "%.5e\n", mvalues[j]);	
				
			//~ }
			

		//~ fflush(file);
		//~ fclose(file);
		
		//~ free(values);
		//~ free(mvalues);
	//~ }
	
	// Set Boundary Conditions
	for (c=1;c<=NoOfCols;c++)
	{
		Press[0][c] = Press[1][c];                      //bottom wall
		Press[NoOfRows+1][c] =  Press[NoOfRows][c];      // top wall
	}
	for (r=1;r<=NoOfRows;r++)
	{
		
		
		Press[r][0] = Press[r][1];                      //left wall	//neumann
		Press[r][NoOfCols+1] =  Press[r][NoOfCols];      //right wall 	//neumann
	}

	// Calculate residue
		double Res_inf_norm = 0;			//reset residue norm to zero only here, dont move up or down the loop
		
		for (r=1; r<=NoOfRows; r++)
		{
			for (c=1; c<=NoOfCols; c++)
			{
				double temp1 = (Press[r][c+1]-Press[r][c])*eta[c+1]/((rho[r][c]+rho[r][c+1])*RF[c]);
				double temp2 = (Press[r][c]-Press[r][c-1])*eta[c]/((rho[r][c]+rho[r][c-1])*RF[c]);
				double temp3 = (Press[r+1][c]-Press[r][c])/(rho[r][c]+rho[r+1][c]);
				double temp4 = (Press[r][c]-Press[r-1][c])/(rho[r][c]+rho[r-1][c]);
				Poissons_residue[r][c] = Source_Poisson[r][c] - (temp1 - temp2 + temp3 - temp4)/del_sq;
				
				
				
						//~ Poissons_residue[r][c] = ((X_Velocity[r][c+1]*eta[c+1]   -	X_Velocity[r][c]*eta[c])/RF[c] + (Y_Velocity[r+1][c]	-	Y_Velocity[r][c]))/del;
							
							//~ sum1 =sum1+ (X_Velocity_star[r][c+1]*eta[c+1]   -	X_Velocity_star[r][c]*eta[c])/RF[c];
							//~ sum2 = sum2+(Y_Velocity_star[r+1][c]	-	Y_Velocity_star[r][c])/delTdel;
							//~ sum3 = sum3+ Y_Velocity_star[r+1][c];
							//~ sum4 = sum4+ Y_Velocity_star[r][c];
							//~ printf("\n%d\t%d\t%e", r,c,X_Velocity_star[r][c]);
							
				    
					
				
				
				if (fabs(Poissons_residue[r][c])>Res_inf_norm)
				{
						Res_inf_norm=fabs(Poissons_residue[r][c]);
				}
			}
		}
		
		Res_inf_norm = 2*delT*Res_inf_norm;
	printf(" Div: %e",Res_inf_norm); 
		
	X_Velocity_Correction();
	Y_Velocity_Correction();	
	
	/* Free memory */
	HYPRE_StructGridDestroy(grid);
	HYPRE_StructStencilDestroy(stencil);
	HYPRE_StructMatrixDestroy(A);
	HYPRE_StructVectorDestroy(b);
	HYPRE_StructVectorDestroy(x);
	HYPRE_StructPFMGDestroy(solver);
	
		freeArray(Source_Poisson);
		freeArray(Poissons_residue);

	/* Finalize MPI */
	MPI_Finalize();
}


