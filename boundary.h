
void BC_implicit()
{
	int r,c;
	
	
	
	// Boundary condition for x-velocity-star
	
	//left
			for (r=1; r<=NoOfRows; r++)
			{	if (BC_Dirichlet_U_left==1)
				{
					X_Velocity_star[r][1] = u_vel_left  ;
					//~ X_Velocity_imp[r][1] = X_Velocity[r][1]   ;
				}
				if (BC_Neumann_U_left==1)
				{
					X_Velocity_star[r][1] = X_Velocity_star[r][2]  ;
				}
				
			}

	//right
			for (r=1; r<=NoOfRows; r++)
			{	if (BC_Dirichlet_U_right==1)
				{
					X_Velocity_star[r][NoOfCols+1] = u_vel_right ; 
					//~ X_Velocity_star[r][NoOfCols+1] = X_Velocity[r][NoOfCols+1] ; 
				}
				if (BC_Neumann_U_right==1)
				{
					X_Velocity_star[r][NoOfCols+1] = X_Velocity_star[r][NoOfCols]  ;
				}
			}
	
			
	//top
			for (c=1;c<=NoOfCols; c++)
			{	if (BC_Dirichlet_U_top==1)
				{
					X_Velocity_star[NoOfRows+1][c] = 2.0*u_vel_top - X_Velocity_star[NoOfRows][c]  ; 
					//~ X_Velocity_star[NoOfRows+1][c] = 2.0*pow(sin(M_PI*(c-1)*del),2) - X_Velocity_star[NoOfRows][c]  ; 
					//~ X_Velocity_star[NoOfRows+1][c] = X_Velocity[NoOfRows+1][c]  ; 
				}
				if (BC_Neumann_U_top==1)
				{
					X_Velocity_star[NoOfRows+1][c] = X_Velocity_star[NoOfRows][c]  ;
				}
			}
	
	//bottom
			for (c=1;c<=NoOfCols; c++)
			{	if (BC_Dirichlet_U_bottom==1)
				{
					X_Velocity_star[0][c] =2.0*u_vel_bottom - X_Velocity_star[1][c] ;
					//~ X_Velocity_star[0][c] =X_Velocity[0][c] ;
				}
				if (BC_Neumann_U_top==1)
				{
					X_Velocity_star[0][c] = X_Velocity_star[1][c]  ;
				}
			}	

			
	// Boundary condiiton for y-velocity-star
	//left			
	
			for (r=1; r<=NoOfRows; r++)
			{
				if (BC_Dirichlet_V_left==1)
				{
					Y_Velocity_star[r][0] = 2.0*v_vel_left- Y_Velocity_star[r][1] ;
					//~ Y_Velocity_star[r][0] = Y_Velocity[r][0] ;
				}
				if (BC_Neumann_V_left==1)
				{
					Y_Velocity_star[r][0] =Y_Velocity_star[r][1] ;
				}
			}
		
			
	//right	
		
			for (r=1; r<=NoOfRows; r++)
			{
				if (BC_Dirichlet_V_right==1)
				{
					Y_Velocity_star[r][NoOfCols+1] = 2.0* v_vel_right - Y_Velocity_star[r][NoOfCols] ;
					//~ Y_Velocity_star[r][NoOfCols+1] = Y_Velocity[r][NoOfCols+1] ;
				}
				if (BC_Neumann_V_right==1)
				{
					Y_Velocity_star[r][NoOfCols+1] = Y_Velocity_star[r][NoOfCols] ;
				}
					
			}
		
			
	//top
		
			for (c=1;c<=NoOfCols; c++)
			{
				if (BC_Dirichlet_V_top==1)
				{
					Y_Velocity_star[NoOfRows+1][c] = v_vel_top;
					//~ Y_Velocity_star[NoOfRows+1][c] = Y_Velocity[NoOfRows+1][c];
				}
				if (BC_Neumann_V_top==1)
				{
					Y_Velocity_star[NoOfRows+1][c] = Y_Velocity_star[NoOfRows][c];
				}
			}
		
			
	//bottom	
			for (c=1;c<=NoOfCols; c++)
			{
				if (BC_Dirichlet_V_bottom==1)
				{
					Y_Velocity_star[1][c] = v_vel_bottom;
					//~ Y_Velocity_star[1][c] = Y_Velocity[1][c] ;
				}
				if (BC_Neumann_V_bottom==1)
				{
					Y_Velocity_star[1][c] = Y_Velocity_star[2][c] ;
				}
			}			
					
			
	

}

void BcDirichlet_U(char wall[10],double value)
{
	int r,c;
	
	if(strcmp(wall,"left")==0)
	{
		u_vel_left=value;
		BC_Dirichlet_U_left=1;
			for (r=1; r<=NoOfRows; r++)
			{
				X_Velocity[r][1] = value;
			}
	}
			
	else if(strcmp(wall,"right")==0)
	{
		u_vel_right=value;
		BC_Dirichlet_U_right=1;
			for (r=1; r<=NoOfRows; r++)
			{
				X_Velocity[r][NoOfCols+1] = value;
			}
	}
			
	else if(strcmp(wall,"top")==0)
	{
		u_vel_top=value;
		BC_Dirichlet_U_top=1;
			for (c=1;c<=NoOfCols; c++)
			{
				X_Velocity[NoOfRows+1][c] = 2*value - X_Velocity[NoOfRows][c];
			}
	}
			
	else if(strcmp(wall,"bottom")==0)
	{
		u_vel_bottom=value;
		BC_Dirichlet_U_bottom=1;
			for (c=1;c<=NoOfCols; c++)
			{
				X_Velocity[0][c] = 2*value - X_Velocity[1][c];
			}	
	}			
			
	
}

void BcNeumann_U(char wall[1])
{
	int r,c;
	
	
		
		if(strcmp(wall,"left")==0)
		{
			
			BC_Neumann_U_left=1;
			for (r=1; r<=NoOfRows; r++)
			{
				X_Velocity[r][1] = X_Velocity[r][2] ;
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{
			BC_Neumann_U_right=1;
			for (r=1; r<=NoOfRows; r++)
			{
				X_Velocity[r][NoOfCols+1] = X_Velocity[r][NoOfCols] ;
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{
			BC_Neumann_U_top=1;
			for (c=1;c<=NoOfCols; c++)
			{
				X_Velocity[NoOfRows+1][c] =  X_Velocity[NoOfRows][c];
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{
			BC_Neumann_U_bottom=1;
			for (c=1;c<=NoOfCols; c++)
			{
				X_Velocity[0][c] =  X_Velocity[1][c];
			}
		}			
			
	
}

void BcDirichlet_V(char wall[1],double value)
{
	int r,c;
	
	
		
		if(strcmp(wall,"left")==0)
		{
			v_vel_left=value;
			BC_Dirichlet_V_left=1;
			for (r=1; r<=NoOfRows; r++)
			{
			
				Y_Velocity[r][0] = 2*value - Y_Velocity[r][1] ;
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{
			v_vel_right=value;
			BC_Dirichlet_V_right=1;
			for (r=1; r<=NoOfRows; r++)
			{
				Y_Velocity[r][NoOfCols+1] = 2*value - Y_Velocity[r][NoOfCols] ;
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{
			v_vel_top=value;
			BC_Dirichlet_V_top=1;
			for (c=1;c<=NoOfCols; c++)
			{
				Y_Velocity[NoOfRows+1][c] = value;
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{
			v_vel_bottom=value;
			BC_Dirichlet_V_bottom=1;
			for (c=1;c<=NoOfCols; c++)
			{
				Y_Velocity[1][c] = value;
			}			
		}			
			
	
}

void BcNeumann_V(char wall[1])
{
	int r,c;
	
	
		
		if(strcmp(wall,"left")==0)
		{
			
			BC_Neumann_V_left=1;
			for (r=1; r<=NoOfRows; r++)
			{
				
				Y_Velocity[r][0] =  Y_Velocity[r][1] ;
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{
			BC_Neumann_V_right=1;
			for (r=1; r<=NoOfRows; r++)
			{
				Y_Velocity[r][NoOfCols+1] =  Y_Velocity[r][NoOfCols] ;
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{
			BC_Neumann_V_top=1;
			for (c=1;c<=NoOfCols; c++)
			{
				Y_Velocity[NoOfRows+1][c] = Y_Velocity[NoOfRows][c] ;
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{
			BC_Neumann_V_bottom=1;
			for (c=1;c<=NoOfCols; c++)
			{
				Y_Velocity[1][c] = Y_Velocity[2][c] ;
			}		
		}
			
	
}


void BcNeumann_F(char wall[1])
{
	int r,c;
	
	

	
		if(strcmp(wall,"left")==0)
		{
			for (r=1; r<=NoOfRows; r++)
			{
				
				Cellattrib[r][0].VolFrac = Cellattrib[r][1].VolFrac;
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{
			for (r=1; r<=NoOfRows; r++)
			{
				Cellattrib[r][NoOfCols+1].VolFrac = Cellattrib[r][NoOfCols].VolFrac;
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{
			for (c=1;c<=NoOfCols; c++)
			{
					Cellattrib[NoOfRows+1][c].VolFrac = Cellattrib[NoOfRows][c].VolFrac; 
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{
			for (c=1;c<=NoOfCols; c++)
			{
			
				Cellattrib[0][c].VolFrac = Cellattrib[1][c].VolFrac;	
				if (contact){
					if (Cellattrib[1][c].VolFrac>LOWER_LIMIT){
						Cellattrib[0][c].VolFrac=1;
					}
				}
					
			}		
		}
			
	
}



void BcNeumann_P(char wall[1])
{
	int r,c;
	
	

	
		if(strcmp(wall,"left")==0)
		{	BcNeumann_P_left=1;
			for (r=1; r<=NoOfRows; r++)
			{
				
				Press[r][0] = Press[r][1];
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{	BcNeumann_P_right=1;
			for (r=1; r<=NoOfRows; r++)
			{
				Press[r][NoOfCols+1] = Press[r][NoOfCols];
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{	BcNeumann_P_top=1;
			for (c=1;c<=NoOfCols; c++)
			{
					Press[NoOfRows+1][c] = Press[NoOfRows][c]; 
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{	BcNeumann_P_bottom=1;
			for (c=1;c<=NoOfCols; c++)
			{
			
				Press[0][c] = Press[1][c];	
			}		
		}
			
	
}


void BcDirichlet_P(char wall[1],double value)
{
	int r,c;
	
	
		
		if(strcmp(wall,"left")==0)
		{	BcDirichlet_P_left=1;
			press_left=value;
			for (r=1; r<=NoOfRows; r++)
			{
			
				Press[r][0] = 2*value - Press[r][1] ;
			}
		}
			
		else if(strcmp(wall,"right")==0)
		{	BcDirichlet_P_right=1;
			press_right=value;
			for (r=1; r<=NoOfRows; r++)
			{
				Press[r][NoOfCols+1] = 2*value - Press[r][NoOfCols] ;
			}
		}
			
		else if(strcmp(wall,"top")==0)
		{	BcDirichlet_P_top=1;
			press_top=value;
			for (c=1;c<=NoOfCols; c++)
			{
				Press[NoOfRows+1][c] = 2*value- Press[NoOfRows][c];
			}
		}
			
		else if(strcmp(wall,"bottom")==0)
		{	BcDirichlet_P_bottom=1;
			press_bottom=value;
			for (c=1;c<=NoOfCols; c++)
			{
				Press[0][c] = 2*value - Press[1][c];
			}			
		}			
			
	
}

void Pressure_BC()
{
	if (BcNeumann_P_left==1)
	{
		BcNeumann_P("left");
	}
	if (BcNeumann_P_right==1)
	{
		BcNeumann_P("right");
	}
	if (BcNeumann_P_bottom==1)
	{
		BcNeumann_P("bottom");
	}
	if (BcNeumann_P_top==1)
	{
		BcNeumann_P("top");
	}
	
	
	// dirichlet
	
	if (BcDirichlet_P_left==1)
	{
		BcDirichlet_P("left",press_left);
	}
	if (BcDirichlet_P_right==1)
	{
		BcDirichlet_P("right",press_right);
	}
	if (BcDirichlet_P_bottom==1)
	{
		BcDirichlet_P("bottom",press_bottom);
	}
	if (BcDirichlet_P_top==1)
	{
		BcDirichlet_P("top",press_top);
	}
	
	
	
}
void BcPeriodic(char dir[1])
{
	int r,c;
	
	if(strcmp(dir,"X")==0)
		{
			
			for (r=1; r<=NoOfRows; r++)
			{
				X_Velocity[r][1] = 0.5*(X_Velocity[r][2] + X_Velocity[r][NoOfCols]);
				Y_Velocity[r][0] =  Y_Velocity[r][NoOfCols] ;
				Cellattrib[r][0].VolFrac =  Cellattrib[r][NoOfCols].VolFrac ;
				
				X_Velocity[r][NoOfCols+1] = 0.5*(X_Velocity[r][2] + X_Velocity[r][NoOfCols]);
				Y_Velocity[r][NoOfCols+1] =  Y_Velocity[r][1] ;
				Cellattrib[r][NoOfCols+1].VolFrac =  Cellattrib[r][1].VolFrac ;
			}
		}
			
	else if(strcmp(dir,"Y")==0)
		{
			for (c=1; c<=NoOfCols; c++)
			{
				Y_Velocity[1][c] = 0.5*(Y_Velocity[2][c] + Y_Velocity[NoOfRows][c]);
				X_Velocity[0][c] =  X_Velocity[NoOfRows][c] ;
				Cellattrib[0][c].VolFrac =  Cellattrib[NoOfRows][c].VolFrac ;
				
				Y_Velocity[NoOfRows+1][c] = 0.5*(Y_Velocity[2][c] + Y_Velocity[NoOfRows][c]);
				X_Velocity[NoOfRows+1][c] =  X_Velocity[1][c] ;
				Cellattrib[NoOfRows+1][c].VolFrac =  Cellattrib[1][c].VolFrac ;
			}
		}
			
			
		
			
	
}


	 /********************************
NAME : Apply_BoundaryConditions()
DESCRIPTION: apply velocity Boundary conditions
*****************************/
void Apply_Default_BoundaryConditions()
{
	//left 
	BcDirichlet_U("left",0);
	BcNeumann_V("left");
	BcNeumann_F("left");
	BcNeumann_P("left");
	
	//right
	BcDirichlet_U("right",0);
	BcNeumann_V("right");
	BcNeumann_F("right");
	BcNeumann_P("right");
	
	//bottom
	BcDirichlet_V("bottom",0);
	BcNeumann_U("bottom");
	BcNeumann_F("bottom");
	BcNeumann_P("bottom");
	//top
	BcDirichlet_V("top",0);
	BcNeumann_U("top");
	BcNeumann_F("top");
	BcNeumann_P("top");

	

}
  
